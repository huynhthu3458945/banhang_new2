﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Core
{
    public class DownloadFile
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageDefault { get; set; }
        public string ImageFolder { get; set; }
        public string FilePath { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }
    }
}
