﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Core
{
    public class PermissionRef
    {
        [Key]
        public int Id { get; set; }

        public long EntityId { get; set; }

        public string UserName { get; set; }

        public bool IsEditor { get; set; }

        public bool IsManager { get; set; }
    }
}
