﻿using MainProject.Core.Enums;
using System.ComponentModel.DataAnnotations;

namespace MainProject.Core
{
    public class Partner
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageDefault { get; set; }
        public string ImageFolder { get; set; }
        public string ImageAlt { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }
        public PartnerTypeCollection Type { get; set; }
    }
}
