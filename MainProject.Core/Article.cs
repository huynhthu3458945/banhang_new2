﻿using System;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;

namespace MainProject.Core
{
    public class Article: Base
    {
        [Key]
        public long Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Body { get; set; }

        public string ImageDefault { get; set; }

        public string ImageFolder { get; set; }

        public virtual Category Category { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public string LastUpdatedBy { get; set; }

        public bool IsPublished { get; set; }

        public int Order { get; set; }

        public string Author { get; set; }

        public string DateLeft { get; set; }

        public string PromotionTitleLeft { get; set; }

        public string PromotionTitleRight { get; set; }

        public string PromotionBodyLeft { get; set; }

        public string PromotionBodyRight { get; set; }

        public string DetailBanner { get; set; }

        public virtual Language Language { get; set; }

        public bool IsHot { get; set; }
        public bool IsIntroduction { get; set; }

        public string BigImage { get; set; }

        public string TitleImage { get; set; }      
        public string GetUrl()
        {
            return Category.GetPrefixUrl() + "/" + SeName;
        }
    }
}
