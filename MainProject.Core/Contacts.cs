﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MainProject.Core
{
    public class Contacts
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Address { get; set; }
    }
}
