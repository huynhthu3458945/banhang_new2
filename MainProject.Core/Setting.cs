﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MainProject.Core
{
    public class Setting
    {
        [Key]
        public int Id { get; set; }

        public string Key { get; set; }

        [AllowHtml]
        public string Value { get; set; }
    }
}
