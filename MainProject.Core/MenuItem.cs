﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;
using MainProject.Core.Enums;

namespace MainProject.Core
{
    public class MenuItem: BaseLanguage
    {
        [Key]
        public long Id { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public LinkTargetTypeCollection LinkTargetType { get; set; }

        public bool IsPublished { get; set; }

        public int Order { get; set; }

        public string IconClass { get; set; }

        public virtual MenuItem Parent { get; set; }

        public virtual Menu Menu { get; set; }

        public virtual Language Language { get; set; }
    }
}
