﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;

namespace MainProject.Core
{
    public class Solution: BaseLanguage
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Link { get; set; }

        public virtual Language Language { get; set; }

        public int Order { get; set; }

        public bool IsPublished { get; set; }
    }
}
