﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;
using MainProject.Core.Enums;

namespace MainProject.Core
{
    public class Feedback : BaseLanguage
    {
        [Key]
        public int Id { get; set; }
        public string ImageDefault { get; set; }
        public string ImageFolder { get; set; }
        public string FullName { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }
        public virtual Language Language { get; set; }
    }
}
