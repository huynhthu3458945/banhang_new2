﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MainProject.Core.UserInfos
{
    public class ContentBehavior
    {
        [Key]
        public long Id { get; set; }
        public virtual Content Content { get; set; }
        public virtual Behavior Behavior { get; set; }
    }
}
