﻿using System;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.Enums;

namespace MainProject.Core.UserInfos
{
    public class Behavior
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public BehaviorTypeCollection Type { get; set; }
    }
}
