﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Core.UserInfos
{
    public class RoleContentBehavior
    {
        [Key]
        public long Id { get;set;}
        public virtual Role Role { get; set; }
        public virtual ContentBehavior ContentBehavior { get; set; }
    }
}
