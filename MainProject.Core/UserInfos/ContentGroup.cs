﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MainProject.Core.UserInfos
{
    public class ContentGroup
    {
        [Key]
        public int Id { get; set; }

        public string GroupName { get; set; }

        public int Order { get; set; }

        public string IconClass { get; set; }
    }
}
