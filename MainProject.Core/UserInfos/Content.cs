﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MainProject.Core.UserInfos
{
    public class Content
    {
        [Key]
        public long Id { get; set; }

        public string Title { get; set; }

        public string ControllerName { get; set; }

        public int Order { get; set; }

        public virtual ContentGroup ContentGroup { get; set; }
    }
}
