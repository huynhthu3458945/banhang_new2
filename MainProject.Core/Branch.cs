﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Core
{
    public class Branch
    {
        public Branch()
        {
            Order = 0;
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Street { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public double Lng { get; set; }
        public double Lat { get; set; }
        public bool IsPublished { get; set; }
        public bool IsOnFooter { get; set; }
        public int Order { get; set; }
        public string Url { get; set; }

        public virtual Region Region { get; set; }
    }
}
