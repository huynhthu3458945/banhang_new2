﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    public enum RegionTypeCollection
    {
        [Description("City")]
        City = 1,

        [Description("District")]
        District = 2
    }
}
