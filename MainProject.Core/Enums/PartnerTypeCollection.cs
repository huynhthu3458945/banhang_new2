﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Core.Enums
{
    public enum PartnerTypeCollection
    {
        [Description("Đối tác")]
        Partner = 1,

        [Description("Chứng nhận sản phẩm")]
        Certifications = 2
    }
}
