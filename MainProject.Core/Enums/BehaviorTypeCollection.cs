﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    public enum BehaviorTypeCollection
    {
        [Description("Xem")]
        ViewTemplate = 1,

        [Description("Thêm")]
        CreateTemplate = 2,

        [Description("Sửa")]
        EditTemplate = 3,

        [Description("Xóa")]
        DeleteTemplate = 4,

        [Description("Chi tiết")]
        DetailTemplate = 5

    }
}
