﻿
using System.ComponentModel;

namespace MainProject.Core.Enums
{
    public enum EntityTypeCollection
    {
        [Description("News")]
        Articles = 1,

        [Description("News version")]
        NewsVersion = 2,

        [Description("News categories")]
        Categories = 3,

        [Description("Products")]
        Products = 4,

        [Description("Attachments")]
        Attachments = 5,

        [Description("Attchment Categories")]
        AttachmentCategories = 6,

        [Description("Images")]
        Images = 7,

        [Description("Static page")]
        StaticPage = 9,

        [Description("Tags")]
        Tags = 10,

        [Description("Comment")]
        Comment = 11,

        [Description("Poll Campaign")]
        PollCampaign = 13,

        [Description("Poll item")]
        PollItem,

        [Description("AllTypes")]
        AllTypes = 12,

        [Description("FaqCategory")]
        FaqCategory = 14,

        [Description("FaqItem")]
        FaqItem = 15,

        [Description("Event")]
        Event,

        [Description("VideoClip")]
        VideoClips = 18,

        [Description("ImageGallery")]
        ImageGallery = 19,

        [Description("Regions")]
        Regions = 20,

        [Description("Branches")]
        Branches = 21

    }
}
