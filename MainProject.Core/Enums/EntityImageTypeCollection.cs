﻿namespace MainProject.Core.Enums
{
    public enum EntityImageTypeCollection
    {
        Article,

        Product,

        Category,

        SecondPosition
    }
}
