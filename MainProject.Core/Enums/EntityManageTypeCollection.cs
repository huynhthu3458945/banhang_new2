﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    public enum EntityManageTypeCollection
    {
        ManageVariables = -4,

        ManageSettings = -5,

        ManageUsers = -6,

        ManagePermissions = -7,

        ManageCategories = -8,

        [Description("Quản lý tin")] 
        ManageNews = -1,

        ManageBlackWords = -9,

        [Description("Quản lý bình luận")] 
        ManageComments = -2,

        ManageEvents = -10,

        ManageAttchmentCategories = -11,

        ManageAttchments = -12,

        ManageDistricts = -13,

        ManageBranchs = -14,

        ManagePollCategories = -15,

        ManageResource = -16,

        [Description("Quản lý khảo sát")] ManagePolls = -3,

        [Description("Quản lý FAQ")] ManageFaqCategory = -17,
        ManageFaqItem = -18,

        ManageYahoo = -19,

        ManageSlide = -20,

        ManageContact = -21,

        ManageProduct = -22,

        ManageOrders = -23,

        ManageTimeLine = -24

    }
}
