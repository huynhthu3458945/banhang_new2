﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    /// <summary>
    /// The Types of target link in Html <a>
    /// </summary>
    public enum LinkTargetTypeCollection
    {
        [Description("Cửa sổ hiện tại")]
        SelfLink = 1,

        [Description("Cửa sổ mới")]
        BlankLink = 2
    }
}
