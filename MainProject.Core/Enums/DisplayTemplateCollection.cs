﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    /// <summary>
    /// The Templates of Pages
    /// </summary>
    public enum DisplayTemplateCollection
    {
        [Description("Trang Chủ")]
        HomeTemplate = 1,
        
        [Description("Trang Tổ Yến")]
        ProductTemplate = 2,

        [Description("Trang Yến Con Cấp 1")]
        Child1ProductTemplate = 3,

        [Description("Quà Tặng Doanh Nghiệp")]
        BusinessGiftsTemplate = 4,

        [Description("Yến Chưng")]
        YenChungTemplate = 5,

        [Description("Tư Vấn")]

        AdviseTemplate = 6,

        [Description("Tư Vấn Con Cấp 1")]

        Child1AdviseTemplate = 7,

        [Description("Trang Ngự Thiện")]
        NguThienTemplate = 8,

        [Description("Trang Giới Thiệu")]
        IntroductionTemplate = 9,

        [Description("Trang Mua Hàng")]

        PurchaseTemplate = 10,

        [Description("Trang FAQs")]

        FaqTemplate = 11,

        [Description("Trang Đôi Trả và Hoàn Tiền")]

        ExchangeTemplate = 12,

        [Description("Trang Thanh Toán")]

        PayTemplate = 13,

        [Description("Trang Bảo Mật")]

        SecurityTemplate = 14,

        [Description("Trang Liện Hệ")]

        ContactTemplate = 15,
    }
}
