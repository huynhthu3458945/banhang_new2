﻿namespace MainProject.Core.Enums
{
    public enum OrderLogTypeCollection
    {
        ChangeStatus = 1,

        ChangeNote = 2,

        AddNotesForAction = 3,

        ChangePaymentType = 4
    }
}
