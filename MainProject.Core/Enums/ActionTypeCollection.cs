﻿namespace MainProject.Core.Enums
{
    public enum ActionTypeCollection
    {
        Temp = 1,

        Create = 2,

        Edit = 3,

        Delete = 4,

        Approval = 6,

        Reject = 7
    }
}
