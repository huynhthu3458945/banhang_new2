﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    public enum PositionTypeCollection
    {
        [Description("Vị trí 1: Tin dùng bởi thương hiệu")]
        FirstPosition = 1,

        [Description("Vị trí 2: Mô tả quà danh nghiệp")]
        SecondPosition = 2,

        [Description("Vị trí 3: Độc đáo-Am hiểu-Xu hướng-Tiện lợi")]
        ThirdPosition = 3,

        [Description("Vị trí 4: Sản phẩm")]
        FourthPosition = 4,

        [Description("Vị trí 5: Mô tả giới thiệu")]
        FifthPosition = 5,

        [Description("Vị trí 6: Tại sao chọn thương hiệu yến")]
        SixthPosition = 6,

        [Description("Vị trí 7: Ablunm ảnh")]
        SeventhPosition = 7,

        [Description("Vị trí 8: Ý nghĩa thương hiệu")]
        EighthPosition = 8,

        [Description("Vị trí 9: Đam mê-nhiệt hiết-tôn nghiêm-tầm nhìn")]
        NinethPosition = 9,

        [Description("Vị trí 10")]
        TenthPosition = 10,

        [Description("Vị trí 11")]
        EleventhPosition = 11,

        [Description("Vị trí 12")]
        TwelvethPosition = 12
    }
}
