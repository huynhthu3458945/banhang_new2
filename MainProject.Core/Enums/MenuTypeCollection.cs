﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    /// <summary>
    /// The Types of Menu
    /// </summary>
    public enum MenuTypeCollection
    {
        [Description("Menu Header")]
        HeaderMenu = 1,

        [Description("Menu Footer")]
        FooterMenu = 2,

        [Description("Menu Support")]
        SupportMenu = 3,

        [Description("Menu Mobi Footer")]
        MobiFooterMenu = 4
    }
}
