﻿using System.ComponentModel;

namespace MainProject.Core.Enums
{
    public enum OrderStatusCollection
    {
        [Description("Đang chờ xử lý")]
        JustCreate = 1,

        [Description("Đang giao hàng")]
        Shipping = 2,

        [Description("Đã hoàn thành")]
        Close = 3,

        [Description("Đã hủy đơn hàng")]
        Cancel = 4
    }
}
