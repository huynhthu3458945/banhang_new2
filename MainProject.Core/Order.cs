﻿
using System;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.Enums;

namespace MainProject.Core
{
    public class Order
    {
        [Key]
        public long Id { get; set; }

        public string BuyerName { get; set; }

        public string BuyerEmail { get; set; }

        public string BuyerPhone { get; set; }

        public string OtherBuyerPhone { get; set; }

        public string BuyerAddress { get; set; }

        public string Note { get; set; }

        public DateTime OrderTime { get; set; }

        public decimal Amount { get; set; }

        public virtual OrderType OrderType { get; set; }

        public OrderStatusCollection OrderStatus { get; set; }

    }
}
