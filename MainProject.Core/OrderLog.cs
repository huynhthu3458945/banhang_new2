﻿using System;
using MainProject.Core.Enums;

namespace MainProject.Core
{
    public class OrderLog
    {
        public long Id { get; set; }

        public string ActionBy { get; set; }

        public DateTime ActionTime { get; set; }

        public virtual Order Order { get; set; }

        public OrderLogTypeCollection OrderLogType { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public string Notes { get; set; }
    }
}
