﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;
using MainProject.Core.Enums;

namespace MainProject.Core
{
    public class LandingpageGroup : BaseLanguage
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageDefault { get; set; }

        public string ImageFolder { get; set; }

        public bool IsPublished { get; set; }

        public PositionTypeCollection PositionType { get; set; }

        public virtual Language Language { get; set; }
    }
}
