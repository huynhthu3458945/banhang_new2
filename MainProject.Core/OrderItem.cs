﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Core
{
    public class OrderItem
    {
        [Key]
        public long Id { get; set; }

        public virtual Product Product { get; set; }

        public virtual Order Order { get; set; }

        public decimal Price { get; set; }

        public int ItemCount { get; set; }

        public decimal Amount { get; set; }
    }
}
