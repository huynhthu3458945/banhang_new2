﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;

namespace MainProject.Core
{
    public class OrderType: BaseLanguage
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public virtual Language Language { get; set; }

        public int Order { get; set; }

        public bool IsPublished { get; set; }
    }
}
