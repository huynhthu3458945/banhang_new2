﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;

namespace MainProject.Core
{
    public class SlideShow: BaseLanguage
    {
        public SlideShow()
        {
            Order = 0;
        }
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public virtual Category Category {get;set;}
        public string Description { get; set; }
        public string Image { get; set; }
        public string ImageMobile { get; set; }
        public string Link { get; set; }
        public int Order { get; set; }
        public bool IsPublished { get; set; }
        public bool IsHomePage { get; set; }
        public string ImageFolder { get; set; }
        public string VideoId { get; set; }
        public bool IsVideo { get; set; }

        public virtual Language Language { get; set; }
    }
}
