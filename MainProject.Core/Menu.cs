﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.Enums;

namespace MainProject.Core
{
    public class Menu
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public int Order { get; set; }

        public MenuTypeCollection MenuType { get; set; }
    }
}
