﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Core.BaseEntities
{
    public class Base: BaseLanguage
    {
        [StringLength(500)]
        public string MetaTitle { get; set; }

        [StringLength(500)]
        public string MetaDescription { get; set; }

        [StringLength(500)]
        public string MetaKeywords { get; set; }

        public string MetaImage { get; set; }

        public string SeName { get; set; }
    }
}
