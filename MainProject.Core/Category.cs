﻿using System;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.Enums;
using MainProject.Core.BaseEntities;

namespace MainProject.Core
{
    public class Category : Base
    {
        public Category()
        {
            Order = 1;
        }

        [Key]
        public long Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
        public string Benefit { get; set; }
        public string Why { get; set; }
        public string LinkYouTuBe { get; set; }

        public bool IsSystem { get; set; }

        public string ImageDefault { get; set; }

        public string ImageDefault2 { get; set; }

        public string ImageFolder { get; set; }

        public int Order { get; set; }

        public virtual Category Parent { get; set; }

        public DisplayTemplateCollection DisplayTemplate { get; set; }

        public EntityTypeCollection EntityType { get; set; }

        public bool PrivateArea { get; set; }

        public bool IsPublished { get; set; }

        public bool IsShowOnMenu { get; set; }

        public string ExternalLink { get; set; }

        public bool IsHotProductCategory { get; set; }

        public string BannerLink { get; set; }

        public virtual Language Language { get; set; }

        public string ImageIcon { get; set; }    

        public string GetPrefixUrl()
        {
            if (Parent == null) return IsSystem ? "" : "/" + SeName;
            return String.Format("{0}/{1}", Parent.GetPrefixUrl(), SeName);
        }
    }
}
