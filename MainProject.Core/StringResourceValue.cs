﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MainProject.Core
{
    public class StringResourceValue
    {
        [Key]
        public long Id { get; set; }

        public virtual StringResourceKey Key { get; set; }

        [AllowHtml]
        public string Value { get; set; }

        public virtual Language Language { get; set; }
    }
}
