﻿
using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;

namespace MainProject.Core
{
    public class Product: Base
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string ImageDefault { get; set; }
        public string ImageFolder { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Preserve { get; set; }
        public int Order { get; set; }
        public bool IsPublished { get; set; }
        public bool IsPriceContact { get; set; }
        public bool IsPromotion { get; set; }
        public bool IsLandingpage { get; set; }
        public decimal Price { get; set; }


        public int PromotionPercent { get; set; }
        public decimal PromotionPrice { get; set; }
        public string  ProductCode { get; set; }

        public bool IsVideo { get; set; }
        public string VideoId { get; set; }
        public string SalesCommitment { get; set; }
        public string DetailInfo { get; set; }

        public virtual Language Language { get; set; }

        public virtual Category Category { get; set; }

        public string GetUrl()
        {
            return Category.GetPrefixUrl() + "/" + SeName;
        }
    }
}
