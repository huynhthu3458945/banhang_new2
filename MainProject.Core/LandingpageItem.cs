﻿using System.ComponentModel.DataAnnotations;
using MainProject.Core.BaseEntities;

namespace MainProject.Core
{
    public class LandingpageItem : BaseLanguage
    {
        [Key]
        public long Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageDefault { get; set; }

        public string ImageFolder { get; set; }
        public string Content { get; set; }
        public string LinkYoube { get; set; }
        public string ImageProduct { get; set; }
        public string Size { get; set; }
        public string Material { get; set; }
        public string MassShip { get; set; }
        public string MassFood { get; set; }
        public string Logo { get; set; }

        public string Url { get; set; }

        public bool IsPublished { get; set; }

        public int Order { get; set; }

        public virtual LandingpageGroup LandingpageGroup { get; set; }

        public virtual Language Language { get; set; }
    }
}
