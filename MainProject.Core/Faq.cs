﻿using MainProject.Core.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Core
{
    public class Faq : BaseLanguage
    {
        [Key]
        public int Id { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public virtual Language Language { get; set; }

        public int Order { get; set; }

        public bool IsPublished { get; set; }
    }
}
