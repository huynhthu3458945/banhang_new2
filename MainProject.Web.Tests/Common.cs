﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Web.Mvc;
//using MainProject.Core.Enums;
//using MainProject.Framework.Helper;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace MainProject.Web.Tests
//{
//    [TestClass]
//    public class Common
//    {
//        [TestMethod]
//        public void can_get_selectlist_from_enum()
//        {
//            // good
//            var selectList = new List<SelectListItem>();
//            foreach (var t in Enum.GetValues(typeof(InputTypeCollection)))
//            {
//                var val = (int)t;
//                selectList.Add(new SelectListItem() { Text = t.ToString(), Value = ((int)t).ToString() });
//            }
//            //end good

//            var enumType = typeof(InputTypeCollection);
//            var fields = from field in enumType.GetFields()
//                         where field.IsLiteral
//                         select field;

//            foreach (FieldInfo field in fields)
//            {
//                selectList.Add(new SelectListItem() { Text = field.Name, Value = (string)field.GetValue(enumType) });
//            }

//            foreach (var selectListItem in selectList)
//            {
//                Console.WriteLine(selectListItem.Text + ":" + selectListItem.Value);
//            }

//        }

//        [TestMethod]
//        public void can_remove_bbcode_from_string()
//        {
//            var text = "chuc mung nam moi [b]cộng               [/b][i]sản[/i] [i]tài [u]phiệt   [/u][/i] [i][u]phản   động:)[/u][/i]";
//            while (text.Contains("  "))
//            {
//                text = text.Replace("  ", " ");
//            }
//            Console.WriteLine(StringHelper.RemoveBBCode(text));

//            text = "abc    nj [i][u]phản   động:)";
//            Console.WriteLine(StringHelper.RemoveBBCode(text));
//        }
//    }
//}
