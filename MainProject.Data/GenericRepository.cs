﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Linq.Expressions;

namespace MainProject.Data
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        // Declare Class Variables for Database Context and Entity Set that the Repository is instantiated for
        internal MainDbContext context;
        internal DbSet<TEntity> dbSet;

        // The constructor accepts a database context instance and initializes the entity set variable
        public GenericRepository(MainDbContext context)
        {
            this.context = context;
            dbSet = context.Set<TEntity>();
        }

        /* The Get method uses lambda expressions to allow the calling code to specify a filter 
        condition and a column to order the results by, and a string parameter lets the caller provide a 
        comma-delimited list of navigation properties for eager loading: */
        public virtual IEnumerable<TEntity> FindFilter(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            if (includeProperties == null)
            {
                throw new ArgumentNullException(nameof(includeProperties));
            }

            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        // Find the object by predicate - return TEntity
        public virtual TEntity FindUnique(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate != null)
            {
                return dbSet.Where(predicate).FirstOrDefault();
            }
            else
            {
                throw new ArgumentNullException("Predicate value must be passed to FindSingleBy<T>.");
            }
        }

        // Find the object by predicate - return IQueryable<TEntity>
        public virtual IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate != null)
            {
                    return dbSet.Where(predicate).AsQueryable();
            }
            else
            {
                throw new ArgumentNullException("Predicate value must be passed to FindBy<T,TKey>.");
            }
        }

        // Find all object
        public virtual IQueryable<TEntity> FindAll() => dbSet;

        // Find the object by Id
        public virtual TEntity FindById(object id) => dbSet.Find(id);

        // Insert Object
        public virtual void Insert(TEntity entity)=> dbSet.Add(entity);

        // Delete Object by Id
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        // Delete Object
        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        // Update Object
        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }
}