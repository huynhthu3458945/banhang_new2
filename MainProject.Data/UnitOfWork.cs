﻿using System;
using MainProject.Core;
using MainProject.Core.UserInfos;

namespace MainProject.Data
{
    public class UnitOfWork : IDisposable
    {

        // Creates class variables for database context and each repository
        // Initializes a new database context 
        private MainDbContext context = new MainDbContext();
        private GenericRepository<UserProfile> userProfileRepository;
        private GenericRepository<Article> articleRepository;
        private GenericRepository<OrderLog> orderLogRepository;
        private GenericRepository<Order> orderRepository;
        private GenericRepository<OrderItem> orderItemRepository;
        private GenericRepository<Category> categoryRepository;
        private GenericRepository<Contacts> contactRepository;
        private GenericRepository<Product> productRepository;
        private GenericRepository<PermissionRef> permissionRefRepository;
        private GenericRepository<StringResourceKey> stringResourceKeyRepository;
        private GenericRepository<StringResourceValue> stringResourceValueRepository;
        private GenericRepository<Setting> settingRepository;
        private GenericRepository<SlideShow> slideShowRepository;
        private GenericRepository<LogHistory> logHistoryRepository;
        private GenericRepository<Language> languageRepository;
        private GenericRepository<OrderType> orderTypeRepository;
        private GenericRepository<UrlRecord> urlRecordRepository;
        private GenericRepository<ContentGroup> contentGroupRepository;
        private GenericRepository<Content> contentRepository;
        private GenericRepository<Behavior> behaviorRepository;
        private GenericRepository<ContentBehavior> contentBehaviorRepository;
        private GenericRepository<RoleContentBehavior> roleContentBehaviorRepository;
        private GenericRepository<Role> roleRepository;
        private GenericRepository<Menu> menuRepository;
        private GenericRepository<MenuItem> menuItemRepository;
        private GenericRepository<Partner> partnerRepository;
        private GenericRepository<Solution> solutionRepository;
        private GenericRepository<LandingpageGroup> landingpageGroupRepository;
        private GenericRepository<LandingpageItem> landingpageItemRepository;
        private GenericRepository<Region> regionRepository;
        private GenericRepository<Branch> branchRepository;
        private GenericRepository<DownloadFile> downloadFileRepository;
        private GenericRepository<Faq> faqRepository;
        private GenericRepository<Feedback> feedbackRepository;

        //Create repository properties
        public GenericRepository<UserProfile> UserProfileRepository
        {
            get { return userProfileRepository ?? new GenericRepository<UserProfile>(context); }
        }
        public GenericRepository<Article> ArticleRepository
        {
            get { return articleRepository ?? new GenericRepository<Article>(context); }
        }
        public GenericRepository<OrderLog> OrderLogRepository
        {
            get { return orderLogRepository ?? new GenericRepository<OrderLog>(context); }
        }
        public GenericRepository<Order> OrderRepository
        {
            get { return orderRepository ?? new GenericRepository<Order>(context); }
        }
        public GenericRepository<OrderItem> OrderItemRepository
        {
            get { return orderItemRepository ?? new GenericRepository<OrderItem>(context); }
        }
        public GenericRepository<Category> CategoryRepository
        {
            get { return categoryRepository ?? new GenericRepository<Category>(context); }
        }
        public GenericRepository<Contacts> ContactRepository
        {
            get { return contactRepository ?? new GenericRepository<Contacts>(context); }
        }
        public GenericRepository<Product> ProductRepository
        {
            get { return productRepository ?? new GenericRepository<Product>(context); }
        }
        public GenericRepository<PermissionRef> PermissionRefRepository
        {
            get { return permissionRefRepository ?? new GenericRepository<PermissionRef>(context); }
        }
        public GenericRepository<StringResourceKey> StringResourceKeyRepository
        {
            get { return stringResourceKeyRepository ?? new GenericRepository<StringResourceKey>(context); }
        }
        public GenericRepository<StringResourceValue> StringResourceValueRepository
        {
            get { return stringResourceValueRepository ?? new GenericRepository<StringResourceValue>(context); }
        }
        public GenericRepository<Setting> SettingRepository
        {
            get
            {
                return settingRepository ?? new GenericRepository<Setting>(context);
            }
        }
        public GenericRepository<SlideShow> SlideShowRepository
        {
            get { return slideShowRepository ?? new GenericRepository<SlideShow>(context); }
        }
        public GenericRepository<LogHistory> LogHistoryRepository
        {
            get { return logHistoryRepository ?? new GenericRepository<LogHistory>(context); }
        }
        public GenericRepository<Language> LanguageRepository
        {
            get { return languageRepository ?? new GenericRepository<Language>(context); }
        }
        public GenericRepository<OrderType> OrderTypeRepository
        {
            get { return orderTypeRepository ?? new GenericRepository<OrderType>(context); }
        }
        public GenericRepository<UrlRecord> UrlRecordRepository
        {
            get { return urlRecordRepository ?? new GenericRepository<UrlRecord>(context); }
        }

        public GenericRepository<ContentGroup> ContentGroupRepository
        {
            get { return contentGroupRepository ?? new GenericRepository<ContentGroup>(context); }
        }
        public GenericRepository<Content> ContentRepository
        {
            get { return contentRepository ?? new GenericRepository<Content>(context); }
        }
        public GenericRepository<Behavior> BehaviorRepository
        {
            get { return behaviorRepository ?? new GenericRepository<Behavior>(context); }
        }
        public GenericRepository<ContentBehavior> ContentBehaviorRepository
        {
            get { return contentBehaviorRepository ?? new GenericRepository<ContentBehavior>(context); }
        }
        public GenericRepository<RoleContentBehavior> RoleContentBehaviorRepository
        {
            get { return roleContentBehaviorRepository ?? new GenericRepository<RoleContentBehavior>(context); }
        }
        public GenericRepository<Role> RoleRepository
        {
            get { return roleRepository ?? new GenericRepository<Role>(context); }
        }
        public GenericRepository<Menu> MenuRepository
        {
            get { return menuRepository ?? new GenericRepository<Menu>(context); }
        }

        public GenericRepository<MenuItem> MenuItemRepository
        {
            get { return menuItemRepository ?? new GenericRepository<MenuItem>(context); }
        }
        public GenericRepository<Partner> PartnerRepository
        {
            get { return partnerRepository ?? new GenericRepository<Partner>(context); }
        }
        public GenericRepository<Solution> SolutionRepository
        {
            get { return solutionRepository ?? new GenericRepository<Solution>(context); }
        }

        public GenericRepository<LandingpageGroup> LandingpageGroupRepository
        {
            get { return landingpageGroupRepository ?? new GenericRepository<LandingpageGroup>(context); }
        }
        public GenericRepository<LandingpageItem> LandingpageItemRepository
        {
            get { return landingpageItemRepository ?? new GenericRepository<LandingpageItem>(context); }
        }

        public GenericRepository<Region> RegionRepository
        {
            get { return regionRepository ?? new GenericRepository<Region>(context); }
        }
        public GenericRepository<Branch> BranchRepository
        {
            get { return branchRepository ?? new GenericRepository<Branch>(context); }
        }

        public GenericRepository<DownloadFile> DownloadFileRepository
        {
            get { return downloadFileRepository ?? new GenericRepository<DownloadFile>(context); }
        }
        public GenericRepository<Faq> FaqRepository
        {
            get { return faqRepository ?? new GenericRepository<Faq>(context); }
        }
        public GenericRepository<Feedback> FeedbackRepository
        {
            get { return feedbackRepository ?? new GenericRepository<Feedback>(context); }
        }

        // The Save method calls SaveChanges on the database context
        public void Save()
        {
            context.SaveChanges();
        }

        // Disposes the context
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}