﻿using System.Data.Entity;
using MainProject.Core;
using MainProject.Core.UserInfos;

namespace MainProject.Data
{
    public class MainDbContext : DbContext
    {
        public MainDbContext() : base("DefaultConnection") { }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<PermissionRef> PermissionRefs { get; set; }

        public DbSet<LogHistory> LogHistories { get; set; }

        public DbSet<StringResourceKey> StringResourceKeys { get; set; }

        public DbSet<StringResourceValue> StringResourceValues { get; set; }

        public DbSet<UrlRecord> UrlRecords { get; set; }

        public DbSet<Setting> Settings { get; set; }

        public DbSet<SlideShow> SlideShows { get; set; }

        // Other

        public DbSet<Contacts> Contacts { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderItem> OrderItems { get; set; }

        public DbSet<OrderLog> OrderLogs { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<OrderType> OrderTypes { get; set; }

        public DbSet<Menu> Menus { get; set; }

        public DbSet<MenuItem> MenuItems { get; set; }

        public DbSet<Partner> Partners { get; set; }

        public DbSet<Solution> Solutions { get; set; }

        public DbSet<LandingpageGroup> LandingpageGroups { get; set; }

        public DbSet<LandingpageItem> LandingpageItems { get; set; }

        public DbSet<Region> Regions { get; set; }

        public DbSet<Branch> Branches { get; set; }

        public DbSet<DownloadFile> DownloadFiles { get; set; }

        // Authorize Tables
        public DbSet<ContentGroup> ContentGroups { get; set; }

        public DbSet<Content> Contents { get; set; }

        public DbSet<Behavior> Behaviors { get; set; }

        public DbSet<ContentBehavior> Content_Behaviors { get; set; }

        public DbSet<RoleContentBehavior> Role_Behaviors { get; set; }

        public DbSet<Role> Roles { get; set; }
        public DbSet<Faq> Faqs { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }

    }
}
