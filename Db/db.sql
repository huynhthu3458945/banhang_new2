USE [bal50917_BanhangNew]
GO
/****** Object:  User [bal50917_BanhangNew]    Script Date: 9/28/2022 9:04:07 AM ******/
CREATE USER [bal50917_BanhangNew] FOR LOGIN [bal50917_BanhangNew] WITH DEFAULT_SCHEMA=[bal50917_BanhangNew]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [bal50917_BanhangNew]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [bal50917_BanhangNew]
GO
ALTER ROLE [db_datareader] ADD MEMBER [bal50917_BanhangNew]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [bal50917_BanhangNew]
GO
/****** Object:  Schema [bal50917_7balltop]    Script Date: 9/28/2022 9:04:07 AM ******/
CREATE SCHEMA [bal50917_7balltop]
GO
/****** Object:  Schema [bal50917_BanhangNew]    Script Date: 9/28/2022 9:04:07 AM ******/
CREATE SCHEMA [bal50917_BanhangNew]
GO
/****** Object:  Table [dbo].[Articles]    Script Date: 9/28/2022 9:04:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articles](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
	[SeName] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[LastUpdatedBy] [nvarchar](max) NULL,
	[IsVideo] [bit] NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[Category_Id] [bigint] NULL,
	[VideoLink] [nvarchar](max) NULL,
	[ExternalLink] [nvarchar](max) NULL,
	[MetaTitle] [nvarchar](500) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[MetaKeywords] [nvarchar](500) NULL,
	[MetaImage] [nvarchar](max) NULL,
	[Author] [nvarchar](max) NULL,
	[PromotionTitleLeft] [nvarchar](max) NULL,
	[PromotionTitleRight] [nvarchar](max) NULL,
	[PromotionBodyLeft] [nvarchar](max) NULL,
	[PromotionBodyRight] [nvarchar](max) NULL,
	[DateLeft] [nvarchar](max) NULL,
	[DetailBanner] [nvarchar](max) NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[BigImage] [nvarchar](max) NULL,
	[IsHot] [bit] NOT NULL,
	[TitleImage] [nvarchar](max) NULL,
 CONSTRAINT [PK__Articles__3214EC0768BD7F23] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Behaviors]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Behaviors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Type] [int] NOT NULL,
 CONSTRAINT [PK_Behaviors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Branches]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branches](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Street] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[Fax] [nvarchar](max) NULL,
	[Lng] [float] NOT NULL,
	[Lat] [float] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[Region_Id] [int] NOT NULL,
	[IsOnFooter] [bit] NOT NULL,
 CONSTRAINT [PK_Branches_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[SeName] [nvarchar](max) NULL,
	[IsSystem] [bit] NOT NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[DisplayTemplate] [int] NOT NULL,
	[EntityType] [int] NOT NULL,
	[PrivateArea] [bit] NOT NULL,
	[Parent_Id] [bigint] NULL,
	[IsPublished] [bit] NOT NULL,
	[IsShowOnMenu] [bit] NOT NULL,
	[ExternalLink] [nvarchar](max) NULL,
	[BannerLink] [nvarchar](max) NULL,
	[MetaTitle] [nvarchar](500) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[MetaKeywords] [nvarchar](500) NULL,
	[MetaImage] [nvarchar](max) NULL,
	[Language_Id] [int] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[ImageDefault2] [nvarchar](max) NULL,
	[IsHotProductCategory] [bit] NOT NULL,
	[ImageIcon] [nvarchar](max) NULL,
 CONSTRAINT [PK__Categori__3214EC07705EA0EB] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[Address] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContentBehaviors]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContentBehaviors](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Content_Id] [bigint] NOT NULL,
	[Behavior_Id] [int] NOT NULL,
 CONSTRAINT [PK_ContentBehaviors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContentGroups]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContentGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](max) NOT NULL,
	[Order] [int] NOT NULL,
	[IconClass] [nvarchar](max) NULL,
 CONSTRAINT [PK_ContentGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contents]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contents](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[ControllerName] [nvarchar](max) NOT NULL,
	[Order] [int] NOT NULL,
	[ContentGroup_Id] [int] NOT NULL,
 CONSTRAINT [PK_Contents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomInfoes]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomInfoes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK__CustomIn__3214EC07173876EA] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DownloadFiles]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DownloadFiles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[FilePath] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
 CONSTRAINT [PK_DownloadFiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LandingpageGroups]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LandingpageGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[PositionType] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_LandingpageGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LandingpageItems]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LandingpageItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[LandingpageGroup_Id] [int] NOT NULL,
 CONSTRAINT [PK_LandingpageItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageName] [nvarchar](max) NULL,
	[LanguageKey] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogHistories]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogHistories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EntityType] [int] NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[ActionType] [int] NOT NULL,
	[ActionBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Comment] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuItems]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NULL,
	[LinkTargetType] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[IconClass] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[Parent_Id] [bigint] NULL,
	[Menu_Id] [int] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_Menus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menus]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[MenuType] [int] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItems]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Product_Id] [bigint] NOT NULL,
	[Order_Id] [bigint] NOT NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[ItemCount] [int] NOT NULL,
	[Amount] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_OrderItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderLogs]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ActionBy] [nvarchar](max) NULL,
	[ActionTime] [datetime] NOT NULL,
	[OrderLogType] [int] NOT NULL,
	[OldValue] [nvarchar](max) NULL,
	[NewValue] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL,
	[Order_Id] [bigint] NULL,
 CONSTRAINT [PK_OrderLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BuyerName] [nvarchar](max) NULL,
	[BuyerEmail] [nvarchar](max) NULL,
	[BuyerPhone] [nvarchar](max) NULL,
	[BuyerAddress] [nvarchar](max) NULL,
	[OrderTime] [datetime] NOT NULL,
	[Amount] [decimal](18, 0) NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[OtherBuyerPhone] [nvarchar](max) NULL,
	[OrderType_Id] [int] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderTypes]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_OrderTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Partners]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partners](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[ImageAlt] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
 CONSTRAINT [PK_Partners] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermissionRefs]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionRefs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[IsEditor] [bit] NOT NULL,
	[IsManager] [bit] NOT NULL,
 CONSTRAINT [PK_PermissionRefs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[SeName] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[Code] [nvarchar](max) NULL,
	[ImageCart] [nvarchar](max) NULL,
	[PromotionPrice] [decimal](18, 0) NULL,
	[PromotionPercent] [int] NULL,
	[ProductCode] [nchar](10) NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[Category_Id] [bigint] NOT NULL,
	[MetaTitle] [nvarchar](500) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[MetaKeywords] [nvarchar](500) NULL,
	[MetaImage] [nvarchar](max) NULL,
	[IsVideo] [bit] NOT NULL,
	[VideoId] [nvarchar](max) NULL,
	[DetailInfo] [nvarchar](max) NULL,
	[Advantages] [nvarchar](max) NULL,
	[Instruction] [nvarchar](max) NULL,
	[SalesCommitment] [nvarchar](max) NULL,
	[IsPromotion] [bit] NOT NULL,
	[ShortDescription] [nvarchar](max) NULL,
	[IsPriceContact] [bit] NULL,
	[IsLandingpage] [bit] NOT NULL,
 CONSTRAINT [PK_MainProducts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Regions]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Regions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Lng] [float] NOT NULL,
	[Lat] [float] NOT NULL,
	[Order] [int] NOT NULL,
	[Parent_Id] [int] NULL,
	[RegionType] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
 CONSTRAINT [PK_Regions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleContentBehaviors]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleContentBehaviors](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Role_RoleId] [int] NOT NULL,
	[ContentBehavior_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_RoleContentBehaviors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SlideShows]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SlideShows](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[IsHomePage] [bit] NOT NULL,
	[Category_Id] [bigint] NOT NULL,
	[ImageMobile] [nvarchar](max) NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[VideoId] [nvarchar](max) NULL,
	[IsVideo] [bit] NOT NULL,
 CONSTRAINT [PK__SlideSho__3214EC07168449D3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Solutions]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Solutions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Link] [nvarchar](max) NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_Solutions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StringResourceKeys]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StringResourceKeys](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK__StringRe__3214EC071A54DAB7] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StringResourceValues]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StringResourceValues](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Key_Id] [bigint] NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK__StringRe__3214EC071E256B9B] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Table]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table](
	[Id] [int] NOT NULL,
	[Description] [nchar](10) NULL,
	[Seconds] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UrlRecords]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UrlRecords](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SeName] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NULL,
	[EntityId] [bigint] NOT NULL,
	[EntityType] [int] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK__UrlRecor__3214EC0725C68D63] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[LastedIP] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Videos]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Videos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[YoutubeLink] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[VideoId] [nvarchar](max) NULL,
	[Phase] [int] NULL,
	[TripName] [nvarchar](max) NULL,
	[Story] [nvarchar](max) NULL,
	[FullName] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Videos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Membership]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Membership](
	[UserId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ConfirmationToken] [nvarchar](128) NULL,
	[IsConfirmed] [bit] NULL,
	[LastPasswordFailureDate] [datetime] NULL,
	[PasswordFailuresSinceLastSuccess] [int] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordChangedDate] [datetime] NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[PasswordVerificationToken] [nvarchar](128) NULL,
	[PasswordVerificationTokenExpirationDate] [datetime] NULL,
 CONSTRAINT [PK__webpages__1788CC4C481BA567] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_OAuthMembership]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership](
	[Provider] [nvarchar](30) NOT NULL,
	[ProviderUserId] [nvarchar](100) NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Provider] ASC,
	[ProviderUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Roles]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[RoleDescription] [nvarchar](max) NULL,
	[IsSystem] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_UsersInRoles]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_UsersInRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[faq]    Script Date: 9/28/2022 9:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Faqs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Question] [nvarchar](max) NOT NULL,
	[Answer] [nvarchar](max) NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[Order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF__MainProdu__Promo__01142BA1]  DEFAULT ((0)) FOR [PromotionPrice]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF__MainProdu__Promo__02084FDA]  DEFAULT ((0)) FOR [PromotionPercent]
GO
ALTER TABLE [dbo].[Products] ADD  DEFAULT ((0)) FOR [IsLandingpage]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  CONSTRAINT [DF__webpages___IsCon__4A03EDD9]  DEFAULT ((0)) FOR [IsConfirmed]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  CONSTRAINT [DF__webpages___Passw__4AF81212]  DEFAULT ((0)) FOR [PasswordFailuresSinceLastSuccess]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [Article_Category] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [Article_Category]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [FK_Articles_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [FK_Articles_Languages]
GO
ALTER TABLE [dbo].[Branches]  WITH CHECK ADD  CONSTRAINT [FK_Branches_Regions] FOREIGN KEY([Region_Id])
REFERENCES [dbo].[Regions] ([Id])
GO
ALTER TABLE [dbo].[Branches] CHECK CONSTRAINT [FK_Branches_Regions]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [Category_Parent] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [Category_Parent]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [FK_Categories_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [FK_Categories_Languages]
GO
ALTER TABLE [dbo].[ContentBehaviors]  WITH CHECK ADD  CONSTRAINT [FK_ContentBehaviors_Behaviors] FOREIGN KEY([Behavior_Id])
REFERENCES [dbo].[Behaviors] ([Id])
GO
ALTER TABLE [dbo].[ContentBehaviors] CHECK CONSTRAINT [FK_ContentBehaviors_Behaviors]
GO
ALTER TABLE [dbo].[ContentBehaviors]  WITH CHECK ADD  CONSTRAINT [FK_ContentBehaviors_Contents] FOREIGN KEY([Content_Id])
REFERENCES [dbo].[Contents] ([Id])
GO
ALTER TABLE [dbo].[ContentBehaviors] CHECK CONSTRAINT [FK_ContentBehaviors_Contents]
GO
ALTER TABLE [dbo].[Contents]  WITH CHECK ADD  CONSTRAINT [FK_Contents_ContentGroups] FOREIGN KEY([ContentGroup_Id])
REFERENCES [dbo].[ContentGroups] ([Id])
GO
ALTER TABLE [dbo].[Contents] CHECK CONSTRAINT [FK_Contents_ContentGroups]
GO
ALTER TABLE [dbo].[LandingpageGroups]  WITH CHECK ADD  CONSTRAINT [FK_LandingpageGroups_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[LandingpageGroups] CHECK CONSTRAINT [FK_LandingpageGroups_Languages]
GO
ALTER TABLE [dbo].[LandingpageItems]  WITH CHECK ADD  CONSTRAINT [FK_LandingpageItems_LandingpageGroups] FOREIGN KEY([LandingpageGroup_Id])
REFERENCES [dbo].[LandingpageGroups] ([Id])
GO
ALTER TABLE [dbo].[LandingpageItems] CHECK CONSTRAINT [FK_LandingpageItems_LandingpageGroups]
GO
ALTER TABLE [dbo].[LandingpageItems]  WITH CHECK ADD  CONSTRAINT [FK_LandingpageItems_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[LandingpageItems] CHECK CONSTRAINT [FK_LandingpageItems_Languages]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_Languages]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_Menus] FOREIGN KEY([Menu_Id])
REFERENCES [dbo].[Menus] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_Menus]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_Menus_Menus] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[MenuItems] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_Menus_Menus]
GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_OrderItems_MainProducts] FOREIGN KEY([Product_Id])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[OrderItems] CHECK CONSTRAINT [FK_OrderItems_MainProducts]
GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_OrderItems_Orders] FOREIGN KEY([Order_Id])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderItems] CHECK CONSTRAINT [FK_OrderItems_Orders]
GO
ALTER TABLE [dbo].[OrderLogs]  WITH CHECK ADD  CONSTRAINT [FK_OrderLogs_Orders] FOREIGN KEY([Order_Id])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderLogs] CHECK CONSTRAINT [FK_OrderLogs_Orders]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderTypes] FOREIGN KEY([OrderType_Id])
REFERENCES [dbo].[OrderTypes] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_OrderTypes]
GO
ALTER TABLE [dbo].[OrderTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrderTypes_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[OrderTypes] CHECK CONSTRAINT [FK_OrderTypes_Languages]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Categories] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Categories]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Languages]
GO
ALTER TABLE [dbo].[Regions]  WITH CHECK ADD  CONSTRAINT [FK_Regions_Regions] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[Regions] ([Id])
GO
ALTER TABLE [dbo].[Regions] CHECK CONSTRAINT [FK_Regions_Regions]
GO
ALTER TABLE [dbo].[RoleContentBehaviors]  WITH CHECK ADD  CONSTRAINT [FK_RoleContentBehaviors_ContentBehaviors] FOREIGN KEY([ContentBehavior_Id])
REFERENCES [dbo].[ContentBehaviors] ([Id])
GO
ALTER TABLE [dbo].[RoleContentBehaviors] CHECK CONSTRAINT [FK_RoleContentBehaviors_ContentBehaviors]
GO
ALTER TABLE [dbo].[RoleContentBehaviors]  WITH CHECK ADD  CONSTRAINT [FK_RoleContentBehaviors_webpages_Roles] FOREIGN KEY([Role_RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[RoleContentBehaviors] CHECK CONSTRAINT [FK_RoleContentBehaviors_webpages_Roles]
GO
ALTER TABLE [dbo].[SlideShows]  WITH CHECK ADD  CONSTRAINT [FK_SlideShows_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[SlideShows] CHECK CONSTRAINT [FK_SlideShows_Languages]
GO
ALTER TABLE [dbo].[Solutions]  WITH CHECK ADD  CONSTRAINT [FK_Solutions_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Solutions] CHECK CONSTRAINT [FK_Solutions_Languages]
GO
ALTER TABLE [dbo].[StringResourceValues]  WITH CHECK ADD  CONSTRAINT [FK_StringResourceValues_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[StringResourceValues] CHECK CONSTRAINT [FK_StringResourceValues_Languages]
GO
ALTER TABLE [dbo].[StringResourceValues]  WITH CHECK ADD  CONSTRAINT [StringResourceValue_Key] FOREIGN KEY([Key_Id])
REFERENCES [dbo].[StringResourceKeys] ([Id])
GO
ALTER TABLE [dbo].[StringResourceValues] CHECK CONSTRAINT [StringResourceValue_Key]
GO
ALTER TABLE [dbo].[UrlRecords]  WITH CHECK ADD  CONSTRAINT [FK_UrlRecords_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[UrlRecords] CHECK CONSTRAINT [FK_UrlRecords_Languages]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_RoleId]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_UserId]
GO
ALTER TABLE [dbo].[Faqs]  WITH CHECK ADD  CONSTRAINT [FK_Faqs_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Partners] 
add [Type] [int] NOT NULL
GO