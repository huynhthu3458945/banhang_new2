
/****** Object:  Table [dbo].[Articles]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articles](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
	[SeName] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[LastUpdatedBy] [nvarchar](max) NULL,
	[IsVideo] [bit] NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[Category_Id] [bigint] NULL,
	[VideoLink] [nvarchar](max) NULL,
	[ExternalLink] [nvarchar](max) NULL,
	[MetaTitle] [nvarchar](500) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[MetaKeywords] [nvarchar](500) NULL,
	[MetaImage] [nvarchar](max) NULL,
	[Author] [nvarchar](max) NULL,
	[PromotionTitleLeft] [nvarchar](max) NULL,
	[PromotionTitleRight] [nvarchar](max) NULL,
	[PromotionBodyLeft] [nvarchar](max) NULL,
	[PromotionBodyRight] [nvarchar](max) NULL,
	[DateLeft] [nvarchar](max) NULL,
	[DetailBanner] [nvarchar](max) NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[BigImage] [nvarchar](max) NULL,
	[IsHot] [bit] NOT NULL,
	[TitleImage] [nvarchar](max) NULL,
 CONSTRAINT [PK__Articles__3214EC0768BD7F23] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Behaviors]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Behaviors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Type] [int] NOT NULL,
 CONSTRAINT [PK_Behaviors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Branches]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branches](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Street] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[Fax] [nvarchar](max) NULL,
	[Lng] [float] NOT NULL,
	[Lat] [float] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[Region_Id] [int] NOT NULL,
	[IsOnFooter] [bit] NOT NULL,
 CONSTRAINT [PK_Branches_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[SeName] [nvarchar](max) NULL,
	[IsSystem] [bit] NOT NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[DisplayTemplate] [int] NOT NULL,
	[EntityType] [int] NOT NULL,
	[PrivateArea] [bit] NOT NULL,
	[Parent_Id] [bigint] NULL,
	[IsPublished] [bit] NOT NULL,
	[IsShowOnMenu] [bit] NOT NULL,
	[ExternalLink] [nvarchar](max) NULL,
	[BannerLink] [nvarchar](max) NULL,
	[MetaTitle] [nvarchar](500) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[MetaKeywords] [nvarchar](500) NULL,
	[MetaImage] [nvarchar](max) NULL,
	[Language_Id] [int] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[ImageDefault2] [nvarchar](max) NULL,
	[IsHotProductCategory] [bit] NOT NULL,
	[ImageIcon] [nvarchar](max) NULL,
 CONSTRAINT [PK__Categori__3214EC07705EA0EB] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[Address] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContentBehaviors]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContentBehaviors](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Content_Id] [bigint] NOT NULL,
	[Behavior_Id] [int] NOT NULL,
 CONSTRAINT [PK_ContentBehaviors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContentGroups]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContentGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](max) NOT NULL,
	[Order] [int] NOT NULL,
	[IconClass] [nvarchar](max) NULL,
 CONSTRAINT [PK_ContentGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contents]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contents](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[ControllerName] [nvarchar](max) NOT NULL,
	[Order] [int] NOT NULL,
	[ContentGroup_Id] [int] NOT NULL,
 CONSTRAINT [PK_Contents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomInfoes]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomInfoes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK__CustomIn__3214EC07173876EA] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DownloadFiles]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DownloadFiles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[FilePath] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
 CONSTRAINT [PK_DownloadFiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LandingpageGroups]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LandingpageGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[PositionType] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_LandingpageGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LandingpageItems]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LandingpageItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[LandingpageGroup_Id] [int] NOT NULL,
 CONSTRAINT [PK_LandingpageItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageName] [nvarchar](max) NULL,
	[LanguageKey] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogHistories]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogHistories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EntityType] [int] NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[ActionType] [int] NOT NULL,
	[ActionBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Comment] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuItems]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NULL,
	[LinkTargetType] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[IconClass] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[Parent_Id] [bigint] NULL,
	[Menu_Id] [int] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_Menus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menus]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[MenuType] [int] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItems]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Product_Id] [bigint] NOT NULL,
	[Order_Id] [bigint] NOT NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[ItemCount] [int] NOT NULL,
	[Amount] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_OrderItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderLogs]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ActionBy] [nvarchar](max) NULL,
	[ActionTime] [datetime] NOT NULL,
	[OrderLogType] [int] NOT NULL,
	[OldValue] [nvarchar](max) NULL,
	[NewValue] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL,
	[Order_Id] [bigint] NULL,
 CONSTRAINT [PK_OrderLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BuyerName] [nvarchar](max) NULL,
	[BuyerEmail] [nvarchar](max) NULL,
	[BuyerPhone] [nvarchar](max) NULL,
	[BuyerAddress] [nvarchar](max) NULL,
	[OrderTime] [datetime] NOT NULL,
	[Amount] [decimal](18, 0) NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[OtherBuyerPhone] [nvarchar](max) NULL,
	[OrderType_Id] [int] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderTypes]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_OrderTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Partners]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partners](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[ImageAlt] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
 CONSTRAINT [PK_Partners] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermissionRefs]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionRefs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[IsEditor] [bit] NOT NULL,
	[IsManager] [bit] NOT NULL,
 CONSTRAINT [PK_PermissionRefs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[SeName] [nvarchar](max) NULL,
	[ImageDefault] [nvarchar](max) NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[Code] [nvarchar](max) NULL,
	[ImageCart] [nvarchar](max) NULL,
	[PromotionPrice] [decimal](18, 0) NULL,
	[PromotionPercent] [int] NULL,
	[ProductCode] [nchar](10) NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[Category_Id] [bigint] NOT NULL,
	[MetaTitle] [nvarchar](500) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[MetaKeywords] [nvarchar](500) NULL,
	[MetaImage] [nvarchar](max) NULL,
	[IsVideo] [bit] NOT NULL,
	[VideoId] [nvarchar](max) NULL,
	[DetailInfo] [nvarchar](max) NULL,
	[Advantages] [nvarchar](max) NULL,
	[Instruction] [nvarchar](max) NULL,
	[SalesCommitment] [nvarchar](max) NULL,
	[IsPromotion] [bit] NOT NULL,
	[ShortDescription] [nvarchar](max) NULL,
	[IsPriceContact] [bit] NULL,
	[IsLandingpage] [bit] NOT NULL,
 CONSTRAINT [PK_MainProducts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Regions]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Regions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Lng] [float] NOT NULL,
	[Lat] [float] NOT NULL,
	[Order] [int] NOT NULL,
	[Parent_Id] [int] NULL,
	[RegionType] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
 CONSTRAINT [PK_Regions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleContentBehaviors]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleContentBehaviors](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Role_RoleId] [int] NOT NULL,
	[ContentBehavior_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_RoleContentBehaviors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SlideShows]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SlideShows](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[ImageFolder] [nvarchar](max) NULL,
	[IsHomePage] [bit] NOT NULL,
	[Category_Id] [bigint] NOT NULL,
	[ImageMobile] [nvarchar](max) NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[Language_Id] [int] NOT NULL,
	[VideoId] [nvarchar](max) NULL,
	[IsVideo] [bit] NOT NULL,
 CONSTRAINT [PK__SlideSho__3214EC07168449D3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Solutions]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Solutions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Link] [nvarchar](max) NULL,
	[OriginalValue] [uniqueidentifier] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK_Solutions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StringResourceKeys]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StringResourceKeys](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK__StringRe__3214EC071A54DAB7] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StringResourceValues]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StringResourceValues](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Key_Id] [bigint] NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK__StringRe__3214EC071E256B9B] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Table]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table](
	[Id] [int] NOT NULL,
	[Description] [nchar](10) NULL,
	[Seconds] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UrlRecords]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UrlRecords](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SeName] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NULL,
	[EntityId] [bigint] NOT NULL,
	[EntityType] [int] NOT NULL,
	[Language_Id] [int] NOT NULL,
 CONSTRAINT [PK__UrlRecor__3214EC0725C68D63] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[LastedIP] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Videos]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Videos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[YoutubeLink] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[VideoId] [nvarchar](max) NULL,
	[Phase] [int] NULL,
	[TripName] [nvarchar](max) NULL,
	[Story] [nvarchar](max) NULL,
	[FullName] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Videos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Membership]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Membership](
	[UserId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ConfirmationToken] [nvarchar](128) NULL,
	[IsConfirmed] [bit] NULL,
	[LastPasswordFailureDate] [datetime] NULL,
	[PasswordFailuresSinceLastSuccess] [int] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordChangedDate] [datetime] NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[PasswordVerificationToken] [nvarchar](128) NULL,
	[PasswordVerificationTokenExpirationDate] [datetime] NULL,
 CONSTRAINT [PK__webpages__1788CC4C481BA567] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_OAuthMembership]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership](
	[Provider] [nvarchar](30) NOT NULL,
	[ProviderUserId] [nvarchar](100) NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Provider] ASC,
	[ProviderUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Roles]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[RoleDescription] [nvarchar](max) NULL,
	[IsSystem] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_UsersInRoles]    Script Date: 7/26/2021 10:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_UsersInRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Articles] ON 

INSERT [dbo].[Articles] ([Id], [Title], [Description], [Body], [SeName], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [LastUpdatedBy], [IsVideo], [IsPublished], [Order], [Category_Id], [VideoLink], [ExternalLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Author], [PromotionTitleLeft], [PromotionTitleRight], [PromotionBodyLeft], [PromotionBodyRight], [DateLeft], [DetailBanner], [OriginalValue], [Language_Id], [BigImage], [IsHot], [TitleImage]) VALUES (1, N'dự án 1', NULL, NULL, N'du-an-1', NULL, N'/Upload/News/9c0d98de-48dd-4c3f-87d2-2c975d16041e', CAST(N'2019-05-13T10:37:57.883' AS DateTime), CAST(N'2019-05-13T10:37:57.883' AS DateTime), NULL, NULL, 1, 0, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'5132771f-1c38-430a-96ce-ff3e19c32fa2', 1, NULL, 0, NULL)
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Body], [SeName], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [LastUpdatedBy], [IsVideo], [IsPublished], [Order], [Category_Id], [VideoLink], [ExternalLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Author], [PromotionTitleLeft], [PromotionTitleRight], [PromotionBodyLeft], [PromotionBodyRight], [DateLeft], [DetailBanner], [OriginalValue], [Language_Id], [BigImage], [IsHot], [TitleImage]) VALUES (2, N'dự án 2', NULL, NULL, N'du-an-2', NULL, N'/Upload/News/6d04321e-be32-4966-92bf-c21d36991be9', CAST(N'2019-05-13T10:38:22.873' AS DateTime), CAST(N'2019-05-13T10:38:22.873' AS DateTime), NULL, NULL, 1, 0, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'54b5d66e-921c-4aea-868d-ad910c60d541', 1, NULL, 0, NULL)
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Body], [SeName], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [LastUpdatedBy], [IsVideo], [IsPublished], [Order], [Category_Id], [VideoLink], [ExternalLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Author], [PromotionTitleLeft], [PromotionTitleRight], [PromotionBodyLeft], [PromotionBodyRight], [DateLeft], [DetailBanner], [OriginalValue], [Language_Id], [BigImage], [IsHot], [TitleImage]) VALUES (3, N'Dự án 3', NULL, NULL, N'du-an-3', NULL, N'/Upload/News/2303aa1a-a575-4618-b6ca-924cf9eb4d81', CAST(N'2019-05-13T10:38:38.560' AS DateTime), CAST(N'2019-05-13T10:38:38.560' AS DateTime), NULL, NULL, 1, 0, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'249ce470-0684-4fc7-82cc-7672ec88a1ec', 1, NULL, 0, NULL)
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Body], [SeName], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [LastUpdatedBy], [IsVideo], [IsPublished], [Order], [Category_Id], [VideoLink], [ExternalLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Author], [PromotionTitleLeft], [PromotionTitleRight], [PromotionBodyLeft], [PromotionBodyRight], [DateLeft], [DetailBanner], [OriginalValue], [Language_Id], [BigImage], [IsHot], [TitleImage]) VALUES (4, N'Dự án 4', NULL, NULL, N'du-an-4', NULL, N'/Upload/News/d69c2170-e0de-4270-ae53-90237413b85a', CAST(N'2019-05-13T10:38:51.603' AS DateTime), CAST(N'2019-05-13T10:38:51.603' AS DateTime), NULL, NULL, 1, 0, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'15bc9d0c-f03c-4046-bdfb-ddd215bcfdf0', 1, NULL, 0, NULL)
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Body], [SeName], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [LastUpdatedBy], [IsVideo], [IsPublished], [Order], [Category_Id], [VideoLink], [ExternalLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Author], [PromotionTitleLeft], [PromotionTitleRight], [PromotionBodyLeft], [PromotionBodyRight], [DateLeft], [DetailBanner], [OriginalValue], [Language_Id], [BigImage], [IsHot], [TitleImage]) VALUES (5, N'Dự án 5', NULL, NULL, N'du-an-5', NULL, N'/Upload/News/78de5f0b-4fcf-41e1-ae60-61b8d44d5dc3', CAST(N'2019-05-13T10:40:23.940' AS DateTime), CAST(N'2019-05-13T10:40:23.940' AS DateTime), NULL, NULL, 1, 0, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'3e2885e0-9c3f-41d9-b093-c238571652e1', 1, NULL, 0, NULL)
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Body], [SeName], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [LastUpdatedBy], [IsVideo], [IsPublished], [Order], [Category_Id], [VideoLink], [ExternalLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Author], [PromotionTitleLeft], [PromotionTitleRight], [PromotionBodyLeft], [PromotionBodyRight], [DateLeft], [DetailBanner], [OriginalValue], [Language_Id], [BigImage], [IsHot], [TitleImage]) VALUES (6, N'Dự án 6', NULL, NULL, N'du-an-6', NULL, N'/Upload/News/8178e4d1-8dfb-424e-803e-104c9c6379cd', CAST(N'2019-05-13T10:40:41.517' AS DateTime), CAST(N'2019-05-13T10:40:41.517' AS DateTime), NULL, NULL, 1, 0, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0671943c-9134-4b73-b419-02696cca4a0a', 1, NULL, 0, NULL)
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Body], [SeName], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [LastUpdatedBy], [IsVideo], [IsPublished], [Order], [Category_Id], [VideoLink], [ExternalLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Author], [PromotionTitleLeft], [PromotionTitleRight], [PromotionBodyLeft], [PromotionBodyRight], [DateLeft], [DetailBanner], [OriginalValue], [Language_Id], [BigImage], [IsHot], [TitleImage]) VALUES (7, N'Dự án 7', NULL, NULL, N'du-an-7', NULL, N'/Upload/News/3f57bb3a-6483-4d67-84b1-2d3abe39ba66', CAST(N'2019-05-13T10:41:09.333' AS DateTime), CAST(N'2019-05-13T10:41:09.333' AS DateTime), NULL, NULL, 1, 0, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'7431680b-5aea-4419-9762-52780c8f47ad', 1, NULL, 0, NULL)
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Body], [SeName], [ImageDefault], [ImageFolder], [CreateDate], [UpdateDate], [LastUpdatedBy], [IsVideo], [IsPublished], [Order], [Category_Id], [VideoLink], [ExternalLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Author], [PromotionTitleLeft], [PromotionTitleRight], [PromotionBodyLeft], [PromotionBodyRight], [DateLeft], [DetailBanner], [OriginalValue], [Language_Id], [BigImage], [IsHot], [TitleImage]) VALUES (8, N'Dự án 8', NULL, NULL, N'du-an-8', NULL, N'/Upload/News/a1907c01-41ff-41ba-94f5-1f88e2b2a50d', CAST(N'2019-05-13T10:41:36.287' AS DateTime), CAST(N'2019-05-13T10:41:36.287' AS DateTime), NULL, NULL, 1, 0, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1edc5487-722b-40c2-8352-5849afe81424', 1, NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[Articles] OFF
GO
SET IDENTITY_INSERT [dbo].[Behaviors] ON 

INSERT [dbo].[Behaviors] ([Id], [Name], [Type]) VALUES (1, N'Xem', 1)
INSERT [dbo].[Behaviors] ([Id], [Name], [Type]) VALUES (2, N'Thêm', 2)
INSERT [dbo].[Behaviors] ([Id], [Name], [Type]) VALUES (3, N'Sửa', 3)
INSERT [dbo].[Behaviors] ([Id], [Name], [Type]) VALUES (4, N'Xóa', 4)
INSERT [dbo].[Behaviors] ([Id], [Name], [Type]) VALUES (6, N'Chi tiết', 5)
SET IDENTITY_INSERT [dbo].[Behaviors] OFF
GO
SET IDENTITY_INSERT [dbo].[Branches] ON 

INSERT [dbo].[Branches] ([Id], [Name], [Street], [Phone], [Fax], [Lng], [Lat], [IsPublished], [Order], [Region_Id], [IsOnFooter]) VALUES (1, N'Chi nhánh 1', N'47/51/19A Nguyễn Tư Giản , P12, Q. Gò Vấp, Tp. HCM', N'(84-28) 3921 3177', N'(84-28) 3921 3179', 106.63761, 10.83368, 1, 2, 19, 1)
INSERT [dbo].[Branches] ([Id], [Name], [Street], [Phone], [Fax], [Lng], [Lat], [IsPublished], [Order], [Region_Id], [IsOnFooter]) VALUES (2, N'Chi nhánh 2', N'Số 312 Tôn Đức Thắng - Q. Đống Đa - TP. Hà Nội', N'(84-28) 3921 3177', N'(84-28) 3921 3179', 105.83018, 21.01917, 1, 3, 30, 0)
INSERT [dbo].[Branches] ([Id], [Name], [Street], [Phone], [Fax], [Lng], [Lat], [IsPublished], [Order], [Region_Id], [IsOnFooter]) VALUES (4, N'Trụ sở chính', N'Số 38 Đường số 13, Khu Đô Thị Vạn Phúc, P.Hiệp Bình Phước, Q. Thủ Đức, Tp. HCM', N'(84-8) 988.802.432', NULL, 106.770592, 10.849618, 1, 1, 4, 1)
SET IDENTITY_INSERT [dbo].[Branches] OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (1, NULL, NULL, N'root-news-category', 1, NULL, N'/Upload/Categoties/c4cd5e09-5aa3-4866-9acd-ee86443154e1', 0, 0, 3, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'd7411edf-c8f2-43f0-a84f-d851f6bd3c31', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (2, N'Trang chủ', NULL, N'trang-chu', 0, NULL, N'/Upload/Categoties/963c4b57-f8a3-48ac-85b1-22cfe83e80da', 1, 1, 3, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'11793a62-adec-4c2e-bc40-1529b2df0370', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (3, N'Sản phẩm', NULL, N'san-pham', 0, NULL, N'/Upload/Categoties/561094d3-8cc1-4114-8e2b-8adf0d89cd4d', 2, 3, 3, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'623b0393-cb0b-486a-a4c0-5c672d8effc6', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (4, N'Camera quan sát', NULL, N'camera-quan-sat', 0, NULL, N'/Upload/Categoties/701f613d-5ea0-4525-9024-4b80ca9e6c21', 1, 4, 3, 0, 3, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'503b4b83-ee59-4490-b63e-2bb3b87a7835', NULL, 0, N'/files/files/MenuIcons/casino-cctv.png')
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (5, N'Camera Hlkvision', NULL, N'camera-hlkvision', 0, N'/Upload/Categoties/32fa5b06-74bf-451a-9a04-efc5c8d2b387/item_cate.png', N'/Upload/Categoties/32fa5b06-74bf-451a-9a04-efc5c8d2b387', 2, 5, 3, 0, 4, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'8f88ae69-5dfe-432c-881a-40c0b776157b', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (6, N'Camera Hitech', NULL, N'camera-hitech', 0, N'/Upload/Categoties/6ba99e9e-94c2-4a0f-bf26-d287210fbc37/item_big_cate.jpg', N'/Upload/Categoties/6ba99e9e-94c2-4a0f-bf26-d287210fbc37', 1, 5, 3, 0, 9, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'88d43db1-1ce9-4765-a07b-f25ee0beeba9', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (7, N'Analog Camera Series HD', NULL, N'analog-camera-series-hd', 0, NULL, N'/Upload/Categoties/fa8ab5e2-29ab-43f1-a4f0-c8de6af30462', 1, 6, 3, 0, 6, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'4eed6279-7eb4-4c25-a53b-08e8c3b76232', NULL, 1, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (8, N'Giới thiệu', NULL, N'gioi-thieu', 0, NULL, N'/Upload/Categoties/202e233b-c00f-4daf-bab7-64e30693d07b', 3, 2, 3, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'4e953ce3-0abd-4b27-9c71-c5eba7d5c266', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (9, N'Tổng đài điện thoại', NULL, N'tong-dai-dien-thoai', 0, NULL, N'/Upload/Categoties/3bf2b064-66aa-4261-b957-c73e39bb8bec', 2, 4, 3, 0, 3, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'f68cd648-daed-47ac-b680-e59e82cb0e62', NULL, 0, N'/Upload/Categoties/3bf2b064-66aa-4261-b957-c73e39bb8bec/tong-dai-dien-thoai-iconpng24.png')
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (10, N'Tổng đài panasonic', NULL, N'tong-dai-panasonic', 0, NULL, N'/Upload/Categoties/a48238a0-6153-40e9-935a-ab8263b9c395', 1, 5, 3, 0, 9, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'7cda7a04-b2c4-4c6d-89d4-9e3bbeda0efc', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (11, N'Báo cháy báo trộm', NULL, N'bao-chay-bao-trom', 0, NULL, N'/Upload/Categoties/54b8ba82-42a6-4c4f-bb46-b127a00bef65', 3, 4, 3, 0, 3, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'5a00405a-2338-4eda-81b8-b2f83840395c', NULL, 0, N'/Upload/Categoties/54b8ba82-42a6-4c4f-bb46-b127a00bef65/baotrom24.jpg')
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (12, N'Giải pháp', NULL, N'giai-phap', 0, NULL, N'/Upload/Categoties/7fdb7bcd-9bee-458d-ab83-d25dc3247b89', 4, 7, 3, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'ecb014dc-4c91-46d6-9365-5b1a60f54ed8', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (13, N'Dự Án', NULL, N'du-an', 0, NULL, N'/Upload/Categoties/27e7a72a-47ce-4040-ae14-d19406696794', 5, 8, 3, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'637071b3-1c83-4619-be34-26ddfcb99ba0', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (14, N'Tin tức', NULL, N'tin-tuc', 0, NULL, N'/Upload/Categoties/9905535b-5efa-4013-b489-b247a5bb0f0b', 6, 9, 3, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'9ee770a0-0b50-47de-8aea-75978369958b', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (15, N'Liên hệ', NULL, N'lien-he', 0, NULL, N'/Upload/Categoties/8374e8f7-38df-4e2e-9143-f5cbf0bb10d7', 8, 10, 3, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'1e397d54-ed69-4136-8778-02a7468290be', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (16, N'Camera IP HIKVision', NULL, N'camera-ip-hikvision', 0, NULL, N'/Upload/Categoties/f5aaf0b4-c1f2-4e52-bf92-f20545a573df', 1, 6, 3, 0, 5, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'ef735858-9046-4c8f-ba56-2aed39f892cc', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (17, N'Camera Analog HIKVision', NULL, N'camera-analog-hikvision', 0, NULL, N'/Upload/Categoties/34cbab18-8b34-4c8c-92ce-f33d8039e12c', 2, 6, 3, 0, 5, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'3ed3f11d-0944-4a5f-a777-f2db2b32d848', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (18, N'Panasonic', NULL, N'panasonic', 0, N'/Upload/Categoties/ac1e7060-147d-4170-8691-af224ed5974f/item_cate.png', N'/Upload/Categoties/ac1e7060-147d-4170-8691-af224ed5974f', 3, 5, 3, 0, 4, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'2a3290ab-4194-4510-8142-54c59e4d04f7', NULL, 1, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (19, N'Camera AVTech', NULL, N'camera-avtech', 0, N'	/Upload/Categoties/a5e38d01-b6f6-40b7-8a82-44b25ac55011/item_cate.png', N'/Upload/Categoties/a5e38d01-b6f6-40b7-8a82-44b25ac55011', 4, 5, 3, 0, 4, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'd09be5e5-0c44-4f9b-aa87-febda10040af', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (20, N'Phụ kiện Camera', NULL, N'phu-kien-camera', 0, N'	/Upload/Categoties/fa68cca3-bcd1-4f19-80bf-5a85badbb996/item_cate.png', N'/Upload/Categoties/fa68cca3-bcd1-4f19-80bf-5a85badbb996', 5, 5, 3, 0, 4, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'ddc87f92-299a-4fe7-937d-ce1696ec1461', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (21, N'Camera SamSung', NULL, N'camera-samsung', 0, N'/Upload/Categoties/9cbfc919-acc9-48df-96f4-29da268519a2/item_big_cate.jpg', N'/Upload/Categoties/9cbfc919-acc9-48df-96f4-29da268519a2', 6, 5, 3, 0, 4, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'376ee8ca-379a-4472-b17b-f85ebbb59ea9', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (22, N'Camera HIKVision Plus', NULL, N'camera-hikvision-plus', 0, N'/Upload/Categoties/b54ea2a4-6150-4d9c-8a6e-6ce0a4821342/item_big_cate.jpg', N'/Upload/Categoties/b54ea2a4-6150-4d9c-8a6e-6ce0a4821342', 7, 5, 3, 0, 4, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'459ef84a-7867-4044-b595-77c5799c232f', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (24, N'Landing page', NULL, N'landing-page', 0, NULL, N'/Upload/Categoties/d98ec199-d749-43f8-b110-81065429525f', 1, 11, 3, 0, 11, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'0c73ce22-6f94-4953-bec2-f98f657ba4cc', NULL, 0, NULL)
INSERT [dbo].[Categories] ([Id], [Title], [Description], [SeName], [IsSystem], [ImageDefault], [ImageFolder], [Order], [DisplayTemplate], [EntityType], [PrivateArea], [Parent_Id], [IsPublished], [IsShowOnMenu], [ExternalLink], [BannerLink], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [Language_Id], [OriginalValue], [ImageDefault2], [IsHotProductCategory], [ImageIcon]) VALUES (25, N'Download', NULL, N'download', 0, NULL, N'/Upload/Categoties/8c5931c8-c359-446a-894a-20a4ee86d4b9', 7, 12, 3, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'9f539a61-d34c-4487-8314-bfc68a78de8e', NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Contacts] ON 

INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (1, N'Đà Nẵng 6', N'tin.ho@web4gsolutions.com', N'1234567890', CAST(N'2019-01-01T20:59:16.780' AS DateTime), N'Đường Nguyễn Oanh', N'aaa')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (2, N'Nguyễn Minh Trường', N'anhchangvt1994@gmail.com', N'0948621519', CAST(N'2019-01-05T21:55:47.627' AS DateTime), N'64 Phu Tho Hoa', N'hello everybody')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (3, N'Nguyên thanh tung', N'mrtung1121@gmail.com', N'0986207949', CAST(N'2019-01-09T12:29:02.553' AS DateTime), N'28-10 ma lo', N'Mình là thanh tung bạn liên hệ với mình nha')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (4, N'Đà Nẵng 6', N'contact@convoivang.vn', N'1234567890', CAST(N'2019-01-09T16:38:23.300' AS DateTime), N'Đường Nguyễn Oanh', N'dđ')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (5, N'Thẻ Tín Dụng', N'tinhqit@gmail.com', N'1234567890', CAST(N'2019-01-09T17:32:25.887' AS DateTime), N'Đường Nguyễn Oanh', N'jjjjj')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (6, N'NOVABEACH CAM RANH', N'tin.ho@web4gsolutions.com', N'1234567890', CAST(N'2019-03-27T21:44:05.973' AS DateTime), NULL, N'fff')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (7, N'NOVABEACH CAM RANH', N'tinhqit@gmail.com', N'1234567890', CAST(N'2019-03-27T21:46:12.917' AS DateTime), NULL, N'ffff')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (8, N'Banner MOTULTECH', N'tinhqit@gmail.com', N'1234567890', CAST(N'2019-05-04T23:54:33.977' AS DateTime), NULL, N'ddd')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (9, N'Banner có gì mới', N'tinhqit@gmail.com', N'9099999999', CAST(N'2019-05-05T00:04:35.440' AS DateTime), NULL, N'gggg')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (10, N'NOVABEACH CAM RANH', N'tinhqit@gmail.com', N'1234567890', CAST(N'2019-05-05T00:06:24.540' AS DateTime), NULL, N'ggggd')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (11, N'Banner MOTULTECH', N'tinhqit@gmail.com', N'1234567890', CAST(N'2019-05-05T18:49:30.983' AS DateTime), NULL, N'gggg')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (12, N'Banner có gì mới', N'tinhqit@gmail.com', N'1234567890', CAST(N'2019-05-05T18:59:57.093' AS DateTime), NULL, N'gg')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (13, N'Dầu gia công không pha nước', N'tinhqit@gmail.com', N'1234567890', CAST(N'2019-05-05T19:04:45.057' AS DateTime), NULL, N'gggg')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (14, N'Banner có gì mới', N'tinhqit@gmail.com', N'1234567890', CAST(N'2019-05-05T19:09:51.080' AS DateTime), NULL, N'gggg')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (15, N'NOVABEACH CAM RANH', N'contact@convoivang.vn', N'1234567890', CAST(N'2019-05-05T19:15:50.040' AS DateTime), NULL, N'dđ')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (16, N'NOVABEACH CAM RANH', N'contact@convoivang.vn', N'1234567890', CAST(N'2019-05-05T19:18:27.917' AS DateTime), NULL, N'ghfhfg')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (17, N'Banner có gì mới', N'contact@convoivang.vn', N'1234567890', CAST(N'2019-05-05T19:20:17.217' AS DateTime), NULL, N'ggggg')
INSERT [dbo].[Contacts] ([Id], [Name], [Email], [Phone], [CreatedDate], [Address], [Content]) VALUES (18, N'Banner có gì mới', N'contact@convoivang.vn', N'1234567890', CAST(N'2019-05-05T19:21:59.070' AS DateTime), NULL, N'fgdf')
SET IDENTITY_INSERT [dbo].[Contacts] OFF
GO
SET IDENTITY_INSERT [dbo].[ContentBehaviors] ON 

INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (1, 1, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (3, 1, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (5, 2, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (7, 2, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (10, 3, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (11, 3, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (12, 3, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (13, 3, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (14, 4, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (15, 4, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (16, 4, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (17, 4, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (18, 5, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (19, 5, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (20, 5, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (21, 5, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (22, 6, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (23, 6, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (24, 6, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (25, 6, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (31, 9, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (32, 9, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (33, 9, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (34, 9, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (39, 11, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (40, 11, 6)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (41, 12, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (42, 12, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (43, 12, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (44, 12, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (45, 13, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (46, 13, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (47, 13, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (48, 13, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (49, 14, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (50, 14, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (51, 14, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (52, 14, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (53, 15, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (54, 15, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (55, 15, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (56, 15, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (57, 17, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (58, 17, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (59, 17, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (60, 17, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (61, 16, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (63, 16, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (64, 16, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (65, 16, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (66, 18, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (67, 18, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (68, 18, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (69, 18, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (70, 19, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (71, 19, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (72, 19, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (73, 19, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (74, 20, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (75, 20, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (76, 20, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (77, 20, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (78, 21, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (79, 21, 6)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (80, 21, 4)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (81, 22, 1)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (82, 22, 2)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (83, 22, 3)
INSERT [dbo].[ContentBehaviors] ([Id], [Content_Id], [Behavior_Id]) VALUES (84, 22, 4)
SET IDENTITY_INSERT [dbo].[ContentBehaviors] OFF
GO
SET IDENTITY_INSERT [dbo].[ContentGroups] ON 

INSERT [dbo].[ContentGroups] ([Id], [GroupName], [Order], [IconClass]) VALUES (1, N'Cài đặt', 1, N'fa fa-wrench')
INSERT [dbo].[ContentGroups] ([Id], [GroupName], [Order], [IconClass]) VALUES (2, N'Nội dung', 3, N'fa fa-database')
INSERT [dbo].[ContentGroups] ([Id], [GroupName], [Order], [IconClass]) VALUES (3, N'Thông tin liên hệ', 6, N'fa-address-card')
INSERT [dbo].[ContentGroups] ([Id], [GroupName], [Order], [IconClass]) VALUES (4, N'Menu', 2, N'fa fa-bars')
INSERT [dbo].[ContentGroups] ([Id], [GroupName], [Order], [IconClass]) VALUES (5, N'Landing page', 4, N'fa-pagelines')
INSERT [dbo].[ContentGroups] ([Id], [GroupName], [Order], [IconClass]) VALUES (6, N'E-commerce', 5, N'fa-shopping-cart')
SET IDENTITY_INSERT [dbo].[ContentGroups] OFF
GO
SET IDENTITY_INSERT [dbo].[Contents] ON 

INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (1, N'Thiết lập', N'SettingAdm', 1, 1)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (2, N'Tài nguyên', N'ResourceAdmin', 2, 1)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (3, N'Danh mục', N'CategoryAdmin', 1, 2)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (4, N'Bài viết', N'ArticleAdmin', 2, 2)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (5, N'Banner', N'SlideShowAdmin', 3, 2)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (6, N'Đối tác', N'PartnerAdmin', 4, 2)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (9, N'Files download', N'DownloadFileAdmin', 6, 2)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (11, N'Danh sách liên hệ', N'ContactAdmin', 4, 3)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (12, N'Loại menu', N'MenuAdmin', 1, 4)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (13, N'Danh sách menu', N'MenuItemAdmin', 2, 4)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (14, N'Sản phẩm', N'ProductAdmin', 1, 6)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (15, N'Giải pháp trang chủ', N'SolutionAdmin', 5, 2)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (16, N'Danh mục ', N'LandingpageGroupAdmin', 1, 5)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (17, N'Bài viết', N'LandingpageItemAdmin', 2, 5)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (18, N'Tỉnh/Thành phố', N'CityAdmin', 1, 3)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (19, N'Quận/Huyện', N'DistrictAdmin', 2, 3)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (20, N'Chi nhánh', N'BranchAdmin', 3, 3)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (21, N'Đơn hàng', N'OrderAdmin', 2, 6)
INSERT [dbo].[Contents] ([Id], [Title], [ControllerName], [Order], [ContentGroup_Id]) VALUES (22, N'Hình thức đặt hàng', N'OrderTypeAdmin', 3, 6)
SET IDENTITY_INSERT [dbo].[Contents] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomInfoes] ON 

INSERT [dbo].[CustomInfoes] ([Id], [Key], [Value], [IsPublished], [OriginalValue], [Language_Id]) VALUES (1, N'PhoneNumber1', N'0908234455', 1, N'ba96a035-be76-4a9e-987f-2806160529c3', 1)
INSERT [dbo].[CustomInfoes] ([Id], [Key], [Value], [IsPublished], [OriginalValue], [Language_Id]) VALUES (2, N'PhoneNumber1', N'0908234455', 1, N'ba96a035-be76-4a9e-987f-2806160529c3', 2)
INSERT [dbo].[CustomInfoes] ([Id], [Key], [Value], [IsPublished], [OriginalValue], [Language_Id]) VALUES (3, N'PhoneNumber1', N' 0908234455', 1, N'ba96a035-be76-4a9e-987f-2806160529c3', 3)
INSERT [dbo].[CustomInfoes] ([Id], [Key], [Value], [IsPublished], [OriginalValue], [Language_Id]) VALUES (4, N'PhoneNumber2', N'0908234455', 1, N'a7ba138c-a0d4-49bc-ae48-e9cf279c3ce8', 1)
SET IDENTITY_INSERT [dbo].[CustomInfoes] OFF
GO
SET IDENTITY_INSERT [dbo].[DownloadFiles] ON 

INSERT [dbo].[DownloadFiles] ([Id], [Title], [Description], [ImageDefault], [ImageFolder], [FilePath], [Order], [IsPublished]) VALUES (1, N'File 1', N'This is file 1', N'/Upload/Popup/1074945b-6def-4565-8088-34cb8b915fcb/201IPHD.jpg', N'/Upload/Popup/1074945b-6def-4565-8088-34cb8b915fcb', N'/Upload/FileDownloadle/Brief Agency - 2018 (1).ppt', 1, 1)
SET IDENTITY_INSERT [dbo].[DownloadFiles] OFF
GO
SET IDENTITY_INSERT [dbo].[LandingpageGroups] ON 

INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (1, N'Sản phẩm', NULL, N'/Upload/LandingpageGroup/1b97d008-e19a-430c-83e9-5b9b8a48e91a', NULL, 1, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (2, N'Quy trình', NULL, N'/Upload/LandingpageGroup/c053365e-cbad-4eb7-b3a0-169cd0066e08', NULL, 2, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (3, N'Thông tin liên lạc', NULL, N'/Upload/LandingpageGroup/0a52f7e0-5761-4075-b7d7-a491edfc8b43', NULL, 3, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (4, N'Giải pháp', NULL, N'/Upload/LandingpageGroup/2df69423-7c66-4f26-a8a7-80c7e19d2c83', N'/Upload/LandingpageGroup/2df69423-7c66-4f26-a8a7-80c7e19d2c83/itc3.jpg', 4, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (5, N'Thiết bị nổi bật', NULL, N'/Upload/LandingpageGroup/89d4a7b9-a94c-4714-b84b-09398f81cd76', NULL, 5, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (6, N'Đến showroom CameraFHD.com test camera để an tâm về chất lượng', N'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas quia doloribus aperiam autem in, magni deleniti repudiandae nihil voluptates accusantium.', N'/Upload/LandingpageGroup/cf1b16f0-fd15-4383-afaa-47d832b98a4d', NULL, 6, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (7, N'HỆ THỐNG AN NINH 24/24 GIỜ', NULL, N'/Upload/LandingpageGroup/96755531-4d87-44d2-b484-8944e0cbdd3e', NULL, 7, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (8, N'SẢN PHẨM LIÊN QUAN', NULL, N'/Upload/LandingpageGroup/e6044075-9690-4165-bff9-2f3a6699c3bb', NULL, 8, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (9, N'TƯ VẤN', N'Quý khách chỉ cần liên hệ với bộ phận kinh doanh của itcgroup.vn ngay lập tức sẽ nhận được tư vấn nhiệt tình giúp quý khách chọn được bộ sản phẩm camera phù hợp nhất.', N'/Upload/LandingpageGroup/f441a1ac-d209-4bb1-94ec-ae80c908f437', NULL, 9, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (10, N'LẮP ĐẶT', NULL, N'/Upload/LandingpageGroup/a3b1fa73-862d-477e-bb66-f35eb72ac156', NULL, 10, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (11, N'BẢO HÀNH', N'Với các quy định bảo hành chu đáo, CameraFHD.com cam kết quyền lợi tốt nhất cho khách hàng.', N'/Upload/LandingpageGroup/d3a6ea7b-883d-4716-8dde-68e2d9e15763', NULL, 11, 0, N'00000000-0000-0000-0000-000000000000', 1)
INSERT [dbo].[LandingpageGroups] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [PositionType], [IsPublished], [OriginalValue], [Language_Id]) VALUES (12, N'4 Tiêu chí', NULL, N'/Upload/LandingpageGroup/ef12aac7-582b-40b6-8de1-8a416202b4b4', NULL, 12, 0, N'00000000-0000-0000-0000-000000000000', 1)
SET IDENTITY_INSERT [dbo].[LandingpageGroups] OFF
GO
SET IDENTITY_INSERT [dbo].[LandingpageItems] ON 

INSERT [dbo].[LandingpageItems] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [Url], [IsPublished], [Order], [OriginalValue], [Language_Id], [LandingpageGroup_Id]) VALUES (1, N'Trọn bộ 1 camera', NULL, N'/Upload/LandingpageItem/353ff235-f169-4d45-a8dd-724caaa1c716', N'/Upload/LandingpageItem/353ff235-f169-4d45-a8dd-724caaa1c716/item_big_cate.jpg', N'/san-pham/camera-quan-sat/camera-hitech', 1, 1, N'26d0644c-c955-4905-a221-0ce525a6261d', 1, 1)
INSERT [dbo].[LandingpageItems] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [Url], [IsPublished], [Order], [OriginalValue], [Language_Id], [LandingpageGroup_Id]) VALUES (2, N'Trọn bộ 2 camera', NULL, N'/Upload/LandingpageItem/0a92da26-6ebe-48c6-a453-054ddee39ab7', N'/Upload/LandingpageItem/0a92da26-6ebe-48c6-a453-054ddee39ab7/item_cate.png', N'/san-pham/camera-quan-sat/camera-hlkvision', 1, 2, N'b4430290-7b32-493e-9bb8-615211dd1e25', 1, 1)
INSERT [dbo].[LandingpageItems] ([Id], [Title], [Description], [ImageFolder], [ImageDefault], [Url], [IsPublished], [Order], [OriginalValue], [Language_Id], [LandingpageGroup_Id]) VALUES (3, N'Thông tin showroom', N'<p class="txt_place">Trực tiếp tại showroom</p>
									<p class="name_place">itcgroup.vn</p>', N'/Upload/LandingpageItem/3886b7fb-9b37-4858-95b2-deec36eb9749', NULL, NULL, 1, 1, N'0e99753c-54d8-4183-b313-dd7ed3bddb3b', 1, 3)
SET IDENTITY_INSERT [dbo].[LandingpageItems] OFF
GO
SET IDENTITY_INSERT [dbo].[Languages] ON 

INSERT [dbo].[Languages] ([Id], [LanguageName], [LanguageKey], [Image]) VALUES (1, N'Vietnamese', N'vi', N'/Content/style_admin/custom/images/vi.jpg')
INSERT [dbo].[Languages] ([Id], [LanguageName], [LanguageKey], [Image]) VALUES (2, N'English', N'en', N'/Content/style_admin/custom/images/en.jpg ')
SET IDENTITY_INSERT [dbo].[Languages] OFF
GO
SET IDENTITY_INSERT [dbo].[LogHistories] ON 

INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1, 7, 0, 4, N'administrator', CAST(N'2014-10-18T00:10:34.020' AS DateTime), N'/Upload/SlideShow/0b0a8434-ca76-4ee0-a853-fce10e211c40/h_8.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (2, 1, 1, 2, N'administrator', CAST(N'2014-10-18T00:14:42.963' AS DateTime), N'/Upload/News/13aba707-155d-4c77-8179-c86d64769c42')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (3, 1, 2, 2, N'administrator', CAST(N'2014-10-18T00:20:59.310' AS DateTime), N'/Upload/News/8f010039-65b3-46bb-af4b-2f728723dd66')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (4, 1, 3, 2, N'administrator', CAST(N'2014-10-18T00:21:53.977' AS DateTime), N'/Upload/News/836c9518-1f17-429b-adcc-a2148d1d3ff3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (5, 1, 4, 2, N'administrator', CAST(N'2014-10-18T00:23:07.843' AS DateTime), N'/Upload/News/8c599666-d7ce-41bf-b799-da0ef6f8aa1f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (6, 1, 5, 2, N'administrator', CAST(N'2014-10-18T00:23:55.927' AS DateTime), N'/Upload/News/50c7a5f2-030c-4fb5-81fc-756653dce177')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (7, 1, 6, 2, N'administrator', CAST(N'2014-10-18T00:26:13.053' AS DateTime), N'/Upload/News/305720f5-a865-481b-9dab-e2728cd7068d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (8, 1, 7, 2, N'administrator', CAST(N'2014-10-18T00:29:12.173' AS DateTime), N'/Upload/News/1911a655-be54-4f5e-9206-6a2847447ac6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (9, 1, 8, 2, N'administrator', CAST(N'2014-10-18T00:30:23.400' AS DateTime), N'/Upload/News/e58b0d4b-114f-4491-950e-aa9b394decd2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (10, 1, 9, 2, N'administrator', CAST(N'2014-10-18T00:31:49.490' AS DateTime), N'/Upload/News/0666ca3c-d251-4bbf-85cd-b723f9cbee54')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (11, 1, 10, 2, N'administrator', CAST(N'2014-10-18T00:32:58.497' AS DateTime), N'/Upload/News/2d505485-d43b-4f3d-965b-b50307dfb9ac')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (12, 1, 11, 2, N'administrator', CAST(N'2014-10-18T00:34:00.077' AS DateTime), N'/Upload/News/9dd43552-804d-4c18-aae9-d129051ae562')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (13, 1, 12, 2, N'administrator', CAST(N'2014-10-18T00:36:33.827' AS DateTime), N'/Upload/News/d3aa2b24-c853-463a-b166-0928861671b2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (14, 1, 13, 2, N'administrator', CAST(N'2014-10-18T00:58:58.020' AS DateTime), N'/Upload/News/00902d17-356c-495a-9749-ccf79fd6854b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (15, 1, 14, 2, N'administrator', CAST(N'2014-10-18T01:00:58.420' AS DateTime), N'/Upload/News/87f6c375-5cef-4397-bacc-e7caf8d9f17d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (16, 1, 15, 2, N'administrator', CAST(N'2014-10-18T01:02:51.890' AS DateTime), N'/Upload/News/853c5873-8492-40f8-85f5-542ac04e6528')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (17, 1, 16, 2, N'administrator', CAST(N'2014-10-18T12:45:36.557' AS DateTime), N'/Upload/News/7815906b-85d6-4045-9017-d282a0fe501c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (18, 1, 17, 2, N'administrator', CAST(N'2014-10-18T12:47:31.803' AS DateTime), N'/Upload/News/2cc9a6e5-7f83-44e8-9f92-8e768aa72e86')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (19, 1, 18, 2, N'administrator', CAST(N'2014-10-19T16:32:17.437' AS DateTime), N'/Upload/News/87d7a722-b012-4c7c-9902-136fff179210')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (20, 1, 19, 2, N'administrator', CAST(N'2014-10-19T16:34:53.783' AS DateTime), N'/Upload/News/b308c55d-4cf1-4c48-8f88-d085c0ce7d6f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (21, 1, 20, 2, N'administrator', CAST(N'2014-10-19T16:36:11.623' AS DateTime), N'/Upload/News/d2e010f3-3218-410a-9828-4bb8a13442a5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (22, 1, 21, 2, N'administrator', CAST(N'2014-10-19T20:42:31.957' AS DateTime), N'/Upload/News/93a2f78d-f7b6-440e-8b28-bf1381e6df30')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (23, 1, 22, 2, N'administrator', CAST(N'2014-10-22T11:31:00.073' AS DateTime), N'/Upload/News/85325894-5fbe-4ed5-9d59-b5a85741cce9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (24, 1, 23, 2, N'administrator', CAST(N'2014-10-22T11:33:27.477' AS DateTime), N'/Upload/News/11ec17f6-d5a2-4f4b-83b3-a6e06ea65607')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (25, 1, 0, 1, N'administrator', CAST(N'2014-10-22T11:37:26.680' AS DateTime), N'/Upload/News/5a998fc4-21ae-41e5-9be6-5c675056c92a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (26, 1, 0, 1, N'administrator', CAST(N'2014-10-22T11:49:02.253' AS DateTime), N'/Upload/News/81c51a56-b808-49a9-b6d4-98ce2054fe71')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (27, 1, 24, 2, N'administrator', CAST(N'2014-10-22T15:18:28.937' AS DateTime), N'/Upload/News/af466d07-a9cd-469d-9af6-28f3b8ffafac')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (28, 1, 25, 2, N'administrator', CAST(N'2014-10-22T15:20:49.923' AS DateTime), N'/Upload/News/4e7a3be9-74a1-4073-97e1-de6112c6dbcf')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (29, 1, 26, 2, N'administrator', CAST(N'2014-10-22T15:22:04.530' AS DateTime), N'/Upload/News/1ae03d0d-9714-4fb1-8894-afa1bec45cef')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (30, 1, 27, 2, N'administrator', CAST(N'2014-10-22T16:13:54.287' AS DateTime), N'/Upload/News/b9ffd88a-a5de-4fa8-877f-ee4925b44bc2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (31, 1, 28, 2, N'administrator', CAST(N'2014-10-22T16:16:34.437' AS DateTime), N'/Upload/News/a2784fbc-52d9-4ee2-84bb-418d53f1c6aa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (32, 1, 29, 2, N'administrator', CAST(N'2014-10-22T16:20:45.540' AS DateTime), N'/Upload/News/4b5f293d-3e0b-467a-a292-2d181969bb0b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (33, 1, 30, 2, N'administrator', CAST(N'2014-10-22T16:22:35.530' AS DateTime), N'/Upload/News/0af3536f-cb9c-43c4-8ab7-3083eadf4a5e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (34, 1, 31, 2, N'administrator', CAST(N'2014-10-22T16:23:34.917' AS DateTime), N'/Upload/News/8e40a6b7-627c-46c1-9945-0280f9d431fa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (35, 1, 32, 2, N'administrator', CAST(N'2014-10-22T16:29:12.367' AS DateTime), N'/Upload/News/967ae473-f90d-40b4-b944-600d4ec93b9f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (36, 1, 33, 2, N'administrator', CAST(N'2014-10-22T16:30:40.267' AS DateTime), N'/Upload/News/fd176014-9579-4cc0-b43f-6e0abb824a68')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (37, 1, 34, 2, N'administrator', CAST(N'2014-10-22T16:31:44.410' AS DateTime), N'/Upload/News/c8a990e5-883a-4e23-8d6c-8cb54db4a8bd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (38, 1, 35, 2, N'administrator', CAST(N'2014-10-22T16:32:16.957' AS DateTime), N'/Upload/News/3988fa06-1374-4602-96a6-576c88855f66')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (39, 1, 36, 2, N'administrator', CAST(N'2014-10-22T16:35:31.957' AS DateTime), N'/Upload/News/b5d2a75f-d92e-4b2b-a8fd-1cacf255f18d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (40, 1, 37, 2, N'administrator', CAST(N'2014-10-22T16:36:15.407' AS DateTime), N'/Upload/News/cd08e51c-d730-4691-b887-1ae0bb75ba9c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (41, 1, 38, 2, N'administrator', CAST(N'2014-10-22T16:37:06.267' AS DateTime), N'/Upload/News/48e277bf-acc4-49d2-9eeb-d44c5a1129fc')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (42, 1, 0, 1, N'administrator', CAST(N'2014-10-22T16:38:14.227' AS DateTime), N'/Upload/News/760d878c-d239-4d2c-946d-3549f2be4488')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (43, 1, 0, 1, N'administrator', CAST(N'2014-10-22T16:41:04.503' AS DateTime), N'/Upload/News/c610b2a2-a93d-42c7-8f90-d55ad3904913')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (44, 1, 39, 2, N'administrator', CAST(N'2014-10-22T16:41:10.873' AS DateTime), N'/Upload/News/bb5612de-3ca4-4db7-be79-0fcff2a68c0b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (45, 1, 40, 2, N'administrator', CAST(N'2014-10-22T16:42:25.403' AS DateTime), N'/Upload/News/4caa2cd5-e361-467e-b1db-39056af31e5a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (46, 1, 41, 2, N'administrator', CAST(N'2014-10-22T21:40:54.257' AS DateTime), N'/Upload/News/be2a5cc6-d3c8-47a4-a7d7-44deff9afca1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (47, 1, 42, 2, N'administrator', CAST(N'2014-10-22T21:45:04.557' AS DateTime), N'/Upload/News/6f17448d-3ad4-4767-8fa0-ce46244a100e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (48, 7, 0, 4, N'administrator', CAST(N'2014-10-24T18:01:21.747' AS DateTime), N'/Upload/Categories/8f5a59ec-8447-4459-8f24-c30bbd8e3196/10.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (49, 1, 0, 1, N'administrator', CAST(N'2014-10-28T10:23:41.273' AS DateTime), N'/Upload/News/a955fd8d-d045-4a85-9a49-3c9aaf72497c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (50, 1, 0, 1, N'administrator', CAST(N'2014-10-28T10:35:56.287' AS DateTime), N'/Upload/News/d020c64c-da4d-4495-8cb0-0cc567db94cc')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (51, 1, 1, 2, N'administrator', CAST(N'2014-10-28T15:05:26.270' AS DateTime), N'/Upload/News/11e12c5b-76fe-4351-9056-c68a3c1599ea')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (52, 1, 0, 1, N'administrator', CAST(N'2014-10-28T15:14:51.597' AS DateTime), N'/Upload/News/cb7f74aa-9440-4feb-9a99-2c42418207b9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (53, 1, 0, 1, N'administrator', CAST(N'2014-10-28T15:15:54.893' AS DateTime), N'/Upload/News/64f511c7-9ea3-42b5-887c-d59ae822ece2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (54, 7, 0, 4, N'administrator', CAST(N'2014-10-29T23:11:59.490' AS DateTime), N'/Upload/Videos/a4c3da37-1104-4e94-a7d0-1c61de94f069/12.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (55, 7, 0, 4, N'administrator', CAST(N'2014-10-30T00:19:16.630' AS DateTime), N'/Upload/Videos/94496d7d-e3d8-40ef-bff7-983349f17d49/1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (56, 7, 0, 4, N'administrator', CAST(N'2014-10-30T10:30:38.070' AS DateTime), N'/Upload/Videos/cf57ea6f-1d39-4617-8d6c-3a62cec3d33e/Halloween.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (57, 7, 0, 4, N'administrator', CAST(N'2014-10-30T16:10:56.653' AS DateTime), N'/Upload/Videos/cf57ea6f-1d39-4617-8d6c-3a62cec3d33e/Halloween.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (58, 7, 0, 4, N'administrator', CAST(N'2014-10-30T16:11:51.297' AS DateTime), N'/Upload/Videos/aeb5241d-76d3-4c1c-8cd9-4097f7ac8a96/Nature-Wallpapers-Stone.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (59, 7, 0, 4, N'administrator', CAST(N'2014-10-30T16:52:26.540' AS DateTime), N'/Upload/Videos/cf57ea6f-1d39-4617-8d6c-3a62cec3d33e/12.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (60, 7, 0, 4, N'administrator', CAST(N'2014-10-31T18:44:16.037' AS DateTime), N'/Upload/Categories/da7a7661-3712-44af-be9a-0bef9eda3a51/39.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (61, 1, 2, 2, N'administrator', CAST(N'2014-11-01T00:16:49.997' AS DateTime), N'/Upload/News/7500ad7d-7922-46a8-87fd-1ce61959f0c6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (62, 1, 3, 2, N'administrator', CAST(N'2014-11-01T00:18:33.333' AS DateTime), N'/Upload/News/0e3561ee-62be-46f6-b021-1611e4da2494')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (63, 1, 4, 2, N'administrator', CAST(N'2014-11-01T00:19:43.467' AS DateTime), N'/Upload/News/5f5b7080-c70d-437e-9a8c-c59c13c22d71')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (64, 1, 5, 2, N'administrator', CAST(N'2014-11-01T00:20:44.637' AS DateTime), N'/Upload/News/f596f1fa-c9e0-418a-8d23-00c36cf7c0f7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (65, 1, 6, 2, N'administrator', CAST(N'2014-11-01T00:21:33.013' AS DateTime), N'/Upload/News/a71c9f45-7a27-40d6-aef4-97be104771eb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (66, 1, 7, 2, N'administrator', CAST(N'2014-11-01T00:22:19.807' AS DateTime), N'/Upload/News/6284cbd1-8af9-4339-ad09-9753164ec103')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (67, 7, 0, 4, N'administrator', CAST(N'2014-11-02T02:57:43.867' AS DateTime), N'/Upload/Products/ee41b400-b89a-49f0-ad5c-2e615f203743/43.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (68, 7, 0, 4, N'administrator', CAST(N'2014-11-02T02:59:58.277' AS DateTime), N'/Upload/Products/ee41b400-b89a-49f0-ad5c-2e615f203743/34-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (69, 1, 0, 1, N'administrator', CAST(N'2014-11-02T11:12:18.860' AS DateTime), N'/Upload/News/f3dc7a93-e71e-43e9-9713-d7e4da291bb9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (70, 7, 0, 4, N'administrator', CAST(N'2014-11-02T11:19:11.013' AS DateTime), N'/Upload/News/f3dc7a93-e71e-43e9-9713-d7e4da291bb9/15.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (71, 1, 0, 1, N'administrator', CAST(N'2014-11-02T11:19:37.123' AS DateTime), N'/Upload/News/9cb6a937-1e00-47ee-9682-3c999f74c8a2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (72, 1, 8, 2, N'administrator', CAST(N'2014-11-02T11:22:47.377' AS DateTime), N'/Upload/News/7506b4e8-cc2e-405c-aa72-13aea7b84f55')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (73, 1, 9, 2, N'administrator', CAST(N'2014-11-02T14:37:58.357' AS DateTime), N'/Upload/News/c759b9b1-f572-49e7-8ec1-9428a416d8ca')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (74, 7, 0, 4, N'administrator', CAST(N'2014-11-02T14:39:51.303' AS DateTime), N'/Upload/News/c759b9b1-f572-49e7-8ec1-9428a416d8ca/34.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (75, 7, 0, 4, N'administrator', CAST(N'2014-11-02T14:40:26.180' AS DateTime), N'/Upload/News/c759b9b1-f572-49e7-8ec1-9428a416d8ca/31.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (76, 7, 0, 4, N'administrator', CAST(N'2014-11-02T14:40:27.493' AS DateTime), N'/Upload/News/c759b9b1-f572-49e7-8ec1-9428a416d8ca/30.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (77, 7, 0, 4, N'administrator', CAST(N'2014-11-02T14:53:37.237' AS DateTime), N'/Upload/News/c759b9b1-f572-49e7-8ec1-9428a416d8ca/34.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (78, 1, 10, 2, N'administrator', CAST(N'2014-11-02T15:07:52.417' AS DateTime), N'/Upload/News/b0c63f22-612a-4e47-a1e2-e4494244fa53')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (79, 1, 11, 2, N'administrator', CAST(N'2014-11-02T15:40:17.427' AS DateTime), N'/Upload/News/1cba42d4-9153-4735-9346-c5a706d65aa1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (80, 1, 12, 2, N'administrator', CAST(N'2014-11-02T15:40:52.340' AS DateTime), N'/Upload/News/730e95b8-fd84-40d5-ac46-44d7c62becbb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (81, 1, 13, 2, N'administrator', CAST(N'2014-11-02T18:59:33.767' AS DateTime), N'/Upload/News/ab9b6d84-7d01-45c7-ac08-2efdc0e614e9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (82, 1, 14, 2, N'administrator', CAST(N'2014-11-02T19:01:17.650' AS DateTime), N'/Upload/News/04918d3d-e091-45ca-8ce2-5b31c909536f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (83, 1, 15, 2, N'administrator', CAST(N'2014-11-02T19:02:06.833' AS DateTime), N'/Upload/News/a65cbcdf-a906-4ef4-87e5-a48da6f7d8c6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (84, 1, 16, 2, N'administrator', CAST(N'2014-11-02T19:03:17.480' AS DateTime), N'/Upload/News/29e47c42-4b74-49f2-b823-97f431b74e72')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (85, 1, 17, 2, N'administrator', CAST(N'2014-11-02T19:04:05.040' AS DateTime), N'/Upload/News/f039ef4b-44b2-4e63-b0d7-f61b86cf8ff8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (86, 1, 18, 2, N'administrator', CAST(N'2014-11-02T19:06:21.840' AS DateTime), N'/Upload/News/e9d7f1b4-5deb-4550-939a-8cc947cb1b86')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (87, 1, 19, 2, N'administrator', CAST(N'2014-11-02T23:28:29.197' AS DateTime), N'/Upload/News/30f5f0f1-c578-4916-bc31-9047b1834c1c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (88, 1, 20, 2, N'administrator', CAST(N'2014-11-02T23:29:55.857' AS DateTime), N'/Upload/News/e5d74e4c-c4c0-4497-8140-f64d99ecab7d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (89, 1, 21, 2, N'administrator', CAST(N'2014-11-02T23:31:58.040' AS DateTime), N'/Upload/News/d0f2aa07-b009-40bc-a426-2c580234a5e2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (90, 1, 22, 2, N'administrator', CAST(N'2014-11-02T23:47:34.297' AS DateTime), N'/Upload/News/a05a128c-fa15-4784-9f35-12884681b0a9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (91, 1, 23, 2, N'administrator', CAST(N'2014-11-03T20:03:04.907' AS DateTime), N'/Upload/News/8eccb035-c100-43ac-93b8-6ebe9223c5fd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (92, 7, 0, 4, N'administrator', CAST(N'2014-11-04T00:40:51.783' AS DateTime), N'/Upload/News/30f5f0f1-c578-4916-bc31-9047b1834c1c/15.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (93, 1, 0, 1, N'administrator', CAST(N'2014-11-07T02:49:55.853' AS DateTime), N'/Upload/News/d76e7897-5a6a-4287-b4e6-6c1d40d3917f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (94, 1, 24, 2, N'administrator', CAST(N'2014-11-07T02:51:57.637' AS DateTime), N'/Upload/News/29858795-2e98-4519-ae59-0bc70296014b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (95, 1, 25, 2, N'administrator', CAST(N'2014-11-07T14:12:45.430' AS DateTime), N'/Upload/News/5fee834b-2816-4166-a98c-23707ab9125a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (96, 1, 26, 2, N'administrator', CAST(N'2014-11-07T15:58:56.230' AS DateTime), N'/Upload/News/9ea6ae2c-a809-4786-945c-95d933462ec5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (97, 1, 27, 2, N'administrator', CAST(N'2014-11-07T16:37:54.987' AS DateTime), N'/Upload/News/96acd197-552a-4857-8ec1-423efe1ef5ae')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (98, 1, 1, 2, N'administrator', CAST(N'2014-11-14T17:04:45.997' AS DateTime), N'/Upload/News/aed43987-cc31-4c80-b2b6-0d612842acbb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (99, 1, 2, 2, N'administrator', CAST(N'2014-11-14T17:15:32.560' AS DateTime), N'/Upload/News/89a92a32-e93a-4a85-8bb7-9dc0fe1a7d3b')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (100, 1, 0, 1, N'administrator', CAST(N'2014-11-14T17:16:58.193' AS DateTime), N'/Upload/News/39bfed4b-7300-454b-af06-c9b554b22a8c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (101, 1, 3, 2, N'administrator', CAST(N'2014-11-14T17:41:21.577' AS DateTime), N'/Upload/News/feec05c5-6c48-4fa3-a241-30840354fbfe')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (102, 1, 4, 2, N'administrator', CAST(N'2014-11-14T17:44:10.400' AS DateTime), N'/Upload/News/c7232c38-e502-49f3-be2b-1cbf1a0feb49')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (103, 1, 5, 2, N'administrator', CAST(N'2014-11-14T18:19:35.033' AS DateTime), N'/Upload/News/c3b824c7-5945-4cfc-ba0d-4db0ebfe92d1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (104, 1, 6, 2, N'administrator', CAST(N'2014-11-14T18:21:11.053' AS DateTime), N'/Upload/News/4d29c67c-9688-45d0-a89d-d62eda32efe5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (105, 1, 7, 2, N'administrator', CAST(N'2014-11-14T18:32:06.840' AS DateTime), N'/Upload/News/3580aa22-9135-4ab3-bc7f-59c95c29f97a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (106, 1, 0, 1, N'administrator', CAST(N'2014-11-14T18:33:12.487' AS DateTime), N'/Upload/News/dd411373-8a83-4c5d-bc6a-2dc7c1da351b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (107, 1, 8, 2, N'administrator', CAST(N'2014-11-14T19:22:14.850' AS DateTime), N'/Upload/News/a2e18539-5ec1-48aa-a076-73cdc204738c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (108, 1, 9, 2, N'administrator', CAST(N'2014-11-15T09:45:52.613' AS DateTime), N'/Upload/News/63c97184-4190-4c55-83aa-b0fbc7d983c4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (109, 1, 10, 2, N'administrator', CAST(N'2014-11-15T09:47:04.783' AS DateTime), N'/Upload/News/38a3a2a4-9353-425d-b1b9-75e430951528')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (110, 1, 11, 2, N'administrator', CAST(N'2014-11-15T09:48:05.817' AS DateTime), N'/Upload/News/dec5382a-adcf-4083-9e7c-b1421834c694')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (111, 1, 12, 2, N'administrator', CAST(N'2014-11-15T09:48:40.603' AS DateTime), N'/Upload/News/9bf0c43b-3938-4cd3-9887-fcce70d0df04')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (112, 1, 13, 2, N'administrator', CAST(N'2014-11-15T09:49:33.227' AS DateTime), N'/Upload/News/df3e38c8-f96a-4cd9-bfef-825b691c2393')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (113, 1, 14, 2, N'administrator', CAST(N'2014-11-15T12:14:16.800' AS DateTime), N'/Upload/News/f1ca6175-8772-4d31-81b7-cf4c467e004a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (114, 1, 15, 2, N'administrator', CAST(N'2014-11-15T13:47:03.580' AS DateTime), N'/Upload/News/27f95b7b-efb6-41f7-8e49-91dc3ed0d10d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (115, 1, 16, 2, N'administrator', CAST(N'2014-11-15T13:51:04.770' AS DateTime), N'/Upload/News/b4187bcf-512f-4fff-acdf-d9375ca3ff38')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (116, 1, 17, 2, N'administrator', CAST(N'2014-11-15T13:52:28.287' AS DateTime), N'/Upload/News/8445aed1-4646-46fa-8552-0f6993e77c81')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (117, 1, 18, 2, N'administrator', CAST(N'2014-11-15T14:25:03.473' AS DateTime), N'/Upload/News/f6b16c03-2d5e-48b2-849d-8f269acf86d2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (118, 1, 19, 2, N'administrator', CAST(N'2014-11-16T00:16:53.523' AS DateTime), N'/Upload/News/46f4f024-0e30-4d1d-a34f-32958e459472')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (119, 1, 20, 2, N'administrator', CAST(N'2014-11-16T00:21:33.223' AS DateTime), N'/Upload/News/d2a4b0ae-56fb-46ea-ad16-5c427994b12b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (120, 1, 21, 2, N'administrator', CAST(N'2014-11-16T00:23:03.760' AS DateTime), N'/Upload/News/3904e4ed-29d9-4ee2-9d27-e509d3af2783')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (121, 1, 22, 2, N'administrator', CAST(N'2014-11-16T00:35:14.890' AS DateTime), N'/Upload/News/307608ea-c202-4cdc-97df-50a25a97849b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (122, 1, 23, 2, N'administrator', CAST(N'2014-11-16T00:36:37.083' AS DateTime), N'/Upload/News/116d0f25-d322-4340-9858-70c5b08e726c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (123, 1, 24, 2, N'administrator', CAST(N'2014-11-16T00:38:21.360' AS DateTime), N'/Upload/News/e6ccecfe-1490-4b21-8c17-8c4094434942')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (124, 1, 25, 2, N'administrator', CAST(N'2014-11-16T01:06:19.307' AS DateTime), N'/Upload/News/d84f5a9f-3486-495d-ae6b-5acb14178816')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (125, 1, 26, 2, N'administrator', CAST(N'2014-11-16T23:31:29.873' AS DateTime), N'/Upload/News/e970044a-5c7e-4eeb-9294-1546b876c01c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (126, 7, 0, 4, N'administrator', CAST(N'2014-11-27T15:29:48.463' AS DateTime), N'/Upload/SlideShow/55172144-a3ed-4bde-a664-d11d4db093de/1.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (127, 7, 0, 4, N'administrator', CAST(N'2014-11-27T15:29:49.450' AS DateTime), N'/Upload/SlideShow/55172144-a3ed-4bde-a664-d11d4db093de/2.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (128, 7, 0, 4, N'administrator', CAST(N'2014-11-27T15:29:50.147' AS DateTime), N'/Upload/SlideShow/55172144-a3ed-4bde-a664-d11d4db093de/3.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (129, 1, 27, 2, N'administrator', CAST(N'2014-11-27T16:05:19.913' AS DateTime), N'/Upload/News/4efaf020-b5f3-4fd3-802f-a5389db563b6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (130, 1, 28, 2, N'administrator', CAST(N'2014-11-27T16:07:36.290' AS DateTime), N'/Upload/News/189d2eed-e1c3-4949-9cfc-d7b9c72e9475')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (131, 1, 29, 2, N'administrator', CAST(N'2014-11-27T16:11:58.783' AS DateTime), N'/Upload/News/a78430d3-ca76-4bee-aac7-b2bcf266f369')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (132, 1, 30, 2, N'administrator', CAST(N'2014-11-27T16:17:21.757' AS DateTime), N'/Upload/News/8d948445-634e-474b-a111-b863a523e163')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (133, 1, 31, 2, N'administrator', CAST(N'2014-11-27T16:23:02.097' AS DateTime), N'/Upload/News/b31d3a07-1f96-429d-acfd-fea7b64e8566')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (134, 1, 32, 2, N'administrator', CAST(N'2014-11-27T16:24:46.600' AS DateTime), N'/Upload/News/814079a0-e433-448a-8e5c-2bd21344deeb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (135, 1, 33, 2, N'administrator', CAST(N'2014-11-27T16:27:05.117' AS DateTime), N'/Upload/News/94245331-14a6-4dc3-819d-ff36545bdbee')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (136, 1, 0, 1, N'administrator', CAST(N'2014-11-27T16:28:07.627' AS DateTime), N'/Upload/News/e6e9b3a7-9c4b-463d-9ffd-05ea305f91d1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (137, 7, 0, 4, N'administrator', CAST(N'2014-11-27T16:29:48.967' AS DateTime), N'/Upload/News/e6e9b3a7-9c4b-463d-9ffd-05ea305f91d1/astop.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (138, 1, 34, 2, N'administrator', CAST(N'2014-11-27T16:29:59.610' AS DateTime), N'/Upload/News/5b0b8b7e-53a6-4931-b68f-10d47a362056')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (139, 1, 35, 2, N'administrator', CAST(N'2014-11-27T16:42:28.753' AS DateTime), N'/Upload/News/bf352739-b314-4f82-bb96-a2c527131754')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (140, 1, 36, 2, N'administrator', CAST(N'2014-11-27T16:46:11.770' AS DateTime), N'/Upload/News/626e2d7f-1aea-42fc-8a3b-bcd6ff8b1b19')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (141, 1, 37, 2, N'administrator', CAST(N'2014-11-27T16:47:32.120' AS DateTime), N'/Upload/News/29d32bd2-1e7a-4542-84ee-29ad782d04e1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (142, 1, 38, 2, N'administrator', CAST(N'2014-11-27T16:48:50.147' AS DateTime), N'/Upload/News/346b3aad-4cdf-49b2-a5e4-92aa61e4877a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (143, 1, 39, 2, N'administrator', CAST(N'2014-11-27T16:52:01.557' AS DateTime), N'/Upload/News/808994b6-3ebe-4f3d-a7f7-e0dfe7c1d4a4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (144, 1, 40, 2, N'administrator', CAST(N'2014-11-27T17:04:30.680' AS DateTime), N'/Upload/News/92f1577b-ea26-4bfd-ad38-05615c66472e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (145, 1, 41, 2, N'administrator', CAST(N'2014-11-27T17:55:14.167' AS DateTime), N'/Upload/News/405f7666-e839-4c48-a0cb-e1d0dc59d4ed')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (146, 1, 42, 2, N'administrator', CAST(N'2014-11-27T17:57:09.867' AS DateTime), N'/Upload/News/b9a640d0-edc3-460a-aaa4-1ca30ca6ebc6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (147, 1, 43, 2, N'administrator', CAST(N'2014-11-27T17:58:50.273' AS DateTime), N'/Upload/News/fc188b7d-65e3-4b95-81c0-ceb8935c0330')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (148, 7, 0, 4, N'administrator', CAST(N'2014-12-28T22:15:55.120' AS DateTime), N'/Upload/Products/c68fd1c2-2945-4b19-8fca-72ae2401c41e/21.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (149, 7, 0, 4, N'administrator', CAST(N'2014-12-31T13:46:16.047' AS DateTime), N'/Upload/Products/2479eb0a-85c6-4461-8fc6-a7965215ef94/33.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (150, 1, 1, 2, N'administrator', CAST(N'2014-12-31T21:53:30.030' AS DateTime), N'/Upload/News/502c6ebb-8d11-4c80-9cda-69f985f96543')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (151, 1, 2, 2, N'administrator', CAST(N'2014-12-31T21:55:13.213' AS DateTime), N'/Upload/News/c83e66c3-f13b-43c8-8712-4d9fcda3c033')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (152, 1, 3, 2, N'administrator', CAST(N'2015-01-02T09:37:39.887' AS DateTime), N'/Upload/News/7df47024-a9de-4fc9-87cd-9647a731ac5d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (153, 1, 4, 2, N'administrator', CAST(N'2015-01-02T09:47:54.997' AS DateTime), N'/Upload/News/1a613bd1-2319-484e-8fa3-ba1331d87f59')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (154, 1, 5, 2, N'administrator', CAST(N'2015-01-02T11:54:34.107' AS DateTime), N'/Upload/News/bbce55d7-93af-49e2-a4f7-67d3d7f47f6a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (155, 1, 6, 2, N'administrator', CAST(N'2015-01-02T11:57:20.973' AS DateTime), N'/Upload/News/77d53d32-aa98-4f61-b081-f9dd21f43f8e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (156, 1, 7, 2, N'administrator', CAST(N'2015-01-02T11:58:16.903' AS DateTime), N'/Upload/News/0a8100cf-f4cd-41b5-b3b7-e0d13546f008')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (157, 1, 8, 2, N'administrator', CAST(N'2015-01-02T11:58:56.763' AS DateTime), N'/Upload/News/6150daa9-2970-434c-b9ae-ea19a582c508')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (158, 1, 9, 2, N'administrator', CAST(N'2015-01-02T11:59:42.330' AS DateTime), N'/Upload/News/dc1daa13-df6d-4eef-823d-a3735624c2d4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (159, 1, 10, 2, N'administrator', CAST(N'2015-01-02T12:00:16.717' AS DateTime), N'/Upload/News/e0a50383-f63f-4982-97a2-bd434f444d20')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (160, 1, 11, 2, N'administrator', CAST(N'2015-01-02T12:45:49.877' AS DateTime), N'/Upload/News/b23b90a8-f0cc-43e2-a764-2617d0b3e335')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (161, 1, 12, 2, N'administrator', CAST(N'2015-01-02T12:46:38.310' AS DateTime), N'/Upload/News/7cfe0b4a-6936-40de-9dad-07303ed0c616')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (162, 1, 13, 2, N'administrator', CAST(N'2015-01-02T14:13:18.903' AS DateTime), N'/Upload/News/adae69d2-d9ec-47fc-bc31-a914550e9f31')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (163, 1, 0, 1, N'administrator', CAST(N'2015-01-06T16:35:48.893' AS DateTime), N'/Upload/News/7faa7eaa-f8a2-473e-8c6b-79859b66701e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (164, 1, 0, 1, N'administrator', CAST(N'2015-01-06T17:27:13.627' AS DateTime), N'/Upload/News/587d5904-5d09-41ef-9016-99bc42f931d0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (165, 1, 14, 2, N'administrator', CAST(N'2015-01-06T21:32:11.927' AS DateTime), N'/Upload/News/49dd1587-b330-428c-8e6d-1c9f319bd5ed')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (166, 1, 15, 2, N'administrator', CAST(N'2015-01-06T21:32:47.593' AS DateTime), N'/Upload/News/cd746446-e7ac-4144-ab14-96e6766931d2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (167, 7, 0, 4, N'administrator', CAST(N'2015-01-06T22:15:21.967' AS DateTime), N'/Upload/Branches/64df07e0-5c9b-419d-afa0-66b20d02da97/33.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (168, 1, 16, 2, N'administrator', CAST(N'2015-01-06T22:29:05.713' AS DateTime), N'/Upload/News/c2b35f69-938c-44cf-80ea-0006b173fe94')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (169, 1, 17, 2, N'administrator', CAST(N'2015-01-06T22:31:11.037' AS DateTime), N'/Upload/News/a3880c26-7394-4095-9ed4-3a4cb2770dac')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (170, 1, 18, 2, N'administrator', CAST(N'2015-01-06T22:32:55.000' AS DateTime), N'/Upload/News/9bebd8d2-0a6f-4fc8-9d06-f8885da50ce4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (171, 1, 19, 2, N'administrator', CAST(N'2015-01-06T22:33:54.920' AS DateTime), N'/Upload/News/94c94781-08d0-49a3-b5ad-a85df19b6811')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (172, 1, 20, 2, N'administrator', CAST(N'2015-01-06T22:35:24.337' AS DateTime), N'/Upload/News/03b0bcf9-489f-4ace-b79f-7cd1089fa061')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (173, 1, 21, 2, N'administrator', CAST(N'2015-01-06T22:50:14.100' AS DateTime), N'/Upload/News/0b81fe73-8025-4511-b433-c6a7cf845f10')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (174, 1, 0, 1, N'administrator', CAST(N'2015-01-10T17:44:11.927' AS DateTime), N'/Upload/News/c685fc83-75a7-4ed1-9288-8bc0a4849860')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (175, 1, 1, 2, N'administrator', CAST(N'2015-01-12T21:49:39.970' AS DateTime), N'/Upload/News/a67aa22b-0e23-4b32-9f72-08152bf9c031')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (176, 1, 2, 2, N'administrator', CAST(N'2015-01-12T22:02:00.530' AS DateTime), N'/Upload/News/7c993f17-2980-41c1-8892-a655b1c830e9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (177, 1, 3, 2, N'administrator', CAST(N'2015-01-12T22:07:52.177' AS DateTime), N'/Upload/News/3e1efb13-a5ea-4362-945d-b6bf545bc8a0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (178, 1, 4, 2, N'administrator', CAST(N'2015-01-12T22:20:21.133' AS DateTime), N'/Upload/News/3992df7a-1b9b-4557-82b3-a165b04a779d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (179, 1, 5, 2, N'administrator', CAST(N'2015-01-12T22:24:07.637' AS DateTime), N'/Upload/News/bd136d41-3d90-4f36-91e4-6db7f7c6f08d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (180, 1, 6, 2, N'administrator', CAST(N'2015-01-12T22:28:01.557' AS DateTime), N'/Upload/News/d811cc31-b5db-4e09-9963-b1672fc896f8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (181, 1, 7, 2, N'administrator', CAST(N'2015-01-12T22:31:12.037' AS DateTime), N'/Upload/News/c5d50fed-b1c5-4fe8-a918-4412ddc4bba3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (182, 1, 0, 1, N'administrator', CAST(N'2015-01-12T22:37:05.780' AS DateTime), N'/Upload/News/2e23992f-3a56-44f0-b2d5-25b7ce28a913')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (183, 1, 8, 2, N'administrator', CAST(N'2015-01-12T22:44:14.967' AS DateTime), N'/Upload/News/e32af01b-fb40-430c-a33e-42c88715430a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (184, 1, 9, 2, N'administrator', CAST(N'2015-01-12T22:51:39.437' AS DateTime), N'/Upload/News/2c0cd3ff-91b8-4156-b798-ed22e63774fb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (185, 1, 10, 2, N'administrator', CAST(N'2015-01-12T22:56:30.890' AS DateTime), N'/Upload/News/21dba29c-3488-44bb-a0ec-17adb3107900')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (186, 1, 11, 2, N'administrator', CAST(N'2015-01-13T01:02:27.237' AS DateTime), N'/Upload/News/fde27b7b-6ae5-4124-ae42-d7144808b5c1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (187, 1, 12, 2, N'administrator', CAST(N'2015-01-13T01:03:06.423' AS DateTime), N'/Upload/News/ae3b5d13-f6c9-4647-94a1-6c0a415d1273')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (188, 1, 13, 2, N'administrator', CAST(N'2015-01-14T12:42:34.857' AS DateTime), N'/Upload/News/8fbaf8e9-78ad-4c79-9b7c-8ea525f60a6a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (189, 1, 14, 2, N'administrator', CAST(N'2015-01-14T12:56:27.537' AS DateTime), N'/Upload/News/6f760b68-2093-49fa-8411-f16a296fd829')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (190, 1, 15, 2, N'administrator', CAST(N'2015-01-14T13:00:29.427' AS DateTime), N'/Upload/News/8ebe6170-d9d7-4432-b2c8-6fc47b418d7f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (191, 7, 0, 4, N'administrator', CAST(N'2015-01-14T15:23:02.240' AS DateTime), N'/Upload/News/8ebe6170-d9d7-4432-b2c8-6fc47b418d7f/phong-4.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (192, 1, 16, 2, N'administrator', CAST(N'2015-01-18T17:42:37.533' AS DateTime), N'/Upload/News/49b0642a-1e07-4337-ab7f-e6ef1df317b3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (193, 1, 17, 2, N'administrator', CAST(N'2015-01-18T17:46:24.817' AS DateTime), N'/Upload/News/c3a1843c-f8e4-4da9-a50b-723c043d27fb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (194, 1, 18, 2, N'administrator', CAST(N'2015-01-18T17:49:22.060' AS DateTime), N'/Upload/News/4006aad0-8936-4575-9f13-d413a4a2e8eb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (195, 1, 19, 2, N'administrator', CAST(N'2015-01-18T17:51:41.160' AS DateTime), N'/Upload/News/60b7f826-8ee1-4cd6-a698-28bde419e3a5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (196, 1, 20, 2, N'administrator', CAST(N'2015-01-18T17:56:44.207' AS DateTime), N'/Upload/News/168b0593-103a-4da6-9081-45ae02397a62')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (197, 1, 21, 2, N'administrator', CAST(N'2015-01-18T18:02:29.600' AS DateTime), N'/Upload/News/76bad9ec-de62-44ab-86ba-19afeae0aaad')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (198, 1, 22, 2, N'administrator', CAST(N'2015-01-18T22:36:35.707' AS DateTime), N'/Upload/News/78b98cbb-bfbd-44d3-8fd3-e828a7878bed')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (199, 1, 23, 2, N'administrator', CAST(N'2015-01-19T00:32:32.463' AS DateTime), N'/Upload/News/1043eb6d-0d4d-4e2d-b146-1c05160dcfa8')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (200, 1, 24, 2, N'administrator', CAST(N'2015-01-19T00:43:34.023' AS DateTime), N'/Upload/News/81907e8a-5347-42ac-b45e-d7f0d8e5452b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (201, 1, 25, 2, N'administrator', CAST(N'2015-01-19T10:34:04.620' AS DateTime), N'/Upload/News/029aa782-84d2-41bc-a600-8a05e9dd4688')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (202, 1, 26, 2, N'administrator', CAST(N'2015-01-19T10:37:07.553' AS DateTime), N'/Upload/News/692df239-daf7-4850-8b57-b45874f3953b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (203, 1, 27, 2, N'administrator', CAST(N'2015-01-19T10:38:13.850' AS DateTime), N'/Upload/News/c899680b-f098-4d9e-bbb4-8d8fdc9ec29d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (204, 1, 0, 1, N'administrator', CAST(N'2015-01-22T17:51:47.073' AS DateTime), N'/Upload/News/f4f5bd4b-19c2-47d2-8c46-1c6552ac2a46')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (205, 1, 28, 2, N'administrator', CAST(N'2015-01-24T10:30:45.113' AS DateTime), N'/Upload/News/3afce8e9-18f5-4ef1-8292-05eaf04716d8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (206, 1, 29, 2, N'administrator', CAST(N'2015-01-24T10:47:54.490' AS DateTime), N'/Upload/News/cd1cee10-b2e0-4e6e-b6e9-37bbd61a0400')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (207, 1, 30, 2, N'administrator', CAST(N'2015-01-24T11:04:46.977' AS DateTime), N'/Upload/News/3f187c8b-6ff4-4696-871b-74295a6a14c6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (208, 1, 0, 1, N'administrator', CAST(N'2015-01-31T01:04:21.787' AS DateTime), N'/Upload/News/83480882-72d3-4633-9e77-23a8887daa6b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (209, 1, 0, 1, N'administrator', CAST(N'2015-01-31T01:18:46.137' AS DateTime), N'/Upload/News/ae22b84b-df86-4f28-8d42-6f7aa4fba2b3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (210, 1, 32, 2, N'administrator', CAST(N'2015-01-31T01:23:36.947' AS DateTime), N'/Upload/News/7d1cd5b4-bae6-4019-ba16-f7d4d35ec289')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (211, 1, 33, 2, N'administrator', CAST(N'2015-01-31T01:25:18.323' AS DateTime), N'/Upload/News/6985da8e-410e-4c87-8fe8-5b17ebdafe35')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (212, 1, 0, 1, N'administrator', CAST(N'2015-02-03T12:03:49.517' AS DateTime), N'/Upload/News/109b9342-1af5-4a84-a852-a994416c5d96')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (213, 1, 0, 1, N'administrator', CAST(N'2015-02-03T12:04:01.163' AS DateTime), N'/Upload/News/58a3bc0d-ba85-402e-b173-499f831a273c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (214, 1, 0, 1, N'administrator', CAST(N'2015-02-03T12:04:09.363' AS DateTime), N'/Upload/News/49245ea1-8f58-40aa-8d86-6194c887a730')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (215, 1, 0, 1, N'administrator', CAST(N'2015-02-03T12:04:57.543' AS DateTime), N'/Upload/News/a6ed7dcc-cc2a-4a2d-81ca-4c4c6133a796')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (216, 1, 0, 1, N'administrator', CAST(N'2015-02-03T12:05:03.437' AS DateTime), N'/Upload/News/f454841a-ee10-42be-ab84-cdeaaa539d5d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (217, 1, 0, 1, N'administrator', CAST(N'2015-02-03T12:06:13.630' AS DateTime), N'/Upload/News/3de159f9-7a43-4549-a182-a1af52abcec1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (218, 1, 1, 2, N'administrator', CAST(N'2015-03-20T09:31:59.293' AS DateTime), N'/Upload/News/c0885cf2-4106-4474-a24a-5b4738b37f9c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (219, 1, 2, 2, N'administrator', CAST(N'2015-03-20T09:34:10.527' AS DateTime), N'/Upload/News/6796c405-2ea4-42a4-ac43-d9a99b7817ca')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (220, 1, 3, 2, N'administrator', CAST(N'2015-03-20T09:35:34.727' AS DateTime), N'/Upload/News/9920b127-32e0-45c9-824d-619eebc3c609')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (221, 1, 4, 2, N'administrator', CAST(N'2015-03-20T09:41:05.583' AS DateTime), N'/Upload/News/13fdb285-c71d-4951-9cb5-d912b5a818ee')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (222, 1, 5, 2, N'administrator', CAST(N'2015-03-20T09:48:18.717' AS DateTime), N'/Upload/News/d1ca0d95-6453-4088-a986-192c496be016')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (223, 1, 6, 2, N'administrator', CAST(N'2015-03-20T10:16:15.167' AS DateTime), N'/Upload/News/758c781c-669d-4b7f-a35a-9d3be99b853f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (224, 1, 7, 2, N'administrator', CAST(N'2015-03-20T10:19:46.127' AS DateTime), N'/Upload/News/a97f6dc8-382c-48b9-a6d3-edc484e8d115')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (225, 1, 8, 2, N'administrator', CAST(N'2015-03-20T10:20:55.157' AS DateTime), N'/Upload/News/7c5c4a46-d8e1-4247-b677-280a0ddbc851')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (226, 1, 9, 2, N'administrator', CAST(N'2015-03-20T10:21:35.237' AS DateTime), N'/Upload/News/4e18535f-5765-4725-b3eb-61416eb2cea5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (227, 1, 10, 2, N'administrator', CAST(N'2015-03-20T10:22:15.600' AS DateTime), N'/Upload/News/55962c08-d210-41a6-93eb-1a8b8f35117f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (228, 1, 11, 2, N'administrator', CAST(N'2015-03-20T10:23:07.673' AS DateTime), N'/Upload/News/def0c910-a5f6-4ea7-9801-86b687e89877')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (229, 1, 12, 2, N'administrator', CAST(N'2015-03-20T10:26:28.380' AS DateTime), N'/Upload/News/ba399986-8490-4906-8a90-83f506b759c4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (230, 1, 13, 2, N'administrator', CAST(N'2015-03-20T10:27:08.827' AS DateTime), N'/Upload/News/fe8d065f-13b7-4512-af70-6c6ac182eb51')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (231, 1, 14, 2, N'administrator', CAST(N'2015-03-20T10:27:48.287' AS DateTime), N'/Upload/News/f1c01ad4-737c-4da1-88fd-8be88dc5eb39')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (232, 1, 15, 2, N'administrator', CAST(N'2015-03-20T10:28:20.667' AS DateTime), N'/Upload/News/fc594415-dc3a-4c5b-8e0c-b4c3fa15fc5d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (233, 1, 16, 2, N'administrator', CAST(N'2015-03-20T10:28:58.663' AS DateTime), N'/Upload/News/34a42012-9666-4812-a27b-e168545b2553')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (234, 1, 17, 2, N'administrator', CAST(N'2015-03-20T10:32:02.887' AS DateTime), N'/Upload/News/5bf6c03a-7361-477b-8eb9-c7ebc4f0a0db')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (235, 1, 18, 2, N'administrator', CAST(N'2015-03-20T10:36:34.970' AS DateTime), N'/Upload/News/2359f5f7-33f8-4c2c-b794-61abb1d7c59e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (236, 1, 19, 2, N'administrator', CAST(N'2015-03-20T10:40:23.910' AS DateTime), N'/Upload/News/31be16dd-9341-47cf-b2cb-56acb9345e27')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (237, 1, 20, 2, N'administrator', CAST(N'2015-03-20T10:41:50.660' AS DateTime), N'/Upload/News/d7135e89-6948-4d1c-8492-54157f76a826')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (238, 1, 21, 2, N'administrator', CAST(N'2015-03-20T10:43:49.650' AS DateTime), N'/Upload/News/7d479f4a-7d0d-4504-ac87-578574214f11')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (239, 1, 22, 2, N'administrator', CAST(N'2015-03-21T23:18:47.060' AS DateTime), N'/Upload/News/57bc3761-b020-4e83-92b9-f4cc28742f5c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (240, 1, 0, 1, N'administrator', CAST(N'2015-03-24T09:56:07.607' AS DateTime), N'/Upload/News/b6ceca75-7d65-494b-bd3e-c311b25fc7ec')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (241, 1, 0, 1, N'administrator', CAST(N'2015-03-24T09:57:22.947' AS DateTime), N'/Upload/News/82889ae5-6c71-49ae-bb73-85db219104da')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (242, 1, 23, 2, N'administrator', CAST(N'2015-03-24T10:09:36.797' AS DateTime), N'/Upload/News/9bb9f60a-fae0-449f-9381-75f9cd9c25ad')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (243, 1, 24, 2, N'administrator', CAST(N'2015-03-24T10:12:07.457' AS DateTime), N'/Upload/News/0da2d5da-1793-4ef7-bb73-0e7e3dcb43e8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (244, 1, 25, 2, N'administrator', CAST(N'2015-03-24T10:13:21.097' AS DateTime), N'/Upload/News/77e96e5e-3ce3-4b81-9c47-c1b53dda2661')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (245, 7, 0, 4, N'administrator', CAST(N'2015-03-24T10:26:05.903' AS DateTime), N'/Upload/News/d1ca0d95-6453-4088-a986-192c496be016/28.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (246, 7, 0, 4, N'administrator', CAST(N'2015-03-24T10:28:11.247' AS DateTime), N'/Upload/News/13fdb285-c71d-4951-9cb5-d912b5a818ee/27.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (247, 1, 26, 2, N'administrator', CAST(N'2015-03-24T10:29:03.347' AS DateTime), N'/Upload/News/a814fcf3-d8bb-46af-875b-6e79bc03d126')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (248, 7, 0, 4, N'administrator', CAST(N'2015-03-24T10:31:37.990' AS DateTime), N'/Upload/News/d1ca0d95-6453-4088-a986-192c496be016/chinhanh2.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (249, 1, 27, 2, N'administrator', CAST(N'2015-03-24T10:37:49.613' AS DateTime), N'/Upload/News/69681d3f-3a03-49e1-84aa-08965b9c44e5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (250, 7, 0, 4, N'administrator', CAST(N'2015-03-24T10:41:10.330' AS DateTime), N'/Upload/News/9920b127-32e0-45c9-824d-619eebc3c609/27.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (251, 1, 0, 1, N'administrator', CAST(N'2015-03-24T10:47:30.827' AS DateTime), N'/Upload/News/d23e91cb-a795-4666-b8c8-b8112db3d8fd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (252, 1, 28, 2, N'administrator', CAST(N'2015-03-24T10:51:26.007' AS DateTime), N'/Upload/News/e3a363bf-39da-4aa6-9259-f903fda458b1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (253, 1, 0, 1, N'administrator', CAST(N'2015-03-24T10:54:53.743' AS DateTime), N'/Upload/News/ad977c57-b1f9-4258-a8f5-bd42822400de')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (254, 1, 29, 2, N'administrator', CAST(N'2015-03-24T11:00:09.757' AS DateTime), N'/Upload/News/13fdb285-c71d-4951-9cb5-d912b5a818ee')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (255, 1, 30, 2, N'administrator', CAST(N'2015-03-24T11:03:55.387' AS DateTime), N'/Upload/News/d1ca0d95-6453-4088-a986-192c496be016')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (256, 1, 31, 2, N'administrator', CAST(N'2015-03-24T11:05:44.003' AS DateTime), N'/Upload/News/a814fcf3-d8bb-46af-875b-6e79bc03d126')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (257, 1, 0, 1, N'administrator', CAST(N'2015-03-24T11:07:21.747' AS DateTime), N'/Upload/News/77aba758-7efc-4324-8072-98cb54eb7b21')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (258, 1, 32, 2, N'administrator', CAST(N'2015-03-24T11:08:31.473' AS DateTime), N'/Upload/News/758c781c-669d-4b7f-a35a-9d3be99b853f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (259, 1, 33, 2, N'administrator', CAST(N'2015-03-24T11:10:25.870' AS DateTime), N'/Upload/News/a97f6dc8-382c-48b9-a6d3-edc484e8d115')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (260, 1, 34, 2, N'administrator', CAST(N'2015-03-24T11:11:09.043' AS DateTime), N'/Upload/News/7c5c4a46-d8e1-4247-b677-280a0ddbc851')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (261, 1, 35, 2, N'administrator', CAST(N'2015-03-24T11:11:46.240' AS DateTime), N'/Upload/News/4e18535f-5765-4725-b3eb-61416eb2cea5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (262, 1, 36, 2, N'administrator', CAST(N'2015-03-24T11:12:33.887' AS DateTime), N'/Upload/News/55962c08-d210-41a6-93eb-1a8b8f35117f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (263, 1, 37, 2, N'administrator', CAST(N'2015-03-24T11:13:31.733' AS DateTime), N'/Upload/News/def0c910-a5f6-4ea7-9801-86b687e89877')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (264, 1, 38, 2, N'administrator', CAST(N'2015-03-24T11:14:07.387' AS DateTime), N'/Upload/News/ba399986-8490-4906-8a90-83f506b759c4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (265, 1, 39, 2, N'administrator', CAST(N'2015-03-24T11:14:43.067' AS DateTime), N'/Upload/News/fe8d065f-13b7-4512-af70-6c6ac182eb51')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (266, 1, 40, 2, N'administrator', CAST(N'2015-03-24T11:15:18.117' AS DateTime), N'/Upload/News/f1c01ad4-737c-4da1-88fd-8be88dc5eb39')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (267, 1, 41, 2, N'administrator', CAST(N'2015-03-24T11:15:51.067' AS DateTime), N'/Upload/News/fc594415-dc3a-4c5b-8e0c-b4c3fa15fc5d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (268, 1, 42, 2, N'administrator', CAST(N'2015-03-24T11:16:33.423' AS DateTime), N'/Upload/News/34a42012-9666-4812-a27b-e168545b2553')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (269, 1, 43, 2, N'administrator', CAST(N'2015-03-24T11:17:50.653' AS DateTime), N'/Upload/News/5bf6c03a-7361-477b-8eb9-c7ebc4f0a0db')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (270, 1, 44, 2, N'administrator', CAST(N'2015-03-24T11:25:51.463' AS DateTime), N'/Upload/News/2359f5f7-33f8-4c2c-b794-61abb1d7c59e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (271, 1, 0, 1, N'administrator', CAST(N'2015-03-24T11:26:23.620' AS DateTime), N'/Upload/News/26bd43c0-f756-4a5c-bef6-5eab2fb635f0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (272, 1, 0, 1, N'administrator', CAST(N'2015-03-24T11:26:32.843' AS DateTime), N'/Upload/News/8cde0e05-0fe7-47fa-9c34-95141ac24e48')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (273, 1, 0, 1, N'administrator', CAST(N'2015-03-24T11:26:45.713' AS DateTime), N'/Upload/News/57bc3761-b020-4e83-92b9-f4cc28742f5c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (274, 1, 0, 1, N'administrator', CAST(N'2015-03-24T11:26:57.310' AS DateTime), N'/Upload/News/2359f5f7-33f8-4c2c-b794-61abb1d7c59e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (275, 1, 0, 1, N'administrator', CAST(N'2015-03-24T11:27:03.483' AS DateTime), N'/Upload/News/f0a4b1d9-2fd1-4ec8-86c6-92a27eacc83c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (276, 1, 0, 1, N'administrator', CAST(N'2015-03-24T11:27:21.730' AS DateTime), N'/Upload/News/136291c9-4275-49f9-98ec-697ab574825c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (277, 1, 45, 2, N'administrator', CAST(N'2015-03-24T11:27:47.823' AS DateTime), N'/Upload/News/8af7079b-5dbd-4c43-9405-74689e0f2d4e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (278, 1, 46, 2, N'administrator', CAST(N'2015-03-24T11:29:50.290' AS DateTime), N'/Upload/News/8af7079b-5dbd-4c43-9405-74689e0f2d4e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (279, 7, 0, 4, N'administrator', CAST(N'2015-03-24T12:13:14.470' AS DateTime), N'/Upload/News/0da2d5da-1793-4ef7-bb73-0e7e3dcb43e8/26.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (280, 1, 47, 2, N'administrator', CAST(N'2015-03-25T09:12:09.210' AS DateTime), N'/Upload/News/4261664f-d686-43b7-9d90-f413d4c93b0e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (281, 1, 48, 2, N'administrator', CAST(N'2015-03-25T09:13:53.540' AS DateTime), N'/Upload/News/7d4c5d0c-0a6a-4249-9c97-a1684d93cd66')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (282, 1, 49, 2, N'administrator', CAST(N'2015-03-25T09:37:51.407' AS DateTime), N'/Upload/News/882f9689-6003-4f37-91c7-333035d5b04d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (283, 1, 50, 2, N'administrator', CAST(N'2015-03-25T13:02:24.247' AS DateTime), N'/Upload/News/a21af9e2-c3d6-4bb4-8f3b-e8c155a377c2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (284, 1, 51, 2, N'teouit', CAST(N'2015-04-06T13:35:33.723' AS DateTime), N'/Upload/News/1b8cdd09-4633-4fa3-bfbc-374a8165ccc0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (285, 1, 1, 2, N'administrator', CAST(N'2015-04-09T11:00:38.597' AS DateTime), N'/Upload/News/6014739c-0b3a-4279-bccf-fef9db3cda9d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (286, 1, 2, 2, N'administrator', CAST(N'2015-04-09T11:07:17.017' AS DateTime), N'/Upload/News/01431c4b-8337-4742-b9de-bf0cf604b989')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (287, 1, 3, 2, N'administrator', CAST(N'2015-04-09T11:11:52.447' AS DateTime), N'/Upload/News/894a6eb1-b586-4859-a6bb-0955dbd1177c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (288, 1, 4, 2, N'administrator', CAST(N'2015-04-09T11:13:58.853' AS DateTime), N'/Upload/News/e51f3ca0-9131-4651-87bf-3fbcca8d5089')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (289, 1, 5, 2, N'administrator', CAST(N'2015-04-09T11:18:29.177' AS DateTime), N'/Upload/News/f4ae62f2-1510-4b06-b7a8-b3f1290d372e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (290, 1, 6, 2, N'administrator', CAST(N'2015-04-09T21:43:20.283' AS DateTime), N'/Upload/News/d260ae82-88c2-4f4c-b6d8-2d562fb25091')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (291, 7, 0, 4, N'administrator', CAST(N'2015-04-10T11:11:03.887' AS DateTime), N'/Upload/Partners/0f12b78f-c729-4748-8156-a3d63cacde94/pierer  cardin .gif')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (292, 7, 0, 4, N'administrator', CAST(N'2015-04-10T11:11:06.920' AS DateTime), N'/Upload/Partners/0f12b78f-c729-4748-8156-a3d63cacde94/pierer  cardin _black.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (293, 1, 1, 2, N'administrator', CAST(N'2015-08-25T16:14:13.267' AS DateTime), N'/Upload/News/97c36e7a-49ff-49f8-8d5a-5628b75756a6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (294, 1, 2, 2, N'administrator', CAST(N'2015-08-25T16:17:48.780' AS DateTime), N'/Upload/News/043e2d19-b11e-4a2f-b273-d2c34af1b35a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (295, 1, 3, 2, N'administrator', CAST(N'2015-08-25T16:19:58.697' AS DateTime), N'/Upload/News/c4d719ed-4195-4a1b-a37d-2901ca42820e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (296, 1, 4, 2, N'administrator', CAST(N'2015-08-25T16:20:56.813' AS DateTime), N'/Upload/News/9c5149ba-f049-4268-8eaa-c31f40db2756')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (297, 1, 5, 2, N'administrator', CAST(N'2015-08-25T16:22:15.180' AS DateTime), N'/Upload/News/4653993a-3859-4b89-a916-d3716c362310')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (298, 1, 6, 2, N'administrator', CAST(N'2015-08-25T16:31:15.397' AS DateTime), N'/Upload/News/c1a79a31-98ba-4283-883f-b91af35c85b6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (299, 1, 7, 2, N'administrator', CAST(N'2015-08-25T16:32:51.617' AS DateTime), N'/Upload/News/71734607-271a-4565-96da-c110a9ed4b18')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (300, 1, 8, 2, N'administrator', CAST(N'2015-08-25T16:36:35.350' AS DateTime), N'/Upload/News/e6c145f0-fb2a-4e85-8787-613294f63771')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (301, 1, 9, 2, N'administrator', CAST(N'2015-08-25T16:37:23.767' AS DateTime), N'/Upload/News/485be13d-f2e6-40a2-885c-3656083feb71')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (302, 1, 10, 2, N'administrator', CAST(N'2015-08-25T16:38:26.983' AS DateTime), N'/Upload/News/cde9d900-fb04-4eaa-802f-6b5e99d6644f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (303, 1, 11, 2, N'administrator', CAST(N'2015-08-25T16:41:54.147' AS DateTime), N'/Upload/News/4b7f2262-df46-4177-8849-1f1144b33724')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (304, 1, 12, 2, N'administrator', CAST(N'2015-08-26T23:17:01.717' AS DateTime), N'/Upload/News/05150700-2297-4984-b040-5040bc08bf9d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (305, 1, 13, 2, N'administrator', CAST(N'2015-08-26T23:17:55.523' AS DateTime), N'/Upload/News/41c111f6-5a7c-46d0-ab48-845ac1d3bb9e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (306, 1, 14, 2, N'administrator', CAST(N'2015-08-26T23:18:27.333' AS DateTime), N'/Upload/News/f926d3f5-a078-4270-aa34-344a368b1d2d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (307, 1, 15, 2, N'administrator', CAST(N'2015-08-26T23:18:59.140' AS DateTime), N'/Upload/News/a6996485-783e-408d-a56b-b15b5e9f14cf')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (308, 1, 0, 1, N'administrator', CAST(N'2015-08-26T23:20:38.007' AS DateTime), N'/Upload/News/9066988c-3b2d-48e1-9571-8194ef0c33a3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (309, 1, 16, 2, N'administrator', CAST(N'2015-08-26T23:20:44.080' AS DateTime), N'/Upload/News/99838f59-3f65-4365-884b-1ca412526e7d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (310, 1, 17, 2, N'administrator', CAST(N'2015-08-27T00:32:51.443' AS DateTime), N'/Upload/News/33ce48f9-4ebc-423f-97b0-63f9e289a0a8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (311, 1, 1, 2, N'administrator', CAST(N'2015-08-27T16:07:12.583' AS DateTime), N'/Upload/News/b2544b41-3548-4472-a146-9172992295ce')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (312, 1, 2, 2, N'administrator', CAST(N'2015-08-27T16:17:23.837' AS DateTime), N'/Upload/News/23033f60-0574-49e5-8380-b101a864d043')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (313, 1, 3, 2, N'administrator', CAST(N'2015-08-28T14:36:42.313' AS DateTime), N'/Upload/News/f4ca31ae-bafe-4364-8f61-cb623430bf5e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (314, 1, 4, 2, N'administrator', CAST(N'2015-08-28T16:41:13.290' AS DateTime), N'/Upload/News/6c1eea35-62d2-4fae-a808-ac4b471d4508')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (315, 1, 5, 2, N'administrator', CAST(N'2015-08-28T17:40:30.977' AS DateTime), N'/Upload/News/49ca460c-38d8-4a84-a59b-82a68e9bcae5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (316, 1, 6, 2, N'administrator', CAST(N'2015-08-31T07:42:19.333' AS DateTime), N'/Upload/News/2ec7e1b6-15e2-4745-9af3-09965aa13a2a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (317, 1, 0, 1, N'administrator', CAST(N'2015-08-31T07:42:53.223' AS DateTime), N'/Upload/News/09979951-0db9-4195-955d-1cca3632a1f2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (318, 1, 7, 2, N'administrator', CAST(N'2015-08-31T07:43:48.640' AS DateTime), N'/Upload/News/29fdc162-9ff0-47c7-9138-57a147a30fb7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (319, 1, 1, 2, N'administrator', CAST(N'2015-09-11T09:45:36.780' AS DateTime), N'/Upload/News/b8892f77-1065-4437-86cc-b1908742782a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (320, 1, 2, 2, N'administrator', CAST(N'2015-09-11T09:49:01.717' AS DateTime), N'/Upload/News/559c3807-64c6-4fa7-82fa-d2358b16aaba')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (321, 1, 3, 2, N'administrator', CAST(N'2015-09-11T09:49:48.000' AS DateTime), N'/Upload/News/631bf698-4318-4d22-8d15-0105d2e3a426')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (322, 1, 4, 2, N'administrator', CAST(N'2015-09-11T09:58:07.670' AS DateTime), N'/Upload/News/594ec032-116f-4d3a-b397-32c6bc254356')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (323, 1, 5, 2, N'administrator', CAST(N'2015-09-11T09:59:31.643' AS DateTime), N'/Upload/News/04c95313-677a-4534-bbc0-0b89a95c3784')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (324, 1, 6, 2, N'administrator', CAST(N'2015-09-11T10:04:18.733' AS DateTime), N'/Upload/News/25d3a5ba-0ff3-45af-8f94-c0680ca442c3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (325, 1, 7, 2, N'administrator', CAST(N'2015-09-11T10:04:52.870' AS DateTime), N'/Upload/News/f2356821-ada7-43bf-82a7-e25237d38c30')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (326, 1, 8, 2, N'administrator', CAST(N'2015-09-11T10:05:37.347' AS DateTime), N'/Upload/News/0b6ab335-ae44-4620-ad52-aa14c4a093af')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (327, 1, 0, 1, N'administrator', CAST(N'2015-09-11T10:10:02.420' AS DateTime), N'/Upload/News/9bb4e0f8-bddb-42f2-9639-feeb45d6977c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (328, 1, 9, 2, N'administrator', CAST(N'2015-09-11T10:18:34.230' AS DateTime), N'/Upload/News/30028c7f-3d4c-484d-876b-a9922e460eb5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (329, 1, 10, 2, N'administrator', CAST(N'2015-09-11T10:19:17.877' AS DateTime), N'/Upload/News/d4c6a096-aa48-4526-9a9a-442d86966b7e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (330, 1, 11, 2, N'administrator', CAST(N'2015-09-11T10:21:39.533' AS DateTime), N'/Upload/News/b0f530ab-11c6-472c-8382-1abb48cd88ce')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (331, 1, 12, 2, N'administrator', CAST(N'2015-09-11T10:25:31.747' AS DateTime), N'/Upload/News/f8d3ec05-0c80-4e0b-b62b-c73c8bda495b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (332, 1, 13, 2, N'administrator', CAST(N'2015-09-11T10:26:36.370' AS DateTime), N'/Upload/News/ed390a94-8256-4a92-becf-61b02f18e656')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (333, 1, 14, 2, N'administrator', CAST(N'2015-09-11T10:27:22.053' AS DateTime), N'/Upload/News/759b40f6-18e4-440d-bb6a-e0532b04cf7b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (334, 1, 15, 2, N'administrator', CAST(N'2015-09-11T10:27:52.250' AS DateTime), N'/Upload/News/b76ecd2e-5534-4afb-8411-657ba380f39f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (335, 7, 0, 4, N'administrator', CAST(N'2015-09-11T10:52:53.873' AS DateTime), N'/Upload/SlideShow/335fd0ef-630d-4096-978f-ca78c2bbe332/slide-1.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (336, 1, 16, 2, N'administrator', CAST(N'2015-09-11T17:02:30.957' AS DateTime), N'/Upload/News/a03a59b0-ef8e-4d07-99fc-f8ce7259456d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (337, 1, 17, 2, N'administrator', CAST(N'2015-09-11T23:56:51.963' AS DateTime), N'/Upload/News/88ba8ae8-ff20-4018-9889-a97e4c22846b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (338, 7, 0, 4, N'administrator', CAST(N'2015-09-13T13:48:15.107' AS DateTime), N'/Upload/News/30028c7f-3d4c-484d-876b-a9922e460eb5/card.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (339, 7, 0, 4, N'administrator', CAST(N'2015-09-13T13:48:17.623' AS DateTime), N'/Upload/News/30028c7f-3d4c-484d-876b-a9922e460eb5/hinhanh-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (340, 7, 0, 4, N'administrator', CAST(N'2015-09-13T13:48:18.510' AS DateTime), N'/Upload/News/30028c7f-3d4c-484d-876b-a9922e460eb5/hinhanh-2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (341, 7, 0, 4, N'administrator', CAST(N'2015-09-13T13:57:29.127' AS DateTime), N'/Upload/News/30028c7f-3d4c-484d-876b-a9922e460eb5/beautiful-landscape-waikawa-bay-sunrise-times-south-island-new-zealand-rock-sea-time-yellow-eyed-penguin-30408970.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (342, 7, 0, 4, N'administrator', CAST(N'2015-09-13T13:57:30.503' AS DateTime), N'/Upload/News/30028c7f-3d4c-484d-876b-a9922e460eb5/1.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (343, 7, 0, 4, N'administrator', CAST(N'2015-09-13T13:57:56.757' AS DateTime), N'/Upload/News/30028c7f-3d4c-484d-876b-a9922e460eb5/beautiful-landscape-waikawa-bay-sunrise-times-south-island-new-zealand-rock-sea-time-yellow-eyed-penguin-30408970.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (344, 1, 18, 2, N'administrator', CAST(N'2015-09-13T14:02:12.093' AS DateTime), N'/Upload/News/01d46fec-bf31-4f7b-af49-2ce0773b1607')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (345, 1, 19, 2, N'administrator', CAST(N'2015-09-13T14:04:38.463' AS DateTime), N'/Upload/News/7a0fd390-d2a9-4e9b-a129-59ef2ecee487')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (346, 7, 0, 4, N'administrator', CAST(N'2015-09-14T10:11:31.657' AS DateTime), N'/Upload/Products/722e6fa4-4bb1-4822-b3c3-44e4fe7c7979/foody-aroi-dessert-cafe-116870-635418114966586962.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (347, 7, 0, 4, N'administrator', CAST(N'2015-09-14T10:13:24.177' AS DateTime), N'/Upload/Products/722e6fa4-4bb1-4822-b3c3-44e4fe7c7979/diem-mat-nhung-sat-thu-cua-nghe-nuong-banh.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (348, 7, 0, 4, N'administrator', CAST(N'2015-09-14T10:15:11.203' AS DateTime), N'/Upload/Products/f69cfaa6-5d0b-4988-b1eb-8fcad4b6a585/box-4.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (349, 7, 0, 4, N'administrator', CAST(N'2015-09-14T10:15:35.650' AS DateTime), N'/Upload/Products/0ee91023-308b-4099-9e13-27ebe9e4dbd8/box-3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (350, 7, 0, 4, N'administrator', CAST(N'2015-09-14T10:15:53.923' AS DateTime), N'/Upload/Products/1b55e7e3-86dc-4e03-90b5-61f55b5269d1/box-2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (351, 7, 0, 4, N'administrator', CAST(N'2015-09-14T10:16:09.697' AS DateTime), N'/Upload/Products/6c21d841-77e1-4f61-b6f5-005c9c5852cc/box-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (352, 1, 20, 2, N'administrator', CAST(N'2015-09-14T11:13:53.630' AS DateTime), N'/Upload/News/01514a4e-cfbd-4a82-9efa-245c7a5e3686')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (353, 1, 0, 1, N'administrator', CAST(N'2015-09-14T16:47:44.937' AS DateTime), N'/Upload/News/aef3e6de-d33f-4291-827d-3119a810244b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (354, 1, 21, 2, N'administrator', CAST(N'2015-09-14T21:40:17.883' AS DateTime), N'/Upload/News/023b1156-3d9c-4cd7-9bb0-2ab45c24e948')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (355, 1, 22, 2, N'administrator', CAST(N'2015-09-14T21:51:10.060' AS DateTime), N'/Upload/News/7c281d0a-957d-4c75-95eb-86aebce991a7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (356, 1, 23, 2, N'administrator', CAST(N'2015-09-16T03:57:39.450' AS DateTime), N'/Upload/News/b8892f77-1065-4437-86cc-b1908742782a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (357, 1, 24, 2, N'administrator', CAST(N'2015-09-16T03:59:12.217' AS DateTime), N'/Upload/News/559c3807-64c6-4fa7-82fa-d2358b16aaba')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (358, 1, 25, 2, N'administrator', CAST(N'2015-09-16T03:59:57.617' AS DateTime), N'/Upload/News/631bf698-4318-4d22-8d15-0105d2e3a426')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (359, 1, 26, 2, N'administrator', CAST(N'2015-09-16T04:01:00.557' AS DateTime), N'/Upload/News/594ec032-116f-4d3a-b397-32c6bc254356')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (360, 1, 27, 2, N'administrator', CAST(N'2015-09-16T04:01:54.153' AS DateTime), N'/Upload/News/04c95313-677a-4534-bbc0-0b89a95c3784')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (361, 1, 28, 2, N'administrator', CAST(N'2015-09-16T04:02:55.530' AS DateTime), N'/Upload/News/b0f530ab-11c6-472c-8382-1abb48cd88ce')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (362, 1, 29, 2, N'administrator', CAST(N'2015-09-16T04:03:40.080' AS DateTime), N'/Upload/News/f8d3ec05-0c80-4e0b-b62b-c73c8bda495b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (363, 1, 30, 2, N'administrator', CAST(N'2015-09-16T04:04:14.563' AS DateTime), N'/Upload/News/a03a59b0-ef8e-4d07-99fc-f8ce7259456d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (364, 1, 31, 2, N'administrator', CAST(N'2015-09-16T04:05:13.470' AS DateTime), N'/Upload/News/88ba8ae8-ff20-4018-9889-a97e4c22846b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (365, 1, 32, 2, N'administrator', CAST(N'2015-09-16T04:07:28.630' AS DateTime), N'/Upload/News/023b1156-3d9c-4cd7-9bb0-2ab45c24e948')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (366, 1, 33, 2, N'administrator', CAST(N'2015-09-16T04:09:04.853' AS DateTime), N'/Upload/News/7c281d0a-957d-4c75-95eb-86aebce991a7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (367, 1, 34, 2, N'administrator', CAST(N'2015-09-16T04:10:29.620' AS DateTime), N'/Upload/News/ed390a94-8256-4a92-becf-61b02f18e656')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (368, 1, 35, 2, N'administrator', CAST(N'2015-09-16T04:11:07.437' AS DateTime), N'/Upload/News/759b40f6-18e4-440d-bb6a-e0532b04cf7b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (369, 1, 36, 2, N'administrator', CAST(N'2015-09-16T04:11:48.247' AS DateTime), N'/Upload/News/b76ecd2e-5534-4afb-8411-657ba380f39f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (370, 1, 37, 2, N'administrator', CAST(N'2015-09-16T04:17:52.233' AS DateTime), N'/Upload/News/30028c7f-3d4c-484d-876b-a9922e460eb5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (371, 1, 38, 2, N'administrator', CAST(N'2015-09-16T04:19:12.067' AS DateTime), N'/Upload/News/01d46fec-bf31-4f7b-af49-2ce0773b1607')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (372, 1, 39, 2, N'administrator', CAST(N'2015-09-16T04:20:00.327' AS DateTime), N'/Upload/News/7a0fd390-d2a9-4e9b-a129-59ef2ecee487')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (373, 1, 1, 2, N'administrator', CAST(N'2015-09-29T07:21:29.680' AS DateTime), N'/Upload/News/40327166-015c-49e7-9ddf-5d8bfedc1af2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (374, 1, 2, 2, N'administrator', CAST(N'2015-09-29T07:22:09.050' AS DateTime), N'/Upload/News/c7db7d0e-08b6-419a-bfb7-82d0fcf8299f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (375, 1, 3, 2, N'administrator', CAST(N'2015-09-29T07:22:57.360' AS DateTime), N'/Upload/News/157e1842-2db3-437c-8748-a2cf552e26c1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (376, 1, 4, 2, N'administrator', CAST(N'2015-09-29T07:24:38.927' AS DateTime), N'/Upload/News/d81e0aa7-6c37-4ea3-a3b4-f47aac45fb99')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (377, 1, 5, 2, N'administrator', CAST(N'2015-09-29T07:25:03.510' AS DateTime), N'/Upload/News/8d247621-e792-4cca-be1a-5dc100bda72a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (378, 1, 6, 2, N'administrator', CAST(N'2015-09-29T07:25:33.813' AS DateTime), N'/Upload/News/786eba59-5d6b-412c-81d0-c5656b0a1aee')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (379, 1, 7, 2, N'administrator', CAST(N'2015-09-29T08:53:23.760' AS DateTime), N'/Upload/News/70ab47cf-09f1-421c-853d-58d5d65e6bc8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (380, 1, 8, 2, N'administrator', CAST(N'2015-09-29T08:59:22.160' AS DateTime), N'/Upload/News/8ac37372-3d54-4bf7-b426-83f7f409217b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (381, 1, 9, 2, N'administrator', CAST(N'2015-09-29T09:00:36.297' AS DateTime), N'/Upload/News/003b2e9d-2001-480e-b773-4a70560fd604')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (382, 1, 10, 2, N'administrator', CAST(N'2015-09-29T09:02:35.357' AS DateTime), N'/Upload/News/f0683fa4-6e90-4ed2-9bd1-620475c5e597')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (383, 1, 0, 1, N'administrator', CAST(N'2015-09-29T09:03:18.223' AS DateTime), N'/Upload/News/9f3d2270-79d2-4a19-b5b1-107ada1c1627')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (384, 1, 11, 2, N'administrator', CAST(N'2015-09-29T09:06:47.437' AS DateTime), N'/Upload/News/34860994-ddc5-445d-a48a-d1330f387ad9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (385, 1, 12, 2, N'administrator', CAST(N'2015-09-29T09:08:35.017' AS DateTime), N'/Upload/News/0530d333-a0b7-4fd2-91b4-97f8f20d052f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (386, 1, 13, 2, N'administrator', CAST(N'2015-09-29T09:10:07.560' AS DateTime), N'/Upload/News/cbefcaf8-ec42-4699-838b-f4edfd41b2e6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (387, 1, 14, 2, N'administrator', CAST(N'2015-09-29T09:12:54.677' AS DateTime), N'/Upload/News/d4e3e2ca-beae-4dfc-a27e-d50eeafc9bf8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (388, 1, 15, 2, N'administrator', CAST(N'2015-09-29T11:19:15.517' AS DateTime), N'/Upload/News/f7598c31-895b-458c-8e51-a62aa0942e48')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (389, 1, 16, 2, N'administrator', CAST(N'2015-09-29T11:21:39.603' AS DateTime), N'/Upload/News/749534ad-b708-4747-8539-41cd65298476')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (390, 1, 17, 2, N'administrator', CAST(N'2015-09-29T11:23:35.900' AS DateTime), N'/Upload/News/1013b7dc-6a30-4b89-8cfd-ad291df10bb6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (391, 1, 18, 2, N'administrator', CAST(N'2015-09-29T11:24:36.263' AS DateTime), N'/Upload/News/48899d8c-7424-4fc4-ad59-c28655334586')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (392, 1, 19, 2, N'administrator', CAST(N'2015-09-29T11:28:50.793' AS DateTime), N'/Upload/News/fe77ff20-e102-451b-9325-347157411e72')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (393, 1, 20, 2, N'administrator', CAST(N'2015-09-29T11:29:53.067' AS DateTime), N'/Upload/News/f5fa9e09-26bb-4724-b076-9fd543b88a9c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (394, 1, 21, 2, N'administrator', CAST(N'2015-09-29T16:18:56.837' AS DateTime), N'/Upload/News/6aa6649f-ba4c-4e1e-91d9-066aa5f8938f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (395, 1, 0, 1, N'administrator', CAST(N'2015-10-01T09:33:32.793' AS DateTime), N'/Upload/News/0530d333-a0b7-4fd2-91b4-97f8f20d052f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (396, 1, 22, 2, N'administrator', CAST(N'2015-10-01T09:33:59.093' AS DateTime), N'/Upload/News/34860994-ddc5-445d-a48a-d1330f387ad9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (397, 1, 23, 2, N'administrator', CAST(N'2015-10-01T09:34:55.977' AS DateTime), N'/Upload/News/0530d333-a0b7-4fd2-91b4-97f8f20d052f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (398, 1, 24, 2, N'administrator', CAST(N'2015-10-01T09:35:40.710' AS DateTime), N'/Upload/News/cbefcaf8-ec42-4699-838b-f4edfd41b2e6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (399, 1, 25, 2, N'administrator', CAST(N'2015-10-01T09:36:45.347' AS DateTime), N'/Upload/News/d4e3e2ca-beae-4dfc-a27e-d50eeafc9bf8')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (400, 1, 26, 2, N'administrator', CAST(N'2015-10-01T09:48:41.200' AS DateTime), N'/Upload/News/6aa6649f-ba4c-4e1e-91d9-066aa5f8938f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (401, 1, 27, 2, N'administrator', CAST(N'2015-10-01T09:52:04.853' AS DateTime), N'/Upload/News/40327166-015c-49e7-9ddf-5d8bfedc1af2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (402, 1, 28, 2, N'administrator', CAST(N'2015-10-01T09:54:36.483' AS DateTime), N'/Upload/News/c7db7d0e-08b6-419a-bfb7-82d0fcf8299f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (403, 1, 29, 2, N'administrator', CAST(N'2015-10-01T09:56:58.047' AS DateTime), N'/Upload/News/157e1842-2db3-437c-8748-a2cf552e26c1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (404, 1, 30, 2, N'administrator', CAST(N'2015-10-01T10:03:23.253' AS DateTime), N'/Upload/News/d81e0aa7-6c37-4ea3-a3b4-f47aac45fb99')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (405, 1, 32, 2, N'administrator', CAST(N'2015-10-01T10:04:03.337' AS DateTime), N'/Upload/News/8d247621-e792-4cca-be1a-5dc100bda72a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (406, 1, 33, 2, N'administrator', CAST(N'2015-10-01T10:05:12.100' AS DateTime), N'/Upload/News/786eba59-5d6b-412c-81d0-c5656b0a1aee')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (407, 1, 34, 2, N'administrator', CAST(N'2015-10-01T10:06:03.273' AS DateTime), N'/Upload/News/70ab47cf-09f1-421c-853d-58d5d65e6bc8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (408, 1, 35, 2, N'administrator', CAST(N'2015-10-01T10:09:26.817' AS DateTime), N'/Upload/News/8ac37372-3d54-4bf7-b426-83f7f409217b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (409, 1, 36, 2, N'administrator', CAST(N'2015-10-01T10:12:15.617' AS DateTime), N'/Upload/News/003b2e9d-2001-480e-b773-4a70560fd604')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (410, 1, 37, 2, N'administrator', CAST(N'2015-10-01T10:12:46.787' AS DateTime), N'/Upload/News/f0683fa4-6e90-4ed2-9bd1-620475c5e597')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (411, 1, 38, 2, N'administrator', CAST(N'2015-10-01T10:22:18.030' AS DateTime), N'/Upload/News/f7598c31-895b-458c-8e51-a62aa0942e48')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (412, 1, 39, 2, N'administrator', CAST(N'2015-10-01T10:30:42.160' AS DateTime), N'/Upload/News/749534ad-b708-4747-8539-41cd65298476')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (413, 1, 40, 2, N'administrator', CAST(N'2015-10-01T10:33:10.643' AS DateTime), N'/Upload/News/1013b7dc-6a30-4b89-8cfd-ad291df10bb6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (414, 1, 41, 2, N'administrator', CAST(N'2015-10-01T10:34:45.223' AS DateTime), N'/Upload/News/48899d8c-7424-4fc4-ad59-c28655334586')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (415, 1, 42, 2, N'administrator', CAST(N'2015-10-01T10:35:35.580' AS DateTime), N'/Upload/News/fe77ff20-e102-451b-9325-347157411e72')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (416, 1, 43, 2, N'administrator', CAST(N'2015-10-01T10:36:14.123' AS DateTime), N'/Upload/News/f5fa9e09-26bb-4724-b076-9fd543b88a9c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (417, 7, 0, 4, N'administrator', CAST(N'2015-10-04T19:05:47.720' AS DateTime), N'/Upload/Partners/8f8e87eb-7a49-4176-a98e-5ba3df2b9a5f/logo-kcn-tp.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (418, 7, 0, 4, N'administrator', CAST(N'2015-10-04T19:06:12.667' AS DateTime), N'/Upload/Partners/8fcf8aa6-3283-40a6-b513-4a7b177751a7/logo-dovina.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (419, 1, 0, 1, N'administrator', CAST(N'2015-11-03T22:51:06.007' AS DateTime), N'/Upload/News/563381d6-b00c-47e1-9478-0e200f9ab3f4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (420, 1, 0, 1, N'administrator', CAST(N'2015-11-03T22:52:16.083' AS DateTime), N'/Upload/News/a81efa78-2652-4827-83db-a46b01975f17')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (421, 1, 44, 2, N'administrator', CAST(N'2015-11-03T22:53:11.123' AS DateTime), N'/Upload/News/dc90c470-2c1f-48e9-b1b4-a71d61663447')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (422, 1, 45, 2, N'administrator', CAST(N'2015-11-03T22:54:26.107' AS DateTime), N'/Upload/News/dc90c470-2c1f-48e9-b1b4-a71d61663447')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (423, 1, 0, 1, N'administrator', CAST(N'2015-11-05T22:42:02.947' AS DateTime), N'/Upload/News/e88cb2cc-164a-4397-be30-6b2e6d3d95a6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (424, 1, 0, 1, N'administrator', CAST(N'2015-11-06T16:41:21.147' AS DateTime), N'/Upload/News/9507d671-024b-4be4-abb7-4d5599616d65')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (425, 7, 0, 4, N'administrator', CAST(N'2015-11-08T14:04:24.883' AS DateTime), N'/Upload/Products/2d2e837e-d613-41a4-9cd5-2157d3fd95f2/image1.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (426, 7, 0, 4, N'administrator', CAST(N'2015-11-08T14:04:27.337' AS DateTime), N'/Upload/Products/2d2e837e-d613-41a4-9cd5-2157d3fd95f2/img-description.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (427, 7, 0, 4, N'administrator', CAST(N'2015-11-08T14:04:29.377' AS DateTime), N'/Upload/Products/2d2e837e-d613-41a4-9cd5-2157d3fd95f2/san-pham-2.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (428, 7, 0, 4, N'administrator', CAST(N'2015-11-10T00:51:16.967' AS DateTime), N'/Upload/Video/9e238680-8046-4a58-a860-69822618c379/inakustik.5.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (429, 1, 46, 2, N'administrator', CAST(N'2015-11-10T11:08:57.053' AS DateTime), N'/Upload/News/6e2c1e05-86c8-47a5-94ab-ddac5b709760')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (430, 1, 47, 2, N'administrator', CAST(N'2015-11-10T11:11:22.503' AS DateTime), N'/Upload/News/3d38cb89-5c71-4f1f-bed0-8ebaac90c314')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (431, 1, 0, 1, N'administrator', CAST(N'2015-11-10T11:11:51.297' AS DateTime), N'/Upload/News/779ebbcf-1b8a-4766-a39f-07a2b5a74feb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (432, 1, 0, 1, N'administrator', CAST(N'2015-11-10T11:12:04.240' AS DateTime), N'/Upload/News/0651d285-2a6f-4e34-ab61-d1130ed91be6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (433, 1, 48, 2, N'administrator', CAST(N'2015-11-12T09:49:15.647' AS DateTime), N'/Upload/News/3d38cb89-5c71-4f1f-bed0-8ebaac90c314')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (434, 1, 0, 1, N'administrator', CAST(N'2015-11-20T10:25:49.043' AS DateTime), N'/Upload/News/a98300ba-dd24-4f39-b555-10a1e922536e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (435, 1, 0, 1, N'administrator', CAST(N'2015-11-26T10:52:38.120' AS DateTime), N'/Upload/Projects/abc12347-6f23-4d3f-9fbc-a9d6b5b2537d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (436, 1, 0, 1, N'administrator', CAST(N'2015-11-26T10:54:51.433' AS DateTime), N'/Upload/Projects/967c1696-64ee-4a64-a6ea-da8fed09dfa6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (437, 1, 0, 1, N'administrator', CAST(N'2015-11-26T10:55:49.580' AS DateTime), N'/Upload/Projects/ffbf267c-59e6-49b8-bf01-42734152c59e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (438, 1, 0, 1, N'administrator', CAST(N'2015-11-26T19:45:38.563' AS DateTime), N'/Upload/Projects/a6e68918-8440-4909-b694-cdf8918f79b9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (439, 7, 0, 4, N'administrator', CAST(N'2015-11-26T19:46:58.547' AS DateTime), N'/Upload/Projects/a6e68918-8440-4909-b694-cdf8918f79b9/new-york.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (440, 7, 0, 4, N'administrator', CAST(N'2015-11-26T19:47:00.157' AS DateTime), N'/Upload/Projects/a6e68918-8440-4909-b694-cdf8918f79b9/paris.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (441, 7, 0, 4, N'administrator', CAST(N'2015-11-26T19:47:01.740' AS DateTime), N'/Upload/Projects/a6e68918-8440-4909-b694-cdf8918f79b9/sydney.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (442, 7, 0, 4, N'administrator', CAST(N'2015-11-26T19:47:02.707' AS DateTime), N'/Upload/Projects/a6e68918-8440-4909-b694-cdf8918f79b9/tokyo.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (443, 1, 1, 2, N'administrator', CAST(N'2015-11-26T19:59:47.727' AS DateTime), N'/Upload/Projects/d26e5800-0787-4205-aea8-0beedddf4d16')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (444, 1, 0, 1, N'administrator', CAST(N'2015-11-26T20:05:44.583' AS DateTime), N'/Upload/Projects/d26e5800-0787-4205-aea8-0beedddf4d16')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (445, 1, 2, 2, N'administrator', CAST(N'2015-11-26T22:52:08.760' AS DateTime), N'/Upload/News/924e155e-bd7b-4a30-9407-0d9e1023fcc3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (446, 1, 3, 2, N'administrator', CAST(N'2015-11-26T22:56:48.177' AS DateTime), N'/Upload/Projects/bf0b355a-6f85-459a-98c5-c345aeae9691')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (447, 7, 0, 4, N'administrator', CAST(N'2015-11-27T00:29:52.540' AS DateTime), N'/Upload/Categoties/bcd73cd6-6690-44a3-b09c-d919133be326/another2.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (448, 1, 4, 2, N'administrator', CAST(N'2015-11-28T00:02:12.550' AS DateTime), N'/Upload/Projects/2a4a3b4f-e1d8-4b29-b459-96af417e3832')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (449, 1, 5, 2, N'administrator', CAST(N'2015-11-28T00:03:51.057' AS DateTime), N'/Upload/Projects/6d5f2e11-6386-4500-84c9-68dcbe313b54')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (450, 1, 6, 2, N'administrator', CAST(N'2015-11-28T00:04:51.347' AS DateTime), N'/Upload/Projects/5c70bfc7-694a-466e-81b6-0eeaa934840e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (451, 1, 7, 2, N'administrator', CAST(N'2015-11-28T00:16:17.520' AS DateTime), N'/Upload/News/0d27deb4-f7c6-48f1-9e71-f710ccaf7d0d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (452, 1, 8, 2, N'administrator', CAST(N'2015-11-28T00:17:51.787' AS DateTime), N'/Upload/News/1ab1b2b6-ced2-40ec-af51-5b440d0412c8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (453, 1, 9, 2, N'administrator', CAST(N'2015-11-28T00:18:59.680' AS DateTime), N'/Upload/News/f4fa50b7-15f9-4be9-9712-3b5d7e7fa249')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (454, 1, 10, 2, N'administrator', CAST(N'2015-12-01T01:08:07.870' AS DateTime), N'/Upload/News/efefdcdf-3ae2-43dd-96b7-dbef0d564b15')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (455, 1, 11, 2, N'administrator', CAST(N'2015-12-01T01:09:27.683' AS DateTime), N'/Upload/News/1ae246a4-de12-49a3-ba23-7748a40cb3d5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (456, 1, 0, 1, N'administrator', CAST(N'2015-12-01T09:08:06.967' AS DateTime), N'/Upload/News/572f2bbb-daf3-4133-90a7-4796939f53a1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (457, 1, 12, 2, N'administrator', CAST(N'2015-12-01T09:11:18.647' AS DateTime), N'/Upload/News/92142008-4a20-4fb1-8dd5-6c747bf574cf')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (458, 1, 13, 2, N'administrator', CAST(N'2015-12-04T21:17:37.083' AS DateTime), N'/Upload/News/e69c76bb-e6f1-4874-9685-fc40486327a8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (459, 1, 14, 2, N'administrator', CAST(N'2015-12-04T21:23:17.597' AS DateTime), N'/Upload/News/6529d83d-e53c-4057-8502-6cb10ac6182f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (460, 1, 15, 2, N'administrator', CAST(N'2015-12-04T21:49:18.080' AS DateTime), N'/Upload/News/3c55f5b7-f688-4b80-82ea-8947ac3147da')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (461, 1, 16, 2, N'administrator', CAST(N'2015-12-04T22:00:18.753' AS DateTime), N'/Upload/News/75bb65ba-78f7-4cff-8d64-4163d4b1bd4c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (462, 1, 17, 2, N'administrator', CAST(N'2015-12-04T22:02:15.367' AS DateTime), N'/Upload/News/9fbf3166-19c5-4f11-98a0-f2161845b96d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (463, 1, 18, 2, N'administrator', CAST(N'2015-12-04T22:03:38.123' AS DateTime), N'/Upload/News/ebab0ed8-c387-47e2-bae9-15e5ff06f739')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (464, 1, 19, 2, N'administrator', CAST(N'2015-12-04T22:05:21.270' AS DateTime), N'/Upload/News/a8e28550-fb84-40c1-a88d-03d938dac825')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (465, 1, 20, 2, N'administrator', CAST(N'2015-12-04T22:06:40.177' AS DateTime), N'/Upload/News/3512092b-8c5d-4bef-9446-4198af5bc213')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (466, 1, 21, 2, N'administrator', CAST(N'2015-12-04T22:08:33.037' AS DateTime), N'/Upload/News/a707bcb8-1dcb-4b1d-af28-5d7f7386bdc7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (467, 1, 22, 2, N'administrator', CAST(N'2015-12-04T22:09:39.940' AS DateTime), N'/Upload/News/5fa868aa-b7e2-46ba-85e9-44c31be93f93')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (468, 1, 23, 2, N'administrator', CAST(N'2015-12-04T22:10:27.060' AS DateTime), N'/Upload/News/27d8a15b-10e6-41f9-a33f-6539801b287b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (469, 1, 24, 2, N'administrator', CAST(N'2015-12-04T22:12:22.387' AS DateTime), N'/Upload/News/0876a5e9-f0b7-410d-8c16-9f9295a2dabb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (470, 1, 25, 2, N'administrator', CAST(N'2015-12-04T22:13:14.580' AS DateTime), N'/Upload/News/1f7ea4b2-577d-4304-81b1-7e49d1afdb52')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (471, 1, 26, 2, N'administrator', CAST(N'2015-12-04T22:13:50.757' AS DateTime), N'/Upload/News/07a1d81f-4fe6-43fc-8138-a1381c18f436')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (472, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:00:02.913' AS DateTime), N'/Upload/News/5fa868aa-b7e2-46ba-85e9-44c31be93f93/08.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (473, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:01:23.500' AS DateTime), N'/Upload/News/27d8a15b-10e6-41f9-a33f-6539801b287b/09.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (474, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:02:44.127' AS DateTime), N'/Upload/News/a707bcb8-1dcb-4b1d-af28-5d7f7386bdc7/07.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (475, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:04:59.847' AS DateTime), N'/Upload/News/3512092b-8c5d-4bef-9446-4198af5bc213/06.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (476, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:05:38.043' AS DateTime), N'/Upload/News/a8e28550-fb84-40c1-a88d-03d938dac825/05.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (477, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:06:04.430' AS DateTime), N'/Upload/News/ebab0ed8-c387-47e2-bae9-15e5ff06f739/04.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (478, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:06:27.487' AS DateTime), N'/Upload/News/9fbf3166-19c5-4f11-98a0-f2161845b96d/03.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (479, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:07:48.820' AS DateTime), N'/Upload/News/75bb65ba-78f7-4cff-8d64-4163d4b1bd4c/02.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (480, 7, 0, 4, N'administrator', CAST(N'2015-12-06T00:08:14.427' AS DateTime), N'/Upload/News/3c55f5b7-f688-4b80-82ea-8947ac3147da/10.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (481, 1, 27, 2, N'administrator', CAST(N'2015-12-06T15:12:12.697' AS DateTime), N'/Upload/News/1094ee1e-23ca-499c-bcad-723e8aaacb40')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (482, 1, 28, 2, N'administrator', CAST(N'2015-12-06T21:55:27.513' AS DateTime), N'/Upload/News/de3684b1-2ddc-404d-a5cf-99a76f9bd713')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (483, 1, 29, 2, N'administrator', CAST(N'2015-12-06T21:55:55.643' AS DateTime), N'/Upload/News/50011887-862a-4031-9560-a53923b6930a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (484, 1, 30, 2, N'administrator', CAST(N'2015-12-06T23:04:57.697' AS DateTime), N'/Upload/News/df687f4c-06dd-4a9e-815f-c22384e1a0e4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (485, 1, 31, 2, N'administrator', CAST(N'2015-12-07T11:06:43.230' AS DateTime), N'/Upload/News/3c55f5b7-f688-4b80-82ea-8947ac3147da')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (486, 1, 0, 1, N'administrator', CAST(N'2015-12-07T11:15:51.960' AS DateTime), N'/Upload/News/3c55f5b7-f688-4b80-82ea-8947ac3147da')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (487, 1, 32, 2, N'administrator', CAST(N'2015-12-07T11:17:38.617' AS DateTime), N'/Upload/News/3c55f5b7-f688-4b80-82ea-8947ac3147da')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (488, 7, 0, 4, N'administrator', CAST(N'2015-12-13T00:40:14.883' AS DateTime), N'/Upload/Partners/b0c410c6-50bb-46ae-a39d-bf9f6c6eb68a/02.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (489, 7, 0, 4, N'administrator', CAST(N'2015-12-13T00:41:05.857' AS DateTime), N'/Upload/Partners/b0c410c6-50bb-46ae-a39d-bf9f6c6eb68a/piifiles.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (490, 7, 0, 4, N'administrator', CAST(N'2015-12-13T00:41:46.333' AS DateTime), N'/Upload/Partners/fc2bcd12-786a-41cd-962a-f58cc7562be6/piifiles.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (491, 7, 0, 4, N'administrator', CAST(N'2015-12-14T23:11:54.990' AS DateTime), N'/Upload/News/df687f4c-06dd-4a9e-815f-c22384e1a0e4/DSC05553.JPG')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (492, 7, 0, 4, N'administrator', CAST(N'2015-12-14T23:12:09.113' AS DateTime), N'/Upload/News/df687f4c-06dd-4a9e-815f-c22384e1a0e4/banner6.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (493, 7, 0, 4, N'administrator', CAST(N'2015-12-14T23:12:09.920' AS DateTime), N'/Upload/News/df687f4c-06dd-4a9e-815f-c22384e1a0e4/CapNgam.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (494, 1, 33, 2, N'administrator', CAST(N'2015-12-25T15:43:07.127' AS DateTime), N'/Upload/News/df687f4c-06dd-4a9e-815f-c22384e1a0e4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (495, 1, 34, 2, N'administrator', CAST(N'2015-12-25T15:45:04.503' AS DateTime), N'/Upload/News/df687f4c-06dd-4a9e-815f-c22384e1a0e4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (496, 1, 0, 1, N'administrator', CAST(N'2015-12-27T17:42:32.537' AS DateTime), N'/Upload/News/36d5429b-bc96-41db-8a11-0836fed53d6e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (497, 1, 0, 1, N'administrator', CAST(N'2015-12-27T17:44:47.960' AS DateTime), N'/Upload/News/b9c03fe2-2658-4d66-8ad7-e4886ae9b795')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (498, 1, 0, 1, N'administrator', CAST(N'2015-12-27T17:45:03.997' AS DateTime), N'/Upload/News/3a94c3c3-5f0c-4aee-b457-43b2ce1765e4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (499, 1, 0, 1, N'administrator', CAST(N'2015-12-27T17:46:39.110' AS DateTime), N'/Upload/News/054ddf8a-df44-431f-ab4b-b6b84cab8112')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (500, 1, 0, 1, N'administrator', CAST(N'2015-12-27T17:47:36.550' AS DateTime), N'/Upload/News/6278d460-70f3-433c-b143-9115a3a45457')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (501, 1, 0, 1, N'administrator', CAST(N'2015-12-27T17:51:49.243' AS DateTime), N'/Upload/News/52eeec16-ca7c-43ef-98dc-37ce0f774142')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (502, 1, 1, 2, N'administrator', CAST(N'2015-12-28T11:38:22.877' AS DateTime), N'/Upload/News/8af198d7-719b-4720-9b4e-0290b4fec022')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (503, 1, 2, 2, N'administrator', CAST(N'2015-12-28T11:42:06.300' AS DateTime), N'/Upload/News/479821d8-f4ee-43ad-b69d-3f23b3a6f81a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (504, 1, 0, 1, N'administrator', CAST(N'2015-12-28T14:06:53.143' AS DateTime), N'/Upload/News/370be382-15da-4e4c-b827-829008190cca')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (505, 1, 3, 2, N'administrator', CAST(N'2015-12-28T14:09:01.800' AS DateTime), N'/Upload/News/7347e125-d5c4-4aab-accb-5a01b9c08639')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (506, 1, 4, 2, N'administrator', CAST(N'2015-12-28T14:11:35.730' AS DateTime), N'/Upload/News/dd52a702-f463-430f-99d6-fad2f9d58feb')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (507, 1, 5, 2, N'administrator', CAST(N'2015-12-28T14:12:56.273' AS DateTime), N'/Upload/News/8f4e8dd6-9823-44cb-86da-ae0c2bce3209')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (508, 1, 6, 2, N'administrator', CAST(N'2015-12-29T10:38:27.623' AS DateTime), N'/Upload/News/0d6f4873-fe7e-4063-a8ce-8076963b53fa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (509, 1, 0, 1, N'administrator', CAST(N'2015-12-29T16:21:09.080' AS DateTime), N'/Upload/News/b1f7cd8f-c4c4-4464-bef5-43ad20bc4c7d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (510, 1, 7, 2, N'administrator', CAST(N'2015-12-29T16:22:00.150' AS DateTime), N'/Upload/News/c94ff96e-73d7-4b4e-b36a-fa209af08b25')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (511, 1, 8, 2, N'administrator', CAST(N'2015-12-29T16:23:01.140' AS DateTime), N'/Upload/News/73a80d23-9bee-4f81-953c-9432f1cbe853')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (512, 1, 0, 1, N'administrator', CAST(N'2016-06-22T09:50:06.230' AS DateTime), N'/Upload/News/c39c0b1e-c231-4a72-ba77-a23f315f9f5c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (513, 1, 0, 1, N'administrator', CAST(N'2016-06-22T10:06:38.733' AS DateTime), N'/Upload/News/bf989841-b45d-4c45-ba09-a798a8a54de3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (514, 1, 2, 2, N'administrator', CAST(N'2016-06-22T10:12:15.527' AS DateTime), N'/Upload/News/ad34348c-1aee-4a59-9519-9e1d66304008')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (515, 1, 0, 1, N'administrator', CAST(N'2016-06-22T10:19:32.760' AS DateTime), N'/Upload/News/f2bbbcc8-c742-4a55-8004-3c5a2d10fa81')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (516, 7, 0, 4, N'administrator', CAST(N'2016-06-23T21:58:10.147' AS DateTime), N'/Upload/SlideShow/6471becf-507b-40eb-ad33-91dd7f4c80be/03.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (517, 1, 3, 2, N'administrator', CAST(N'2016-06-27T11:55:34.827' AS DateTime), N'/Upload/News/98eb4a3d-609b-4e63-a243-98fe5bd5382e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (518, 1, 4, 2, N'administrator', CAST(N'2016-06-27T12:12:43.730' AS DateTime), N'/Upload/News/695d09eb-8eeb-4b76-a5e2-861a08f1c5af')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (519, 1, 5, 2, N'administrator', CAST(N'2016-06-27T14:57:07.747' AS DateTime), N'/Upload/News/c41c11fd-550a-4aec-97d2-252da2cf1742')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (520, 1, 0, 1, N'administrator', CAST(N'2016-06-27T17:25:19.587' AS DateTime), N'/Upload/News/d7b2ed2d-da8b-42e2-8d07-cfd338ea8165')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (521, 1, 6, 2, N'administrator', CAST(N'2016-06-27T18:08:48.053' AS DateTime), N'/Upload/News/8bb2657a-0db9-4851-b98c-22686a626457')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (522, 1, 7, 2, N'administrator', CAST(N'2016-06-27T18:12:50.633' AS DateTime), N'/Upload/News/5d16e6ad-d1f9-468f-997e-814a6384d551')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (523, 1, 8, 2, N'administrator', CAST(N'2016-06-27T18:16:41.463' AS DateTime), N'/Upload/News/d22e4c68-83d1-44d7-b520-a2680b5c7700')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (524, 1, 9, 2, N'administrator', CAST(N'2016-06-27T21:34:58.147' AS DateTime), N'/Upload/News/c1038f1a-d4b1-4f4c-a23f-2d315fd7ed82')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (525, 1, 10, 2, N'administrator', CAST(N'2016-06-27T21:37:31.780' AS DateTime), N'/Upload/News/fda07c17-ccb2-40c4-817f-e6444f10f33f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (526, 1, 11, 2, N'administrator', CAST(N'2016-06-27T21:39:23.107' AS DateTime), N'/Upload/News/578e4f68-c177-402c-a188-31dad75fcfc3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (527, 1, 0, 1, N'administrator', CAST(N'2016-07-03T23:05:20.087' AS DateTime), N'/Upload/News/d23aabdf-59a1-43b4-a93a-fafbdb222b62')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (528, 1, 1, 2, N'administrator', CAST(N'2016-07-03T23:39:20.820' AS DateTime), N'/Upload/News/05a8eaae-b0a0-43fc-ae00-41fa66bb6e22')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (529, 1, 2, 2, N'administrator', CAST(N'2016-07-04T00:59:45.783' AS DateTime), N'/Upload/News/dea31361-fa67-4063-9ac0-0de91e536591')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (530, 1, 0, 1, N'administrator', CAST(N'2016-07-05T22:35:51.643' AS DateTime), N'/Upload/News/4397c639-52a9-435f-ba3a-12b1b44f3e71')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (531, 1, 0, 1, N'administrator', CAST(N'2016-07-08T01:14:48.593' AS DateTime), N'/Upload/News/875db8ad-bad2-4abf-8d6a-455803bca48a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (532, 1, 0, 1, N'administrator', CAST(N'2016-07-08T01:27:18.563' AS DateTime), N'/Upload/News/eadf5b52-8996-4ab1-89cd-8874bdacbe9f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (533, 1, 3, 2, N'administrator', CAST(N'2016-07-08T01:27:31.290' AS DateTime), N'/Upload/News/b65e4d77-8a97-4d94-b2db-d830fbb243c6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (534, 1, 4, 2, N'administrator', CAST(N'2016-07-08T02:02:02.667' AS DateTime), N'/Upload/News/e513c546-8695-4763-959b-498dce0f118b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (535, 1, 5, 2, N'administrator', CAST(N'2016-07-08T02:11:08.587' AS DateTime), N'/Upload/News/f9ac4940-a5f0-498e-9fab-95c86628f85e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (536, 7, 0, 4, N'administrator', CAST(N'2016-07-08T02:12:58.017' AS DateTime), N'/Upload/News/f9ac4940-a5f0-498e-9fab-95c86628f85e/usa.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (537, 1, 6, 2, N'administrator', CAST(N'2016-07-08T02:15:47.390' AS DateTime), N'/Upload/News/0ff4f06a-0e78-4d73-9036-765fe4523f10')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (538, 1, 7, 2, N'administrator', CAST(N'2016-07-08T02:18:30.097' AS DateTime), N'/Upload/News/e306749f-89ff-4e28-b2fa-172a9f9a74a0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (539, 1, 8, 2, N'administrator', CAST(N'2016-07-08T02:22:03.490' AS DateTime), N'/Upload/News/e1441538-76fb-4ec1-8a98-5f74e398f744')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (540, 1, 0, 1, N'administrator', CAST(N'2016-08-05T15:16:19.983' AS DateTime), N'/Upload/News/4bdd6fd2-7293-484e-aca9-13e6ba13a31c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (541, 1, 0, 1, N'administrator', CAST(N'2016-08-06T23:00:31.310' AS DateTime), N'/Upload/News/193763d9-4705-4afe-b6ca-fb2aa6d35d19')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (542, 7, 0, 4, N'administrator', CAST(N'2016-08-07T13:27:27.380' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/14.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (543, 1, 0, 1, N'administrator', CAST(N'2016-08-07T13:30:05.427' AS DateTime), N'/Upload/News/7b154d3c-207a-4a4f-8396-3b4c447533f3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (544, 1, 1, 2, N'administrator', CAST(N'2016-08-07T13:45:04.040' AS DateTime), N'/Upload/News/bd8d5fe5-8a8f-4924-b3ac-6df7d01633ec')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (545, 1, 2, 2, N'administrator', CAST(N'2016-08-07T13:49:21.253' AS DateTime), N'/Upload/News/2eab1739-3f45-458b-ab72-d1a958f5fa2c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (546, 1, 3, 2, N'administrator', CAST(N'2016-08-07T13:50:13.313' AS DateTime), N'/Upload/News/5f6f17de-79e3-4f94-a48b-5c3c18538586')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (547, 1, 4, 2, N'administrator', CAST(N'2016-08-07T19:24:46.897' AS DateTime), N'/Upload/News/b81a8771-4ec9-4ac2-9c38-5671609e5ebf')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (548, 1, 5, 2, N'administrator', CAST(N'2016-08-07T19:26:15.823' AS DateTime), N'/Upload/News/c81f9216-a030-49d8-a5ec-dc00802a1a92')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (549, 1, 6, 2, N'administrator', CAST(N'2016-08-07T19:28:42.217' AS DateTime), N'/Upload/News/e4385bb2-d1f5-4feb-b9bc-2479a8818ba7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (550, 1, 7, 2, N'administrator', CAST(N'2016-08-07T19:30:47.523' AS DateTime), N'/Upload/News/5868ff1a-8043-4afb-90f7-d27a2b3906df')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (551, 1, 8, 2, N'administrator', CAST(N'2016-08-07T19:34:48.800' AS DateTime), N'/Upload/News/4cb1d5ea-2456-4fba-8044-43100ebdd531')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (552, 1, 0, 1, N'administrator', CAST(N'2016-08-08T17:52:43.717' AS DateTime), N'/Upload/News/349d30c5-30cb-4737-a9d8-65c283bd0cd5')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (553, 1, 0, 1, N'administrator', CAST(N'2016-08-08T17:54:11.247' AS DateTime), N'/Upload/News/e3ee3374-919c-4c12-ac0a-1a9efc2f4d71')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (554, 1, 0, 1, N'administrator', CAST(N'2016-08-08T18:19:36.447' AS DateTime), N'/Upload/News/0eab7753-f840-4a64-8b13-d8048f47d296')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (555, 1, 0, 1, N'administrator', CAST(N'2016-08-08T18:24:21.270' AS DateTime), N'/Upload/News/1352f3cc-c965-4363-b7f0-813a3c1e0bc2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (556, 1, 0, 1, N'administrator', CAST(N'2016-08-08T21:10:39.657' AS DateTime), N'/Upload/News/adf60e54-7e7d-4ac1-8e73-592801b6fd7f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (557, 1, 9, 2, N'administrator', CAST(N'2016-08-08T21:27:10.447' AS DateTime), N'/Upload/News/00f0e44f-b44c-467a-9826-55b120f8dc35')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (558, 1, 10, 2, N'administrator', CAST(N'2016-08-08T21:29:49.347' AS DateTime), N'/Upload/News/7bf4e4a3-a5a7-49be-897a-1cf197c8c772')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (559, 1, 11, 2, N'administrator', CAST(N'2016-08-08T21:32:10.857' AS DateTime), N'/Upload/News/5e8bae70-f4a2-4069-a903-1f506854877b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (560, 1, 12, 2, N'administrator', CAST(N'2016-08-08T21:48:49.227' AS DateTime), N'/Upload/News/a5c32eff-af6f-442a-a6bb-6e30c89f96de')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (561, 1, 13, 2, N'administrator', CAST(N'2016-08-08T21:53:10.433' AS DateTime), N'/Upload/News/45eea23d-d4b7-409f-bba4-e75d45343b02')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (562, 1, 14, 2, N'administrator', CAST(N'2016-08-08T22:22:34.267' AS DateTime), N'/Upload/News/d9ea5442-772f-4abb-984b-fdae07ac51dd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (563, 1, 15, 2, N'administrator', CAST(N'2016-08-08T22:24:38.630' AS DateTime), N'/Upload/News/e2bb046a-72c4-4146-a36b-eb6b98dfa9ff')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (564, 1, 16, 2, N'administrator', CAST(N'2016-08-08T22:26:46.690' AS DateTime), N'/Upload/News/e88afa6d-fe08-4506-ad9a-33aaa256b91d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (565, 1, 17, 2, N'administrator', CAST(N'2016-08-08T22:29:02.910' AS DateTime), N'/Upload/News/5667c981-5375-4195-82f0-336be00c7e74')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (566, 1, 18, 2, N'administrator', CAST(N'2016-08-08T22:30:17.697' AS DateTime), N'/Upload/News/0035e003-b2ec-4c2c-8407-151dd182515a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (567, 1, 19, 2, N'administrator', CAST(N'2016-08-08T22:34:15.427' AS DateTime), N'/Upload/News/03c3cb5d-49e1-4fce-b9b6-173d63afec99')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (568, 1, 20, 2, N'administrator', CAST(N'2016-08-08T22:35:27.717' AS DateTime), N'/Upload/News/ece203d1-bec9-4d9d-b024-e86eb38c04c8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (569, 1, 21, 2, N'administrator', CAST(N'2016-08-08T22:37:03.873' AS DateTime), N'/Upload/News/967a8eef-1326-4c3a-ac3d-9c53607a1d15')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (570, 1, 22, 2, N'administrator', CAST(N'2016-08-08T22:40:13.150' AS DateTime), N'/Upload/News/51f54085-99e7-46b0-b0a1-528dd8d95d00')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (571, 1, 23, 2, N'administrator', CAST(N'2016-08-08T22:43:22.877' AS DateTime), N'/Upload/News/4fc66d78-e56a-4b36-97dd-5712f621fffa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (572, 1, 0, 1, N'administrator', CAST(N'2016-08-08T23:56:27.560' AS DateTime), N'/Upload/News/e6c1df13-dc66-4a60-82e5-c169f004b061')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (573, 1, 0, 1, N'administrator', CAST(N'2016-08-09T00:24:19.743' AS DateTime), N'/Upload/News/914dc066-af5d-45b3-8fb0-009f862f75e0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (574, 1, 0, 1, N'administrator', CAST(N'2016-08-09T09:03:16.820' AS DateTime), N'/Upload/News/2abe1cb8-5fd3-439e-8e93-a928c304ed7e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (575, 1, 24, 2, N'administrator', CAST(N'2016-08-09T09:11:54.257' AS DateTime), N'/Upload/News/9e5f893a-79de-4611-a653-101b3aa37364')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (576, 1, 25, 2, N'administrator', CAST(N'2016-08-09T09:33:24.317' AS DateTime), N'/Upload/News/166c42d1-0fed-424c-af5b-43943f8deeba')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (577, 1, 26, 2, N'administrator', CAST(N'2016-08-09T09:37:15.027' AS DateTime), N'/Upload/News/e879c5fc-fdea-415d-885e-a74347f633b2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (578, 1, 27, 2, N'administrator', CAST(N'2016-08-09T13:54:33.767' AS DateTime), N'/Upload/News/c17b9041-8e59-4d6d-aeeb-811e7e47536a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (579, 1, 28, 2, N'administrator', CAST(N'2016-08-09T14:33:30.603' AS DateTime), N'/Upload/News/90ac5e21-f9f9-443f-882c-727a0a9ee6f4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (580, 1, 29, 2, N'administrator', CAST(N'2016-08-09T14:39:31.540' AS DateTime), N'/Upload/News/27d5388f-c7d9-418c-b2b5-850da510ba62')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (581, 1, 30, 2, N'administrator', CAST(N'2016-08-09T14:41:38.447' AS DateTime), N'/Upload/News/58f1e413-8a99-4cb0-9149-2cca90050257')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (582, 1, 31, 2, N'administrator', CAST(N'2016-08-09T14:43:28.367' AS DateTime), N'/Upload/News/651e7b37-6af0-4fb9-824d-afd32a3f4a28')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (583, 1, 32, 2, N'administrator', CAST(N'2016-08-09T14:45:27.347' AS DateTime), N'/Upload/News/89b60584-153b-4321-a9df-90954ef8dac2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (584, 1, 33, 2, N'administrator', CAST(N'2016-08-09T14:47:52.893' AS DateTime), N'/Upload/News/39639d32-3fb0-49d6-9739-6b37dc54c4d4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (585, 1, 34, 2, N'administrator', CAST(N'2016-08-09T14:49:54.793' AS DateTime), N'/Upload/News/30663a57-5303-4c19-954e-a0a75bd4536a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (586, 7, 0, 4, N'administrator', CAST(N'2016-08-09T15:07:30.463' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/14.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (587, 1, 0, 1, N'administrator', CAST(N'2016-08-09T18:17:38.437' AS DateTime), N'/Upload/News/2f624c85-006c-49cf-95b6-d685948892a1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (588, 1, 0, 1, N'administrator', CAST(N'2016-08-09T21:47:22.510' AS DateTime), N'/Upload/News/2916e0e4-95fc-4422-86e4-2a0ad56b6bf6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (589, 1, 0, 1, N'administrator', CAST(N'2016-08-09T21:53:15.787' AS DateTime), N'/Upload/News/5d4b5df1-2bd3-44f9-a6d9-9b09e0d1068b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (590, 1, 35, 2, N'administrator', CAST(N'2016-08-09T21:53:36.940' AS DateTime), N'/Upload/News/0f535383-bf5f-4e49-a45a-c57830d9db8a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (591, 1, 36, 2, N'administrator', CAST(N'2016-08-09T21:54:57.233' AS DateTime), N'/Upload/News/940c2137-bb16-45f9-b0c3-ada2e2e42347')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (592, 1, 37, 2, N'administrator', CAST(N'2016-08-09T21:58:11.687' AS DateTime), N'/Upload/News/bea97a13-36a0-4bb4-a0e0-a639ca707530')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (593, 1, 38, 2, N'administrator', CAST(N'2016-08-09T22:24:29.677' AS DateTime), N'/Upload/News/96dc1d9f-a22c-48a9-ac5a-5e74c60fceda')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (594, 1, 39, 2, N'administrator', CAST(N'2016-08-10T09:18:06.427' AS DateTime), N'/Upload/News/71c923c0-2680-4515-8072-ae83322e7f47')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (595, 7, 0, 4, N'administrator', CAST(N'2016-08-10T12:29:55.113' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/14.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (596, 7, 0, 4, N'administrator', CAST(N'2016-08-10T12:30:22.850' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/nav-slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (597, 7, 0, 4, N'administrator', CAST(N'2016-08-10T15:15:22.347' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (598, 7, 0, 4, N'administrator', CAST(N'2016-08-10T17:37:05.173' AS DateTime), N'/Upload/News/d9ea5442-772f-4abb-984b-fdae07ac51dd/Motorcycle Fork oil.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (599, 7, 0, 4, N'administrator', CAST(N'2016-08-10T17:53:01.907' AS DateTime), N'/Upload/SlideShow/2877bf1a-27ff-4600-b26e-3bf5da170908/Motorcycle Oil levels.jpg')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (600, 1, 40, 2, N'administrator', CAST(N'2016-08-11T06:02:09.373' AS DateTime), N'/Upload/News/8da4d383-5436-49af-a619-9b186abd2997')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (601, 1, 0, 1, N'administrator', CAST(N'2016-08-11T06:04:36.560' AS DateTime), N'/Upload/News/67aecbd9-732a-478f-948c-b1168f3a9faa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (602, 1, 41, 2, N'administrator', CAST(N'2016-08-11T06:36:22.510' AS DateTime), N'/Upload/News/888bf717-01f2-4cb0-9b46-b5ee4eee6397')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (603, 1, 0, 1, N'administrator', CAST(N'2016-08-11T07:52:10.137' AS DateTime), N'/Upload/News/bed2db25-a513-465a-b50d-9723e7186304')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (604, 1, 42, 2, N'administrator', CAST(N'2016-08-11T07:52:26.983' AS DateTime), N'/Upload/News/ce49663c-b3d3-41ce-97e0-56a9db9bc819')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (605, 1, 43, 2, N'administrator', CAST(N'2016-08-11T07:54:37.387' AS DateTime), N'/Upload/News/51f7bf43-9e11-4754-9e99-8185aea7b720')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (606, 1, 44, 2, N'administrator', CAST(N'2016-08-11T07:56:51.357' AS DateTime), N'/Upload/News/e411c0d1-9a1a-4571-a1f2-502c54dfad97')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (607, 1, 45, 2, N'administrator', CAST(N'2016-08-11T07:58:17.453' AS DateTime), N'/Upload/News/2dd0d894-7594-40ab-a6f4-c13a794d49a9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (608, 7, 0, 4, N'administrator', CAST(N'2016-08-12T17:22:42.793' AS DateTime), N'/Upload/SlideShow/2877bf1a-27ff-4600-b26e-3bf5da170908/slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (609, 7, 0, 4, N'administrator', CAST(N'2016-08-12T17:23:06.863' AS DateTime), N'/Upload/SlideShow/a4c0ba4d-50ef-43cb-9d70-56e67fd8573b/slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (610, 7, 0, 4, N'administrator', CAST(N'2016-08-12T17:23:53.367' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (611, 7, 0, 4, N'administrator', CAST(N'2016-08-12T17:24:32.320' AS DateTime), N'/Upload/SlideShow/dcbd1fc5-7757-42ca-95ae-f6be36cf2aa1/slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (612, 7, 0, 4, N'administrator', CAST(N'2016-08-12T17:25:38.730' AS DateTime), N'/Upload/SlideShow/8ada0bb0-8c83-4f29-a486-9f1ca67eab9d/slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (613, 7, 0, 4, N'administrator', CAST(N'2016-08-16T09:36:53.647' AS DateTime), N'/Upload/Products/e830b3b1-7147-4cdd-b0dd-fadbbf3924b3/1L_300V_ROAD_RACING_10W40.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (614, 7, 0, 4, N'administrator', CAST(N'2016-08-16T09:36:55.053' AS DateTime), N'/Upload/Products/e830b3b1-7147-4cdd-b0dd-fadbbf3924b3/1L_300V-ROAD.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (615, 7, 0, 4, N'administrator', CAST(N'2016-08-16T09:37:41.563' AS DateTime), N'/Upload/Products/29d0b5b3-4e6f-466e-b099-3ab346bc69a3/1L_Scooter_Power_LE_5W40_etched_cmyk.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (616, 7, 0, 4, N'administrator', CAST(N'2016-08-16T09:37:42.937' AS DateTime), N'/Upload/Products/29d0b5b3-4e6f-466e-b099-3ab346bc69a3/1L_300V_15W50.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (617, 7, 0, 4, N'administrator', CAST(N'2016-08-16T09:42:29.657' AS DateTime), N'/Upload/Products/29d0b5b3-4e6f-466e-b099-3ab346bc69a3/300V15W50.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (618, 7, 0, 4, N'administrator', CAST(N'2016-08-16T09:56:44.887' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/14.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (619, 7, 0, 4, N'administrator', CAST(N'2016-08-16T10:00:05.177' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/3100GOLD.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (620, 1, 46, 2, N'administrator', CAST(N'2016-08-17T14:47:00.703' AS DateTime), N'/Upload/News/73054122-812a-4072-93f5-f63961fd4242')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (621, 7, 0, 4, N'administrator', CAST(N'2016-08-18T19:21:02.030' AS DateTime), N'/Upload/SlideShow/2877bf1a-27ff-4600-b26e-3bf5da170908/bannerchinh.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (622, 7, 0, 4, N'administrator', CAST(N'2016-08-18T19:21:05.407' AS DateTime), N'/Upload/SlideShow/2877bf1a-27ff-4600-b26e-3bf5da170908/bannerchinh.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (623, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:13:45.637' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/slide-011.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (624, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:13:58.230' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/slide-12.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (625, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:14:09.543' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/20150227_516396fcc1cbc03730f96110e4fdb4c0_1425026287.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (626, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:14:12.827' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/4.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (627, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:15:53.840' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/nav-slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (628, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:15:58.263' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/13886410_1062588790484644_3358506826189832717_n.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (629, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:37:35.920' AS DateTime), N'/Upload/SlideShow/dcbd1fc5-7757-42ca-95ae-f6be36cf2aa1/nav-slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (630, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:37:39.467' AS DateTime), N'/Upload/SlideShow/dcbd1fc5-7757-42ca-95ae-f6be36cf2aa1/slide-011.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (631, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:42:01.060' AS DateTime), N'/Upload/SlideShow/a4c0ba4d-50ef-43cb-9d70-56e67fd8573b/slide-12.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (632, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:42:02.653' AS DateTime), N'/Upload/SlideShow/a4c0ba4d-50ef-43cb-9d70-56e67fd8573b/nav-slide-01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (633, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:42:03.700' AS DateTime), N'/Upload/SlideShow/a4c0ba4d-50ef-43cb-9d70-56e67fd8573b/slide-011.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (634, 7, 0, 4, N'administrator', CAST(N'2016-08-19T09:47:34.297' AS DateTime), N'/Upload/SlideShow/8ada0bb0-8c83-4f29-a486-9f1ca67eab9d/slide-011.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (635, 7, 0, 4, N'administrator', CAST(N'2016-08-26T10:01:35.737' AS DateTime), N'/Upload/SlideShow/a4c0ba4d-50ef-43cb-9d70-56e67fd8573b/unnamed (2).jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (636, 7, 0, 4, N'administrator', CAST(N'2016-08-26T10:01:55.753' AS DateTime), N'/Upload/SlideShow/a4c0ba4d-50ef-43cb-9d70-56e67fd8573b/13582092_1042503382493185_8931041962768373704_o.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (637, 7, 0, 4, N'administrator', CAST(N'2016-08-26T11:08:10.017' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/12829387_963364177073773_4623165652547778289_o.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (638, 7, 0, 4, N'administrator', CAST(N'2016-08-26T11:08:11.127' AS DateTime), N'/Upload/SlideShow/00a07e0a-7942-4150-9a92-d2819b0ceba8/13247852_1017187445024779_1155657810815616812_o.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (639, 7, 0, 4, N'administrator', CAST(N'2016-08-26T11:15:45.803' AS DateTime), N'/Upload/SlideShow/a4c0ba4d-50ef-43cb-9d70-56e67fd8573b/banner2.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (640, 7, 0, 4, N'administrator', CAST(N'2016-08-26T11:18:49.973' AS DateTime), N'/Upload/SlideShow/0709160a-3c84-41ec-8029-f4051ed83a1d/12841312_963363023740555_7668013625073544400_o.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (641, 7, 0, 4, N'administrator', CAST(N'2016-08-26T11:32:41.167' AS DateTime), N'/Upload/SlideShow/dcbd1fc5-7757-42ca-95ae-f6be36cf2aa1/3.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (642, 7, 0, 4, N'administrator', CAST(N'2016-08-26T11:32:42.467' AS DateTime), N'/Upload/SlideShow/dcbd1fc5-7757-42ca-95ae-f6be36cf2aa1/Motor show.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (643, 7, 0, 4, N'administrator', CAST(N'2016-08-26T11:48:40.893' AS DateTime), N'/Upload/SlideShow/b5cfca6b-5da8-4e40-b6f4-b0ed1a046e48/slide-011.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (644, 7, 0, 4, N'administrator', CAST(N'2016-08-26T11:48:41.927' AS DateTime), N'/Upload/SlideShow/b5cfca6b-5da8-4e40-b6f4-b0ed1a046e48/10371761_926836670726524_4834773045386390756_n.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (645, 7, 0, 4, N'administrator', CAST(N'2016-08-29T22:34:53.583' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/-7100_10W50_1L.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (646, 7, 0, 4, N'administrator', CAST(N'2016-08-29T22:34:59.443' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/0.8L_3100_GOLD_10W40.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (647, 7, 0, 4, N'administrator', CAST(N'2016-08-29T22:35:05.723' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/3100GOLD.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (648, 7, 0, 4, N'administrator', CAST(N'2016-08-29T22:35:18.803' AS DateTime), N'/Upload/Products/729e9593-6bcb-47cd-a2c5-0ab2585c9a14/01.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (649, 1, 0, 1, N'administrator', CAST(N'2016-08-30T18:03:00.183' AS DateTime), N'/Upload/News/2217530d-8061-41cd-83fb-12b54144ecd6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (650, 1, 47, 2, N'administrator', CAST(N'2016-08-31T08:33:32.937' AS DateTime), N'/Upload/News/f9069d52-bd43-48ea-9b7c-ea8a05e3e00f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (651, 1, 0, 1, N'administrator', CAST(N'2016-08-31T08:42:41.140' AS DateTime), N'/Upload/News/5588c6c0-92e4-471f-8921-38f7a738f2fa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (652, 7, 0, 4, N'administrator', CAST(N'2016-08-31T08:57:44.737' AS DateTime), N'/Upload/News/5588c6c0-92e4-471f-8921-38f7a738f2fa/Motul-WSBK-1920x1200.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (653, 1, 0, 1, N'administrator', CAST(N'2016-08-31T09:04:57.770' AS DateTime), N'/Upload/News/569e1f78-54ee-4a8d-80b0-62c2a57c3e20')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (654, 1, 48, 2, N'administrator', CAST(N'2016-08-31T09:05:09.630' AS DateTime), N'/Upload/News/84b603a3-75c6-4c07-8fca-fcdcdbfd19e7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (655, 7, 0, 4, N'administrator', CAST(N'2016-08-31T09:08:26.800' AS DateTime), N'/Upload/News/84b603a3-75c6-4c07-8fca-fcdcdbfd19e7/2016 MotoGP.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (656, 7, 0, 4, N'administrator', CAST(N'2016-08-31T09:11:12.207' AS DateTime), N'/Upload/News/84b603a3-75c6-4c07-8fca-fcdcdbfd19e7/Honda_Road_Racing_presentation_John_McGuinness-Conor_Cummins_Honda_Road_Racing_Honda_CBR1000RR_Fireblade_SP_©Steve_Babb.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (657, 7, 0, 4, N'administrator', CAST(N'2016-08-31T09:11:15.223' AS DateTime), N'/Upload/News/84b603a3-75c6-4c07-8fca-fcdcdbfd19e7/Honda_Road_Racing_presentation_John_McGuinness-Conor_Cummins_Honda_Road_Racing_Honda_CBR1000RR_Fireblade_SP_©Steve_Babb.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (658, 7, 0, 4, N'administrator', CAST(N'2016-08-31T09:14:09.397' AS DateTime), N'/Upload/News/84b603a3-75c6-4c07-8fca-fcdcdbfd19e7/Honda_Road_Racing_presentation_John_McGuinness-Conor_Cummins_Honda_Road_Racing_Honda_CBR1000RR_Fireblade_SP_©Steve_Babb.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (659, 7, 0, 4, N'administrator', CAST(N'2016-08-31T09:14:11.880' AS DateTime), N'/Upload/News/84b603a3-75c6-4c07-8fca-fcdcdbfd19e7/Honda_Road_Racing_presentation_John_McGuinness-Conor_Cummins_Honda_Road_Racing_Honda_CBR1000RR_Fireblade_SP_©Steve_Babb.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (660, 7, 0, 4, N'administrator', CAST(N'2016-09-08T14:50:09.963' AS DateTime), N'/Upload/SlideShow/c4e078a9-f886-4ac0-928f-76bb35f5a1de/banner_motul_sound.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (661, 1, 0, 1, N'administrator', CAST(N'2016-09-22T17:04:01.200' AS DateTime), N'/Upload/News/36ec666b-29e6-4feb-a532-3e7c9ec9d2ff')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (662, 1, 0, 1, N'administrator', CAST(N'2016-09-22T17:05:14.967' AS DateTime), N'/Upload/News/a1364e55-e83d-462d-9b8a-c1deca528320')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (663, 1, 49, 2, N'administrator', CAST(N'2016-09-22T17:06:28.617' AS DateTime), N'/Upload/News/7c39d7b1-f327-4127-b8a0-16e26ae679e0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (664, 7, 0, 4, N'administrator', CAST(N'2017-04-14T23:10:24.377' AS DateTime), N'/Upload/Products/b13b4181-d4bd-41f1-a157-37ad16317241/H100.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (665, 7, 0, 4, N'administrator', CAST(N'2017-04-14T23:13:44.587' AS DateTime), N'/Upload/Products/b13b4181-d4bd-41f1-a157-37ad16317241/H100.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (666, 7, 0, 4, N'administrator', CAST(N'2017-04-14T23:14:26.567' AS DateTime), N'/Upload/Products/b13b4181-d4bd-41f1-a157-37ad16317241/h_100.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (667, 1, 50, 2, N'administrator ', CAST(N'2017-04-27T00:29:20.877' AS DateTime), N'/Upload/News/26c7a52b-0e60-4c0a-b6f1-e8c39ca0c34f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (668, 1, 51, 2, N'administrator ', CAST(N'2017-04-27T00:33:31.910' AS DateTime), N'/Upload/News/35812654-68d3-4cd1-b8a3-290bb17db219')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (669, 7, 0, 4, N'administrator', CAST(N'2017-04-28T09:36:22.930' AS DateTime), N'/Upload/News/96dc1d9f-a22c-48a9-ac5a-5e74c60fceda/poster.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (670, 7, 0, 4, N'administrator', CAST(N'2017-04-28T09:36:27.097' AS DateTime), N'/Upload/News/96dc1d9f-a22c-48a9-ac5a-5e74c60fceda/phongvan.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (671, 7, 0, 4, N'administrator', CAST(N'2017-04-28T09:51:32.273' AS DateTime), N'/Upload/News/8da4d383-5436-49af-a619-9b186abd2997/Sales_Kit1_0_0.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (672, 7, 0, 4, N'administrator', CAST(N'2017-04-28T09:51:33.457' AS DateTime), N'/Upload/News/8da4d383-5436-49af-a619-9b186abd2997/aca25.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (673, 7, 0, 4, N'administrator', CAST(N'2017-04-28T09:54:35.447' AS DateTime), N'/Upload/News/4fc66d78-e56a-4b36-97dd-5712f621fffa/10418955_846219488788243_129553858420326818_n.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (674, 7, 0, 4, N'administrator', CAST(N'2017-04-28T09:56:49.483' AS DateTime), N'/Upload/News/30663a57-5303-4c19-954e-a0a75bd4536a/11403059_846218952121630_8401204037054639255_n.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (675, 7, 0, 4, N'administrator', CAST(N'2017-04-28T09:59:41.647' AS DateTime), N'/Upload/News/0035e003-b2ec-4c2c-8407-151dd182515a/_MG_8395.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (676, 7, 0, 4, N'administrator', CAST(N'2017-05-03T15:37:59.850' AS DateTime), N'/Upload/Products/db4cd7ed-63cf-4d10-a4ba-7848ab025779/800 2T FACTORY LINE OFF ROAD.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (677, 7, 0, 4, N'administrator', CAST(N'2017-05-03T15:38:49.290' AS DateTime), N'/Upload/Products/dcd33bfd-5dd7-409a-872c-1a1564ad745d/SCOOTER EXPERT 4T 10W-40 MB.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (678, 7, 0, 4, N'administrator ', CAST(N'2017-05-05T18:27:20.423' AS DateTime), N'/Upload/Products/db4cd7ed-63cf-4d10-a4ba-7848ab025779/800 2T FACTORY LINE OFF ROAD.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (679, 1, 52, 2, N'administrator', CAST(N'2017-05-16T16:56:40.440' AS DateTime), N'/Upload/News/41278a99-d1aa-4f0a-ac17-6f7d409ba59e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (680, 7, 0, 4, N'administrator', CAST(N'2017-05-16T18:23:24.967' AS DateTime), N'/Upload/News/41278a99-d1aa-4f0a-ac17-6f7d409ba59e/Infographic_MOTUL_Timeline_11.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (681, 1, 0, 1, N'administrator', CAST(N'2017-05-18T16:29:54.683' AS DateTime), N'/Upload/News/4f40b83c-d79b-488d-9749-2d7e823e7872')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (682, 1, 0, 1, N'administrator', CAST(N'2017-05-22T15:34:07.287' AS DateTime), N'/Upload/News/7f114224-7624-48a7-b3ca-a785ea519a52')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (683, 7, 0, 4, N'administrator', CAST(N'2017-05-23T10:24:48.140' AS DateTime), N'/Upload/News/96dc1d9f-a22c-48a9-ac5a-5e74c60fceda/banner size vne.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (684, 7, 0, 4, N'administrator', CAST(N'2017-05-23T13:40:39.903' AS DateTime), N'/Upload/News/96dc1d9f-a22c-48a9-ac5a-5e74c60fceda/thongcao1.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (685, 1, 53, 2, N'administrator', CAST(N'2017-05-23T14:14:12.520' AS DateTime), N'/Upload/News/72c7e9e0-ad86-40f3-897a-65faa8b55477')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (686, 7, 0, 4, N'administrator', CAST(N'2017-05-23T15:15:50.230' AS DateTime), N'/Upload/News/72c7e9e0-ad86-40f3-897a-65faa8b55477/Infographic_MOTUL_Timeline_16.5.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (687, 7, 0, 4, N'administrator', CAST(N'2017-05-23T15:16:54.607' AS DateTime), N'/Upload/News/d9ea5442-772f-4abb-984b-fdae07ac51dd/Motorcycle Fork oil.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (688, 7, 0, 4, N'administrator', CAST(N'2017-05-23T15:52:38.617' AS DateTime), N'/Upload/News/41278a99-d1aa-4f0a-ac17-6f7d409ba59e/Infographic_MOTUL_Timeline_16.5.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (689, 7, 0, 4, N'administrator', CAST(N'2017-05-23T15:55:00.323' AS DateTime), N'/Upload/News/41278a99-d1aa-4f0a-ac17-6f7d409ba59e/Infographic_MOTUL_Timeline_23.5.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (690, 7, 0, 4, N'administrator', CAST(N'2017-05-24T13:50:42.067' AS DateTime), N'/Upload/News/72c7e9e0-ad86-40f3-897a-65faa8b55477/Infographic_2_Chon_Nhot_Cho_Xe_Tay_Ga.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (691, 1, 0, 1, N'administrator', CAST(N'2017-05-24T17:11:08.803' AS DateTime), N'/Upload/News/b1f898fa-c435-472e-a4ae-3940a0c568f4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (692, 1, 54, 2, N'administrator', CAST(N'2017-06-06T09:23:05.880' AS DateTime), N'/Upload/News/e88af883-cb49-4c1b-adf0-fb839782be9a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (693, 1, 55, 2, N'administrator', CAST(N'2017-06-06T10:39:32.203' AS DateTime), N'/Upload/News/0692cf4a-fd3f-43dc-8f59-f721e09099e3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (694, 1, 56, 2, N'administrator', CAST(N'2017-06-06T11:32:27.617' AS DateTime), N'/Upload/News/7dd23f57-eec7-41dc-a17b-e7934442b1f7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (695, 1, 57, 2, N'administrator', CAST(N'2017-06-06T11:37:28.600' AS DateTime), N'/Upload/News/1c8e68e0-dc89-4904-bf71-c5c781a4bf0c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (696, 1, 58, 2, N'administrator', CAST(N'2017-06-06T11:41:06.353' AS DateTime), N'/Upload/News/afa848d8-9885-4c6f-a82a-0cc57d74b44f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (697, 7, 0, 4, N'administrator', CAST(N'2017-06-16T18:31:34.927' AS DateTime), N'/Upload/SlideShow/21b29178-323b-40d7-8684-c1988f95aef2/trangchu_image1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (698, 1, 59, 2, N'administrator', CAST(N'2017-06-26T16:26:58.140' AS DateTime), N'/Upload/News/c829976d-aa3e-4844-8c89-0ec37030d043')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (699, 1, 60, 2, N'administrator', CAST(N'2017-06-26T18:45:51.130' AS DateTime), N'/Upload/News/0ecf8b9e-6130-4183-946f-4fec92cb9705')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (700, 1, 61, 2, N'administrator', CAST(N'2017-06-27T10:13:33.847' AS DateTime), N'/Upload/News/d91f25ac-a81b-4817-a262-bc42f94a1b59')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (701, 1, 62, 2, N'administrator', CAST(N'2017-06-27T10:16:44.233' AS DateTime), N'/Upload/News/cfc95978-0cd0-45eb-85f9-0bd585aa4280')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (702, 1, 63, 2, N'administrator', CAST(N'2017-06-27T10:19:23.050' AS DateTime), N'/Upload/News/ee0865a5-069e-46ef-9f54-45ffe79f6b60')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (703, 1, 64, 2, N'administrator', CAST(N'2017-06-27T10:21:56.223' AS DateTime), N'/Upload/News/682eba3b-d9e5-4ac3-8b5e-d790866d5c65')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (704, 1, 65, 2, N'administrator', CAST(N'2017-06-27T10:29:16.730' AS DateTime), N'/Upload/News/ffef1998-610b-4e3e-bcbf-b2eb0ff9a0d2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (705, 1, 66, 2, N'administrator', CAST(N'2017-06-27T10:31:00.743' AS DateTime), N'/Upload/News/9fed043c-c444-4b2b-bfe4-9108494ece35')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (706, 1, 67, 2, N'administrator', CAST(N'2017-06-27T10:40:26.997' AS DateTime), N'/Upload/News/483e2e5c-aca5-4433-8b35-cc3f34b0fd35')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (707, 1, 68, 2, N'administrator', CAST(N'2017-06-27T10:46:30.893' AS DateTime), N'/Upload/News/e9ff91fb-d012-4b4c-9faa-6ba3202e3d1a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (708, 1, 69, 2, N'administrator', CAST(N'2017-07-06T15:13:51.067' AS DateTime), N'/Upload/News/71ef0f73-78fa-473d-84b8-8b009d40fa1f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (709, 1, 70, 2, N'administrator', CAST(N'2017-07-10T09:24:36.940' AS DateTime), N'/Upload/News/47da58f1-9972-434a-b29b-118685bda679')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (710, 1, 71, 2, N'administrator', CAST(N'2017-07-18T16:24:05.773' AS DateTime), N'/Upload/News/a1dcf80f-e241-445e-b697-f142643a1242')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (711, 1, 72, 2, N'administrator', CAST(N'2017-07-18T16:25:46.133' AS DateTime), N'/Upload/News/81c25f3f-cdca-463b-ab4f-fce38dd4458d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (712, 1, 73, 2, N'administrator', CAST(N'2017-07-18T16:28:18.633' AS DateTime), N'/Upload/News/997f4467-9f93-4803-a641-556d0d43a40f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (713, 1, 74, 2, N'administrator', CAST(N'2017-07-18T16:33:37.577' AS DateTime), N'/Upload/News/ed3f0012-6221-4cd0-9b3a-033eee19a6c1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (714, 1, 75, 2, N'administrator', CAST(N'2017-07-18T16:35:07.263' AS DateTime), N'/Upload/News/fb97dc7a-904d-41a0-947f-cd6b2a1b3fc2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (715, 1, 76, 2, N'administrator', CAST(N'2017-07-18T16:36:58.903' AS DateTime), N'/Upload/News/c43a9725-65b7-47f2-9b26-9bb8e390ba20')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (716, 1, 77, 2, N'administrator', CAST(N'2017-07-18T16:38:58.937' AS DateTime), N'/Upload/News/9547c1a0-33fe-457d-8a97-dfedee38544f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (717, 1, 78, 2, N'administrator', CAST(N'2017-07-18T16:40:28.877' AS DateTime), N'/Upload/News/a0fd9fba-f5e6-43d4-8fd3-7b31452c202a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (718, 1, 79, 2, N'administrator', CAST(N'2017-07-18T16:42:08.833' AS DateTime), N'/Upload/News/a7b03ab4-80aa-418e-b773-9a634d599cc6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (719, 1, 80, 2, N'administrator', CAST(N'2017-07-18T16:43:25.270' AS DateTime), N'/Upload/News/429da6a9-b258-4845-a8ea-396d4b6c9d70')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (720, 1, 81, 2, N'administrator', CAST(N'2017-07-24T09:38:29.487' AS DateTime), N'/Upload/News/0ca49d82-ca37-494f-8ba1-c3716b77b667')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (721, 1, 82, 2, N'administrator', CAST(N'2017-08-23T09:27:37.743' AS DateTime), N'/Upload/News/6959e8b8-9045-4741-b84e-2f6ebbeceeaa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (722, 1, 0, 1, N'administrator', CAST(N'2017-08-23T09:30:29.930' AS DateTime), N'/Upload/News/354b562f-e2e8-4eea-8126-390c4022c978')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (723, 1, 83, 2, N'administrator', CAST(N'2017-08-23T09:33:04.777' AS DateTime), N'/Upload/News/111bf3cb-adce-4340-9113-32122c59c5e7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (724, 1, 84, 2, N'administrator', CAST(N'2017-08-23T09:37:53.920' AS DateTime), N'/Upload/News/a0ddb113-c477-47d9-8157-6155ee6acbd9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (725, 1, 85, 2, N'administrator', CAST(N'2017-08-23T09:39:32.217' AS DateTime), N'/Upload/News/e6f69217-2e87-4de8-9229-c636aedaae2d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (726, 1, 86, 2, N'administrator', CAST(N'2017-08-23T09:40:55.403' AS DateTime), N'/Upload/News/5624adc6-0794-4cd6-be6f-71d56646540f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (727, 1, 87, 2, N'administrator', CAST(N'2017-09-07T10:45:16.217' AS DateTime), N'/Upload/News/9e8c4dc6-4c77-4d4e-9083-774c29f96e7b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (728, 1, 88, 2, N'administrator', CAST(N'2017-09-08T10:34:30.527' AS DateTime), N'/Upload/News/3b24cf57-6121-4ecf-9bcb-3f892ec9a003')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (729, 1, 89, 2, N'administrator', CAST(N'2017-09-12T14:00:57.873' AS DateTime), N'/Upload/News/49e33aee-1865-4f29-a9cb-905e41dc9cd2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (730, 1, 90, 2, N'administrator', CAST(N'2017-09-13T11:54:58.080' AS DateTime), N'/Upload/News/2bcbe349-4988-4a9a-a515-499acb6eb589')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (731, 1, 91, 2, N'administrator', CAST(N'2017-09-26T18:06:37.073' AS DateTime), N'/Upload/News/ea62de23-6cf4-4929-a2fa-7e4d241ca5e3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (732, 1, 92, 2, N'administrator', CAST(N'2017-10-31T15:19:54.237' AS DateTime), N'/Upload/News/ddf7f63b-694e-4806-8936-f30ef0d9f9ba')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (733, 1, 93, 2, N'administrator', CAST(N'2017-11-02T14:38:07.797' AS DateTime), N'/Upload/News/d7fa469a-69f1-4ee9-9189-2ed0cda810aa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (734, 1, 94, 2, N'administrator', CAST(N'2017-11-03T21:41:24.787' AS DateTime), N'/Upload/News/183cc4fe-5225-441f-92cd-e8268a8c4e48')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (735, 1, 95, 2, N'administrator', CAST(N'2017-11-13T09:40:27.013' AS DateTime), N'/Upload/News/79c541af-a69f-44e4-a10e-b08e69a466a7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (736, 1, 0, 1, N'administrator', CAST(N'2017-11-29T11:17:08.307' AS DateTime), N'/Upload/News/8d314364-6063-498f-a4dc-ac24278c5b2d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (737, 1, 96, 2, N'administrator', CAST(N'2017-11-29T11:20:57.900' AS DateTime), N'/Upload/News/cfb9e0d7-ce29-4b0e-9acd-fb823c86c090')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (738, 1, 0, 1, N'administrator', CAST(N'2017-11-29T11:24:33.213' AS DateTime), N'/Upload/News/5e34fdbc-8ff1-4868-b07c-ea15e79bd97a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (739, 1, 0, 1, N'administrator', CAST(N'2017-11-29T11:36:51.700' AS DateTime), N'/Upload/News/84a99324-3d54-4b78-9769-e94cf550d599')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (740, 1, 0, 1, N'administrator', CAST(N'2017-11-29T11:49:40.160' AS DateTime), N'/Upload/News/44ad702b-bc23-4033-a6a2-e9882c2f757d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (741, 1, 0, 1, N'administrator', CAST(N'2017-11-29T12:00:17.057' AS DateTime), N'/Upload/News/d582a80a-64b3-4777-8560-327334584f62')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (742, 1, 0, 1, N'administrator', CAST(N'2017-11-29T12:02:16.357' AS DateTime), N'/Upload/News/6aecab4e-0c64-4b67-8cdd-3a5eae37b200')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (743, 1, 0, 1, N'administrator', CAST(N'2017-11-29T12:10:47.673' AS DateTime), N'/Upload/News/7d4a6667-a635-4729-854c-52030cc836cc')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (744, 1, 0, 1, N'administrator', CAST(N'2017-11-29T12:11:39.830' AS DateTime), N'/Upload/News/670da8d8-692e-4ac0-91fc-ee19840863d4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (745, 1, 97, 2, N'administrator', CAST(N'2018-01-16T10:27:53.227' AS DateTime), N'/Upload/News/f8b866ae-00ec-487e-bf19-f5eaac13a76d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (746, 1, 98, 2, N'administrator', CAST(N'2018-01-16T10:46:04.613' AS DateTime), N'/Upload/News/658d563b-5c4b-4f0e-a679-853dbaaf4238')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (747, 1, 99, 2, N'administrator', CAST(N'2018-02-09T10:51:22.487' AS DateTime), N'/Upload/News/f7227b5f-793b-405a-9c67-cfecad45e8f8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (748, 1, 100, 2, N'administrator', CAST(N'2018-02-23T14:21:58.313' AS DateTime), N'/Upload/News/0ba0fc15-aba4-4962-974a-9aad41896678')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (749, 7, 0, 4, N'administrator', CAST(N'2018-02-27T18:22:36.753' AS DateTime), N'/Upload/HotActivity/2d1e8dcb-6280-4408-bd43-8415cc4bb9cc/motul-racing.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (750, 7, 0, 4, N'administrator', CAST(N'2018-02-27T18:22:38.737' AS DateTime), N'/Upload/HotActivity/2d1e8dcb-6280-4408-bd43-8415cc4bb9cc/trangchu_image2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (751, 7, 0, 4, N'administrator', CAST(N'2018-03-01T11:15:07.103' AS DateTime), N'/Upload/HotActivity/6fe5f05d-2b0f-46b8-909b-e8586e99cdca/Thaynhotsom')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (752, 7, 0, 4, N'administrator', CAST(N'2018-03-01T11:15:36.040' AS DateTime), N'/Upload/HotActivity/6fe5f05d-2b0f-46b8-909b-e8586e99cdca/Thaynhotsom')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (753, 1, 0, 1, N'administrator', CAST(N'2018-05-18T14:12:27.417' AS DateTime), N'/Upload/News/539d8a5e-9f0d-4bef-a54f-cdadd1d37478')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (754, 1, 0, 1, N'administrator', CAST(N'2018-05-18T14:31:07.097' AS DateTime), N'/Upload/News/c62f9fe7-be77-426e-bc5b-46afdd47a18d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (755, 1, 101, 2, N'administrator', CAST(N'2018-05-18T14:31:30.190' AS DateTime), N'/Upload/News/2e68ccec-90ad-455e-9e4c-35d6db53342b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (756, 1, 0, 1, N'administrator', CAST(N'2018-05-18T15:29:08.460' AS DateTime), N'/Upload/News/a2631fde-b266-4b47-acd1-b1100fb53513')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (757, 1, 0, 1, N'administrator', CAST(N'2018-05-18T15:41:21.920' AS DateTime), N'/Upload/News/5832e217-25aa-46cf-8f6c-a655ea6be927')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (758, 1, 0, 1, N'administrator', CAST(N'2018-05-18T15:42:04.590' AS DateTime), N'/Upload/News/94cae489-12fe-4ce4-bfab-3cd2b214635a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (759, 1, 0, 1, N'administrator', CAST(N'2018-05-18T15:56:44.703' AS DateTime), N'/Upload/News/f7cee69a-9e3f-4b72-821c-4b74d2ed55ea')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (760, 1, 0, 1, N'administrator', CAST(N'2018-05-18T16:18:00.967' AS DateTime), N'/Upload/News/2ff97e44-7791-4ebd-8a6a-a13e5aa20cbe')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (761, 1, 0, 1, N'administrator', CAST(N'2018-05-18T16:19:28.030' AS DateTime), N'/Upload/News/53a8cb1d-51a5-4c7d-b936-2158d21b44ff')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (762, 1, 102, 2, N'administrator', CAST(N'2018-05-18T16:34:30.107' AS DateTime), N'/Upload/News/13aeb1b8-65f1-457d-898c-05071c4fcde0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (763, 7, 0, 4, N'administrator', CAST(N'2018-05-29T09:27:41.300' AS DateTime), N'/Upload/SlideShow/64451c20-fc90-412e-8fbf-4afd597d28aa/mobile.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (764, 7, 0, 4, N'administrator', CAST(N'2018-05-29T09:27:42.080' AS DateTime), N'/Upload/SlideShow/64451c20-fc90-412e-8fbf-4afd597d28aa/pc.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (765, 7, 0, 4, N'administrator', CAST(N'2018-05-29T09:27:42.927' AS DateTime), N'/Upload/SlideShow/64451c20-fc90-412e-8fbf-4afd597d28aa/slide 3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (766, 1, 0, 1, N'administrator', CAST(N'2018-05-29T10:05:57.603' AS DateTime), N'/Upload/News/ec103369-3bfc-41b6-9103-c3fedd54a45d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (767, 7, 0, 4, N'administrator', CAST(N'2018-05-29T10:15:59.637' AS DateTime), N'/Upload/News/ec103369-3bfc-41b6-9103-c3fedd54a45d/motulstuntfest2018.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (768, 7, 0, 4, N'administrator', CAST(N'2018-05-29T10:16:00.357' AS DateTime), N'/Upload/News/ec103369-3bfc-41b6-9103-c3fedd54a45d/motulstuntfest2018_opt.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (769, 1, 103, 2, N'administrator', CAST(N'2018-05-29T10:18:54.217' AS DateTime), N'/Upload/News/a7c87338-b6e9-48ac-9135-c0d562926085')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (770, 7, 0, 4, N'administrator', CAST(N'2018-05-29T10:19:32.437' AS DateTime), N'/Upload/News/a7c87338-b6e9-48ac-9135-c0d562926085/motulstuntfest2018_opt (1).png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (771, 7, 0, 4, N'administrator', CAST(N'2018-07-03T23:01:30.440' AS DateTime), N'/Upload/SlideShow/28bb653a-240a-4222-95b5-dbca23b65067/bien-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (772, 7, 0, 4, N'administrator', CAST(N'2018-07-03T23:01:33.470' AS DateTime), N'/Upload/SlideShow/28bb653a-240a-4222-95b5-dbca23b65067/bien.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (773, 7, 0, 4, N'administrator', CAST(N'2018-07-03T23:01:34.770' AS DateTime), N'/Upload/SlideShow/28bb653a-240a-4222-95b5-dbca23b65067/bienresult.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (774, 1, 1, 2, N'administrator', CAST(N'2018-07-04T00:52:30.560' AS DateTime), N'/Upload/News/97b11959-4cad-44a0-9c54-a5a3911fadd3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (775, 1, 0, 1, N'Administrator', CAST(N'2018-07-13T15:48:06.913' AS DateTime), N'/Upload/News/b7841713-a17f-462f-a504-e6806ae8ad8e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (776, 1, 0, 1, N'Administrator', CAST(N'2018-07-13T15:49:21.913' AS DateTime), N'/Upload/News/b5de6644-96b7-4ae2-bc12-383003f2af2d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (777, 1, 0, 1, N'Administrator', CAST(N'2018-07-13T15:55:35.100' AS DateTime), N'/Upload/News/fa5c044f-1302-47cd-9eba-2e657ab7b56f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (778, 1, 0, 1, N'Administrator', CAST(N'2018-07-13T16:01:09.220' AS DateTime), N'/Upload/News/b34b1321-25f2-45bb-ace1-6621d0047e4e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (779, 1, 0, 1, N'Administrator', CAST(N'2018-07-13T16:02:33.173' AS DateTime), N'/Upload/News/153d55ed-ccb3-4fc1-ae78-841658ad485c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (780, 1, 2, 2, N'Administrator', CAST(N'2018-07-13T16:03:06.450' AS DateTime), N'/Upload/News/eebfb93e-83e9-44fe-acab-0477714184aa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (781, 1, 0, 1, N'Administrator', CAST(N'2018-07-13T16:08:23.023' AS DateTime), N'/Upload/News/ec4761a2-c45d-4a22-bfdd-ec53af197d87')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (782, 1, 3, 2, N'Administrator', CAST(N'2018-07-13T16:14:41.737' AS DateTime), N'/Upload/News/bda817a3-f733-44f2-af2c-60be2ee3dc0b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (783, 1, 4, 2, N'Administrator', CAST(N'2018-07-13T16:15:40.830' AS DateTime), N'/Upload/News/bbb154db-59bc-480f-8344-f7c68bab26a8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (784, 1, 5, 2, N'Administrator', CAST(N'2018-07-13T16:16:26.703' AS DateTime), N'/Upload/News/77baa9a9-5d67-4aea-b84b-4c8ccbcecae6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (785, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:53:01.597' AS DateTime), N'/Upload/Products/01843706-bb63-451d-bfbd-69cefa58b472/item-pd1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (786, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:53:58.677' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/item-pd2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (787, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:54:19.503' AS DateTime), N'/Upload/Products/95e5492f-8f93-4f5a-94e3-77d0c94ab7b2/item-pd4.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (788, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:54:59.377' AS DateTime), N'/Upload/Products/049e6dbb-8b6f-4606-b116-be3ad27b4c45/item-pd5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (789, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:55:23.830' AS DateTime), N'/Upload/Products/fae439d5-2892-4a01-a210-c20c18984f69/item-pd6.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (790, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:55:57.660' AS DateTime), N'/Upload/Products/2b80ca02-a071-4a72-ba37-7f90e3febe21/item-pd7.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (791, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:56:19.940' AS DateTime), N'/Upload/Products/571dfa17-9f64-4e0b-9b5d-3f07448ba6c5/item-pd8.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (792, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:56:39.160' AS DateTime), N'/Upload/Products/ce556e5f-90ac-4253-9fe4-dabe1cfc41fe/item-pd12.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (793, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:57:02.377' AS DateTime), N'/Upload/Products/31254d21-5c95-4f3c-b474-47c27c3d4cfc/item-pd10.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (794, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:57:22.627' AS DateTime), N'/Upload/Products/7207233b-44f0-4fe6-a5f6-d346d149aa64/item-pd9.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (795, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:58:28.707' AS DateTime), N'/Upload/Products/1734fec9-6b1b-4418-a403-3c32e916e6cf/item-pd13.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (796, 7, 0, 4, N'administrator', CAST(N'2018-07-15T14:58:59.160' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/item-pd2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (797, 7, 0, 4, N'administrator', CAST(N'2018-07-15T15:00:16.350' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/item-pd2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (798, 7, 0, 4, N'administrator', CAST(N'2018-07-15T15:01:11.710' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/item-pd2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (799, 7, 0, 4, N'administrator', CAST(N'2018-07-15T15:01:56.807' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/item-pd2.png')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (800, 7, 0, 4, N'administrator', CAST(N'2018-07-15T15:02:54.663' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/item-pd2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (801, 7, 0, 4, N'administrator', CAST(N'2018-07-15T15:08:33.763' AS DateTime), N'/Upload/Products/1734fec9-6b1b-4418-a403-3c32e916e6cf/item-pd13.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (802, 7, 0, 4, N'administrator', CAST(N'2018-07-15T15:09:24.060' AS DateTime), N'/Upload/Products/1734fec9-6b1b-4418-a403-3c32e916e6cf/item-pd5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (803, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:08:37.067' AS DateTime), N'/Upload/Products/01843706-bb63-451d-bfbd-69cefa58b472/A7.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (804, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:12:40.893' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (805, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:12:47.237' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (806, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:13:35.863' AS DateTime), N'/Upload/Products/95e5492f-8f93-4f5a-94e3-77d0c94ab7b2/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (807, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:13:37.177' AS DateTime), N'/Upload/Products/95e5492f-8f93-4f5a-94e3-77d0c94ab7b2/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (808, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:14:18.397' AS DateTime), N'/Upload/Products/049e6dbb-8b6f-4606-b116-be3ad27b4c45/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (809, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:14:19.520' AS DateTime), N'/Upload/Products/049e6dbb-8b6f-4606-b116-be3ad27b4c45/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (810, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:18:13.860' AS DateTime), N'/Upload/Products/fae439d5-2892-4a01-a210-c20c18984f69/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (811, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:18:14.970' AS DateTime), N'/Upload/Products/fae439d5-2892-4a01-a210-c20c18984f69/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (812, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:18:44.940' AS DateTime), N'/Upload/Products/2b80ca02-a071-4a72-ba37-7f90e3febe21/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (813, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:18:46.767' AS DateTime), N'/Upload/Products/2b80ca02-a071-4a72-ba37-7f90e3febe21/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (814, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:19:13.783' AS DateTime), N'/Upload/Products/571dfa17-9f64-4e0b-9b5d-3f07448ba6c5/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (815, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:19:15.127' AS DateTime), N'/Upload/Products/571dfa17-9f64-4e0b-9b5d-3f07448ba6c5/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (816, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:19:49.033' AS DateTime), N'/Upload/Products/ce556e5f-90ac-4253-9fe4-dabe1cfc41fe/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (817, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:19:50.520' AS DateTime), N'/Upload/Products/ce556e5f-90ac-4253-9fe4-dabe1cfc41fe/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (818, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:21:39.223' AS DateTime), N'/Upload/Products/31254d21-5c95-4f3c-b474-47c27c3d4cfc/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (819, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:21:40.753' AS DateTime), N'/Upload/Products/31254d21-5c95-4f3c-b474-47c27c3d4cfc/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (820, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:22:34.333' AS DateTime), N'/Upload/Products/7207233b-44f0-4fe6-a5f6-d346d149aa64/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (821, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:22:35.723' AS DateTime), N'/Upload/Products/7207233b-44f0-4fe6-a5f6-d346d149aa64/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (822, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:23:19.037' AS DateTime), N'/Upload/Products/fc9c4872-1372-4180-a33b-4928fc104095/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (823, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:23:20.300' AS DateTime), N'/Upload/Products/fc9c4872-1372-4180-a33b-4928fc104095/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (824, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:24:06.457' AS DateTime), N'/Upload/Products/1734fec9-6b1b-4418-a403-3c32e916e6cf/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (825, 7, 0, 4, N'administrator', CAST(N'2018-07-15T20:24:07.800' AS DateTime), N'/Upload/Products/1734fec9-6b1b-4418-a403-3c32e916e6cf/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (826, 7, 0, 4, N'administrator', CAST(N'2018-07-16T15:06:49.137' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/9.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (827, 7, 0, 4, N'administrator', CAST(N'2018-07-16T15:07:12.730' AS DateTime), N'/Upload/Products/80b1a507-c637-4009-bf83-f9dc9cd946f7/E5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (828, 7, 0, 4, N'administrator', CAST(N'2018-07-16T15:08:34.640' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/E5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (829, 7, 0, 4, N'administrator', CAST(N'2018-07-16T15:44:22.933' AS DateTime), N'/Upload/Products/01843706-bb63-451d-bfbd-69cefa58b472/item-pd1-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (830, 7, 0, 4, N'administrator', CAST(N'2018-07-16T15:48:29.277' AS DateTime), N'/Upload/Products/fc9c4872-1372-4180-a33b-4928fc104095/Helios-501.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (831, 7, 0, 4, N'administrator', CAST(N'2018-07-16T15:48:30.857' AS DateTime), N'/Upload/Products/fc9c4872-1372-4180-a33b-4928fc104095/Helios-500.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (832, 7, 0, 4, N'administrator', CAST(N'2018-07-16T15:49:31.340' AS DateTime), N'/Upload/Products/049e6dbb-8b6f-4606-b116-be3ad27b4c45/item-pd5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (833, 7, 0, 4, N'administrator', CAST(N'2018-07-16T17:23:27.480' AS DateTime), N'/Upload/Products/01843706-bb63-451d-bfbd-69cefa58b472/A3.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (834, 7, 0, 4, N'administrator', CAST(N'2018-07-16T17:23:30.717' AS DateTime), N'/Upload/Products/01843706-bb63-451d-bfbd-69cefa58b472/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (835, 7, 0, 4, N'administrator', CAST(N'2018-07-16T17:23:32.997' AS DateTime), N'/Upload/Products/01843706-bb63-451d-bfbd-69cefa58b472/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (836, 7, 0, 4, N'administrator', CAST(N'2018-07-16T17:23:51.560' AS DateTime), N'/Upload/Products/01843706-bb63-451d-bfbd-69cefa58b472/9.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (837, 7, 0, 4, N'administrator', CAST(N'2018-07-16T18:00:24.163' AS DateTime), N'/Upload/Products/01843706-bb63-451d-bfbd-69cefa58b472/A3 (1).jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (838, 7, 0, 4, N'administrator', CAST(N'2018-07-16T18:01:33.883' AS DateTime), N'/Upload/Products/7207233b-44f0-4fe6-a5f6-d346d149aa64/Helios-300.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (839, 7, 0, 4, N'administrator', CAST(N'2018-07-16T18:01:37.397' AS DateTime), N'/Upload/Products/7207233b-44f0-4fe6-a5f6-d346d149aa64/item-pd9.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (840, 7, 0, 4, N'administrator', CAST(N'2018-07-16T18:04:13.680' AS DateTime), N'/Upload/Products/fc9c4872-1372-4180-a33b-4928fc104095/Helios-500-mobile.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (841, 7, 0, 4, N'administrator', CAST(N'2018-07-16T18:04:14.947' AS DateTime), N'/Upload/Products/fc9c4872-1372-4180-a33b-4928fc104095/Helios-500.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (842, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:17:10.607' AS DateTime), N'/Upload/News/77baa9a9-5d67-4aea-b84b-4c8ccbcecae6/item-new-detail.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (843, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:17:48.517' AS DateTime), N'/Upload/News/bbb154db-59bc-480f-8344-f7c68bab26a8/acer-700x475.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (844, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:17:49.703' AS DateTime), N'/Upload/News/bbb154db-59bc-480f-8344-f7c68bab26a8/item-new-detail.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (845, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:18:09.733' AS DateTime), N'/Upload/News/bda817a3-f733-44f2-af2c-60be2ee3dc0b/acer-700x475.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (846, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:18:10.937' AS DateTime), N'/Upload/News/bda817a3-f733-44f2-af2c-60be2ee3dc0b/item-new-detail.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (847, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:18:39.407' AS DateTime), N'/Upload/News/eebfb93e-83e9-44fe-acab-0477714184aa/item-new-detail.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (848, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:18:40.767' AS DateTime), N'/Upload/News/eebfb93e-83e9-44fe-acab-0477714184aa/item-new.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (849, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:23:32.617' AS DateTime), N'/Upload/News/eebfb93e-83e9-44fe-acab-0477714184aa/acer-700x475.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (850, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:24:13.867' AS DateTime), N'/Upload/News/bbb154db-59bc-480f-8344-f7c68bab26a8/Thumb bai E5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (851, 7, 0, 4, N'administrator', CAST(N'2018-07-17T16:24:34.333' AS DateTime), N'/Upload/News/77baa9a9-5d67-4aea-b84b-4c8ccbcecae6/acer-700x475.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (852, 7, 0, 4, N'administrator', CAST(N'2018-07-18T11:21:42.700' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/item-pd2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (853, 7, 0, 4, N'administrator', CAST(N'2018-07-18T11:22:05.560' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/item-pd2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (854, 7, 0, 4, N'administrator', CAST(N'2018-07-18T11:22:58.420' AS DateTime), N'/Upload/Products/80b1a507-c637-4009-bf83-f9dc9cd946f7/itemrs.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (855, 7, 0, 4, N'administrator', CAST(N'2018-07-18T11:23:00.340' AS DateTime), N'/Upload/Products/80b1a507-c637-4009-bf83-f9dc9cd946f7/rsrelation.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (856, 7, 0, 4, N'administrator', CAST(N'2018-07-18T11:23:03.340' AS DateTime), N'/Upload/Products/80b1a507-c637-4009-bf83-f9dc9cd946f7/item-pd3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (857, 7, 0, 4, N'administrator', CAST(N'2018-07-18T11:23:43.777' AS DateTime), N'/Upload/Products/80b1a507-c637-4009-bf83-f9dc9cd946f7/item-pd3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (858, 7, 0, 4, N'administrator', CAST(N'2018-07-18T14:11:16.393' AS DateTime), N'/Upload/Products/80b1a507-c637-4009-bf83-f9dc9cd946f7/E5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (859, 7, 0, 4, N'administrator', CAST(N'2018-07-18T14:11:43.313' AS DateTime), N'/Upload/Products/519b92e0-324a-44b9-a2c7-8e8c55caf4a0/e5-gen-8.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (860, 7, 0, 4, N'administrator', CAST(N'2018-07-18T15:40:22.513' AS DateTime), N'/Upload/News/77baa9a9-5d67-4aea-b84b-4c8ccbcecae6/acervietnam-tintuc-swift5-3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (861, 7, 0, 4, N'administrator', CAST(N'2018-07-18T15:40:23.060' AS DateTime), N'/Upload/News/77baa9a9-5d67-4aea-b84b-4c8ccbcecae6/acer-700x475.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (862, 7, 0, 4, N'administrator', CAST(N'2018-07-18T18:33:48.733' AS DateTime), N'/Upload/Products/83209e6f-75ae-4bca-937e-f3faedd60fc5/SW3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (863, 1, 6, 2, N'administrator', CAST(N'2018-07-19T11:15:26.600' AS DateTime), N'/Upload/News/829e8e19-7c64-412b-acee-f5a5144e17a7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (864, 7, 0, 4, N'administrator', CAST(N'2018-07-19T11:28:01.617' AS DateTime), N'/Upload/News/829e8e19-7c64-412b-acee-f5a5144e17a7/Untitled-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (865, 7, 0, 4, N'administrator', CAST(N'2018-07-20T11:38:09.407' AS DateTime), N'/Upload/News/eebfb93e-83e9-44fe-acab-0477714184aa/acer-700x4751.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (866, 7, 0, 4, N'administrator', CAST(N'2018-07-20T11:39:18.673' AS DateTime), N'/Upload/News/eebfb93e-83e9-44fe-acab-0477714184aa/acer-700x4751.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (867, 7, 0, 4, N'administrator', CAST(N'2018-07-20T11:43:58.940' AS DateTime), N'/Upload/News/eebfb93e-83e9-44fe-acab-0477714184aa/acer-700x4751.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (868, 1, 0, 1, N'Administrator', CAST(N'2018-07-23T18:29:32.920' AS DateTime), N'/Upload/News/e1b9a582-256c-46c2-b12f-b5b5c2841930')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (869, 1, 7, 2, N'Administrator', CAST(N'2018-07-24T11:28:09.703' AS DateTime), N'/Upload/News/fd5499aa-d8df-401b-9a31-3b1132f3ecd4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (870, 1, 8, 2, N'Administrator', CAST(N'2018-07-24T11:32:43.520' AS DateTime), N'/Upload/News/77deaea1-5429-41fc-add3-552a2c03427d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (871, 1, 9, 2, N'Administrator', CAST(N'2018-07-24T11:33:42.950' AS DateTime), N'/Upload/News/adc72208-40d6-4ac8-bf8f-7f648f923d1f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (872, 1, 10, 2, N'Administrator', CAST(N'2018-07-24T11:37:00.483' AS DateTime), N'/Upload/News/e2263e2b-f877-45eb-ba8b-e3f3025a5e18')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (873, 1, 11, 2, N'Administrator', CAST(N'2018-07-24T11:38:01.443' AS DateTime), N'/Upload/News/164e6546-b8e3-4f31-a334-af7cf15f5940')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (874, 1, 12, 2, N'Administrator', CAST(N'2018-07-24T11:38:57.643' AS DateTime), N'/Upload/News/fd25caca-e0c2-4fef-93ce-7718a66c0a6e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (875, 1, 13, 2, N'Administrator', CAST(N'2018-07-24T11:39:30.247' AS DateTime), N'/Upload/News/23553977-41cc-4373-bce4-168c82408f9c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (876, 1, 14, 2, N'Administrator', CAST(N'2018-07-24T11:40:00.597' AS DateTime), N'/Upload/News/7ce7878b-7314-4fc0-b59d-99f14595fbe4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (877, 1, 15, 2, N'Administrator', CAST(N'2018-07-24T11:40:26.393' AS DateTime), N'/Upload/News/7bb3d1d5-a1f6-40a6-975b-afa3da86b4ae')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (878, 1, 0, 1, N'Administrator', CAST(N'2018-07-24T11:41:02.820' AS DateTime), N'/Upload/News/0ae2c8c4-97c1-4ce8-8e9c-c0a6584a513f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (879, 1, 16, 2, N'Administrator', CAST(N'2018-07-24T12:11:19.943' AS DateTime), N'/Upload/News/4197fbf2-7070-47dc-a794-90d8c2859018')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (880, 7, 0, 4, N'Administrator', CAST(N'2018-08-16T17:22:52.283' AS DateTime), N'/Upload/Categoties/9c675ef2-1c65-48f3-ac3b-9a7931107283/item2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (881, 7, 0, 4, N'Administrator', CAST(N'2018-08-16T17:23:02.360' AS DateTime), N'/Upload/Categoties/3091dbc8-f16c-41fa-9761-d6a34142c27f/item2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (882, 7, 0, 4, N'Administrator', CAST(N'2018-08-16T17:23:14.807' AS DateTime), N'/Upload/Categoties/69a7f81a-f6ab-472f-84f6-bc26e86c2c7f/item2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (883, 7, 0, 4, N'Administrator', CAST(N'2018-08-16T17:23:28.143' AS DateTime), N'/Upload/Categoties/35710af7-e9c1-49bc-be69-e6a457acecf1/item2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (884, 7, 0, 4, N'Administrator', CAST(N'2018-08-16T17:57:57.647' AS DateTime), N'/Upload/News/fd5499aa-d8df-401b-9a31-3b1132f3ecd4/itemKM1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (885, 7, 0, 4, N'Administrator', CAST(N'2018-08-16T17:58:22.833' AS DateTime), N'/Upload/News/77deaea1-5429-41fc-add3-552a2c03427d/itemKM2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (886, 7, 0, 4, N'Administrator', CAST(N'2018-08-16T17:58:48.430' AS DateTime), N'/Upload/News/adc72208-40d6-4ac8-bf8f-7f648f923d1f/itemKM1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (887, 1, 17, 2, N'Administrator', CAST(N'2018-08-16T18:08:10.693' AS DateTime), N'/Upload/News/00de78ab-8bd0-473b-8d3a-238803ddf8f8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (888, 1, 0, 1, N'Administrator', CAST(N'2018-08-17T11:14:48.670' AS DateTime), N'/Upload/News/75450f89-5ae0-41ba-af67-d39709efc4e4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (889, 1, 18, 2, N'Administrator', CAST(N'2018-08-17T11:25:30.807' AS DateTime), N'/Upload/News/4b5ba7a8-7678-4ef2-b719-c953a5f41090')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (890, 1, 19, 2, N'Administrator', CAST(N'2018-08-17T11:26:49.227' AS DateTime), N'/Upload/News/d8725d34-a58d-4bc9-bee4-42a40c7d7fb1')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (891, 1, 20, 2, N'Administrator', CAST(N'2018-08-17T11:27:18.510' AS DateTime), N'/Upload/News/45e16d7f-e73c-4474-ab62-e869af4a93ab')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (892, 1, 21, 2, N'Administrator', CAST(N'2018-08-17T11:34:26.257' AS DateTime), N'/Upload/News/b70d080c-31a1-49ae-879d-11026ea50730')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (893, 1, 0, 1, N'Administrator', CAST(N'2018-08-17T15:18:47.957' AS DateTime), N'/Upload/News/7c891c83-25d9-4dc9-8bf9-4d478a0db8da')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (894, 1, 22, 2, N'Administrator', CAST(N'2018-08-17T15:19:39.690' AS DateTime), N'/Upload/News/04316890-70bd-4a33-a1eb-217cb14e5ac9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (895, 1, 23, 2, N'Administrator', CAST(N'2018-08-17T15:20:22.877' AS DateTime), N'/Upload/News/9dbff176-2904-40be-8c31-8cbe49f854ec')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (896, 1, 24, 2, N'Administrator', CAST(N'2018-08-17T15:21:01.540' AS DateTime), N'/Upload/News/3df597d1-27c3-42b8-9719-918d3821a97e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (897, 1, 25, 2, N'Administrator', CAST(N'2018-08-17T15:21:35.937' AS DateTime), N'/Upload/News/2c402e46-25ea-4876-bceb-24d23a879394')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (898, 1, 0, 1, N'Administrator', CAST(N'2018-08-17T15:32:10.640' AS DateTime), N'/Upload/News/3d205eb4-fbe1-46df-89db-c88f9839da06')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (899, 1, 26, 2, N'Administrator', CAST(N'2018-08-23T10:21:23.730' AS DateTime), N'/Upload/News/5efce0f9-72a9-46c3-9899-609004934b12')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (900, 1, 27, 2, N'Administrator', CAST(N'2018-08-23T10:23:40.367' AS DateTime), N'/Upload/News/b9f4303b-7563-482e-972d-4ef30da5b535')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (901, 1, 28, 2, N'Administrator', CAST(N'2018-08-23T10:24:34.357' AS DateTime), N'/Upload/News/38ee278f-6d06-42b3-a62d-98fc2d8deb4f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (902, 1, 29, 2, N'Administrator', CAST(N'2018-08-23T10:25:00.720' AS DateTime), N'/Upload/News/55d1612f-6787-4e02-b205-0411d0202823')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (903, 1, 30, 2, N'Administrator', CAST(N'2018-08-23T10:25:26.903' AS DateTime), N'/Upload/News/2ee55e23-c826-4645-b88e-ad78b3bc507e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (904, 1, 31, 2, N'Administrator', CAST(N'2018-08-23T10:25:56.683' AS DateTime), N'/Upload/News/1d8b45d4-4347-4c4a-9fd8-66304ca4fd82')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (905, 7, 0, 4, N'administrator', CAST(N'2018-08-29T10:05:59.077' AS DateTime), N'/Upload/SlideShow/3eee6d0f-5132-4091-8b3a-8e1aef4ac9fc/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (906, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:18:26.890' AS DateTime), N'/Upload/SlideShow/40822a94-08bf-49e2-af50-988953a40114/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (907, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:18:52.547' AS DateTime), N'/Upload/SlideShow/0af41506-aa0a-4846-8366-423070dd57cc/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (908, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:19:12.483' AS DateTime), N'/Upload/SlideShow/3fd6d430-e03b-4d64-8015-ca184f40bcae/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (909, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:19:38.483' AS DateTime), N'/Upload/SlideShow/a325231b-6296-4fc4-b0d1-171828ea9b16/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (910, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:20:10.953' AS DateTime), N'/Upload/SlideShow/12bef142-962a-4c2b-bcb6-7d3e32ae1bab/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (911, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:25:46.640' AS DateTime), N'/Upload/SlideShow/25e2a68e-807e-45bf-8b88-ed122a56e1e2/Banner - 1250x445.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (912, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:26:34.997' AS DateTime), N'/Upload/SlideShow/12bef142-962a-4c2b-bcb6-7d3e32ae1bab/Banner - 1250x445.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (913, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:28:43.407' AS DateTime), N'/Upload/SlideShow/a325231b-6296-4fc4-b0d1-171828ea9b16/Banner - 1250x445.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (914, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:29:12.297' AS DateTime), N'/Upload/SlideShow/3fd6d430-e03b-4d64-8015-ca184f40bcae/Banner - 1250x445.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (915, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:29:34.827' AS DateTime), N'/Upload/SlideShow/40822a94-08bf-49e2-af50-988953a40114/Banner - 1250x445.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (916, 7, 0, 4, N'administrator', CAST(N'2018-08-29T18:29:59.937' AS DateTime), N'/Upload/SlideShow/0af41506-aa0a-4846-8366-423070dd57cc/Banner - 1250x445.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (917, 7, 0, 4, N'', CAST(N'2018-08-30T09:57:17.437' AS DateTime), N'/Upload/SlideShow/2e6c8e50-867c-421f-9882-96676a06e2b0/banner-tc.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (918, 7, 0, 4, N'administrator', CAST(N'2018-08-30T09:59:52.310' AS DateTime), N'/Upload/SlideShow/3eee6d0f-5132-4091-8b3a-8e1aef4ac9fc/Banner - 1250x445.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (919, 7, 0, 4, N'administrator', CAST(N'2018-08-30T10:25:41.157' AS DateTime), N'/Upload/News/adc72208-40d6-4ac8-bf8f-7f648f923d1f/item2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (920, 7, 0, 4, N'administrator', CAST(N'2018-08-30T10:26:43.593' AS DateTime), N'/Upload/News/77deaea1-5429-41fc-add3-552a2c03427d/item2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (921, 7, 0, 4, N'administrator', CAST(N'2018-08-30T10:27:11.013' AS DateTime), N'/Upload/News/fd5499aa-d8df-401b-9a31-3b1132f3ecd4/internetbanking.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (922, 7, 0, 4, N'administrator', CAST(N'2018-08-30T10:27:12.247' AS DateTime), N'/Upload/News/fd5499aa-d8df-401b-9a31-3b1132f3ecd4/item2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (923, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:04:28.843' AS DateTime), N'/Upload/SlideShow/2e6c8e50-867c-421f-9882-96676a06e2b0/Banner - 1250x445.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (924, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:04:31.780' AS DateTime), N'/Upload/SlideShow/2e6c8e50-867c-421f-9882-96676a06e2b0/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (925, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:10:14.827' AS DateTime), N'/Upload/SlideShow/40822a94-08bf-49e2-af50-988953a40114/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (926, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:10:16.530' AS DateTime), N'/Upload/SlideShow/40822a94-08bf-49e2-af50-988953a40114/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (927, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:11:08.577' AS DateTime), N'/Upload/SlideShow/0af41506-aa0a-4846-8366-423070dd57cc/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (928, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:11:09.497' AS DateTime), N'/Upload/SlideShow/0af41506-aa0a-4846-8366-423070dd57cc/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (929, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:11:49.513' AS DateTime), N'/Upload/SlideShow/3fd6d430-e03b-4d64-8015-ca184f40bcae/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (930, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:11:50.453' AS DateTime), N'/Upload/SlideShow/3fd6d430-e03b-4d64-8015-ca184f40bcae/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (931, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:14:15.327' AS DateTime), N'/Upload/SlideShow/a325231b-6296-4fc4-b0d1-171828ea9b16/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (932, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:14:16.077' AS DateTime), N'/Upload/SlideShow/a325231b-6296-4fc4-b0d1-171828ea9b16/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (933, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:15:01.560' AS DateTime), N'/Upload/SlideShow/12bef142-962a-4c2b-bcb6-7d3e32ae1bab/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (934, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:15:07.373' AS DateTime), N'/Upload/SlideShow/12bef142-962a-4c2b-bcb6-7d3e32ae1bab/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (935, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:15:09.657' AS DateTime), N'/Upload/SlideShow/12bef142-962a-4c2b-bcb6-7d3e32ae1bab/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (936, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:15:44.123' AS DateTime), N'/Upload/SlideShow/25e2a68e-807e-45bf-8b88-ed122a56e1e2/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (937, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:15:44.827' AS DateTime), N'/Upload/SlideShow/25e2a68e-807e-45bf-8b88-ed122a56e1e2/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (938, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:16:22.157' AS DateTime), N'/Upload/SlideShow/890a3f16-a8fe-40d6-828a-5ce8a9db3c7e/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (939, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:16:25.013' AS DateTime), N'/Upload/SlideShow/890a3f16-a8fe-40d6-828a-5ce8a9db3c7e/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (940, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:17:04.107' AS DateTime), N'/Upload/SlideShow/3e95bbca-1e09-4654-a939-872aee78e5ca/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (941, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:17:04.763' AS DateTime), N'/Upload/SlideShow/3e95bbca-1e09-4654-a939-872aee78e5ca/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (942, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:17:39.687' AS DateTime), N'/Upload/SlideShow/f232ce1b-19e1-49af-b63d-497a9c26c2be/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (943, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:17:40.357' AS DateTime), N'/Upload/SlideShow/f232ce1b-19e1-49af-b63d-497a9c26c2be/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (944, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:18:27.013' AS DateTime), N'/Upload/SlideShow/b0d67eb9-bee0-4a2e-b48b-c570fbcf8840/banner-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (945, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:18:27.873' AS DateTime), N'/Upload/SlideShow/b0d67eb9-bee0-4a2e-b48b-c570fbcf8840/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (946, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:50:25.577' AS DateTime), N'/Upload/SlideShow/2e6c8e50-867c-421f-9882-96676a06e2b0/Banner---1250x445.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (947, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:50:43.497' AS DateTime), N'/Upload/SlideShow/3eee6d0f-5132-4091-8b3a-8e1aef4ac9fc/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (948, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:50:45.077' AS DateTime), N'/Upload/SlideShow/3eee6d0f-5132-4091-8b3a-8e1aef4ac9fc/banner-tc.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (949, 7, 0, 4, N'administrator', CAST(N'2018-08-30T17:50:53.640' AS DateTime), N'/Upload/SlideShow/3eee6d0f-5132-4091-8b3a-8e1aef4ac9fc/Banner---1250x445.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (950, 1, 32, 2, N'administrator', CAST(N'2018-09-05T17:42:09.877' AS DateTime), N'/Upload/News/a94cf68c-1d2b-4c7a-9776-4b2e1bb67182')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (951, 7, 0, 4, N'administrator', CAST(N'2018-09-06T11:53:37.797' AS DateTime), N'/Upload/News/e2263e2b-f877-45eb-ba8b-e3f3025a5e18/itemKM3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (952, 7, 0, 4, N'administrator', CAST(N'2018-09-06T11:55:21.330' AS DateTime), N'/Upload/News/164e6546-b8e3-4f31-a334-af7cf15f5940/itemKM1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (953, 7, 0, 4, N'administrator', CAST(N'2018-09-06T15:13:01.720' AS DateTime), N'/Upload/News/b70d080c-31a1-49ae-879d-11026ea50730/itemKM1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (954, 7, 0, 4, N'administrator', CAST(N'2018-09-06T15:31:55.470' AS DateTime), N'/Upload/News/a94cf68c-1d2b-4c7a-9776-4b2e1bb67182/itemKM2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (955, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:44:19.890' AS DateTime), N'/Upload/SlideShow/40822a94-08bf-49e2-af50-988953a40114/Banner---1250x445.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (956, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:45:36.343' AS DateTime), N'/Upload/SlideShow/3fd6d430-e03b-4d64-8015-ca184f40bcae/Banner---1250x445.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (957, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:45:37.533' AS DateTime), N'/Upload/SlideShow/3fd6d430-e03b-4d64-8015-ca184f40bcae/Banner-mobile-900x1458.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (958, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:48:00.767' AS DateTime), N'/Upload/SlideShow/12bef142-962a-4c2b-bcb6-7d3e32ae1bab/Banner---1250x445.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (959, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:48:01.610' AS DateTime), N'/Upload/SlideShow/12bef142-962a-4c2b-bcb6-7d3e32ae1bab/Banner-mobile-900x1458.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (960, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:49:15.843' AS DateTime), N'/Upload/SlideShow/890a3f16-a8fe-40d6-828a-5ce8a9db3c7e/Banner---1250x445.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (961, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:49:16.500' AS DateTime), N'/Upload/SlideShow/890a3f16-a8fe-40d6-828a-5ce8a9db3c7e/Banner-mobile-900x1458.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (962, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:50:04.533' AS DateTime), N'/Upload/SlideShow/f232ce1b-19e1-49af-b63d-497a9c26c2be/Banner-mobile-900x1458.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (963, 7, 0, 4, N'administrator', CAST(N'2018-09-06T16:50:06.517' AS DateTime), N'/Upload/SlideShow/f232ce1b-19e1-49af-b63d-497a9c26c2be/Banner---1250x445.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (964, 7, 0, 4, N'teouit', CAST(N'2018-10-12T17:29:22.103' AS DateTime), N'/Upload/HotProducts/5c8b839a-dd33-4d52-bffa-04912d0489eb/product1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (965, 7, 0, 4, N'teouit', CAST(N'2018-10-12T17:30:42.843' AS DateTime), N'/Upload/HotProducts/912adae6-8ff0-4e13-951b-b21efe98efba/product2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (966, 7, 0, 4, N'teouit', CAST(N'2018-10-12T17:31:25.743' AS DateTime), N'/Upload/HotProducts/9cab387b-ba14-4b9c-9976-d1765b0bcc80/product3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (967, 7, 0, 4, N'teouit', CAST(N'2018-10-12T17:35:10.633' AS DateTime), N'/Upload/MainProducts/13158e54-6e15-4842-bcf6-d0ba60c88f5e/bannersub1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (968, 7, 0, 4, N'teouit', CAST(N'2018-10-12T17:39:48.720' AS DateTime), N'/Upload/MainProducts/3cfdbe78-6cfc-424d-a9be-8342b98333f7/bannersub2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (969, 7, 0, 4, N'teouit', CAST(N'2018-10-14T22:33:08.933' AS DateTime), N'/Upload/Categoties/e1c4dd03-4663-48d6-b43d-cd9a02e5d877/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (970, 7, 0, 4, N'administrator', CAST(N'2018-10-14T22:43:19.767' AS DateTime), N'/Upload/Categoties/e1c4dd03-4663-48d6-b43d-cd9a02e5d877/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (971, 7, 0, 4, N'administrator', CAST(N'2018-10-14T22:43:24.297' AS DateTime), N'/Upload/Categoties/e1c4dd03-4663-48d6-b43d-cd9a02e5d877/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (972, 7, 0, 4, N'administrator', CAST(N'2018-10-14T22:43:24.407' AS DateTime), N'/Upload/Categoties/e1c4dd03-4663-48d6-b43d-cd9a02e5d877/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (973, 7, 0, 4, N'administrator', CAST(N'2018-10-14T22:43:48.077' AS DateTime), N'/Upload/Categoties/e1c4dd03-4663-48d6-b43d-cd9a02e5d877/banner.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (974, 7, 0, 4, N'administrator', CAST(N'2018-10-16T10:08:31.920' AS DateTime), N'/Upload/MainProducts/60f0c6ec-3187-459b-aa1c-91c0594445c8/SCF284_02-IMS-vi_VN.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (975, 7, 0, 4, N'administrator', CAST(N'2018-10-16T14:41:31.203' AS DateTime), N'/Upload/MainProducts/725f3bd1-4bef-4c0f-8c7d-0563fa801531/Layer 11.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (976, 7, 0, 4, N'administrator', CAST(N'2018-10-16T14:44:47.627' AS DateTime), N'/Upload/MainProducts/fbb75370-0fa1-4444-b92e-4fa9658e069a/SCF284_02-IMS-vi_VN.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (977, 7, 0, 4, N'administrator', CAST(N'2018-10-19T16:26:03.440' AS DateTime), N'/Upload/MainProducts/ec164b7d-c7c7-446f-b892-851f3fa194c6/de.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (978, 7, 0, 4, N'administrator', CAST(N'2018-10-20T15:00:43.093' AS DateTime), N'/Upload/SlideShow/0a7ac99d-580c-4a3d-a55f-dcf4c3cffd93/banner2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (979, 7, 0, 4, N'administrator', CAST(N'2018-10-20T15:01:23.657' AS DateTime), N'/Upload/SlideShow/f1079e59-2b44-4d9d-bbac-beb0630dbd7e/banner1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (980, 7, 0, 4, N'administrator', CAST(N'2018-10-20T15:01:31.657' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/banner3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (981, 7, 0, 4, N'administrator', CAST(N'2018-10-22T10:38:28.117' AS DateTime), N'/Upload/MainProducts/9e028c73-2c83-465a-bbe2-28c829a69b0e/thumbnail.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (982, 7, 0, 4, N'administrator', CAST(N'2018-10-22T10:40:07.693' AS DateTime), N'/Upload/MainProducts/13158e54-6e15-4842-bcf6-d0ba60c88f5e/bannersub1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (983, 7, 0, 4, N'administrator', CAST(N'2018-10-22T10:41:28.130' AS DateTime), N'/Upload/MainProducts/725f3bd1-4bef-4c0f-8c7d-0563fa801531/bannersub5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (984, 7, 0, 4, N'administrator', CAST(N'2018-10-22T11:09:55.803' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_08.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (985, 7, 0, 4, N'administrator', CAST(N'2018-10-22T11:11:36.867' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (986, 7, 0, 4, N'administrator', CAST(N'2018-10-22T17:37:28.803' AS DateTime), N'/Upload/HotProducts/cae1cee1-1936-4625-b9d2-a69b7004024b/product2-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (987, 7, 0, 4, N'administrator', CAST(N'2018-10-22T17:44:21.647' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (988, 7, 0, 4, N'administrator', CAST(N'2018-10-22T17:47:08.333' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (989, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:46:59.250' AS DateTime), N'/Upload/MainProducts/9e028c73-2c83-465a-bbe2-28c829a69b0e/website-sua-size000.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (990, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:47:27.690' AS DateTime), N'/Upload/MainProducts/5dd2ac0f-d2bb-46de-a70f-684eaef97a6a/bannersub6.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (991, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:47:44.767' AS DateTime), N'/Upload/MainProducts/725f3bd1-4bef-4c0f-8c7d-0563fa801531/scf355.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (992, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:48:00.957' AS DateTime), N'/Upload/MainProducts/60f0c6ec-3187-459b-aa1c-91c0594445c8/bannersub4.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (993, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:48:15.190' AS DateTime), N'/Upload/MainProducts/fbb75370-0fa1-4444-b92e-4fa9658e069a/bannersub3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (994, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:48:27.987' AS DateTime), N'/Upload/MainProducts/3cfdbe78-6cfc-424d-a9be-8342b98333f7/bannersub2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (995, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:48:39.643' AS DateTime), N'/Upload/MainProducts/13158e54-6e15-4842-bcf6-d0ba60c88f5e/Máy hút sữa điện đôi.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (996, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:49:26.533' AS DateTime), N'/Upload/HotProducts/cae1cee1-1936-4625-b9d2-a69b7004024b/product2-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (997, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:49:47.127' AS DateTime), N'/Upload/HotProducts/9cab387b-ba14-4b9c-9976-d1765b0bcc80/product3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (998, 7, 0, 4, N'administrator', CAST(N'2018-10-30T11:50:01.080' AS DateTime), N'/Upload/HotProducts/5c8b839a-dd33-4d52-bffa-04912d0489eb/product1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (999, 7, 0, 4, N'', CAST(N'2018-10-30T14:59:08.180' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_03 (1).png')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1000, 7, 0, 4, N'', CAST(N'2018-10-30T14:59:09.697' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_03-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1001, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:01:59.777' AS DateTime), N'/Upload/SlideShow/9e0d7265-7055-4e36-a8f4-81926558a406/website-sua-size_06-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1002, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:04:04.687' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1003, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:41:46.037' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_03-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1004, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:41:48.553' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_03-2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1005, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:41:49.770' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_03-3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1006, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:42:09.303' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03 (3)-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1007, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:42:10.880' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03 (3).png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1008, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:42:30.270' AS DateTime), N'/Upload/SlideShow/9e0d7265-7055-4e36-a8f4-81926558a406/website-sua-size_06 (4)-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1009, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:42:32.913' AS DateTime), N'/Upload/SlideShow/9e0d7265-7055-4e36-a8f4-81926558a406/website-sua-size_06 (4).png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1010, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:48:58.787' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_02_op.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1011, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:51:17.073' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1012, 7, 0, 4, N'administrator', CAST(N'2018-10-30T15:52:33.227' AS DateTime), N'/Upload/SlideShow/9e0d7265-7055-4e36-a8f4-81926558a406/website-sua-size_06.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1013, 7, 0, 4, N'administrator', CAST(N'2018-10-30T16:39:27.487' AS DateTime), N'/Upload/MainProducts/3cfdbe78-6cfc-424d-a9be-8342b98333f7/bannersub2-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1014, 7, 0, 4, N'administrator', CAST(N'2018-10-30T16:53:55.350' AS DateTime), N'/Upload/SlideShow/b4edeb5f-2a5a-413b-8990-9ca3b9b108ee/banner4.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1015, 7, 0, 4, N'administrator', CAST(N'2018-10-30T17:03:03.427' AS DateTime), N'/Upload/SlideShow/b4edeb5f-2a5a-413b-8990-9ca3b9b108ee/banner4.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1016, 7, 0, 4, N'', CAST(N'2018-10-30T17:04:13.630' AS DateTime), N'/Upload/SlideShow/b4edeb5f-2a5a-413b-8990-9ca3b9b108ee/banner4.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1017, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:00:39.563' AS DateTime), N'/Upload/MainProducts/5dd2ac0f-d2bb-46de-a70f-684eaef97a6a/bannersub6.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1018, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:00:40.390' AS DateTime), N'/Upload/MainProducts/5dd2ac0f-d2bb-46de-a70f-684eaef97a6a/bannersub6.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1019, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:00:55.907' AS DateTime), N'/Upload/MainProducts/725f3bd1-4bef-4c0f-8c7d-0563fa801531/scf355.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1020, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:01:07.673' AS DateTime), N'/Upload/MainProducts/60f0c6ec-3187-459b-aa1c-91c0594445c8/bannersub4.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1021, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:01:08.797' AS DateTime), N'/Upload/MainProducts/60f0c6ec-3187-459b-aa1c-91c0594445c8/bannersub4.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1022, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:01:21.237' AS DateTime), N'/Upload/MainProducts/fbb75370-0fa1-4444-b92e-4fa9658e069a/bannersub3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1023, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:01:22.343' AS DateTime), N'/Upload/MainProducts/fbb75370-0fa1-4444-b92e-4fa9658e069a/bannersub3.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1024, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:01:33.627' AS DateTime), N'/Upload/MainProducts/3cfdbe78-6cfc-424d-a9be-8342b98333f7/bannersub2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1025, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:01:34.610' AS DateTime), N'/Upload/MainProducts/3cfdbe78-6cfc-424d-a9be-8342b98333f7/bannersub2.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1026, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:01:45.907' AS DateTime), N'/Upload/MainProducts/13158e54-6e15-4842-bcf6-d0ba60c88f5e/Máy hút sữa điện đôi.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1027, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:07:19.377' AS DateTime), N'/Upload/MainProducts/9e028c73-2c83-465a-bbe2-28c829a69b0e/website-sua-size000.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1028, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:07:20.097' AS DateTime), N'/Upload/MainProducts/9e028c73-2c83-465a-bbe2-28c829a69b0e/website-sua-size000-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1029, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:07:35.033' AS DateTime), N'/Upload/MainProducts/5dd2ac0f-d2bb-46de-a70f-684eaef97a6a/bannersub6.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1030, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:07:47.147' AS DateTime), N'/Upload/MainProducts/725f3bd1-4bef-4c0f-8c7d-0563fa801531/scf355.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1031, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:08:02.023' AS DateTime), N'/Upload/MainProducts/60f0c6ec-3187-459b-aa1c-91c0594445c8/bannersub4.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1032, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:08:14.037' AS DateTime), N'/Upload/MainProducts/fbb75370-0fa1-4444-b92e-4fa9658e069a/bannersub3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1033, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:08:27.443' AS DateTime), N'/Upload/MainProducts/3cfdbe78-6cfc-424d-a9be-8342b98333f7/bannersub2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1034, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:08:40.820' AS DateTime), N'/Upload/MainProducts/13158e54-6e15-4842-bcf6-d0ba60c88f5e/Máy-hút-sữa-điện-đôi.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1035, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:09:05.427' AS DateTime), N'/Upload/HotProducts/cae1cee1-1936-4625-b9d2-a69b7004024b/product2-1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1036, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:09:09.820' AS DateTime), N'/Upload/HotProducts/cae1cee1-1936-4625-b9d2-a69b7004024b/product2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1037, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:09:10.757' AS DateTime), N'/Upload/HotProducts/cae1cee1-1936-4625-b9d2-a69b7004024b/product5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1038, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:09:22.477' AS DateTime), N'/Upload/HotProducts/9cab387b-ba14-4b9c-9976-d1765b0bcc80/product3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1039, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:09:34.240' AS DateTime), N'/Upload/HotProducts/5c8b839a-dd33-4d52-bffa-04912d0489eb/product5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1040, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:18:38.650' AS DateTime), N'/Upload/MainProducts/9e028c73-2c83-465a-bbe2-28c829a69b0e/8.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1041, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:18:47.900' AS DateTime), N'/Upload/MainProducts/5dd2ac0f-d2bb-46de-a70f-684eaef97a6a/4.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1042, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:19:00.837' AS DateTime), N'/Upload/MainProducts/725f3bd1-4bef-4c0f-8c7d-0563fa801531/7.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1043, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:19:11.400' AS DateTime), N'/Upload/MainProducts/60f0c6ec-3187-459b-aa1c-91c0594445c8/3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1044, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:19:21.527' AS DateTime), N'/Upload/MainProducts/fbb75370-0fa1-4444-b92e-4fa9658e069a/2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1045, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:19:35.070' AS DateTime), N'/Upload/MainProducts/3cfdbe78-6cfc-424d-a9be-8342b98333f7/1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1046, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:19:45.993' AS DateTime), N'/Upload/MainProducts/13158e54-6e15-4842-bcf6-d0ba60c88f5e/5.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1047, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:26:06.013' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03_op.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1048, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:26:07.123' AS DateTime), N'/Upload/SlideShow/c9d14938-7f1a-44b7-a47a-05035ade79a6/website-sua-size_03_op.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1049, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:26:20.857' AS DateTime), N'/Upload/SlideShow/b4edeb5f-2a5a-413b-8990-9ca3b9b108ee/banner4_op.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1050, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:26:34.747' AS DateTime), N'/Upload/SlideShow/9e0d7265-7055-4e36-a8f4-81926558a406/website-sua-size_06_op.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1051, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:26:35.247' AS DateTime), N'/Upload/SlideShow/9e0d7265-7055-4e36-a8f4-81926558a406/website-sua-size_06_op.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1052, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:26:47.547' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_02_op.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1053, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:26:48.280' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_03.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1054, 7, 0, 4, N'administrator', CAST(N'2018-10-30T18:27:04.873' AS DateTime), N'/Upload/SlideShow/fefc3c22-efea-4f32-923d-87114233f0e8/website-sua-size_03 (1).png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1055, 7, 0, 4, N'administrator', CAST(N'2018-10-31T11:27:37.337' AS DateTime), N'/Upload/HotProducts/cae1cee1-1936-4625-b9d2-a69b7004024b/SCF330_20-IMS-vi_VN.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1056, 7, 0, 4, N'administrator', CAST(N'2018-10-31T11:28:53.507' AS DateTime), N'/Upload/HotProducts/9cab387b-ba14-4b9c-9976-d1765b0bcc80/Layer 14.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1057, 7, 0, 4, N'administrator', CAST(N'2018-10-31T11:32:02.040' AS DateTime), N'/Upload/HotProducts/5c8b839a-dd33-4d52-bffa-04912d0489eb/SCF798_00-IMS-vi_VN.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1058, 1, 0, 1, N'administrator', CAST(N'2018-12-21T00:15:14.383' AS DateTime), N'/Upload/News/bdb80237-e639-4594-bfc9-4a87e209e497')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1059, 1, 0, 1, N'administrator', CAST(N'2018-12-23T01:13:34.103' AS DateTime), N'/Upload/News/ad7bb3cc-79db-4b79-a7e6-53048d9335d2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1060, 1, 0, 1, N'administrator', CAST(N'2018-12-23T01:14:36.607' AS DateTime), N'/Upload/News/6520cf5d-8155-4de7-90dd-c43424cd380b')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1061, 1, 0, 1, N'administrator', CAST(N'2018-12-23T01:43:20.310' AS DateTime), N'/Upload/News/9bd2e14a-f803-435d-9dd1-d9d483f64df0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1062, 1, 0, 1, N'administrator', CAST(N'2018-12-23T02:01:17.403' AS DateTime), N'/Upload/News/2091263d-1969-4670-bbe7-c19284a697bc')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1063, 1, 0, 1, N'administrator', CAST(N'2018-12-23T02:06:20.397' AS DateTime), N'/Upload/News/7bfb0c4c-c692-4455-97f9-b96fdf6e9a99')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1064, 1, 0, 1, N'administrator', CAST(N'2018-12-23T02:07:25.670' AS DateTime), N'/Upload/News/124ced47-1f29-474c-a57c-83c76236bb8e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1065, 1, 0, 1, N'administrator', CAST(N'2018-12-23T02:17:51.217' AS DateTime), N'/Upload/News/09af882d-aab1-4abe-ba7a-e9a2536f8dab')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1066, 1, 0, 1, N'administrator', CAST(N'2018-12-23T02:27:08.767' AS DateTime), N'/Upload/News/2f5a45dd-72f6-4dc6-9b3b-a7383e0d23a2')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1067, 1, 1, 2, N'administrator', CAST(N'2018-12-23T10:56:39.417' AS DateTime), N'/Upload/News/5247c347-71ca-4d96-bfe0-19e61d159faf')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1068, 1, 2, 2, N'administrator', CAST(N'2018-12-23T11:29:30.687' AS DateTime), N'/Upload/News/91c33f10-2d86-4275-a33e-f2e24d6164bd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1069, 1, 3, 2, N'administrator', CAST(N'2018-12-23T11:41:07.847' AS DateTime), N'/Upload/News/fd2462d8-0a7a-4ff5-9ebf-bdda566b32be')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1070, 1, 4, 2, N'administrator', CAST(N'2018-12-23T14:53:41.217' AS DateTime), N'/Upload/News/805d0982-1c00-4720-aa1a-fbfe7c557a68')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1071, 1, 5, 2, N'administrator', CAST(N'2018-12-23T15:24:42.740' AS DateTime), N'/Upload/News/4dc9526f-fcef-4c0f-a32b-1141d9919654')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1072, 7, 0, 4, N'administrator', CAST(N'2018-12-23T17:08:01.670' AS DateTime), N'/Upload/Products/f5acbf25-980c-43e7-a8e3-bd367ab6745a/p1.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1073, 7, 0, 4, N'administrator', CAST(N'2018-12-23T17:08:07.447' AS DateTime), N'/Upload/Products/f5acbf25-980c-43e7-a8e3-bd367ab6745a/p2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1074, 7, 0, 4, N'administrator', CAST(N'2018-12-23T17:08:07.847' AS DateTime), N'/Upload/Products/f5acbf25-980c-43e7-a8e3-bd367ab6745a/p2.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1075, 7, 0, 4, N'administrator', CAST(N'2018-12-23T17:08:07.890' AS DateTime), N'/Upload/Products/f5acbf25-980c-43e7-a8e3-bd367ab6745a/p3.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1076, 1, 6, 2, N'administrator', CAST(N'2018-12-25T01:02:19.020' AS DateTime), N'/Upload/News/779eb57d-52e0-4320-af37-5abdb96222be')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1077, 1, 7, 2, N'administrator', CAST(N'2018-12-25T01:04:02.193' AS DateTime), N'/Upload/News/432c3505-e979-4e94-9f83-5ba6ac3a141d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1078, 1, 8, 2, N'administrator', CAST(N'2018-12-25T01:04:51.067' AS DateTime), N'/Upload/News/20d33d97-6e9c-45f1-a3af-fad4c85f3800')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1079, 1, 9, 2, N'administrator', CAST(N'2018-12-25T01:05:30.390' AS DateTime), N'/Upload/News/c6079c5a-444e-46b4-a397-6e13d64c0b25')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1080, 1, 10, 2, N'administrator', CAST(N'2018-12-25T01:06:25.267' AS DateTime), N'/Upload/News/aee2aacf-cc0f-40bc-9aeb-269a84c308fc')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1081, 1, 11, 2, N'administrator', CAST(N'2018-12-25T01:07:07.047' AS DateTime), N'/Upload/News/9de5b480-c75e-4368-b197-00f4f316f150')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1082, 1, 0, 1, N'administrator', CAST(N'2018-12-26T19:52:39.087' AS DateTime), N'/Upload/News/ae05f6c5-ff4c-49a4-b3e4-fdcd619bd8f8')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1083, 1, 12, 2, N'administrator', CAST(N'2018-12-26T20:17:31.787' AS DateTime), N'/Upload/News/6c8b13d3-bc39-4c9b-8019-93ab868cc8ec')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1084, 1, 0, 1, N'administrator', CAST(N'2018-12-26T20:22:31.797' AS DateTime), N'/Upload/News/fd2462d8-0a7a-4ff5-9ebf-bdda566b32be')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1085, 1, 13, 2, N'administrator', CAST(N'2018-12-26T20:23:12.607' AS DateTime), N'/Upload/News/6c8b13d3-bc39-4c9b-8019-93ab868cc8ec')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1086, 1, 0, 1, N'administrator', CAST(N'2018-12-26T20:49:28.290' AS DateTime), N'/Upload/News/9c580bd7-bbde-428d-83cf-1e2aa54bbbb7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1087, 1, 14, 2, N'administrator', CAST(N'2018-12-26T20:49:39.837' AS DateTime), N'/Upload/News/78edbf77-8c2f-4c65-b201-d7982280fbc4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1088, 1, 15, 2, N'administrator', CAST(N'2018-12-26T20:49:49.867' AS DateTime), N'/Upload/News/78edbf77-8c2f-4c65-b201-d7982280fbc4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1089, 1, 16, 2, N'administrator', CAST(N'2019-01-07T20:37:21.857' AS DateTime), N'/Upload/News/fd2462d8-0a7a-4ff5-9ebf-bdda566b32be')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1090, 1, 17, 2, N'administrator', CAST(N'2019-01-07T20:49:45.047' AS DateTime), N'/Upload/News/805d0982-1c00-4720-aa1a-fbfe7c557a68')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1091, 1, 18, 2, N'administrator', CAST(N'2019-01-07T20:50:34.813' AS DateTime), N'/Upload/News/4dc9526f-fcef-4c0f-a32b-1141d9919654')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1092, 1, 19, 2, N'administrator', CAST(N'2019-01-07T20:51:15.307' AS DateTime), N'/Upload/News/779eb57d-52e0-4320-af37-5abdb96222be')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1093, 1, 20, 2, N'administrator', CAST(N'2019-01-07T20:52:46.010' AS DateTime), N'/Upload/News/779eb57d-52e0-4320-af37-5abdb96222be')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1094, 1, 21, 2, N'administrator', CAST(N'2019-01-07T20:53:28.430' AS DateTime), N'/Upload/News/432c3505-e979-4e94-9f83-5ba6ac3a141d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1095, 1, 0, 1, N'administrator', CAST(N'2019-01-23T20:18:49.720' AS DateTime), N'/Upload/News/e8645aa5-6cef-4932-9fe5-7e3af8de5579')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1096, 1, 0, 1, N'administrator', CAST(N'2019-01-23T20:20:35.940' AS DateTime), N'/Upload/News/83c92070-cf43-4a7d-9f12-3ddfc0ff53a0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1097, 7, 0, 4, N'administrator', CAST(N'2019-03-06T23:55:34.910' AS DateTime), N'/Upload/SlideShow/1d9db4c7-b420-4acf-a553-4d2a11993186/slider1.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1098, 1, 1, 2, N'administrator', CAST(N'2019-03-08T23:38:33.877' AS DateTime), N'/Upload/News/7f71a525-17eb-4455-a1a6-d4f1d0474d9f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1099, 1, 2, 2, N'administrator', CAST(N'2019-03-09T00:25:34.700' AS DateTime), N'/Upload/News/302dda04-3e2d-42ba-80f0-ac2a176da4ce')
GO
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1100, 1, 3, 2, N'administrator', CAST(N'2019-03-09T00:35:47.473' AS DateTime), N'/Upload/News/c168871e-9933-46e7-9691-87c581e573a0')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1101, 1, 4, 2, N'administrator', CAST(N'2019-03-09T00:46:33.797' AS DateTime), N'/Upload/News/ecd5debf-a172-4cda-b27e-e6d7630d1645')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1102, 1, 5, 2, N'administrator', CAST(N'2019-03-09T00:47:21.693' AS DateTime), N'/Upload/News/4e8caa16-96c5-4df4-b34f-ec9d5a2f3ea7')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1103, 1, 6, 2, N'administrator', CAST(N'2019-03-09T00:47:54.140' AS DateTime), N'/Upload/News/259b26bc-8594-4fbf-b6d5-131dad352700')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1104, 1, 7, 2, N'administrator', CAST(N'2019-03-09T00:48:39.123' AS DateTime), N'/Upload/News/de61a92a-e2fd-45bc-9222-52a53bc7b829')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1105, 1, 8, 2, N'administrator', CAST(N'2019-03-09T00:49:14.757' AS DateTime), N'/Upload/News/1194696b-e4a4-4b17-a7ac-a505f005be62')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1106, 1, 9, 2, N'administrator', CAST(N'2019-03-09T00:50:06.560' AS DateTime), N'/Upload/News/11038828-4ec9-42ac-b609-42886bee4943')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1107, 1, 0, 1, N'administrator', CAST(N'2019-03-09T00:53:00.430' AS DateTime), N'/Upload/News/cd71e082-cd82-45bf-b520-b8beb131245c')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1108, 7, 0, 4, N'administrator', CAST(N'2019-03-12T22:41:54.133' AS DateTime), N'/Upload/News/302dda04-3e2d-42ba-80f0-ac2a176da4ce/3.xong.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1109, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:13:14.257' AS DateTime), N'/Upload/News/c168871e-9933-46e7-9691-87c581e573a0/1.xong.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1110, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:13:33.927' AS DateTime), N'/Upload/News/ecd5debf-a172-4cda-b27e-e6d7630d1645/2.xong.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1111, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:14:12.930' AS DateTime), N'/Upload/News/ecd5debf-a172-4cda-b27e-e6d7630d1645/2.xong.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1112, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:15:10.380' AS DateTime), N'/Upload/News/4e8caa16-96c5-4df4-b34f-ec9d5a2f3ea7/3.xong.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1113, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:15:54.170' AS DateTime), N'/Upload/News/259b26bc-8594-4fbf-b6d5-131dad352700/4.PNG')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1114, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:16:33.800' AS DateTime), N'/Upload/News/de61a92a-e2fd-45bc-9222-52a53bc7b829/5.xong.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1115, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:17:09.750' AS DateTime), N'/Upload/News/1194696b-e4a4-4b17-a7ac-a505f005be62/6.xong.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1116, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:17:39.620' AS DateTime), N'/Upload/News/11038828-4ec9-42ac-b609-42886bee4943/4.PNG')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1117, 1, 10, 2, N'administrator', CAST(N'2019-03-13T00:19:48.163' AS DateTime), N'/Upload/News/ad2a1360-8fcd-4bc3-bb1b-00705b85e897')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1118, 1, 11, 2, N'administrator', CAST(N'2019-03-13T00:20:46.887' AS DateTime), N'/Upload/News/3cffaa50-c71c-427b-ac84-739eb439427f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1119, 1, 12, 2, N'administrator', CAST(N'2019-03-13T00:21:20.577' AS DateTime), N'/Upload/News/a5639df3-a9d4-4493-b98c-a016a9d9c0cd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1120, 7, 0, 4, N'administrator', CAST(N'2019-03-13T00:22:10.273' AS DateTime), N'/Upload/News/3cffaa50-c71c-427b-ac84-739eb439427f/3.xong.png')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1121, 1, 0, 1, N'administrator', CAST(N'2019-03-13T00:22:24.343' AS DateTime), N'/Upload/News/98c45e9c-3c35-48ec-a2d8-41b7620eda68')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1122, 1, 13, 2, N'administrator', CAST(N'2019-03-13T00:34:49.540' AS DateTime), N'/Upload/News/a30a8dbb-2abf-4a25-9077-8300e7c62535')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1123, 1, 14, 2, N'administrator', CAST(N'2019-03-13T00:39:08.843' AS DateTime), N'/Upload/News/8616565a-810a-4721-9f7c-ce6fb9ad5336')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1124, 1, 15, 2, N'administrator', CAST(N'2019-03-13T00:40:52.477' AS DateTime), N'/Upload/News/aac33aa8-50cf-486f-9cf3-9831239f8515')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1125, 1, 16, 2, N'administrator', CAST(N'2019-03-13T00:42:21.673' AS DateTime), N'/Upload/News/18f806bc-36e2-4f17-ae9e-f9ab407e59ca')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1126, 1, 17, 2, N'administrator', CAST(N'2019-03-13T00:43:57.287' AS DateTime), N'/Upload/News/e4890c3a-386a-4f45-b17e-fdbe5a9c0d13')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1127, 1, 18, 2, N'administrator', CAST(N'2019-03-13T00:47:28.183' AS DateTime), N'/Upload/News/1d2e3013-b691-46a2-9d5b-f300e198f86e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1128, 1, 19, 2, N'administrator', CAST(N'2019-03-13T00:48:02.873' AS DateTime), N'/Upload/News/9c374e51-d260-40a2-8620-ad2c0e965544')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1129, 1, 0, 1, N'administrator', CAST(N'2019-03-13T00:49:16.203' AS DateTime), N'/Upload/News/1b9706b2-8463-4eca-bdaa-24a9c8b249a6')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1130, 1, 0, 1, N'administrator', CAST(N'2019-03-13T00:55:25.890' AS DateTime), N'/Upload/News/c4b71539-a244-417a-a640-5941f29be663')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1131, 1, 20, 2, N'administrator', CAST(N'2019-03-13T01:04:59.580' AS DateTime), N'/Upload/News/d3775754-a789-4fe7-9189-6c88e0ced320')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1132, 1, 21, 2, N'administrator', CAST(N'2019-03-13T01:08:30.790' AS DateTime), N'/Upload/News/70cedda9-3c97-4216-aada-978dca201d36')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1133, 1, 22, 2, N'administrator', CAST(N'2019-03-13T01:09:39.060' AS DateTime), N'/Upload/News/c1f030df-c186-4371-9ffd-f7a0df338de4')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1134, 1, 23, 2, N'administrator', CAST(N'2019-03-19T02:28:27.697' AS DateTime), N'/Upload/News/7f71a525-17eb-4455-a1a6-d4f1d0474d9f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1135, 1, 24, 2, N'administrator', CAST(N'2019-03-19T02:45:22.760' AS DateTime), N'/Upload/News/7f71a525-17eb-4455-a1a6-d4f1d0474d9f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1136, 1, 25, 2, N'administrator', CAST(N'2019-03-19T22:27:16.423' AS DateTime), N'/Upload/News/ad2a1360-8fcd-4bc3-bb1b-00705b85e897')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1137, 1, 26, 2, N'administrator', CAST(N'2019-03-19T22:33:05.517' AS DateTime), N'/Upload/News/ad2a1360-8fcd-4bc3-bb1b-00705b85e897')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1138, 1, 27, 2, N'administrator', CAST(N'2019-03-19T22:41:49.990' AS DateTime), N'/Upload/News/3cffaa50-c71c-427b-ac84-739eb439427f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1139, 1, 28, 2, N'administrator', CAST(N'2019-03-19T22:42:18.447' AS DateTime), N'/Upload/News/3cffaa50-c71c-427b-ac84-739eb439427f')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1140, 1, 29, 2, N'administrator', CAST(N'2019-03-19T22:54:24.457' AS DateTime), N'/Upload/News/a5639df3-a9d4-4493-b98c-a016a9d9c0cd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1141, 1, 30, 2, N'administrator', CAST(N'2019-03-19T22:54:56.800' AS DateTime), N'/Upload/News/a5639df3-a9d4-4493-b98c-a016a9d9c0cd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1142, 7, 0, 4, N'administrator', CAST(N'2019-04-09T20:14:36.803' AS DateTime), N'/Upload/Products/e40ec02d-a313-4fd5-baa0-52eb6b27dd71/p_26409_DAHUA-NVR2104-P-4KS2.jpg')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1143, 1, 1, 2, N'administrator', CAST(N'2019-05-13T10:37:44.110' AS DateTime), N'/Upload/News/9c0d98de-48dd-4c3f-87d2-2c975d16041e')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1144, 1, 2, 2, N'administrator', CAST(N'2019-05-13T10:38:05.327' AS DateTime), N'/Upload/News/6d04321e-be32-4966-92bf-c21d36991be9')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1145, 1, 3, 2, N'administrator', CAST(N'2019-05-13T10:38:24.970' AS DateTime), N'/Upload/News/2303aa1a-a575-4618-b6ca-924cf9eb4d81')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1146, 1, 4, 2, N'administrator', CAST(N'2019-05-13T10:38:40.347' AS DateTime), N'/Upload/News/d69c2170-e0de-4270-ae53-90237413b85a')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1147, 1, 5, 2, N'administrator', CAST(N'2019-05-13T10:38:54.010' AS DateTime), N'/Upload/News/78de5f0b-4fcf-41e1-ae60-61b8d44d5dc3')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1148, 1, 6, 2, N'administrator', CAST(N'2019-05-13T10:40:26.673' AS DateTime), N'/Upload/News/8178e4d1-8dfb-424e-803e-104c9c6379cd')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1149, 1, 7, 2, N'administrator', CAST(N'2019-05-13T10:40:44.667' AS DateTime), N'/Upload/News/3f57bb3a-6483-4d67-84b1-2d3abe39ba66')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (1150, 1, 8, 2, N'administrator', CAST(N'2019-05-13T10:41:12.500' AS DateTime), N'/Upload/News/a1907c01-41ff-41ba-94f5-1f88e2b2a50d')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (11143, 1, 0, 1, N'administrator', CAST(N'2019-05-16T12:22:36.380' AS DateTime), N'/Upload/News/cf8bbc5f-697f-4b14-a65d-8c8df7a9c8ef')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (11144, 1, 0, 1, N'administrator', CAST(N'2019-05-16T12:26:05.370' AS DateTime), N'/Upload/News/ad5a4093-cead-43de-8abe-da28533cfcfa')
INSERT [dbo].[LogHistories] ([Id], [EntityType], [EntityId], [ActionType], [ActionBy], [CreatedDate], [Comment]) VALUES (11145, 7, 0, 4, N'administrator', CAST(N'2021-07-26T21:38:26.260' AS DateTime), N'/Upload/Products/9be08af9-9345-4cdc-a668-ca4a998301d7/ngu-n-t-ng-camera-12v-30a-nguon-tong.jpg')
SET IDENTITY_INSERT [dbo].[LogHistories] OFF
GO
SET IDENTITY_INSERT [dbo].[MenuItems] ON 

INSERT [dbo].[MenuItems] ([Id], [Title], [Url], [LinkTargetType], [Order], [IconClass], [IsPublished], [Parent_Id], [Menu_Id], [OriginalValue], [Language_Id]) VALUES (1, N'Trang chủ', N'/trang-chu', 1, 1, NULL, 1, NULL, 1, N'b0810bcb-c565-4f05-a62f-02a33f73f0ad', 1)
INSERT [dbo].[MenuItems] ([Id], [Title], [Url], [LinkTargetType], [Order], [IconClass], [IsPublished], [Parent_Id], [Menu_Id], [OriginalValue], [Language_Id]) VALUES (2, N'Sản phẩm', N'/san-pham', 1, 2, NULL, 1, NULL, 1, N'10dd823c-407f-4417-917c-0e010cc6a867', 1)
INSERT [dbo].[MenuItems] ([Id], [Title], [Url], [LinkTargetType], [Order], [IconClass], [IsPublished], [Parent_Id], [Menu_Id], [OriginalValue], [Language_Id]) VALUES (3, N'Camera quan sát', N'/san-pham/camera-quan-sat', 1, 1, NULL, 1, 2, 1, N'bf9e7192-78d1-4889-b525-abae5e45ebe2', 1)
INSERT [dbo].[MenuItems] ([Id], [Title], [Url], [LinkTargetType], [Order], [IconClass], [IsPublished], [Parent_Id], [Menu_Id], [OriginalValue], [Language_Id]) VALUES (4, N'Camera Hitech', N'/san-pham/camera-quan-sat/camera-hitech', 1, 1, NULL, 1, 3, 1, N'e933fbe3-7fcd-485d-9644-31e77c57a63c', 1)
INSERT [dbo].[MenuItems] ([Id], [Title], [Url], [LinkTargetType], [Order], [IconClass], [IsPublished], [Parent_Id], [Menu_Id], [OriginalValue], [Language_Id]) VALUES (5, N'Analog Camera Series HD', N'/san-pham/camera-quan-sat/camera-hitech/analog-camera-series-hd', 1, 1, NULL, 1, 4, 1, N'f4dd511d-5e81-4865-821b-ef4a7de7c589', 1)
INSERT [dbo].[MenuItems] ([Id], [Title], [Url], [LinkTargetType], [Order], [IconClass], [IsPublished], [Parent_Id], [Menu_Id], [OriginalValue], [Language_Id]) VALUES (6, N'Camera Hlkvision', N'/san-pham/camera-quan-sat/camera-hlkvision', 1, 2, NULL, 1, 3, 1, N'28f331fb-cb8e-42f4-bcaf-662cc1483960', 1)
INSERT [dbo].[MenuItems] ([Id], [Title], [Url], [LinkTargetType], [Order], [IconClass], [IsPublished], [Parent_Id], [Menu_Id], [OriginalValue], [Language_Id]) VALUES (7, N'Giới thiệu', N'/gioi-thieu', 1, 3, NULL, 1, NULL, 1, N'a3030119-4608-4f25-9180-535ded3962c2', 1)
SET IDENTITY_INSERT [dbo].[MenuItems] OFF
GO
SET IDENTITY_INSERT [dbo].[Menus] ON 

INSERT [dbo].[Menus] ([Id], [Title], [MenuType], [Order]) VALUES (1, N'Menu Header', 1, 1)
INSERT [dbo].[Menus] ([Id], [Title], [MenuType], [Order]) VALUES (2, N'Menu Footer', 2, 2)
SET IDENTITY_INSERT [dbo].[Menus] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderItems] ON 

INSERT [dbo].[OrderItems] ([Id], [Product_Id], [Order_Id], [Price], [ItemCount], [Amount]) VALUES (1, 4, 1, CAST(200000 AS Decimal(18, 0)), 2, CAST(360000 AS Decimal(18, 0)))
INSERT [dbo].[OrderItems] ([Id], [Product_Id], [Order_Id], [Price], [ItemCount], [Amount]) VALUES (2, 4, 2, CAST(200000 AS Decimal(18, 0)), 1, CAST(180000 AS Decimal(18, 0)))
INSERT [dbo].[OrderItems] ([Id], [Product_Id], [Order_Id], [Price], [ItemCount], [Amount]) VALUES (3, 4, 3, CAST(200000 AS Decimal(18, 0)), 1, CAST(180000 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[OrderItems] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderLogs] ON 

INSERT [dbo].[OrderLogs] ([Id], [ActionBy], [ActionTime], [OrderLogType], [OldValue], [NewValue], [Notes], [Order_Id]) VALUES (1, N'administrator', CAST(N'2019-05-11T01:24:05.267' AS DateTime), 1, N'Đang chờ xử lý', N'Đang giao hàng', NULL, 3)
SET IDENTITY_INSERT [dbo].[OrderLogs] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Id], [BuyerName], [BuyerEmail], [BuyerPhone], [BuyerAddress], [OrderTime], [Amount], [OrderStatus], [Note], [OtherBuyerPhone], [OrderType_Id]) VALUES (1, N'Trụ sở chính', N'tinhqit@gmail.com', N'1234567890', N'Số 38 Đường số 13, Khu Đô Thị Vạn Phúc, P.Hiệp Bình Phước, Q. Thủ Đức, Tp. HCM', CAST(N'2019-05-11T00:44:35.740' AS DateTime), CAST(360000 AS Decimal(18, 0)), 1, NULL, NULL, 3)
INSERT [dbo].[Orders] ([Id], [BuyerName], [BuyerEmail], [BuyerPhone], [BuyerAddress], [OrderTime], [Amount], [OrderStatus], [Note], [OtherBuyerPhone], [OrderType_Id]) VALUES (2, N'Trụ sở chính', N'tinhqit@gmail.com', N'1234567890', N'Số 38 Đường số 13, Khu Đô Thị Vạn Phúc, P.Hiệp Bình Phước, Q. Thủ Đức, Tp. HCM', CAST(N'2019-05-11T01:19:04.367' AS DateTime), CAST(180000 AS Decimal(18, 0)), 1, NULL, NULL, 3)
INSERT [dbo].[Orders] ([Id], [BuyerName], [BuyerEmail], [BuyerPhone], [BuyerAddress], [OrderTime], [Amount], [OrderStatus], [Note], [OtherBuyerPhone], [OrderType_Id]) VALUES (3, N'Trụ sở chính', N'tinhqit@gmail.com', N'1234567890', N'Số 38 Đường số 13, Khu Đô Thị Vạn Phúc, P.Hiệp Bình Phước, Q. Thủ Đức, Tp. HCM', CAST(N'2019-05-11T01:19:22.280' AS DateTime), CAST(180000 AS Decimal(18, 0)), 2, NULL, NULL, 3)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderTypes] ON 

INSERT [dbo].[OrderTypes] ([Id], [Title], [Content], [Order], [IsPublished], [OriginalValue], [Language_Id]) VALUES (1, N'Thanh toán khi nhận hàng', N'<p>Quý khách sẽ thanh toán bằng tiền mặt khi nhận hàng tại nhà</p>

<p>Quý khách sẽ thanh toán bằng tiền mặt khi nhận hàng tại nhà</p>

<p>Quý khách sẽ thanh toán bằng tiền mặt khi nhận hàng tại nhà</p>
', 1, 1, N'cf3e3cab-1d0d-4b09-b5c8-c43c7d6f8eb0', 1)
INSERT [dbo].[OrderTypes] ([Id], [Title], [Content], [Order], [IsPublished], [OriginalValue], [Language_Id]) VALUES (3, N'Chuyển khoản qua ngân hàng', N'<p>1. Công ty TNHH Đầu Tư &amp; Phát Triển I T C Số Tk: <span>228296809 </span>– Ngân Hàng TMCP Á Châu ( ACB ) - CN Tp Hồ Chí Minh.</p>

<p>2. Nguyễn Thanh Việt Số tk: <span> ‪1998 69 519‬ </span>- Ngân Hàng Á Châu TpHCM ( ACB ) - CN Tp Hồ Chí Minh.</p>
', 2, 1, N'cd07077c-976a-4570-9f8b-dad34bc73282', 1)
SET IDENTITY_INSERT [dbo].[OrderTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Partners] ON 

INSERT [dbo].[Partners] ([Id], [Name], [ImageDefault], [ImageFolder], [ImageAlt], [Order], [IsPublished]) VALUES (1, N'Landmark', N'/Upload/Popup/0887dd45-fee8-4313-8c68-e80289fd89a4/Landmark.png', N'/Upload/Popup/0887dd45-fee8-4313-8c68-e80289fd89a4', N'Landmark', 1, 1)
INSERT [dbo].[Partners] ([Id], [Name], [ImageDefault], [ImageFolder], [ImageAlt], [Order], [IsPublished]) VALUES (2, N'Mini Store', N'/Upload/Popup/ea9f7393-9e7e-4ef9-8409-c44218a45f23/ministop.png', N'/Upload/Popup/ea9f7393-9e7e-4ef9-8409-c44218a45f23', N'Mini Store', 2, 1)
INSERT [dbo].[Partners] ([Id], [Name], [ImageDefault], [ImageFolder], [ImageAlt], [Order], [IsPublished]) VALUES (3, N'FamilyMart', N'/Upload/Popup/91bb86bc-5d6f-4b0c-8dc9-46d813a58128/family.jpg', N'/Upload/Popup/91bb86bc-5d6f-4b0c-8dc9-46d813a58128', N'Family Mart', 3, 1)
INSERT [dbo].[Partners] ([Id], [Name], [ImageDefault], [ImageFolder], [ImageAlt], [Order], [IsPublished]) VALUES (4, N'AEON', N'/Upload/Popup/af0d3e43-fc23-46d5-a4c9-5f7814feaa1c/aeon.png', N'/Upload/Popup/af0d3e43-fc23-46d5-a4c9-5f7814feaa1c', N'AEON', 4, 1)
INSERT [dbo].[Partners] ([Id], [Name], [ImageDefault], [ImageFolder], [ImageAlt], [Order], [IsPublished]) VALUES (5, N'Mini Store2', N'/Upload/Popup/5aae9a08-8b00-46b4-a047-26846ecd15dc/ministop.png', N'/Upload/Popup/5aae9a08-8b00-46b4-a047-26846ecd15dc', N'Mini Store2', 5, 1)
SET IDENTITY_INSERT [dbo].[Partners] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [Name], [SeName], [ImageDefault], [ImageFolder], [Link], [Order], [IsPublished], [Description], [Price], [Code], [ImageCart], [PromotionPrice], [PromotionPercent], [ProductCode], [OriginalValue], [Language_Id], [Category_Id], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [IsVideo], [VideoId], [DetailInfo], [Advantages], [Instruction], [SalesCommitment], [IsPromotion], [ShortDescription], [IsPriceContact], [IsLandingpage]) VALUES (2, N'Hitech Pro 2002HD', N'hitech-pro-2002hd', N'/Upload/Products/e40ec02d-a313-4fd5-baa0-52eb6b27dd71/201IPHD-1.jpg', N'/Upload/Products/e40ec02d-a313-4fd5-baa0-52eb6b27dd71', NULL, 0, 1, N'<p>- 1/2.9" Aptina 1.3 Megapixel CMOS</p>

<p>- 25/30fps@960P</p>

<p>- High speed, long distance real-time transmission</p>

<p>- Day/Night(ICR), AWB, AGC, BLC, 2DNR</p>

<p>- 3.6mm fixed lens</p>

<p>- Max.IR LEDs length 20m, Smart IR</p>

<p>- IP67, DC12V</p>
', CAST(800000 AS Decimal(18, 0)), NULL, NULL, CAST(600000 AS Decimal(18, 0)), 25, N'2002HD    ', N'8712bb28-df77-42e1-88f7-93ec861ded0f', 1, 7, NULL, NULL, NULL, NULL, 1, N'O-NBzwxauZE', N'<p>- 1/2.9" Aptina 1.3 Megapixel CMOS</p>

<p>- 25/30fps@960P</p>

<p>- High speed, long distance real-time transmission</p>

<p>- Day/Night(ICR), AWB, AGC, BLC, 2DNR</p>

<p>- 3.6mm fixed lens</p>

<p>- Max.IR LEDs length 20m, Smart IR</p>

<p>- IP67, DC12V</p>
', NULL, NULL, N'<p>1. Hàng chính hãng, mới 100%.</p>

<p>2. Đổi mới 100% nếu xảy ra hư hỏng phần cứng trong tháng đầu tiên.</p>

<p>3. Đổi mới 100% nếu hư hỏng sửa chữa 03 lần liên tiếp nhưng không khắc phục được.</p>

<p>4. Thời gian có mặt để xử lý khi nhận được báo hư không quá 02 giờ.</p>

<p>5. Mỗi giờ chậm trễ bị phạt 10USD bằng thẻ đỏ để khấu trừ vào chi phí sửa chữa nếu có về sau.</p>

<p>6. Có quyền KHÔNG thanh toán chi phí nhân công cài đặt, sửa chữa nếu không hài lòng phong cách và thái độ phục vụ của nhân viên kỹ thuật.</p>
', 0, N'1/2.9" Aptina 1.3 Megapixel CMOS', 0, 0)
INSERT [dbo].[Products] ([Id], [Name], [SeName], [ImageDefault], [ImageFolder], [Link], [Order], [IsPublished], [Description], [Price], [Code], [ImageCart], [PromotionPrice], [PromotionPercent], [ProductCode], [OriginalValue], [Language_Id], [Category_Id], [MetaTitle], [MetaDescription], [MetaKeywords], [MetaImage], [IsVideo], [VideoId], [DetailInfo], [Advantages], [Instruction], [SalesCommitment], [IsPromotion], [ShortDescription], [IsPriceContact], [IsLandingpage]) VALUES (4, N'NGUỒN CAMERA 12V-30A', N'nguon-camera-12v-30a', N'/Upload/Products/9be08af9-9345-4cdc-a668-ca4a998301d7/3957-camera-ezviz-cs-cv246-1080p.jpg', N'/Upload/Products/9be08af9-9345-4cdc-a668-ca4a998301d7', NULL, 0, 1, N'<p>- Nguồn chuyên dụng cho các camera quan sát.</p>
', CAST(200000 AS Decimal(18, 0)), NULL, NULL, CAST(180000 AS Decimal(18, 0)), 10, N'12V-30A   ', N'd668da59-f3c2-46c4-a806-edaba7afb76c', 1, 18, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, N'<p>1. Hàng chính hãng, mới 100%.</p>

<p>2. Đổi mới 100% nếu xảy ra hư hỏng phần cứng trong tháng đầu tiên.</p>

<p>3. Đổi mới 100% nếu hư hỏng sửa chữa 03 lần liên tiếp nhưng không khắc phục được.</p>

<p>4. Thời gian có mặt để xử lý khi nhận được báo hư không quá 02 giờ.</p>

<p>5. Mỗi giờ chậm trễ bị phạt 10USD bằng thẻ đỏ để khấu trừ vào chi phí sửa chữa nếu có về sau.</p>

<p>6. Có quyền KHÔNG thanh toán chi phí nhân công cài đặt, sửa chữa nếu không hài lòng phong cách và thái độ phục vụ của nhân viên kỹ thuật.</p>
', 1, N' Nguồn chuyên dụng cho các camera quan sát.', 0, 0)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[Regions] ON 

INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (1, N'TP. Hồ Chí Minh', 106.6296638, 10.8230989, 1, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (2, N'Hà Nội', 105.801943921748, 21.022773185834, 2, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (3, N'Quận Tân Phú', 106.628189, 10.790052, 17, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (4, N'Quận Thủ Đức', 106.753708, 10.849409, 13, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (5, N'Quận Ba Đình', 105.809994, 21.0349665, 1, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (6, N'Quận 1', 106.700424, 10.775659, 1, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (7, N'Quận 2', 106.749809, 10.787273, 2, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (8, N'Quận 3', 106.68441, 10.78437, 3, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (9, N'Quận 4', 106.701294, 10.757826, 4, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (10, N'Quận 5', 106.663375, 10.754028, 5, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (11, N'Quận 6', 106.635236, 10.748093, 6, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (12, N'Quận 7', 106.721581, 10.734034, 7, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (13, N'Quận 8', 106.665913, 10.740353, 8, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (14, N'Quận 9', 106.818608, 10.8258, 9, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (15, N'Quận 10', 106.667953, 10.774596, 10, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (16, N'Quận 11', 106.650085, 10.762974, 11, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (17, N'Quận 12', 106.641335, 10.867153, 12, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (18, N'Quận Bình Thạnh', 106.709145, 10.810583, 14, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (19, N'Quận Gò Vấp', 106.665291, 10.838678, 15, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (20, N'Quận Phú Nhuận', 106.680264, 10.799194, 16, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (21, N'Quận Tân Bình', 106.652596, 10.801466, 18, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (22, N'Quận Bình Tân', 106.603851, 10.765258, 19, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (23, N'Huyện Nhà Bè', 106.704874, 10.695264, 20, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (24, N'Huyện Bình Chánh', 106.593857, 10.687392, 21, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (25, N'Huyện Hóc Môn', 106.5925, 10.87833333, 22, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (26, N'Huyện Củ Chi', 106.49222, 10.97528, 23, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (27, N'Huyện Cần Giờ', 106.919999, 10.386938, 24, 1, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (28, N'Quận Hoàn Kiếm', 105.8525, 21.02889, 2, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (29, N'Quận Hai Bà Trưng', 105.84778, 21.01167, 3, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (30, N'Quận Đống Đa', 105.8333, 21.0167, 4, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (31, N'Quận Tây Hồ', 105.81194, 21.07083, 5, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (32, N'Quận Cầu Giấy', 105.8, 21.0333, 6, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (33, N'Quận Thanh Xuân', 105.79845, 20.99345, 7, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (34, N'Quận Hoàng Mai', 105.84833, 20.96806, 8, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (35, N'Quận Long Biên', 105.88583, 21.0175, 9, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (36, N'Quận Bắc Từ Liêm', 105.768, 21.0394, 10, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (37, N'Huyện Thanh Trì', 105.85, 20.95, 29, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (38, N'Huyện Gia Lâm', 105.95, 21.05, 15, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (39, N'Huyện Đông Anh', 105.8333, 21.1667, 13, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (40, N'Huyện Sóc Sơn', 105.82917, 21.27083, 14, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (41, N'Quận Hà Đông', 105.76861, 20.95472, 12, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (42, N'Huyện Ba Vì', 105.3826, 21.1378, 16, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (43, N'Huyện Phúc Thọ', 105.53694, 21.10861, 17, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (44, N'Huyện Thạch Thất', 105.5833, 21.0667, 18, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (45, N'Huyện Quốc Oai', 105.6167, 20.9333, 19, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (46, N'Huyện Chương Mỹ', 105.6667, 20.8833, 20, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (47, N'Huyện Đan Phượng', 105.678603, 21.119193, 21, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (48, N'Huyện Hoài Đức', 105.7, 21, 22, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (49, N'Huyện Thanh Oai', 105.8, 20.8833, 23, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (50, N'Huyện Mỹ Đức', 105.7167, 20.6667, 24, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (51, N'Huyện Ứng Hòa', 105.7833, 20.7167, 25, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (52, N'Huyện Thường Tín', 105.9167, 20.8667, 26, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (53, N'Huyện Phú Xuyên', 105.9167, 20.7167, 27, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (54, N'Huyện Mê Linh', 105.6667, 21.1667, 28, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (55, N'Quận Nam Từ Liêm', 105.768, 21.0394, 11, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (56, N'Thị xã Sơn Tây', 105.479, 21.1022, 30, 2, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (57, N'Thành phố Đà Nẵng', 108.1994819636748, 16.04486485380982, 3, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (58, N'Thành phố Cần Thơ', 105.77867746331324, 10.034119028345181, 4, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (59, N'Thành phố Hải Phòng', 106.670951839769, 20.8477965136903, 5, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (60, N'An Giang', 105.125893, 10.521584, 6, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (61, N'Bà Rịa Vũng Tàu', 107.111833, 10.375811, 7, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (62, N'Bạc Liêu', 105.716317, 9.270596, 8, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (63, N'Bắc Giang', 106.629128, 21.301495, 9, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (64, N'Bắc Kạn', 105.876007, 22.303291, 10, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (65, N'Bắc Ninh', 106.06214, 21.178743, 11, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (66, N'Bến Tre', 106.374008, 10.240797, 12, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (67, N'Bình Dương', 106.659146, 11.068104, 13, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (68, N'Bình Định', 109.183457, 13.789571, 14, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (69, N'Bình Phước', 106.886422, 11.543215, 15, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (70, N'Bình Thuận', 108.072075, 11.09037, 16, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (71, N'Cao Bằng', 106.252213, 22.635689, 17, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (72, N'Cà Mau', 105.196075, 9.152673, 18, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (73, N'Gia Lai', 108.109375, 13.807894, 19, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (74, N'Hòa Bình', 105.313118, 20.686127, 20, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (75, N'Hà Giang', 104.938889, 22.766207, 21, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (76, N'Hà Nam', 105.922989, 20.583521, 22, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (77, N'Hà Tĩnh', 105.90145, 18.35515, 23, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (78, N'Hưng Yên', 106.016998, 20.852571, 24, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (79, N'Hải Dương', 106.314552, 20.937342, 25, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (80, N'Hậu Giang', 105.641251, 9.757898, 26, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (81, N'Điện Biên', 103.049631, 21.466667, 27, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (82, N'Đăk Lăk', 108.092071, 12.682331, 28, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (83, N'Đăk Nông', 107.662955, 12.237052, 29, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (84, N'Đồng Nai', 106.84993742848748, 10.969842372989961, 30, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (85, N'Đồng Tháp', 105.7667, 10.3, 31, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (86, N'Khánh Hòa', 109.18333, 12.25, 32, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (87, N'Kiên Giang', 105.21821, 9.971666, 33, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (88, N'Kon Tum', 108.01, 14.35, 34, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (89, N'Lai Châu', 103.296129, 22.338163, 35, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (90, N'Long An', 106.357040398289, 10.545646391944, 36, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (91, N'Lào Cai', 103.995, 22.4194, 37, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (92, N'Lâm Đồng', 108.410339365946, 11.9183038223416, 38, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (93, N'Lạng Sơn', 106.75778, 21.84778, 39, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (94, N'Nam Định', 106.16833, 20.42, 40, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (95, N'Nghệ An', 104.841692, 19.284088, 41, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (96, N'Ninh Bình', 105.975, 20.25389, 42, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (97, N'Ninh Thuận', 109.00256, 11.699326, 43, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (98, N'Phú Thọ', 105.111032, 21.310327, 44, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (99, N'Phú Yên', 109.285975, 13.076413, 45, NULL, 1, 1)
GO
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (100, N'Quảng Bình', 106.332614, 17.506271, 46, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (101, N'Quảng Nam', 108.071207, 15.525857, 47, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (102, N'Quãng Ngãi', 108.8, 15.13, 48, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (103, N'Quảng Ninh', 107.129841, 21.131126, 49, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (104, N'Quảng Trị', 106.987843, 16.790085, 50, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (105, N'Sóc Trăng', 105.97, 9.6, 51, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (106, N'Sơn La', 104.129982, 21.315469, 52, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (107, N'Thanh Hóa', 105.77639, 19.8075, 53, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (108, N'Thái Bình', 106.32, 20.44, 54, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (109, N'Thái Nguyên', 105.8333, 21.6, 55, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (110, N'Thừa Thiên Huế', 107.620862, 16.421597, 56, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (111, N'Tiền Giang', 106.33449, 10.400458, 57, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (112, N'Trà Vinh', 106.33, 9.93, 58, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (113, N'Tuyên Quang', 105.21667, 21.81667, 59, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (114, N'Tây Ninh', 106.103918, 11.371542, 60, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (115, N'Vĩnh Long', 105.96, 10.26, 61, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (116, N'Vĩnh Phúc', 105.60095, 21.310512, 62, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (117, N'Yên Bái', 104.882907, 21.702147, 63, NULL, 1, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (118, N'Huyện Thuận An', 106.699289, 10.900114, 0, 67, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (119, N'Quận Ninh Kiều', 105.768536, 10.013869, 0, 58, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (120, N'Quận Hải Châu', 108.21615, 16.0661, 0, 57, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (121, N'Hải Phòng', 106.678066, 20.854628, 0, 59, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (122, N'An Giang', 105.384578, 10.410129, 0, 60, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (123, N'Vũng Tàu', 107.091119, 10.353472, 0, 61, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (124, N'Bạc Liêu', 105.715971, 9.292794, 0, 62, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (125, N'Bắc Giang', 106.672505, 21.365694, 0, 63, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (126, N'Bắc Kạn', 105.825389, 22.16192, 0, 64, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (127, N'Bắc Ninh', 106.069229, 21.180148, 0, 65, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (128, N'Bến Tre', 106.367087, 10.247364, 0, 66, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (129, N'Bình Dương', 106.367039, 11.068104, 0, 67, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (130, N'Bình Định', 109.183457, 13.789571, 0, 68, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (131, N'Bình Phước', 106.886422, 11.543215, 0, 69, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (132, N'Bình Thuận', 108.072075, 11.09037, 0, 70, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (133, N'Cao Bằng', 106.252213, 22.635689, 0, 71, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (134, N'Cà Mau', 105.196075, 9.152673, 0, 72, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (135, N'Gia Lai', 108.109375, 13.807894, 0, 73, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (136, N'Hòa Bình', 105.313118, 20.686127, 0, 74, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (137, N'Hà Giang', 104.938889, 22.766207, 0, 75, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (138, N'Hà Nam', 105.922989, 20.583521, 0, 76, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (139, N'Hà Tĩnh', 105.90145, 18.35515, 0, 77, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (140, N'Hưng Yên', 106.016998, 20.852571, 0, 78, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (141, N'Hải Dương', 106.314552, 20.937342, 0, 79, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (142, N'Hậu Giang', 105.641251, 9.757898, 0, 80, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (143, N'Điện Biên', 103.049631, 21.466667, 0, 81, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (144, N'Đăk Lăk', 108.092071, 12.682331, 0, 82, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (145, N'Đăk Nông', 107.662955, 12.237052, 0, 83, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (146, N'Sơn La', 104.129982, 21.315469, 0, 106, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (147, N'Đồng Nai', 106.877048, 10.954135, 0, 84, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (148, N'Đồng Tháp', 105.7667, 10.3, 0, 85, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (149, N'Khánh Hòa', 109.18333, 12.25, 0, 86, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (150, N'Kiên Giang', 105.21821, 9.971666, 0, 87, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (151, N'Kon Tum', 108.01, 14.35, 0, 88, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (152, N'Lai Châu', 103.296129, 22.338163, 0, 89, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (153, N'Thanh Khê', 108.19121, 16.0714, 0, 57, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (154, N'Nghệ An', 104.841692, 19.284088, 0, 95, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (155, N'Tiền Giang', 106.33449, 10.400458, 0, 111, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (156, N'Thái Nguyên', 105.8333, 21.6, 0, 109, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (157, N'Long An', 106.357040398289, 10.545646391944, 0, 90, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (158, N'Lào Cai', 103.995, 22.4194, 0, 91, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (159, N'Lâm Đồng', 108.410339365946, 11.9183038223416, 0, 92, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (160, N'Lạng Sơn', 106.75778, 21.84778, 0, 93, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (161, N'Nam Định', 106.16833, 20.42, 0, 94, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (162, N'Ninh Bình', 105.975, 20.25389, 0, 96, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (163, N'Ninh Thuận', 109.00256, 11.699326, 0, 97, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (164, N'Phú Thọ', 105.111032, 21.310327, 0, 98, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (165, N'Phú Yên', 109.285975, 13.076413, 0, 99, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (166, N'Quảng Bình', 106.332614, 17.506271, 0, 100, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (167, N'Quảng Nam', 108.071207, 15.525857, 0, 101, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (168, N'Quảng Ngãi', 108.8, 15.13, 0, 102, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (169, N'Quảng Ninh', 107.129841, 21.131126, 0, 103, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (170, N'Quảng Trị', 106.987843, 16.790085, 0, 104, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (171, N'Sóc Trăng', 105.97, 9.6, 0, 105, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (172, N'Thanh Hóa', 105.77639, 19.8075, 0, 107, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (173, N'Thái Bình', 106.32, 20.44, 0, 108, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (174, N'Thừa Thiên Huế', 107.620862, 16.421597, 0, 110, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (175, N'Trà Vinh', 106.364856, 9.93, 0, 112, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (176, N'Tuyên Quang', 105.21667, 21.81667, 0, 113, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (177, N'Tây Ninh', 106.103918, 11.371542, 0, 114, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (178, N'Vĩnh Long', 106.10529, 10.26, 0, 115, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (179, N'Vĩnh Phúc', 105.60095, 21.310512, 0, 116, 2, 1)
INSERT [dbo].[Regions] ([Id], [Name], [Lng], [Lat], [Order], [Parent_Id], [RegionType], [IsPublished]) VALUES (180, N'Yên Bái', 104.882907, 21.702147, 0, 117, 2, 1)
SET IDENTITY_INSERT [dbo].[Regions] OFF
GO
SET IDENTITY_INSERT [dbo].[RoleContentBehaviors] ON 

INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (620, 1, 1)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (621, 1, 5)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (622, 1, 10)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (623, 1, 14)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (624, 1, 18)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (625, 1, 22)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (626, 1, 53)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (627, 1, 31)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (628, 1, 61)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (629, 1, 57)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (630, 1, 49)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (631, 1, 78)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (632, 1, 81)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (633, 1, 66)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (634, 1, 70)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (635, 1, 74)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (636, 1, 39)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (637, 1, 11)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (638, 1, 15)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (639, 1, 19)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (640, 1, 23)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (641, 1, 54)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (642, 1, 32)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (643, 1, 63)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (644, 1, 58)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (645, 1, 50)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (646, 1, 82)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (647, 1, 67)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (648, 1, 71)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (649, 1, 75)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (650, 1, 3)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (651, 1, 7)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (652, 1, 12)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (653, 1, 16)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (654, 1, 20)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (655, 1, 24)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (656, 1, 55)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (657, 1, 33)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (658, 1, 64)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (659, 1, 59)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (660, 1, 51)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (661, 1, 83)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (662, 1, 68)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (663, 1, 72)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (664, 1, 76)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (665, 1, 13)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (666, 1, 17)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (667, 1, 21)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (668, 1, 25)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (669, 1, 56)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (670, 1, 34)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (671, 1, 65)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (672, 1, 60)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (673, 1, 52)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (674, 1, 80)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (675, 1, 84)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (676, 1, 69)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (677, 1, 73)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (678, 1, 77)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (679, 1, 79)
INSERT [dbo].[RoleContentBehaviors] ([Id], [Role_RoleId], [ContentBehavior_Id]) VALUES (680, 1, 40)
SET IDENTITY_INSERT [dbo].[RoleContentBehaviors] OFF
GO
SET IDENTITY_INSERT [dbo].[Settings] ON 

INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (16, N'SmtpServer', N'smtp.gmail.com')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (17, N'SmtpPort', N'587')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (18, N'UserEmailAccountName', N'abc@gmail.com')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (19, N'EmailAccountPassword', N'abc@654321')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (20, N'FromEmailAccount', N'teouit@gmail.com')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (21, N'EnableSsl', N'true')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (22, N'SEOHomeTitle', N'Shop Tóc Xinh')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (23, N'SEOHomeDescription', N'Shop Tóc Xinh')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (24, N'SEOHomeKeyword', N'Shop Tóc Xinh')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (26, N'FacebookPage', N'https://www.facebook.com/itcvietnam/')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (27, N'YoutubePage', N'https://www.youtube.com/channel/UCCoIHX4YKlLpOz0S0p7qHSQ/videos')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (28, N'MailSubject', N'Shop Tóc Xinh')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (29, N'MailBody', N'Chúng tôi đã nhận được thông tin của bạn và sẽ phản hồi sớm nhất có thể!')
INSERT [dbo].[Settings] ([Id], [Key], [Value]) VALUES (30, N'OrderMailBody', N'<div style="border-bottom:solid 2px #5d5d5d;padding: 7px;"><img alt="logo" src="http://test.shopgoihandmade.com/Content/layout/resources/images/logo.png" style="width: 160px; height: 148px;" /></div>

<p>&nbsp;</p>

<p>T&oacute;c Xinh ch&acirc;n th&agrave;nh cảm ơn bạn <strong>{UserName}</strong> đ&atilde; đặt h&agrave;ng tại <a href="http://tocmay.com">tocmay.com</a><br />
M&atilde; số đơn h&agrave;ng của bạn l&agrave; [TOCXINH-{OrderCode}]</p>

<p>Đơn h&agrave;ng của qu&yacute; kh&aacute;ch đ&atilde; được tiếp nhận v&agrave; nh&acirc;n vi&ecirc;n của T&oacute;c Xinh sẽ li&ecirc;n hệ qu&yacute; kh&aacute;ch để x&aacute;c nhận đơn h&agrave;ng trong v&ograve;ng 24 giờ (giờ h&agrave;nh ch&iacute;nh).</p>

<p>&nbsp;</p>

<p>Dưới đ&acirc;y l&agrave; th&ocirc;ng tin đặt h&agrave;ng của bạn:<br />
Họ t&ecirc;n kh&aacute;ch h&agrave;ng: {UserName}<br />
Số điện thoại: {UserPhone}<br />
Email: {UserEmail}<br />
Địa chỉ nhận h&agrave;ng: {UserAddress}</p>

<p>Thời gian giao h&agrave;ng dự kiến:&nbsp;Kể từ khi x&aacute;c nhận&nbsp;th&agrave;nh c&ocirc;ng, đơn h&agrave;ng dự kiến sẽ giao đến qu&yacute; kh&aacute;ch trong v&ograve;ng:<br />
2 ng&agrave;y trong khu vực nội th&agrave;nh H&agrave; Nội v&agrave; Th&agrave;nh phố Hồ Ch&iacute; Minh<br />
3 - 5 ng&agrave;y tại c&aacute;c khu vực kh&aacute;c</p>

<p>Ph&iacute; vận chuyển: <strong>Miễn Ph&iacute;</strong><br />
Chi tiết đơn h&agrave;ng:<br />
{Order}<br />
Tổng gi&aacute; trị thanh to&aacute;n: {OrderTotal}<br />
Ghi ch&uacute; kh&aacute;c:{OrderNote}</p>

<p>Trường hợp qu&yacute; kh&aacute;ch cần hỗ trợ, vui l&ograve;ng li&ecirc;n hệ Hotline Chăm s&oacute;c kh&aacute;ch h&agrave;ng:<br />
CSKH ph&iacute;a Bắc: 090.170.1516 - 510<br />
CSKH ph&iacute;a Nam: 090.170.1516 - 610<br />
Một lần nữa, T&oacute;c Xinh cảm ơn qu&yacute; kh&aacute;ch.</p>

<p>&nbsp;</p>
')
SET IDENTITY_INSERT [dbo].[Settings] OFF
GO
SET IDENTITY_INSERT [dbo].[Solutions] ON 

INSERT [dbo].[Solutions] ([Id], [Title], [Link], [OriginalValue], [IsPublished], [Order], [Language_Id]) VALUES (1, N'Bạn cảm thấy bối rối khi phải chọn lựa từng thiết bị camera 1?', N'https://vnexpress.net/', N'00000000-0000-0000-0000-000000000000', 1, 1, 1)
INSERT [dbo].[Solutions] ([Id], [Title], [Link], [OriginalValue], [IsPublished], [Order], [Language_Id]) VALUES (2, N'Bạn cảm thấy bối rối khi phải chọn lựa từng thiết bị camera 2?', N'https://vnexpress.net/', N'00000000-0000-0000-0000-000000000000', 1, 2, 1)
INSERT [dbo].[Solutions] ([Id], [Title], [Link], [OriginalValue], [IsPublished], [Order], [Language_Id]) VALUES (3, N'Bạn cảm thấy bối rối khi phải chọn lựa từng thiết bị camera 3?', N'https://vnexpress.net/', N'00000000-0000-0000-0000-000000000000', 1, 3, 1)
INSERT [dbo].[Solutions] ([Id], [Title], [Link], [OriginalValue], [IsPublished], [Order], [Language_Id]) VALUES (4, N'Bạn cảm thấy bối rối khi phải chọn lựa từng thiết bị camera 4?', N'https://vnexpress.net/', N'00000000-0000-0000-0000-000000000000', 1, 4, 1)
INSERT [dbo].[Solutions] ([Id], [Title], [Link], [OriginalValue], [IsPublished], [Order], [Language_Id]) VALUES (5, N'Bạn cảm thấy bối rối khi phải chọn lựa từng thiết bị camera 5?', N'https://vnexpress.net/', N'00000000-0000-0000-0000-000000000000', 1, 5, 1)
INSERT [dbo].[Solutions] ([Id], [Title], [Link], [OriginalValue], [IsPublished], [Order], [Language_Id]) VALUES (6, N'Bạn cảm thấy bối rối khi phải chọn lựa từng thiết bị camera 6?', N'https://vnexpress.net/', N'00000000-0000-0000-0000-000000000000', 1, 6, 1)
SET IDENTITY_INSERT [dbo].[Solutions] OFF
GO
SET IDENTITY_INSERT [dbo].[StringResourceKeys] ON 

INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (1, N'HotLineTitle')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (2, N'HotLineContent')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (3, N'EmailContent')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (4, N'InputSearchKeywords')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (5, N'WorkingHourTitle')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (6, N'WorkingHourContent')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (7, N'LeftTitleFooterTitle')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (8, N'MiddleTitleFooter')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (9, N'RightTitleFooter')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (10, N'OtherProducts')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (11, N'OtherNews')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (12, N'OtherSolutions')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (13, N'OtherProjects')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (14, N'Fullname')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (15, N'PhoneNumber')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (16, N'Email')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (17, N'Content')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (18, N'IntroductionSectionTitle')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (19, N'ProductSectionTitle')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (20, N'SolutionSectionTitle')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (21, N'ProjectSectionTitle')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (22, N'NewsSectionTitle')
INSERT [dbo].[StringResourceKeys] ([Id], [Name]) VALUES (23, N'Address')
SET IDENTITY_INSERT [dbo].[StringResourceKeys] OFF
GO
SET IDENTITY_INSERT [dbo].[StringResourceValues] ON 

INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (1, N'Hotline', 1, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (2, N'0988 802 432', 2, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (3, N'vietitcgroup@gmail.com', 3, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (4, N'Nhập từ khóa tìm kiếm', 4, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (5, N'Giờ làm việc', 5, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (6, N'8h00 đến 17h00', 6, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (7, N'Địa chỉ', 7, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (8, N'Menu', 8, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (9, N'Facebook', 9, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (10, N'Sản phẩm tương tự', 10, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (11, N'Tin tức liên quan', 11, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (12, N'Giải pháp khác', 12, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (13, N'Dự án khác', 13, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (14, N'Họ tên', 14, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (15, N'Số điện thoại', 15, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (16, N'Email', 16, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (17, N'Nội dung', 17, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (18, N'Giới thiệu', 18, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (19, N'Sản phẩm', 19, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (20, N'Giải pháp', 20, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (21, N'Dự án', 21, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (22, N'Tin tức', 22, 1)
INSERT [dbo].[StringResourceValues] ([Id], [Value], [Key_Id], [Language_Id]) VALUES (23, N'Địa chỉ', 23, 1)
SET IDENTITY_INSERT [dbo].[StringResourceValues] OFF
GO
SET IDENTITY_INSERT [dbo].[UrlRecords] ON 

INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (1, N'trang-chu', N'Trang chủ', N'/trang-chu', 2, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (2, N'san-pham', N'Sản phẩm', N'/san-pham', 3, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (3, N'camera-quan-sat', N'Camera quan sát', N'/san-pham/camera-quan-sat', 4, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (4, N'camera-hlkvision', N'Camera Hlkvision', N'/san-pham/camera-quan-sat/camera-hlkvision', 5, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (5, N'camera-hitech', N'Camera Hitech', N'/san-pham/tong-dai-dien-thoai/camera-hitech', 6, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (6, N'analog-camera-series-hd', N'Analog Camera Series HD', N'/san-pham/tong-dai-dien-thoai/camera-hitech/analog-camera-series-hd', 7, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (7, N'hitech-pro-2002hd', N'Hitech Pro 2002HD', N'/san-pham/tong-dai-dien-thoai/camera-hitech/analog-camera-series-hd/hitech-pro-2002hd', 2, 4, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (8, N'gioi-thieu', N'Giới thiệu', N'/gioi-thieu', 8, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (9, N'tong-dai-dien-thoai', N'Tổng đài điện thoại', N'/san-pham/tong-dai-dien-thoai', 9, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (10, N'tong-dai-panasonic', N'Tổng đài panasonic', N'/san-pham/tong-dai-dien-thoai/tong-dai-panasonic', 10, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (11, N'bao-chay-bao-trom', N'Báo cháy báo trộm', N'/san-pham/bao-chay-bao-trom', 11, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (12, N'giai-phap', N'Giải pháp', N'/giai-phap', 12, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (13, N'du-an', N'Dự Án', N'/du-an', 13, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (14, N'tin-tuc', N'Tin tức', N'/tin-tuc', 14, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (15, N'lien-he', N'Liên hệ', N'/lien-he', 15, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (16, N'camera-ip-hikvision', N'Camera IP HIKVision', N'/san-pham/camera-quan-sat/camera-hlkvision/camera-ip-hikvision', 16, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (17, N'camera-analog-hikvision', N'Camera Analog HIKVision', N'/san-pham/camera-quan-sat/camera-hlkvision/camera-analog-hikvision', 17, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (18, N'panasonic', N'Panasonic', N'/san-pham/camera-quan-sat/panasonic', 18, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (19, N'camera-avtech', N'Camera AVTech', N'/san-pham/camera-quan-sat/camera-avtech', 19, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (20, N'phu-kien-camera', N'Phụ kiện Camera', N'/san-pham/camera-quan-sat/phu-kien-camera', 20, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (21, N'camera-samsung', N'Camera SamSung', N'/san-pham/camera-quan-sat/camera-samsung', 21, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (22, N'camera-hikvision-plus', N'Camera HIKVision Plus', N'/san-pham/camera-quan-sat/camera-hikvision-plus', 22, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (23, N'nguon-camera-12v-30a', N'NGUỒN CAMERA 12V-30A', N'/san-pham/camera-quan-sat/panasonic/nguon-camera-12v-30a', 4, 4, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (25, N'landing-page', N'Landing page', N'/san-pham/bao-chay-bao-trom/landing-page', 24, 3, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (26, N'du-an-1', N'dự án 1', N'/du-an/du-an-1', 1, 1, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (27, N'du-an-2', N'dự án 2', N'/du-an/du-an-2', 2, 1, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (28, N'du-an-3', N'Dự án 3', N'/du-an/du-an-3', 3, 1, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (29, N'du-an-4', N'Dự án 4', N'/du-an/du-an-4', 4, 1, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (30, N'du-an-5', N'Dự án 5', N'/du-an/du-an-5', 5, 1, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (31, N'du-an-6', N'Dự án 6', N'/du-an/du-an-6', 6, 1, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (32, N'du-an-7', N'Dự án 7', N'/du-an/du-an-7', 7, 1, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (33, N'du-an-8', N'Dự án 8', N'/du-an/du-an-8', 8, 1, 1)
INSERT [dbo].[UrlRecords] ([Id], [SeName], [Title], [Url], [EntityId], [EntityType], [Language_Id]) VALUES (10026, N'download', N'Download', N'/download', 25, 3, 1)
SET IDENTITY_INSERT [dbo].[UrlRecords] OFF
GO
SET IDENTITY_INSERT [dbo].[UserProfile] ON 

INSERT [dbo].[UserProfile] ([UserId], [UserName], [Email], [IsActive], [LastedIP]) VALUES (7, N'administrator', N'admin@gmail.com', 1, NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [Email], [IsActive], [LastedIP]) VALUES (12, N'Root', N'tinhqit@gmail.com', 1, NULL)
SET IDENTITY_INSERT [dbo].[UserProfile] OFF
GO
SET IDENTITY_INSERT [dbo].[Videos] ON 

INSERT [dbo].[Videos] ([Id], [Name], [YoutubeLink], [Order], [IsPublished], [VideoId], [Phase], [TripName], [Story], [FullName], [Phone], [Email], [CreatedDate]) VALUES (2, N'NOVABEACH CAM RANH', NULL, 1, 1, N'1GJ0XWfhSqc', 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2019-03-06T23:33:10.187' AS DateTime))
INSERT [dbo].[Videos] ([Id], [Name], [YoutubeLink], [Order], [IsPublished], [VideoId], [Phase], [TripName], [Story], [FullName], [Phone], [Email], [CreatedDate]) VALUES (3, N'Avani Cam Ranh Resort & Villas', NULL, 2, 1, N'NX0N_fky4lI', 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2019-03-06T23:40:48.540' AS DateTime))
SET IDENTITY_INSERT [dbo].[Videos] OFF
GO
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (7, CAST(N'2018-10-14T15:42:26.703' AS DateTime), NULL, 1, CAST(N'2021-07-26T15:09:56.727' AS DateTime), 0, N'AL3sXs0CPetzy9F08p4Jp6I2KVjag4yLASldM/6Yrkb6KRTtWv89A1rCdE2u80qMAw==', CAST(N'2021-07-26T15:10:52.300' AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (12, CAST(N'2019-02-27T18:35:12.813' AS DateTime), NULL, 1, CAST(N'2021-07-26T14:55:34.927' AS DateTime), 0, N'ACgwfbdWPN1TiYZWF+BDbhlUD/AjF2m5XnQlQ0rzSzEYx4uZourIciLwQK8K7GdzrA==', CAST(N'2021-07-26T15:08:56.950' AS DateTime), N'', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[webpages_Roles] ON 

INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName], [RoleDescription], [IsSystem]) VALUES (1, N'Admin', N'Tài khoản quản trị toàn bộ nội dung', 0)
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName], [RoleDescription], [IsSystem]) VALUES (2, N'Mod', NULL, 0)
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName], [RoleDescription], [IsSystem]) VALUES (3, N'Customer', NULL, 0)
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName], [RoleDescription], [IsSystem]) VALUES (4, N'Root', NULL, 1)
SET IDENTITY_INSERT [dbo].[webpages_Roles] OFF
GO
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (7, 1)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (12, 4)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__webpages__8A2B6160E7D2124E]    Script Date: 7/26/2021 10:16:56 PM ******/
ALTER TABLE [dbo].[webpages_Roles] ADD UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF__MainProdu__Promo__01142BA1]  DEFAULT ((0)) FOR [PromotionPrice]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF__MainProdu__Promo__02084FDA]  DEFAULT ((0)) FOR [PromotionPercent]
GO
ALTER TABLE [dbo].[Products] ADD  DEFAULT ((0)) FOR [IsLandingpage]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  CONSTRAINT [DF__webpages___IsCon__4A03EDD9]  DEFAULT ((0)) FOR [IsConfirmed]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  CONSTRAINT [DF__webpages___Passw__4AF81212]  DEFAULT ((0)) FOR [PasswordFailuresSinceLastSuccess]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [Article_Category] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [Article_Category]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [FK_Articles_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [FK_Articles_Languages]
GO
ALTER TABLE [dbo].[Branches]  WITH CHECK ADD  CONSTRAINT [FK_Branches_Regions] FOREIGN KEY([Region_Id])
REFERENCES [dbo].[Regions] ([Id])
GO
ALTER TABLE [dbo].[Branches] CHECK CONSTRAINT [FK_Branches_Regions]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [Category_Parent] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [Category_Parent]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [FK_Categories_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [FK_Categories_Languages]
GO
ALTER TABLE [dbo].[ContentBehaviors]  WITH CHECK ADD  CONSTRAINT [FK_ContentBehaviors_Behaviors] FOREIGN KEY([Behavior_Id])
REFERENCES [dbo].[Behaviors] ([Id])
GO
ALTER TABLE [dbo].[ContentBehaviors] CHECK CONSTRAINT [FK_ContentBehaviors_Behaviors]
GO
ALTER TABLE [dbo].[ContentBehaviors]  WITH CHECK ADD  CONSTRAINT [FK_ContentBehaviors_Contents] FOREIGN KEY([Content_Id])
REFERENCES [dbo].[Contents] ([Id])
GO
ALTER TABLE [dbo].[ContentBehaviors] CHECK CONSTRAINT [FK_ContentBehaviors_Contents]
GO
ALTER TABLE [dbo].[Contents]  WITH CHECK ADD  CONSTRAINT [FK_Contents_ContentGroups] FOREIGN KEY([ContentGroup_Id])
REFERENCES [dbo].[ContentGroups] ([Id])
GO
ALTER TABLE [dbo].[Contents] CHECK CONSTRAINT [FK_Contents_ContentGroups]
GO
ALTER TABLE [dbo].[LandingpageGroups]  WITH CHECK ADD  CONSTRAINT [FK_LandingpageGroups_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[LandingpageGroups] CHECK CONSTRAINT [FK_LandingpageGroups_Languages]
GO
ALTER TABLE [dbo].[LandingpageItems]  WITH CHECK ADD  CONSTRAINT [FK_LandingpageItems_LandingpageGroups] FOREIGN KEY([LandingpageGroup_Id])
REFERENCES [dbo].[LandingpageGroups] ([Id])
GO
ALTER TABLE [dbo].[LandingpageItems] CHECK CONSTRAINT [FK_LandingpageItems_LandingpageGroups]
GO
ALTER TABLE [dbo].[LandingpageItems]  WITH CHECK ADD  CONSTRAINT [FK_LandingpageItems_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[LandingpageItems] CHECK CONSTRAINT [FK_LandingpageItems_Languages]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_Languages]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_Menus] FOREIGN KEY([Menu_Id])
REFERENCES [dbo].[Menus] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_Menus]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_Menus_Menus] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[MenuItems] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_Menus_Menus]
GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_OrderItems_MainProducts] FOREIGN KEY([Product_Id])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[OrderItems] CHECK CONSTRAINT [FK_OrderItems_MainProducts]
GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_OrderItems_Orders] FOREIGN KEY([Order_Id])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderItems] CHECK CONSTRAINT [FK_OrderItems_Orders]
GO
ALTER TABLE [dbo].[OrderLogs]  WITH CHECK ADD  CONSTRAINT [FK_OrderLogs_Orders] FOREIGN KEY([Order_Id])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderLogs] CHECK CONSTRAINT [FK_OrderLogs_Orders]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderTypes] FOREIGN KEY([OrderType_Id])
REFERENCES [dbo].[OrderTypes] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_OrderTypes]
GO
ALTER TABLE [dbo].[OrderTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrderTypes_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[OrderTypes] CHECK CONSTRAINT [FK_OrderTypes_Languages]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Categories] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Categories]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Languages]
GO
ALTER TABLE [dbo].[Regions]  WITH CHECK ADD  CONSTRAINT [FK_Regions_Regions] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[Regions] ([Id])
GO
ALTER TABLE [dbo].[Regions] CHECK CONSTRAINT [FK_Regions_Regions]
GO
ALTER TABLE [dbo].[RoleContentBehaviors]  WITH CHECK ADD  CONSTRAINT [FK_RoleContentBehaviors_ContentBehaviors] FOREIGN KEY([ContentBehavior_Id])
REFERENCES [dbo].[ContentBehaviors] ([Id])
GO
ALTER TABLE [dbo].[RoleContentBehaviors] CHECK CONSTRAINT [FK_RoleContentBehaviors_ContentBehaviors]
GO
ALTER TABLE [dbo].[RoleContentBehaviors]  WITH CHECK ADD  CONSTRAINT [FK_RoleContentBehaviors_webpages_Roles] FOREIGN KEY([Role_RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[RoleContentBehaviors] CHECK CONSTRAINT [FK_RoleContentBehaviors_webpages_Roles]
GO
ALTER TABLE [dbo].[SlideShows]  WITH CHECK ADD  CONSTRAINT [FK_SlideShows_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[SlideShows] CHECK CONSTRAINT [FK_SlideShows_Languages]
GO
ALTER TABLE [dbo].[Solutions]  WITH CHECK ADD  CONSTRAINT [FK_Solutions_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Solutions] CHECK CONSTRAINT [FK_Solutions_Languages]
GO
ALTER TABLE [dbo].[StringResourceValues]  WITH CHECK ADD  CONSTRAINT [FK_StringResourceValues_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[StringResourceValues] CHECK CONSTRAINT [FK_StringResourceValues_Languages]
GO
ALTER TABLE [dbo].[StringResourceValues]  WITH CHECK ADD  CONSTRAINT [StringResourceValue_Key] FOREIGN KEY([Key_Id])
REFERENCES [dbo].[StringResourceKeys] ([Id])
GO
ALTER TABLE [dbo].[StringResourceValues] CHECK CONSTRAINT [StringResourceValue_Key]
GO
ALTER TABLE [dbo].[UrlRecords]  WITH CHECK ADD  CONSTRAINT [FK_UrlRecords_Languages] FOREIGN KEY([Language_Id])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[UrlRecords] CHECK CONSTRAINT [FK_UrlRecords_Languages]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_RoleId]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_UserId]
GO
