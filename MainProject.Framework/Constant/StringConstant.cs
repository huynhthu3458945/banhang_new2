﻿namespace MainProject.Framework.Constant
{
    public static class StringConstant
    {
        public const string DbContextKeyName = "DbContextPerRequest";

        public const string CurrentLanguageKey = "UserCurrentLanguage";

        public const string ViewForRedirectToHomePage = "/Views/Shared/RedirectToHomePage.cshtml";

        public const string ViewForRedirectToAnotherPage = "/Views/Shared/RedirectToAnotherPage.cshtml";

        public const string DefaultAdministrator = "Root";
        //public const string DefaultAdministrator = "Administrator";
    }

    public static class RoleName
    {
        public const string RootUser = "Root";
        //public const string Admin = "Admin";

        //public const string Mod = "Mod";

        //public const string Employee = "Employee";

        //public const string Guest = "Guest";

        //public const string Customer = "Customer";
    }

    public static class SessionKey
    {
        public const string CurrentUser = "CurrentUser";
        
        public const string CurrentUserId = "CurrentUserId";

        public const string TimeBeginTesting = "TimeBeginTesting";

        public const string IdOfMediaCampaignsViewed = "IdOfMediaCampaignViewed";

        public const string ShoppingCartItems = "ShoppingCartItems";

        public const string OrderModel = "OrderModel";
    }
}
