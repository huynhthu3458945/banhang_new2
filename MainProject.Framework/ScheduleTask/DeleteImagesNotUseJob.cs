﻿using System;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Framework.Helper;
using Quartz;

namespace MainProject.Framework.ScheduleTask
{
    class DeleteImagesNotUseJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var dbContext = DalHelper.InitDbContext();
            dbContext.LogHistories.Add(new LogHistory
                                           {
                                               ActionBy = "quartz",
                                               ActionType = ActionTypeCollection.Create,
                                               Comment = "Schedule hang ngay",
                                               CreatedDate = DateTime.Now,
                                               EntityId = 1,
                                               EntityType = EntityTypeCollection.Categories
                                           });
            dbContext.SaveChanges();
            dbContext.Dispose();
        }
    }
}
