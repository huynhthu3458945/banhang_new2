﻿using System;
using System.Linq;
using MainProject.Framework.Models;

namespace MainProject.Framework.Helper
{
    public static class UrlRecordHelper
    {
        public static RouterModel GetUrlRecordFromUrl(string request)
        {
            var routerModel = new RouterModel();
            var paramValues = request.Split('/').ToList();
            var otherParams = string.Empty;
            if (paramValues.Count > 0)
            {
                var lastParamValue = paramValues[paramValues.Count - 1];
                if (!lastParamValue.Contains("."))
                {
                    var seName = lastParamValue;
                    //incase has param
                    if (seName.Contains("?"))
                    {
                        otherParams = seName.Substring(seName.IndexOf("?") + 1);
                        seName = seName.Substring(0, seName.IndexOf("?"));
                    }
                    var dbContext = DalHelper.InvokeDbContext();
                    var urlRecords = dbContext.UrlRecords.Where(c => c.SeName.Equals(seName)).ToList();
                    //incase has paging
                    var pageIndex = 0;
                    if (urlRecords.Count == 0 && paramValues.Count > 1)
                    {
                        try
                        {
                            pageIndex = Convert.ToInt32(lastParamValue);
                        }
                        catch (Exception) { }
                        if (pageIndex > 0)
                        {
                            lastParamValue = paramValues[paramValues.Count - 2];
                            seName = lastParamValue;
                            //incase has param
                            if (seName.Contains("?"))
                            {
                                otherParams = seName.Substring(seName.IndexOf("?") + 1);
                                seName = seName.Substring(0, seName.IndexOf("?"));
                            }
                            urlRecords = dbContext.UrlRecords.Where(c => c.SeName.Equals(seName)).ToList();

                            request = request.Substring(0, request.LastIndexOf("/"));
                        }
                    }
                    if (urlRecords.Count > 0)
                    {
                        foreach (var urlRecord in urlRecords)
                        {
                            //if (urlRecord.Url.Equals(request, StringComparison.OrdinalIgnoreCase))
                            //{
                            //    routerModel.HasResult = true;
                            //    routerModel.PageIndex = pageIndex;
                            //    routerModel.UrlRecord = urlRecord;
                            //    routerModel.UrlRequest = request;
                            //    routerModel.OtherParams = otherParams;

                            //    break;
                            //}   
                            if (urlRecord.Url.Equals(request.Replace(string.Format("?{0}", otherParams), ""), StringComparison.OrdinalIgnoreCase))
                            {
                                routerModel.HasResult = true;
                                routerModel.PageIndex = pageIndex;
                                routerModel.UrlRecord = urlRecord;
                                routerModel.UrlRequest = request;
                                routerModel.OtherParams = otherParams;

                                break;
                            }   
                        }
                    }
                }
            }
            return routerModel;
        }
    }
}
