﻿//using System.Linq;
//using System.Web;
//using System.Web.Security;
//using MainProject.Core.Enums;
//using MainProject.Framework.Constant;

//namespace MainProject.Framework.Helper
//{
//    public static class EntityManageTypeCollectionExtension
//    {
//        public static bool ValidatePermission(this EntityManageTypeCollection entityManageType)
//        {
//            var isValid = false;
//            switch (entityManageType)
//            {
//                case EntityManageTypeCollection.ManageCategories:
//                case EntityManageTypeCollection.ManageContact:
//                case EntityManageTypeCollection.ManageSlide:
//                    isValid = (Roles.IsUserInRole(RoleName.Admin) || Roles.IsUserInRole(RoleName.Mod));
//                    break;
//                case EntityManageTypeCollection.ManageVariables:
//                case EntityManageTypeCollection.ManageSettings:
//                case EntityManageTypeCollection.ManageUsers:
//                case EntityManageTypeCollection.ManagePermissions:
//                case EntityManageTypeCollection.ManageBlackWords:
//                case EntityManageTypeCollection.ManageEvents:
//                case EntityManageTypeCollection.ManageAttchmentCategories:
//                case EntityManageTypeCollection.ManageDistricts:
//                case EntityManageTypeCollection.ManageBranchs:
//                case EntityManageTypeCollection.ManagePollCategories:
//                case EntityManageTypeCollection.ManageResource:
//                case EntityManageTypeCollection.ManageFaqCategory:
//                    isValid = Roles.IsUserInRole(RoleName.Admin);
//                    break;
//                case EntityManageTypeCollection.ManageNews:
//                case EntityManageTypeCollection.ManageProduct:
//                    isValid = (Roles.IsUserInRole(RoleName.Admin) || Roles.IsUserInRole(RoleName.Mod));
//                    if (!isValid && Roles.IsUserInRole(RoleName.Mod))
//                    {
//                        var cateId = ConfigItemHelper.GetRootNewsId();
//                        isValid = CheckHasPermissionOnCategorySystem(cateId);
//                    }
//                    break;
//                case EntityManageTypeCollection.ManageAttchments:
//                    isValid = (Roles.IsUserInRole(RoleName.Admin) || Roles.IsUserInRole(RoleName.Mod));
//                    if (!isValid && Roles.IsUserInRole(RoleName.Mod))
//                    {
//                        var cateId = ConfigItemHelper.GetRootAttachmentId();
//                        isValid = CheckHasPermissionOnCategorySystem(cateId);
//                    }
//                    break;
//                case EntityManageTypeCollection.ManageComments:
//                case EntityManageTypeCollection.ManagePolls:
//                case EntityManageTypeCollection.ManageFaqItem:
//                    isValid = Roles.IsUserInRole(RoleName.Admin);
//                    if (!isValid) isValid = CheckHasPermissionOnPermissionSystem(entityManageType);
//                    break;
//                case EntityManageTypeCollection.ManageOrders:
//                    isValid = (Roles.IsUserInRole(RoleName.Admin) || Roles.IsUserInRole(RoleName.Customer) || Roles.IsUserInRole(RoleName.Mod));
//                    break;
//            }
//            return isValid;
//        }

//        private static bool CheckHasPermissionOnCategorySystem(long cateId)
//        {
//            var isValid = false;
//            var currentUserName = HttpContext.Current.User.Identity.Name;
//            var dbContext = DalHelper.InvokeDbContext();

//            var entityIds =
//                dbContext.PermissionRefs.Where(c => c.UserName == currentUserName && c.EntityId > 0).Select(
//                    c => c.EntityId).ToList();
//            foreach (var entityId in entityIds)
//            {
//                var category = dbContext.Categories.FirstOrDefault(c => c.Id == entityId);
//                while (category != null)
//                {
//                    if (category.Id == cateId)
//                    {
//                        isValid = true;
//                        category = null;
//                    }
//                    else
//                    {
//                        category = category.Parent;
//                    }
//                }
//                if (isValid) break;
//            }
//            return isValid;
//        }

//        private static bool CheckHasPermissionOnPermissionSystem(EntityManageTypeCollection entityManageType)
//        {
//            var isValid = false;
//            var currentUserName = HttpContext.Current.User.Identity.Name;
//            var dbContext = DalHelper.InvokeDbContext();
//            var entityId = (int) entityManageType;
//            if (Roles.IsUserInRole(RoleName.Mod) && dbContext.PermissionRefs.Any(c=>c.UserName == currentUserName && c.EntityId == entityId))
//            {
//                isValid = true;
//            }
//            return isValid;
//        }
//    }
//}
