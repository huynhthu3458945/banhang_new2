﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace MainProject.Framework.Helper
{
    public static class EnumHelper
    {
        public static IDictionary<Enum, string> ToDictionary(this Enum enumObj)
        {
            return Enum.GetValues(enumObj.GetType())
                .Cast<Enum>().ToDictionary(t => t, t => t.GetDescription());
        }

        public static string GetDescription(this Enum value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            var description = value.ToString();
            var fieldInfo = value.GetType().GetField(description);
            var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
            {
                description = attributes[0].Description;
            }

            return description;
        }

        public static string GetName(this Enum value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            string name = value.ToString();
            var fieldInfo = value.GetType().GetField(name);

            return fieldInfo.Name;
        }

        public static void Copyavatar(string pathfileavatarTemp, string pathfileavatar)
        {
            if (!File.Exists(pathfileavatarTemp))
                return;
            File.Copy(pathfileavatarTemp, pathfileavatar, true);
            File.Delete(pathfileavatarTemp);
        }

        public static IDictionary<Enum, string> ToDictionary(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            var dics = new Dictionary<Enum, string>();
            var enumValues = Enum.GetValues(type);

            foreach (Enum value in enumValues)
            {
                dics.Add(value, GetDescription(value));
            }

            return dics;
        }

        public static List<SelectListItem> ToSelectList(Type type)
        {
            var enumValues = Enum.GetValues(type);
            var selectList = new List<SelectListItem>();

            foreach (Enum value in enumValues)
            {
                selectList.Add(new SelectListItem() { Text = GetDescription(value), Value = value.ToString()});
            }

            return selectList;
        }
    }
}
