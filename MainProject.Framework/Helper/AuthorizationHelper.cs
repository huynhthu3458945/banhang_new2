﻿//using System.Linq;
//using System.Web;
//using System.Web.Security;
//using MainProject.Core.Enums;
//using MainProject.Framework.Constant;

//namespace MainProject.Framework.Helper
//{
//    public static class AuthorizationHelper
//    {
//        public static bool CheckRoleForNewsAndAttachments(RoleManageTypeCollection roleManageType, long categoryId)
//        {
//            if (Roles.IsUserInRole(RoleName.Admin)) return true;
//            if (!Roles.IsUserInRole(RoleName.Mod)) return false;
//            if (EntityManageTypeCollection.ManageComments.ValidatePermission()) return true;

//            var dbContext = DalHelper.InvokeDbContext();
//            var currentUserName = HttpContext.Current.User.Identity.Name;
//            var isValid = false;
//            if (categoryId > 0)
//            {
//                var category = dbContext.Categories.FirstOrDefault(c => c.Id == categoryId);
//                if (category != null)
//                {
//                    while (!isValid && category.Parent != null)
//                    {
//                        var item =
//                            dbContext.PermissionRefs.FirstOrDefault(
//                                c => c.EntityId == category.Id && c.UserName.ToLower() == currentUserName.ToLower());
//                        if (item != null)
//                        {
//                            switch (roleManageType)
//                            {
//                                case RoleManageTypeCollection.Editor:
//                                    if (item.IsEditor) isValid = true;
//                                    break;
//                                case RoleManageTypeCollection.Manager:
//                                    if (item.IsManager) isValid = true;
//                                    break;
//                                case RoleManageTypeCollection.Any:
//                                    isValid = item.IsEditor || item.IsManager;
//                                    break;
//                            }
//                            if (isValid) break;
//                        }
//                        category = category.Parent;
//                    }
//                }
//            }
//            return isValid;
//        }

//        public static bool CheckRoleForPoll(RoleManageTypeCollection roleManageType, string culture)
//        {
//            if (Roles.IsUserInRole(RoleName.Admin)) return true;
//            if (!Roles.IsUserInRole(RoleName.Mod)) return false;

//            const int enumValue = (int)EntityManageTypeCollection.ManagePolls;
//            return CheckForSystemPermission(roleManageType, enumValue, culture);
//        }

//        public static bool CheckRoleForComment(RoleManageTypeCollection roleManageType, string culture)
//        {
//            if (Roles.IsUserInRole(RoleName.Admin)) return true;
//            if (!Roles.IsUserInRole(RoleName.Mod)) return false;

//            const int enumValue = (int)EntityManageTypeCollection.ManageComments;
//            return CheckForSystemPermission(roleManageType, enumValue, culture);
//        }

//        public static bool CheckRoleForFaqItem(RoleManageTypeCollection roleManageType, string culture)
//        {
//            if (Roles.IsUserInRole(RoleName.Admin)) return true;
//            if (!Roles.IsUserInRole(RoleName.Mod)) return false;

//            const int enumValue = (int)EntityManageTypeCollection.ManageComments;
//            return CheckForSystemPermission(roleManageType, enumValue, culture);
//        }

//        private static bool CheckForSystemPermission(RoleManageTypeCollection roleManageType, int enumValue, string culture)
//        {
//            var dbContext = DalHelper.InvokeDbContext();
//            var currentUserName = HttpContext.Current.User.Identity.Name;

//            var isValid = false;
//            var item = dbContext.PermissionRefs.FirstOrDefault(c => c.EntityId == enumValue
//                                                                    && c.UserName.ToLower() == currentUserName.ToLower());
//            if (item != null)
//            {
//                switch (roleManageType)
//                {
//                    case RoleManageTypeCollection.Editor:
//                        if (item.IsEditor) isValid = true;
//                        break;
//                    case RoleManageTypeCollection.Manager:
//                        if (item.IsManager) isValid = true;
//                        break;
//                    case RoleManageTypeCollection.Any:
//                        isValid = item.IsEditor || item.IsManager;
//                        break;
//                }
//            }
//            return isValid;
//        }
//    }
//}
