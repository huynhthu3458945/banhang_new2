﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Helpers;

namespace MainProject.Framework.Helper
{
    public class MailHelper
    {
        public static void Send(IList<string> listMailToSend, string subject, string body, IEnumerable<string> filesToAttach = null)
        {
            var dbContext = DalHelper.InvokeDbContext();

            var smtpServerKey = ConfigItemHelper.GetSmtpServer();
            var userNameKey = ConfigItemHelper.GetUserEmailAccountName();
            var passWordKey = ConfigItemHelper.GetEmailAccountPassword();
            var fromKey = ConfigItemHelper.GetFromEmailAccount();

            var smtpServerSetting = dbContext.Settings.FirstOrDefault(d => d.Key == smtpServerKey);
            var userNameSetting = dbContext.Settings.FirstOrDefault(d => d.Key == userNameKey);
            var passWordSetting = dbContext.Settings.FirstOrDefault(d => d.Key == passWordKey);
            var fromSetting = dbContext.Settings.FirstOrDefault(d => d.Key == fromKey);

            string smtpServer = smtpServerSetting != null ? smtpServerSetting.Value : "";
            string userName = userNameSetting != null ? userNameSetting.Value : "";
            string passWord = passWordSetting != null ? passWordSetting.Value : "";
            WebMail.SmtpUseDefaultCredentials = false;
            try
            {
                WebMail.SmtpPort = Convert.ToInt32(SettingHelper.GetValueSetting(ConfigItemHelper.GetSmtpPort()));
            }
            catch (Exception)
            {
                WebMail.SmtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            }
            try
            {
                WebMail.EnableSsl = Convert.ToBoolean(SettingHelper.GetValueSetting(ConfigItemHelper.GetEnableSsl()));
            }
            catch (Exception)
            {
                WebMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            }
            WebMail.SmtpServer = smtpServer;
            WebMail.UserName = userName;
            WebMail.Password = passWord;
            var from = fromSetting != null ? fromSetting.Value : "";

            var emails = "";
            var count = 0;
            var totalCount = 0;
            var listEmails = new List<string>();
            foreach (var item in listMailToSend)
            {
                totalCount++;
                if (item.Count(c => c == '@') == 1)
                {
                    count++;
                    emails += item.Replace("\r\n", "") + ",";
                    if (count == 5 || totalCount == listMailToSend.Count())
                    {
                        emails = emails.Substring(0, emails.Length - 1);
                        listEmails.Add(emails);
                        count = 0;
                        emails = string.Empty;
                    }
                }
            }
            foreach (var item in listEmails)
            {
                WebMail.Send(to: from, subject: subject, body: body, bcc: item, filesToAttach: filesToAttach);
            }
        }
        public static void SendOrderInfo(IList<string> listMailToSend,
            MainProject.Core.Order model,
            List<MainProject.Core.OrderItem> orderItems,
            string body,
            IEnumerable<string> filesToAttach = null)
        {
            var dbContext = DalHelper.InvokeDbContext();

            var smtpServerKey = ConfigItemHelper.GetSmtpServer();
            var userNameKey = ConfigItemHelper.GetUserEmailAccountName();
            var passWordKey = ConfigItemHelper.GetEmailAccountPassword();
            var fromKey = ConfigItemHelper.GetFromEmailAccount();

            var smtpServerSetting = dbContext.Settings.FirstOrDefault(d => d.Key == smtpServerKey);
            var userNameSetting = dbContext.Settings.FirstOrDefault(d => d.Key == userNameKey);
            var passWordSetting = dbContext.Settings.FirstOrDefault(d => d.Key == passWordKey);
            var fromSetting = dbContext.Settings.FirstOrDefault(d => d.Key == fromKey);

            string smtpServer = smtpServerSetting != null ? smtpServerSetting.Value : "";
            string userName = userNameSetting != null ? userNameSetting.Value : "";
            string passWord = passWordSetting != null ? passWordSetting.Value : "";
            WebMail.SmtpUseDefaultCredentials = false;
            try
            {
                WebMail.SmtpPort = Convert.ToInt32(SettingHelper.GetValueSetting(ConfigItemHelper.GetSmtpPort()));
            }
            catch (Exception)
            {
                WebMail.SmtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            }
            try
            {
                WebMail.EnableSsl = Convert.ToBoolean(SettingHelper.GetValueSetting(ConfigItemHelper.GetEnableSsl()));
            }
            catch (Exception)
            {
                WebMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            }
            WebMail.SmtpServer = smtpServer;
            WebMail.UserName = userName;
            WebMail.Password = passWord;
            WebMail.From = "\"ITC Group\"" + userName;
            var fromEmail = fromSetting != null ? fromSetting.Value : "";

            var emails = "";
            var count = 0;
            var totalCount = 0;
            var listEmails = new List<string>();
            //Send email
            foreach (var item in listMailToSend)
            {
                totalCount++;
                if (item.Count(c => c == '@') == 1)
                {
                    count++;
                    emails += item.Replace("\r\n", "") + ",";
                    if (count == 5 || totalCount == listMailToSend.Count())
                    {
                        emails = emails.Substring(0, emails.Length - 1);
                        listEmails.Add(emails);
                        count = 0;
                        emails = string.Empty;
                    }
                }
            }

            // Replace UserName and OrderCode
            while (body.Contains("{UserName}")) body = body.Replace("{UserName}", model.BuyerName);
            while (body.Contains("{OrderCode}")) body = body.Replace("{OrderCode}", model.Id.ToString());
            while (body.Contains("{UserPhone}")) body = body.Replace("{UserPhone}", model.BuyerPhone);
            while (body.Contains("{UserEmail}")) body = body.Replace("{UserEmail}", model.BuyerEmail);
            while (body.Contains("{UserAddress}")) body = body.Replace("{UserAddress}", model.BuyerAddress);
            while (body.Contains("{OrderTotal}")) body = body.Replace("{OrderTotal}", string.Format("{0:0,0}", model.Amount));
            while (body.Contains("{OrderNote}")) body = body.Replace("{OrderNote}", model.Note);
            if (body.Contains("{Order}"))
            {
                var str =  "<table style=\"border: 1px solid #dddddd;border-collapse: collapse;text-align: center;\">"
                    + "<tr style=\"background-color: #873780;color: #fff\">"
                    + "<th style=\"padding: 8px;\">STT</th>"
                    + "<th style=\"padding: 8px;\">Sản phẩm</th>"
                    + "<th style=\"padding: 8px;\">Đơn giá(VND)</th>"
                    + "<th style=\"padding: 8px;\">Số lượng</th>"
                    + "<th style=\"padding: 8px;\">Giảm giá</th>"
                    + "<th style=\"padding: 8px;\">Thành tiền(VND)</th></tr>";
                var stt = 1;
                foreach (var item in orderItems)
                {
                    if (stt % 2 == 0)
                    {
                        str += "<tr style=\"border-bottom: 1px solid #dddddd;\">"
                        + "<td style=\"padding: 8px;\">" + (stt++) + "</td>"
                        + "<td style=\"padding: 8px;\">" + item.Product.Name + "</td>"
                        + "<td style=\"padding: 8px;\">" + string.Format("{0:0,0}", item.Product.Price) + "</td>"
                        + "<td style=\"padding: 8px;\">" + item.ItemCount + "</td>"
                        + "<td style=\"padding: 8px;\">" + item.Product.PromotionPercent + "%</td>"
                        + "<td style=\"padding: 8px;\">" + string.Format("{0:0,0}", item.Amount) + "</td>"
                        + "</tr>";
                    }
                    else
                    {
                        str += "<tr style=\"border-bottom: 1px solid #dddddd;background-color: #F6B26D;\">"
                        + "<td style=\"padding: 8px;\">" + (stt++) + "</td>"
                         + "<td style=\"padding: 8px;\">" + item.Product.Name + "</td>"
                        + "<td style=\"padding: 8px;\">" + string.Format("{0:0,0}", item.Product.Price) + "</td>"
                        + "<td style=\"padding: 8px;\">" + item.ItemCount + "</td>"
                         + "<td style=\"padding: 8px;\">" + item.Product.PromotionPercent + "%</td>"
                        + "<td style=\"padding: 8px;\">" + string.Format("{0:0,0}", item.Amount) + "</td>"
                         + "</tr>";
                    }

                }
                str += "<tr style=\"border-bottom: 1px solid #dddddd;background-color: #F6B26D;\">"
                    + "<td style=\"padding: 8px;\"></td>"
                    + "<td style=\"padding: 8px;\"></td>"
                    + "<td style=\"padding: 8px;\"></td>"
                    + "<td style=\"padding: 8px;\"></td>"
                    + "<td style=\"padding: 8px;\">TỔNG CỘNG</td>"
                    + "<td style=\"padding: 8px;\">" + string.Format("{0:0,0}", model.Amount) + "</td>"
                + "</tr></table>";
                body = body.Replace("{Order}", str);
            }

            foreach (var item in listEmails)
            {
                WebMail.Send(to: item, subject: "Thông tin đặt hàng", body: body, bcc: fromEmail, filesToAttach: filesToAttach);
            }
        }
        public static void ResetPassUserByEmail(IList<string> listMailToSend, string fullName, string newPass, IEnumerable<string> filesToAttach = null)
        {
            var dbContext = DalHelper.InvokeDbContext();

            var smtpServerKey = ConfigItemHelper.GetSmtpServer();
            var userNameKey = ConfigItemHelper.GetUserEmailAccountName();
            var passWordKey = ConfigItemHelper.GetEmailAccountPassword();
            var fromKey = ConfigItemHelper.GetFromEmailAccount();

            var smtpServerSetting = dbContext.Settings.FirstOrDefault(d => d.Key == smtpServerKey);
            var userNameSetting = dbContext.Settings.FirstOrDefault(d => d.Key == userNameKey);
            var passWordSetting = dbContext.Settings.FirstOrDefault(d => d.Key == passWordKey);
            var fromSetting = dbContext.Settings.FirstOrDefault(d => d.Key == fromKey);

            string smtpServer = smtpServerSetting != null ? smtpServerSetting.Value : "";
            string userName = userNameSetting != null ? userNameSetting.Value : "";
            string passWord = passWordSetting != null ? passWordSetting.Value : "";
            WebMail.SmtpUseDefaultCredentials = false;
            try
            {
                WebMail.SmtpPort = Convert.ToInt32(SettingHelper.GetValueSetting(ConfigItemHelper.GetSmtpPort()));
            }
            catch (Exception)
            {
                WebMail.SmtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            }
            try
            {
                WebMail.EnableSsl = Convert.ToBoolean(SettingHelper.GetValueSetting(ConfigItemHelper.GetEnableSsl()));
            }
            catch (Exception)
            {
                WebMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            }
            WebMail.SmtpServer = smtpServer;
            WebMail.UserName = userName;
            WebMail.Password = passWord;
            WebMail.From = "\"Lấy lại mật khẩu đăng nhập Avent\"" + userName;
            var from = fromSetting != null ? fromSetting.Value : "";

            var emails = "";
            var count = 0;
            var totalCount = 0;
            var listEmails = new List<string>();
            //Send email

            foreach (var item in listMailToSend)
            {
                totalCount++;
                if (item.Count(c => c == '@') == 1)
                {
                    count++;
                    emails += item.Replace("\r\n", "") + ",";
                    if (count == 5 || totalCount == listMailToSend.Count())
                    {
                        emails = emails.Substring(0, emails.Length - 1);
                        listEmails.Add(emails);
                        count = 0;
                        emails = string.Empty;
                    }
                }
            }
            foreach (var item in listEmails)
            {
                string body = "<div><b>Xin chào " + fullName + ",</b></div>";
                body += "<div>Thông tin password mới của bạn là: " + newPass;


                WebMail.Send(to: item, subject: "Lấy lại mật khẩu đăng nhập Motul Racing", body: body, filesToAttach: filesToAttach);
            }
        }

        public static void SendContactInfomation(IList<string> listMailToSend, Core.Contacts contact, IEnumerable<string> filesToAttach = null)
        {
            var dbContext = DalHelper.InvokeDbContext();

            var smtpServerKey = ConfigItemHelper.GetSmtpServer();
            var userNameKey = ConfigItemHelper.GetUserEmailAccountName();
            var passWordKey = ConfigItemHelper.GetEmailAccountPassword();
            var fromKey = ConfigItemHelper.GetFromEmailAccount();

            var smtpServerSetting = dbContext.Settings.FirstOrDefault(d => d.Key == smtpServerKey);
            var userNameSetting = dbContext.Settings.FirstOrDefault(d => d.Key == userNameKey);
            var passWordSetting = dbContext.Settings.FirstOrDefault(d => d.Key == passWordKey);
            var fromSetting = dbContext.Settings.FirstOrDefault(d => d.Key == fromKey);

            string smtpServer = smtpServerSetting != null ? smtpServerSetting.Value : "";
            string userName = userNameSetting != null ? userNameSetting.Value : "";
            string passWord = passWordSetting != null ? passWordSetting.Value : "";
            WebMail.SmtpUseDefaultCredentials = false;
            try
            {
                WebMail.SmtpPort = Convert.ToInt32(SettingHelper.GetValueSetting(ConfigItemHelper.GetSmtpPort()));
            }
            catch (Exception)
            {
                WebMail.SmtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            }
            try
            {
                WebMail.EnableSsl = Convert.ToBoolean(SettingHelper.GetValueSetting(ConfigItemHelper.GetEnableSsl()));
            }
            catch (Exception)
            {
                WebMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            }
            WebMail.SmtpServer = smtpServer;
            WebMail.UserName = userName;
            WebMail.Password = passWord;
            WebMail.From = "\"ITC GROUP\"" + userName;
            var fromEmail = fromSetting != null ? fromSetting.Value : "";

            var emails = "";
            var count = 0;
            var totalCount = 0;
            var listEmails = new List<string>();
            //Send email
            string body = "<fieldset><legend>Thông tin liên hệ</legend>";
            body += "<div><b>Tên khách hàng:</b></div>";
            body += "<div>" + contact.Name + "</div>";
            body += "<div><b>Điện thoại:</b></div>";
            body += "<div>" + contact.Phone + "</div>";
            body += "<div><b>Email:</b></div>";
            body += "<div>" + contact.Email + "</div>";
            body += "<div><b>Nội dung :</b></div>";
            body += "<div>" + contact.Content + "</div>";

            body += "</fieldset>";

            // In case: have email from users
            if (listMailToSend.Count > 0)
            {

                foreach (var item in listMailToSend)
                {
                    totalCount++;
                    if (item.Count(c => c == '@') == 1)
                    {
                        count++;
                        emails += item.Replace("\r\n", "") + ",";
                        if (count == 5 || totalCount == listMailToSend.Count())
                        {
                            emails = emails.Substring(0, emails.Length - 1);
                            listEmails.Add(emails);
                            count = 0;
                            emails = string.Empty;
                        }
                    }
                }
                //foreach (var item in listEmails)
                //{
                //    WebMail.Send(to: item, subject: "Thông tin liên hệ", body: body, bcc: fromEmail, filesToAttach: filesToAttach);
                //}
            }
            else // In case: Have no email from users
            {
                WebMail.Send(to: fromEmail, subject: "Thông tin liên hệ", body: body, filesToAttach: filesToAttach);
            }
        }
    }
}
