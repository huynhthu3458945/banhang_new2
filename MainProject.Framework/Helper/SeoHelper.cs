﻿using System;
using System.Collections.Generic;
using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;

namespace MainProject.Framework.Helper
{
    public static class SeoHelper
    {
        public static string VaidateSeNameAndSubmit(EntityTypeCollection entityType, long entityId, string name)
        {
            var dbContext = DalHelper.InvokeDbContext();

            var seName = StringHelper.GetSeName(name);
            var url = string.Empty;
            var languageId = dbContext.Languages.First().Id;
            var seExists = new List<string>();
            switch (entityType)
            {
                case EntityTypeCollection.Categories:
                    var category = dbContext.Categories.FirstOrDefault(c => c.Id == entityId);
                    seExists =
                        dbContext.Categories.Where(c => c.Parent.Id == category.Parent.Id && c.Id != entityId).Select(c => c.SeName).ToList();
                    url = category.Parent != null ? category.Parent.GetPrefixUrl() : "";
                    languageId = category.Language.Id;
                    if (string.IsNullOrWhiteSpace(seName) || seName == "-")
                    {
                        var cateorther = dbContext.Categories.FirstOrDefault(d => d.OriginalValue == category.OriginalValue && d.Language.Id != languageId);
                        if (cateorther == null)
                        {
                            seName = Guid.NewGuid().ToString();
                            while (dbContext.Categories.Where(d => d.SeName == seName).Any())
                            {
                                seName = Guid.NewGuid().ToString();
                            }
                        }
                        else
                        {
                            seName = cateorther.SeName + "-cn";
                        }
                    }
                    break;
                case EntityTypeCollection.Articles:
                    var article = dbContext.Articles.FirstOrDefault(c => c.Id == entityId);
                    seExists =
                        dbContext.Articles.Where(c => c.Category.Id == article.Category.Id && c.Id != entityId).Select(c => c.SeName).ToList();
                    url = article.Category.GetPrefixUrl();
                    languageId = article.Language.Id;
                    if (string.IsNullOrWhiteSpace(seName))
                    {
                        var articleother =
                            dbContext.Articles.FirstOrDefault(d => d.OriginalValue == article.OriginalValue);
                        if (articleother == null)
                        {
                            seName = Guid.NewGuid().ToString();
                            while (dbContext.Articles.Where(d => d.SeName == seName).Any())
                            {
                                seName = Guid.NewGuid().ToString();
                            }
                        }
                        else
                        {
                            seName = articleother.SeName + "-cn";
                        }
                    }
                    break;
                case EntityTypeCollection.Products:
                    var product = dbContext.Products.FirstOrDefault(c => c.Id == entityId);
                    seExists =
                        dbContext.Products.Where(c => c.Category.Id == product.Category.Id && c.Id != entityId).Select(c => c.SeName).ToList();
                    url = product.Category.GetPrefixUrl();
                    languageId = product.Language.Id;
                    if (string.IsNullOrWhiteSpace(seName))
                    {
                        var productother =
                            dbContext.Products.FirstOrDefault(d => d.OriginalValue == product.OriginalValue);
                        if (productother == null)
                        {
                            seName = Guid.NewGuid().ToString();
                            while (dbContext.Products.Where(d => d.SeName == seName).Any())
                            {
                                seName = Guid.NewGuid().ToString();
                            }
                        }
                        else
                        {
                            seName = productother.SeName + "-cn";
                        }
                    }
                    break;
            }
            var i = 1;
            var tempSeName = seName;
            var tempUrl = (url + "/" + seName).ToLower();

            var item = dbContext.UrlRecords.FirstOrDefault(c => c.Url.ToLower().Equals(tempUrl));
            while (seExists.Contains(seName) || (item != null && item.EntityId != entityId))
            {
                seName = string.Format("{0}-{1}", tempSeName, i);
                tempUrl = (url + "/" + seName);
                item = dbContext.UrlRecords.FirstOrDefault(c => c.Url.ToLower().Equals(tempUrl));
                i++;
            }
            url += "/" + seName;
            //SaveToUrlRecord(dbContext, entityType, entityId, languageId, seName, url, name);
            SaveToUrlData(dbContext, entityType, entityId, languageId, seName, url, name);
            if (entityType == EntityTypeCollection.Categories)
            {
                var category = dbContext.Categories.FirstOrDefault(c => c.Id == entityId);
                if (category != null) category.SeName = seName;

                //update article url
                var articleChildren = dbContext.Articles.Where(c => c.Category.Id == entityId).ToList();
                foreach (var article in articleChildren)
                {
                    var articleSeName = VaidateSeNameAndSubmit(EntityTypeCollection.Articles, article.Id, article.Title);
                    article.SeName = articleSeName;
                }
                //update product url
                var productChildrens = dbContext.Products.Where(c => c.Category.Id == entityId).ToList();
                foreach (var product in productChildrens)
                {
                    var productSeName = VaidateSeNameAndSubmit(EntityTypeCollection.Products, product.Id, product.Name);
                    product.SeName = productSeName;
                }

                var cateogryChildren = dbContext.Categories.Where(c => c.Parent.Id == entityId).ToList();
                foreach (var cateogryChild in cateogryChildren)
                {
                    var categorySeName = VaidateSeNameAndSubmit(entityType, cateogryChild.Id, cateogryChild.Title);
                    cateogryChild.SeName = categorySeName;
                }
                dbContext.SaveChanges();
            }
            return seName;
        }

        private static void SaveToUrlData(MainDbContext dbContext, EntityTypeCollection entityType, long entityId, int languageId, string seName, string url, string title)
        {
            var urlRecord =
                dbContext.UrlRecords.FirstOrDefault(c => c.EntityType == entityType && c.EntityId == entityId);
            // Create method
            if (urlRecord == null)
            {

                // Create UrlRecord
                urlRecord = new UrlRecord
                                {
                                    EntityId = entityId,
                                    EntityType = entityType,
                                    Language = dbContext.Languages.FirstOrDefault(c => c.Id == languageId),
                                    SeName = seName,
                                    Url = url,
                                    Title = title
                                };
                dbContext.UrlRecords.Add(urlRecord);
            }
            // Edit method
            else
            {
                // Edit UrlRecord
                urlRecord.SeName = seName;
                urlRecord.Url = url;
                urlRecord.Title = title;
            }
            dbContext.SaveChanges();
        }

        public static void DeleteUrlData(EntityTypeCollection entityType, long entityId)
        {
            var dbContext = DalHelper.InvokeDbContext();

            // Delete UrlRecord
            var urlRecords =
                dbContext.UrlRecords.Where(c => c.EntityType == entityType && c.EntityId == entityId).ToList();
            foreach (var urlRecord in urlRecords)
            {
                dbContext.UrlRecords.Remove(urlRecord);
            }
            dbContext.SaveChanges();
        }
    }
}