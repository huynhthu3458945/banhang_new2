﻿using System.Linq;
using MainProject.Core.Enums;
using System.Web.Mvc;
using System.Collections.Generic;

namespace MainProject.Framework.Helper
{
    public class CustomViewAuthorizeAttribute : AuthorizeAttribute
    {
        public CustomViewAuthorizeAttribute(params string[] roleKeys)
        {
            var roles = new List<string>();
            var dbContext = DalHelper.InvokeDbContext();
            var controllerName = roleKeys[0];
            var actionName = roleKeys[1];

            var roleCBs = dbContext.Role_Behaviors;
            if (actionName == "View")
            {
                roles = roleCBs.Where(x => x.ContentBehavior.Content.ControllerName == controllerName
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.ViewTemplate)
                                                                .Select(x => x.Role.RoleName).ToList();
            }
            if (actionName == "Create")
            {
                roles = roleCBs.Where(x => x.ContentBehavior.Content.ControllerName == controllerName
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate)
                                                                .Select(x => x.Role.RoleName).ToList();
            }
            if (actionName == "Edit")
            {
                roles = roleCBs.Where(x => x.ContentBehavior.Content.ControllerName == controllerName
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate)
                                                                .Select(x => x.Role.RoleName).ToList();
            }
            if (actionName == "Delete")
            {
                roles = roleCBs.Where(x => x.ContentBehavior.Content.ControllerName == controllerName
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate)
                                                                .Select(x => x.Role.RoleName).ToList();
            }
            Roles = string.Join(",", roles);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (filterContext.Result is HttpUnauthorizedResult)
            {
                var urlHelper = new UrlHelper(filterContext.RequestContext);
                filterContext.Result = new RedirectResult(urlHelper.Action("Logon", "ITCAdmin", new { area = "Admin" }));
            }
        }
    }
}
