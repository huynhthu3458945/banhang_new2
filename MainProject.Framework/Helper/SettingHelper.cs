﻿using System.Configuration;
using System.Linq;

namespace MainProject.Framework.Helper
{
    public class SettingHelper
    {
        public SettingHelper(){}

        public static string GetValueSetting(string key1Table)
        {
            var dbContext = DalHelper.InvokeDbContext();
            var key1Setting = dbContext.Settings.FirstOrDefault(d => d.Key == key1Table);
            if (key1Setting != null && !string.IsNullOrEmpty(key1Setting.Value))
            {
                return key1Setting.Value;
            }
            return string.Empty;
        }

        public static string GetValueSetting(string key1Table, string key2Config)
        {
            var dbContext = DalHelper.InvokeDbContext();
            var key1Setting = dbContext.Settings.FirstOrDefault(d => d.Key == key1Table);
            if(key1Setting!=null&& !string.IsNullOrEmpty(key1Setting.Value))
            {
                return key1Setting.Value;
            }
            else
            {
                string key2Value = ConfigurationManager.AppSettings[key2Config];
                if (!string.IsNullOrEmpty(key2Value)) return key2Value;
                return string.Empty;
            }
        }
    }
}
