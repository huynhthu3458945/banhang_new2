﻿using System;
using MainProject.Data;

namespace MainProject.Framework.Helper
{
    public static class ConfigItemHelper
    {
        private static MainDbContext _dbContext = DalHelper.InvokeDbContext();

        public static long GetRootNewsId()
        {
            var rootCategoryIdStr = System.Configuration.ConfigurationManager.AppSettings["rootNewsId"];
            long rootCategoryId = 0;
            try
            {
                rootCategoryId = Convert.ToInt64(rootCategoryIdStr);
            }
            catch (Exception) { }

            return rootCategoryId;
        }

        public static long GetRootAttachmentId()
        {
            var rootCategoryIdStr = System.Configuration.ConfigurationManager.AppSettings["rootAttachmentId"];
            long rootCategoryId = 0;
            try
            {
                rootCategoryId = Convert.ToInt64(rootCategoryIdStr);
            }
            catch (Exception) { }

            return rootCategoryId;
        }

        public static string GetVisitedKey() => "Visited";

        public static string GetSkypeKey() => "SkypeAccount";

        public static string GetFacebookKey() => "FacebookPage";

        public static string GetWebsiteKey() => "WebsitePage";

        public static string GetQQKey() => "QQAccount"; 

        public static string GetYoutubePage()=> "YoutubePage";

        public static string GetRssPage() => "RssPage";

        public static string GetTwitterPage() => "TwitterPage";

        public static string GetEmailKey() => "EmailAccount";

        public static string GetYahooKey() => "YahooAccount";

        public static string GetGooglePlusKey() => "GooglePlus";

        public static string GetMapKey() => "MyMap";

        public static string GetMyPhone() => "MyPhone";

        public static string GetMyCellPhone() => "MyCellPhone";

        public static string GetMyFax() => "MyFax";

        public static string GetOnOffIntro() => "OnOffIntro";

        public static string GetOnOffChart() => "OnOffChart";

        public static string GetSmtpPort() => "SmtpPort";//25

        public static string GetSmtpServer() => "SmtpServer";

        public static String GetEnableSsl() => "EnableSsl"; //true

        public static string GetUserEmailAccountName() => "UserEmailAccountName";

        public static string GetEmailAccountPassword() => "EmailAccountPassword";

        public static string GetFromEmailAccount() => "FromEmailAccount";

        public static string GetToEmailAccount() => "ToEmailAccount";

        public static int GetItemsCountPerPage()
        {
            var itemsCountStr = System.Configuration.ConfigurationManager.AppSettings["ItemsPerPageCount"];
            var itemsCount = 20;
            try
            {
                itemsCount = Convert.ToInt32(itemsCountStr);
            }
            catch (Exception) { }

            return itemsCount;
        }

        public static int GetMaxFileUploadSize() 
            => Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["fileUploadSize"]);

        public static int GetMaxCommentCharacter() => 200;

        public static string GetGAUsername() => System.Configuration.ConfigurationManager.AppSettings["GAUsername"];

        public static string GetGAPassword() => System.Configuration.ConfigurationManager.AppSettings["GAPassword"];

        public static string GetGAAppKey() => System.Configuration.ConfigurationManager.AppSettings["GAPassword"];

        public static string GetGAProfileId() => System.Configuration.ConfigurationManager.AppSettings["GAProfileId"];

        public static string GetGAStartDate() => System.Configuration.ConfigurationManager.AppSettings["GAStartDate"];

        public static string GetSytleFilePath() => "/Content/Site.css";
    }
}
