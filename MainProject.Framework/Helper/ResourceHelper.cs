﻿using System.Linq;
using MainProject.Core.Enums;
using MainProject.Framework.Constant;

namespace MainProject.Framework.Helper
{
    public static class ResourceHelper
    {
        public static string GetResource(ResourceKeyCollection resourceKey, string language)
        {
            var strResourceKey = resourceKey.ToString();
            return GetResource(strResourceKey, language);
        }

        public static string GetResource(string strResourceKey, string language)
        {
            var dbContext = DalHelper.InvokeDbContext();
            var cacheKey = CacheConstant.GetResourceKey(strResourceKey, language);

            var strValue = CacheHelper.GetValueCache(cacheKey);
            if (strValue == null)
            {
                var stringResourceValue =
                    dbContext.StringResourceValues.FirstOrDefault(
                        c => c.Key.Name.Equals(strResourceKey) && c.Language.LanguageKey.Equals(language));

                if (stringResourceValue != null)
                {
                    CacheHelper.InsertOrUpdate(cacheKey, stringResourceValue.Value ?? string.Empty);
                    strValue = stringResourceValue.Value;
                }
            }

            return strValue == null ? string.Empty : strValue.ToString();
        }

        public static void UpdateResourceOnCache(string strResourceKey, string languageKey, string value)
        {
            var cacheKey = CacheConstant.GetResourceKey(strResourceKey, languageKey);

            CacheHelper.InsertOrUpdate(cacheKey, value ?? string.Empty);
        }
    }
}
