﻿using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Framework.BbCode.Cyotek;
using MainProject.Framework.Models;

namespace MainProject.Framework.Helper
{
    public static class MvcHtmlHelper
    {
        #region Paging

        public static HtmlString RenderPagingNew(this HtmlHelper helper, PagingModel model, PagingStyleModel styleModel)
        {
            var strBuilder = new StringBuilder();
            if (model != null && model.HasPaging)
            {
                var actionForNextPage = (model.CurrentPage < model.TotalPages) ? BuildActionCode(model.ActionCode, (model.CurrentPage + 1)) : "";
                var actionForPreviousPage = (model.CurrentPage > 1) ? BuildActionCode(model.ActionCode, (model.CurrentPage - 1)) : "";
                strBuilder.Append("<ul class='" + styleModel.DivContainerStyle + "'>");

                if (model.TotalPages > 1)
                {
                    if (model.CurrentPage > 1)
                    {
                        strBuilder.Append("<li><a " + actionForPreviousPage + " " + styleModel.ItemStyle + "><i class='ic-Arrow-left'></i></a></li>");
                    }
                    if (model.TotalPages > 7)
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            if (i <= 3)
                            {
                                strBuilder.Append("<li><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                            else
                            {
                                strBuilder.Append("<li><a>...</a></li>");
                                break;
                            }
                        }
                        if ((model.TotalPages - model.CurrentPage) < 2)
                        {
                            for (var i = model.TotalPages - 2; i < model.CurrentPage; i++)
                            {
                                strBuilder.Append("<li><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                        }
                        strBuilder.Append("<li class='active'><a href='#' " + styleModel.CurrentItemStyle + ">" + model.CurrentPage + "</a></li>");
                        if (model.CurrentPage < 3)
                        {
                            for (var i = model.CurrentPage + 1; i <= 3; i++)
                            {
                                strBuilder.Append("<li><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                        }

                        var hasDot = false;
                        for (var i = (model.CurrentPage + 1); i <= model.TotalPages; i++)
                        {
                            if ((model.TotalPages - i) < 3)
                            {
                                strBuilder.Append("<li><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                            else
                            {
                                if (!hasDot)
                                {
                                    strBuilder.Append("<li><a>...</a></li>");
                                    hasDot = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            strBuilder.Append("<li><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                        }
                        strBuilder.Append("<li class='active'><a href='#'>" + model.CurrentPage + "</a></li>");
                        for (var i = model.CurrentPage + 1; i <= model.TotalPages; i++)
                        {
                            strBuilder.Append("<li><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                        }

                    }
                    if (model.CurrentPage < model.TotalPages)
                    {
                        strBuilder.Append("<li><a " + actionForNextPage + " " + styleModel.ItemStyle + "><i class='ic-Arrow-right'></i></a></li>");
                    }
                }
                strBuilder.Append("</ul>");
            }

            return new HtmlString(strBuilder.ToString());
        }
        public static HtmlString RenderPaging(this HtmlHelper helper, PagingModel model, PagingStyleModel styleModel)
        {
            var strBuilder = new StringBuilder();
            if (model != null && model.HasPaging)
            {
                var actionForNextPage = (model.CurrentPage < model.TotalPages) ? BuildActionCode(model.ActionCode, (model.CurrentPage + 1)) : "";
                var actionForPreviousPage = (model.CurrentPage > 1) ? BuildActionCode(model.ActionCode, (model.CurrentPage - 1)) : "";
                strBuilder.Append("<div " + styleModel.DivContainerStyle + ">");

                if (model.TotalPages > 1)
                {
                    if (model.CurrentPage > 1)
                    {
                        strBuilder.Append("<a " + actionForPreviousPage + " " + styleModel.ItemStyle + "><</a>");
                    }
                    if (model.TotalPages > 7)
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            if (i <= 3)
                            {
                                strBuilder.Append("<a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a>");
                            }
                            else
                            {
                                strBuilder.Append("<span>...</span>");
                                break;
                            }
                        }
                        if ((model.TotalPages - model.CurrentPage) < 2)
                        {
                            for (var i = model.TotalPages - 2; i < model.CurrentPage; i++)
                            {
                                strBuilder.Append("<a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a>");
                            }
                        }
                        strBuilder.Append("<span " + styleModel.CurrentItemStyle + ">" + model.CurrentPage + "</span>");
                        if (model.CurrentPage < 3)
                        {
                            for (var i = model.CurrentPage + 1; i <= 3; i++)
                            {
                                strBuilder.Append("<a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a>");
                            }
                        }

                        var hasDot = false;
                        for (var i = (model.CurrentPage + 1); i <= model.TotalPages; i++)
                        {
                            if ((model.TotalPages - i) < 3)
                            {
                                strBuilder.Append("<a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a>");
                            }
                            else
                            {
                                if (!hasDot)
                                {
                                    strBuilder.Append("<span>...</span>");
                                    hasDot = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            strBuilder.Append("<a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a>");
                        }
                        strBuilder.Append("<span " + styleModel.CurrentItemStyle + ">" + model.CurrentPage + "</span>");
                        for (var i = model.CurrentPage + 1; i <= model.TotalPages; i++)
                        {
                            strBuilder.Append("<a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a>");
                        }

                    }
                    if (model.CurrentPage < model.TotalPages)
                    {
                        strBuilder.Append("<a " + actionForNextPage + " " + styleModel.ItemStyle + ">></a>");
                    }
                }
                strBuilder.Append("</div>");
            }

            return new HtmlString(strBuilder.ToString());
        }
        public static HtmlString RenderPagingAdmin(this HtmlHelper helper, PagingModel model, PagingStyleModel styleModel)
        {
            var strBuilder = new StringBuilder();
            if (model != null && model.HasPaging)
            {
                var actionForNextPage = (model.CurrentPage < model.TotalPages) ? BuildActionCode(model.ActionCode, (model.CurrentPage + 1)) : "";
                var actionForPreviousPage = (model.CurrentPage > 1) ? BuildActionCode(model.ActionCode, (model.CurrentPage - 1)) : "";
                strBuilder.Append("<ul " + styleModel.DivContainerStyle + " class='pagination'>");

                if (model.TotalPages > 1)
                {
                    if (model.CurrentPage > 1)
                    {
                        strBuilder.Append("<li class='paginate_button'><a " + actionForPreviousPage + " " + styleModel.ItemStyle + "><</a></li>");
                    }
                    if (model.TotalPages > 7)
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            if (i <= 3)
                            {
                                strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                            else
                            {
                                strBuilder.Append("<li class='paginate_button'><span>...</span></li>");
                                break;
                            }
                        }
                        if ((model.TotalPages - model.CurrentPage) < 2)
                        {
                            for (var i = model.TotalPages - 2; i < model.CurrentPage; i++)
                            {
                                strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                        }
                        strBuilder.Append("<li class='paginate_button " + styleModel.CurrentItemStyle +"'><a href='javascript:void(0)' >" + model.CurrentPage + "</a></li>");
                        if (model.CurrentPage < 3)
                        {
                            for (var i = model.CurrentPage + 1; i <= 3; i++)
                            {
                                strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                        }

                        var hasDot = false;
                        for (var i = (model.CurrentPage + 1); i <= model.TotalPages; i++)
                        {
                            if ((model.TotalPages - i) < 3)
                            {
                                strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                            else
                            {
                                if (!hasDot)
                                {
                                    strBuilder.Append("<li class='paginate_button'><span>...</span></li>");
                                    hasDot = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                        }
                        strBuilder.Append("<li class='paginate_button "+styleModel.CurrentItemStyle+"'><a href='javascript:void(0)'>" + model.CurrentPage + "</a></li>");
                        for (var i = model.CurrentPage + 1; i <= model.TotalPages; i++)
                        {
                            strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                        }

                    }
                    if (model.CurrentPage < model.TotalPages)
                    {
                        strBuilder.Append("<li class='paginate_button'><a " + actionForNextPage + " " + styleModel.ItemStyle + ">></a></li>");
                    }
                }
                strBuilder.Append("</ul>");
            }

            return new HtmlString(strBuilder.ToString());
        }
        public static HtmlString RenderPagingAdminMobile(this HtmlHelper helper, PagingModel model, PagingStyleModel styleModel)
        {
            var strBuilder = new StringBuilder();
            if (model != null && model.HasPaging)
            {
                var actionForNextPage = (model.CurrentPage < model.TotalPages) ? BuildActionCode(model.ActionCode, (model.CurrentPage + 1)) : "";
                var actionForPreviousPage = (model.CurrentPage > 1) ? BuildActionCode(model.ActionCode, (model.CurrentPage - 1)) : "";
                strBuilder.Append("<ul " + styleModel.DivContainerStyle + " class='pagination'>");

                if (model.TotalPages > 1)
                {
                    if (model.CurrentPage > 1)
                    {
                        strBuilder.Append("<li class='paginate_button'><a " + actionForPreviousPage + " " + styleModel.ItemStyle + "><</a></li>");
                    }
                    if (model.TotalPages > 5)
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            if (i <= 3)
                            {
                                strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                            else
                            {
                                strBuilder.Append("<li class='paginate_button'><span>...</span></li>");
                                break;
                            }
                        }
                        if ((model.TotalPages - model.CurrentPage) < 2)
                        {
                            for (var i = model.TotalPages - 2; i < model.CurrentPage; i++)
                            {
                                strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                        }
                        strBuilder.Append("<li class='paginate_button " + styleModel.CurrentItemStyle + "'><a href='javascript:void(0)' >" + model.CurrentPage + "</a></li>");
                        if (model.CurrentPage < 3)
                        {
                            for (var i = model.CurrentPage + 1; i <= 3; i++)
                            {
                                strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                        }

                        var hasDot = false;
                        for (var i = (model.CurrentPage + 1); i <= model.TotalPages; i++)
                        {
                            if ((model.TotalPages - i) < 3)
                            {
                                strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                            }
                            else
                            {
                                if (!hasDot)
                                {
                                    strBuilder.Append("<li class='paginate_button'><span>...</span></li>");
                                    hasDot = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (var i = 1; i < model.CurrentPage; i++)
                        {
                            strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                        }
                        strBuilder.Append("<li class='paginate_button " + styleModel.CurrentItemStyle + "'><a href='javascript:void(0)'>" + model.CurrentPage + "</a></li>");
                        for (var i = model.CurrentPage + 1; i <= model.TotalPages; i++)
                        {
                            strBuilder.Append("<li class='paginate_button'><a " + styleModel.ItemStyle + " " + BuildActionCode(model.ActionCode, i) + ">" + i + "</a></li>");
                        }

                    }
                    if (model.CurrentPage < model.TotalPages)
                    {
                        strBuilder.Append("<li class='paginate_button'><a " + actionForNextPage + " " + styleModel.ItemStyle + ">></a></li>");
                    }
                }
                strBuilder.Append("</ul>");
            }

            return new HtmlString(strBuilder.ToString());
        }
        private static string BuildActionCode(string strFormat, int pageIndex)
        {
            return string.Format(strFormat, pageIndex);
        }
        #endregion
        public static string FormatCurrency(this HtmlHelper helper, decimal value)
        {
            //return string.Format(FormatProviderConstant.FormatProvider_vi, "{0:C0}", value); //40.500 đ
            return string.Format("{0:0,0}", value);
        }

        public static string FormatBbCode(this HtmlHelper helper, string text)
        {
            //return BbCodeProcessor.Format(helper.Encode(text));
            return BbCodeProcessor.Format(text);
        }

        public static string Truncate(this HtmlHelper helper, string input, int length)
        {
            if (input.Length <= length)
            {
                return input;
            }
            else
            {
                return input.Substring(0, length) + "...";
            }
        }

        public static string GenerateBreadcum(this HtmlHelper helper, string seoStandardUrl, string splitCharacter)
        {
            var dbContext = DalHelper.InvokeDbContext();
            var urlRecord = dbContext.UrlRecords.FirstOrDefault(x => x.Url == seoStandardUrl);
            //var homePage = "<img src='/Content/layout/images/icon_home.png' alt='home'>";
            //if (string.IsNullOrEmpty(homePage)) homePage = "<img src='/Content/layout/images/icon_home.png' alt='home'>";
            string homePage = "Trang chủ";
            var result = "";
            int index = 0;
            if (urlRecord != null)
            {
                while (!string.IsNullOrEmpty(seoStandardUrl) && urlRecord != null)
                {
                    // Check is the first element of breadcrumb

                    string temp;

                    if(index == 0)
                    {
                        temp = string.Format("<li>{0}</li>", urlRecord.Title);
                    }
                    else // In case: the last element
                    {
                        temp = string.Format("<li><a href='{0}'>{1}</a></li>", urlRecord.Url, urlRecord.Title);
                    }

                    if (string.IsNullOrEmpty(result))
                    {
                        result = temp;
                    }
                    else
                    {
                        result = temp + result;
                    }
                    seoStandardUrl = seoStandardUrl.Substring(0, seoStandardUrl.LastIndexOf("/"));
                    urlRecord = dbContext.UrlRecords.Where(x => x.Url == seoStandardUrl).FirstOrDefault();
                    index++;
                }
            }
            result = string.Format("<li><a href='/'>{0}</a></li>", homePage) + result;
            return result;
        }

    #region Methods Updated by TinHo
        public static string FormatCurrency(decimal value)
        {
            return string.Format("{0:0,0}", value);
        }
        public static string GenerateBreadcum(string seoStandardUrl, string splitCharacter)
        {
            var dbContext = DalHelper.InvokeDbContext();
            var urlRecord = dbContext.UrlRecords.FirstOrDefault(x => x.Url == seoStandardUrl);
            string homePage = "Trang chủ";
            var result = "";
            int index = 0;
            if (urlRecord != null)
            {
                while (!string.IsNullOrEmpty(seoStandardUrl) && urlRecord != null)
                {
                    // Check is the first element of breadcrumb

                    string temp;

                    if (index == 0)
                    {
                        temp = string.Format("<span  class=\"breadcrumb-item\">{0}</span>", urlRecord.Title);
                    }
                    else // In case: the last element
                    {
                        temp = string.Format("<a class=\"breadcrumb-item\" href='{0}'>{1}</a>", urlRecord.Url, urlRecord.Title);
                    }

                    if (string.IsNullOrEmpty(result))
                    {
                        result = temp;
                    }
                    else
                    {
                        result = temp + result;
                    }
                    seoStandardUrl = seoStandardUrl.Substring(0, seoStandardUrl.LastIndexOf("/"));
                    urlRecord = dbContext.UrlRecords.Where(x => x.Url == seoStandardUrl).FirstOrDefault();
                    index++;
                }
            }
            result = string.Format("<a class=\"breadcrumb-item\" href='/'>{0}</a>", homePage) + result;
            return result;
        }
    #endregion
    }
}
