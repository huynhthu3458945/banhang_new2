﻿
namespace MainProject.Framework.BbCode.Cyotek
{
  internal interface IHtmlFormatter
  {
    string Format(string data);
  }
}
