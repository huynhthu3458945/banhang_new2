﻿using System;
using MainProject.Framework.Helper;

namespace MainProject.Framework.DatabaseFramework
{
    // Add data to already database
    public class AddDb
    {
        public static string Init()
        {
            try 
            {
                DalHelper.SeedMembership();
                return "Add database successfully";
            }
            catch(Exception ex){
                return ex.ToString();
            }
        }
    }
}
