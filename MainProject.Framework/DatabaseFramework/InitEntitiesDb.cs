﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Transactions;
using WebMatrix.WebData;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;

namespace MainProject.Framework.DatabaseFramework
{
    internal class InitEntitiesDb : IDatabaseInitializer<MainDbContext>
    {
        #region IDatabaseInitializer<> Members

        public void InitializeDatabase(MainDbContext context)
        {
            bool dbExists;
            using (new TransactionScope(TransactionScopeOption.Suppress))
            {
                dbExists = context.Database.Exists();
            }
            if (dbExists)
            {
                // create all tables
                var dbCreationScript = ((IObjectContextAdapter)context).ObjectContext.CreateDatabaseScript();
                context.Database.ExecuteSqlCommand(dbCreationScript);

                //create membership tables
                if (!WebSecurity.Initialized)
                {
                    WebSecurity.InitializeDatabaseConnection("DefaultConnection",
                        "UserProfile", "UserId", "UserName", autoCreateTables: true);
                }

                DalHelper.SeedMembership();
                SeedEntities(context);
            }
            else
            {
                throw new ApplicationException("No database instance");
            }
        }

        #endregion

        #region Methods

        protected virtual void SeedEntities(MainDbContext context)
        {
           // context.SaveChanges();

            context.Categories.Add(new Category
                                       {
                                           EntityType = EntityTypeCollection.Categories,
                                           IsSystem = true,
                                           Order = 0,
                                           SeName = "root-news-category"
                                       });
            context.Categories.Add(new Category
                                       {
                                           EntityType = EntityTypeCollection.AttachmentCategories,
                                           IsSystem = true,
                                           Order = 0,
                                           SeName = "root-attachments-category"
                                       });

            ///// Add Authorize data
            //// Add ContentGroup
            //var authCG = new ContentGroup
            //{
            //    GroupName = "Nội dung phân quyền",
            //    IconClass = "fa fa-users",
            //    Order = 1
            //};
            //context.ContentGroups.Add(authCG);

            //// Add Behaviors
            //var viewBH = new Behavior
            //{
            //    Name = "Xem",
            //    Type = BehaviorTypeCollection.ViewTemplate
            //};
            //var addBH = new Behavior
            //{
            //    Name = "Thêm",
            //    Type = BehaviorTypeCollection.CreateTemplate
            //};
            //var editBH = new Behavior
            //{
            //    Name = "Sửa",
            //    Type = BehaviorTypeCollection.EditTemplate
            //};
            //var deleteBH = new Behavior
            //{
            //    Name = "Xóa",
            //    Type = BehaviorTypeCollection.DeleteTemplate
            //};
            //context.Behaviors.Add(viewBH);
            //context.Behaviors.Add(addBH);
            //context.Behaviors.Add(editBH);
            //context.Behaviors.Add(deleteBH);

            //// Add Contents


            context.SaveChanges();
        }

        //protected virtual void SeedMembership()
        //{
        //    var roles = (SimpleRoleProvider)Roles.Provider;
        //    var membership = (SimpleMembershipProvider)Membership.Provider;
        //    if (!roles.RoleExists(RoleName.RootUser))
        //    {
        //        roles.CreateRole(RoleName.RootUser);
        //    }
        //    if (membership.GetUser(StringConstant.DefaultAdministrator, false) == null)
        //    {
        //        membership.CreateUserAndAccount(StringConstant.DefaultAdministrator, "@654321");
        //    }
        //    if (!roles.GetRolesForUser(StringConstant.DefaultAdministrator).ToList().Contains(RoleName.RootUser))
        //    {
        //        roles.AddUsersToRoles(new[] { StringConstant.DefaultAdministrator }, new[] { RoleName.RootUser });
        //    }

        //    //if (!roles.RoleExists(RoleName.Admin))
        //    //{
        //    //    roles.CreateRole(RoleName.Admin);
        //    //}
        //    //if (!roles.RoleExists(RoleName.Mod))
        //    //{
        //    //    roles.CreateRole(RoleName.Mod);
        //    //}
        //    //if (!roles.RoleExists(RoleName.Employee))
        //    //{
        //    //    roles.CreateRole(RoleName.Employee);
        //    //}
        //    //if (!roles.RoleExists(RoleName.Guest))
        //    //{
        //    //    roles.CreateRole(RoleName.Guest);
        //    //}
        //    //if (membership.GetUser(StringConstant.DefaultAdministrator, false) == null)
        //    //{
        //    //    membership.CreateUserAndAccount(StringConstant.DefaultAdministrator, "@654321");
        //    //}
        //    //if (!roles.GetRolesForUser(StringConstant.DefaultAdministrator).ToList().Contains(RoleName.Admin))
        //    //{
        //    //    roles.AddUsersToRoles(new[] { StringConstant.DefaultAdministrator }, new[] { RoleName.Admin });
        //    //}

        //}
        #endregion
    }
}