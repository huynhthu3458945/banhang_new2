﻿using System.Web.Mvc;
using MainProject.Core.Enums;

namespace MainProject.Framework.ActionFilters
{
    public class FilterPermission : ActionFilterAttribute
    {
        public EntityManageTypeCollection EntityManageType { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //var isValid = Roles.IsUserInRole(RoleName.RootUser) || EntityManageType.ValidatePermission();
            var isValid = true;
            if (!isValid)
            {
                var urlHelper = new UrlHelper(filterContext.RequestContext);
                filterContext.Result = new RedirectResult(urlHelper.Action("Logon", "ITCAdmin", new { area = "Admin" }));
            }
        }
    }
}
