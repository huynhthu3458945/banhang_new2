(function($){
    var event = function(){
        $('.attribute-filter').click(function() {
            var page_url = window.location.href;
            var ids_arr = [];
            $('.attribute-filter input[type="checkbox"]:checked').each(function() {
                ids_arr.push(this.value);
            });
            var ids_str = ids_arr.join('%2C');
            page_url = add_update_url_param(page_url, 'pa', ids_str);
            if (page_url != window.location) {
                window.history.pushState({
                    path: page_url
                }, '', page_url);
            }
            location.reload(true);
        });

        // nut dat hang o vong lap
        $('body').on('click', '.btn_add_to_cart', function(e){
            e.preventDefault();
            var product_id = $(this).data('product_id');
            var qty = 1;
            var loading = $(this).find('img');            
            add_to_cart(product_id, qty, loading);

        });

        // nut dat hang o trang single
        $('body').on('click', '.btn_add_to_cart_single', function(e){
            e.preventDefault();
            var product_id = $(this).data('product_id');
            var qty = $('.input_qty_single').val();
            var loading = $(this).find('img');
            add_to_cart(product_id, qty, loading);
        }); 

        // xóa sp trong giỏ hàng
        $('body').on('click', '.btn_remove_item_cart', function(e){
            e.preventDefault();
            var cart_body = $('.card .cart-body');
            var loading = cart_body.find('.loading');
            var product_id = $(this).data('product_id');
            var key_item = $(this).data('key_item'); 
            $.ajax({
                type: "POST",
                url: url_ajax,
                data: {
                    action: 'remove_item_from_cart',
                    'product_id': product_id,
                    'key_item' : key_item
                },
                beforeSend: function(){
                    loading.show();                    
                },
                success: function(res) {
                    loading.hide();                    
                    cart_body.html(res);                                      
                }
            });
        }); 

        // cập nhật số lượng trong giỏ hàng
        $('body').on('change', '.input_qty_item_cart', function(){
            var cart_body = $('.card .cart-body');
            var loading = cart_body.find('.loading');
            var key_item = $(this).data('key_item');
            var product_id = $(this).data('product_id');
            var tr_item = $(this).closest('tr');
            var qty = $(this).val();
            if( qty > 0){
                $.ajax({
                type: "POST",
                url: url_ajax,
                data: {
                    action: 'update_item_from_cart',
                    'product_id': product_id,
                    'key_item' : key_item,
                    'qty': qty
                },
                beforeSend: function(){
                    loading.show();
                },
                success: function(res) {
                    if (res) {
                        loading.hide();                        
                        cart_body.html(res);
                        //load_content_cart();
                    }
                }
            });
            } else {
                alert('Số lượng bạn nhập chưa chính xác!');
            }
        });

        /*$(window).on('load', function(){
            load_content_cart();
        });*/ 

        // ajax tinh thanh - quan huyen
        $('body').on('change', '#selectCity', function(){
            var city_id = $(this).val();  
            var option_district = option_district_by_city_id(city_id);
            $('#selectDistrict').html(option_district);          
            /*$.ajax({
                type: "POST",
                url: url_ajax, //url_ajax,
                data: {
                    action: 'city_district',
                    'city_id': city_id
                },
                success: function( res ) {
                    if( res ){
                        $('#district-wrap').html(res);
                    }
                }
            });*/            
        });

        // chon dia chi nhan hang
        $('body').on('click', 'input[name=payment_method]', function(){
            var payment_method = $('input[name=payment_method]:checked').val();
            var option_cod = $('.option_cod');
            var option_warehouse = $('.option_warehouse');            

            if( payment_method=='cod' ){
                option_cod.show();
                option_warehouse.hide();
            } else {
                option_cod.hide();
                option_warehouse.show();
            }            
        });

        // Gui kem thiep tang ban be, nguoi than (mien phi)
        $('body').on('click', '.gift_checkbox', function(){
            var gift_checkbox = $('input.gift_checkbox');
            var gift_message = $('.gift_message');
            if(gift_checkbox.is(':checked')){
                gift_message.fadeIn(500);
                gift_message.find('textarea').focus();
            } else {
                gift_message.fadeOut();
                gift_message.find('textarea').val('');
            }
        });

        // dat hang
        $('body').on('click', '.btn_place_order', function(e){
            e.preventDefault();
            var loading = $(this).find('img');            
            var genre = $('input[name=genre]:checked', '.form-order').data('text');
            var name_el = $('.form-order .txt-name');
            var name = name_el.val(); //validate
            var phone_el = $('.form-order .txt-phone');                
            var phone = phone_el.val(); //validate
            var email_el = $('.form-order .txt-email');                
            var email = email_el.val(); //validate
            var note = $('.form-order .txt-message').val();            
            var payment_method_title = $('input[name=payment_method]:checked', '.form-order').data('text');
            var payment_method = $('input[name=payment_method]:checked', '.form-order').val(); 
            var addresses;
            var gift_checkbox_el = $('.form-order .gift_checkbox');
            var gift_checkbox = $('input[name=gift_checkbox]:checked', '.form-order').val();
            var gift_message_el = $('.form-order .gift-message');
            var gift_message = gift_message_el.val();

            // chon vi
            var chon_vi = [];
            /*$('select[name^="product_chon_vi"]').each(function() {
                var vi = $(this).val();
                if( vi=='multi' ){
                    var stt = $(this).attr('stt');
                    var product_title = $(this).attr('title');
                    var txt_multi = $('.multi-option-row-'+stt+' .txt_multi').val(); //$('input[name=txt_multi]').val();
                    vi = product_title + ' - Chọn nhiều vị: ' + txt_multi;
                }
                chon_vi.push(vi);
            });*/
            $('input[name^="txt_multi"]').each(function() {                
                var txt_multi = $(this).val().trim();
                if(txt_multi){
                    var product_title = $(this).attr('title');
                    vi = product_title + ' - Chọn vị: ' + txt_multi;
                    chon_vi.push(vi);
                }                
            });
            // end chon vi
            // console.log(chon_vi);

            // dành cho thông báo khi submit đơn hàng thành công
            var cart_categories = $('input[name=cart_categories]').val();
            cart_categories = $.parseJSON(cart_categories);
            var input_city = '';

            if(payment_method=='cod'){
                var address_el = $('.form-order .txt-address');                
                var district_el = $("#selectDistrict option:selected");
                var city_el = $("#selectCity option:selected");
                var address = address_el.val(); //validate
                var district = district_el.text(); //validate
                var city = city_el.text(); //validate 
                var city_id = city_el.val();
                addresses = address+', '+district+', '+city;

                // dành cho thông báo khi submit đơn hàng thành công
                if(city_id=='5'){ // TP.HCM
                    input_city = city_id;
                }
            } else {
                var warehouse_name = $('input[name=warehouse]:checked', '.form-order').data('text');
                addresses = 'Đến nhận hàng tại '+warehouse_name;

                // dành cho thông báo khi submit đơn hàng thành công
                var warehouse_val = $('input[name=warehouse]:checked', '.form-order').val();
                if(warehouse_val=='1'){
                    input_city = '5';
                }
            }

            // Lấy thông báo khi đặt hàng thành công
            var message_code = '';
            var message_success = '';
            var yen_chung_tuoi = 24; // id cat Yến Chưng Tươi
            if( $.inArray(yen_chung_tuoi, cart_categories) != -1 ){
                // nếu giỏ hàng CÓ sp thuộc cat Yến Chưng Tươi 
                if(input_city=='5'){
                    // nếu KH ở TP.HCM
                    message_code = 'MQ==' // 1;
                    message_success = 'Cảm ơn quý khách đã tin tưởng sản phẩm Thượng Yến. Thượng Yến sẽ liên hệ và giao nóng sản phẩm Yến Chưng Tươi trong vòng 2 giờ.<span class="mt-10 text-center">Hỗ trợ quý khách hàng nhanh chóng thông qua <b><a class="btn-link" href="https://zalo.me/0929123678">Zalo Thượng Yến (0929 123 678)</a></b>.</span>';
                } else {
                    message_code = 'Mg=='; //2;
                    message_success = 'Cảm ơn quý khách. Sản phẩm "Yến Chưng Tươi" giao nóng chỉ cung cấp tại TP.HCM. Quý khách có thể tham khảo các sản phẩm "Tổ Yến Chưa Chế Biến" ngay bên dưới.';
                }
            } else {
                // nếu giỏ hàng KHÔNG CÓ sp thuộc cat Yến Chưng Tươi 
                message_code = 'Mw=='; //3;
                message_success = 'Cảm ơn quý khách đã tin tưởng sản phẩm Thượng Yến. Bộ phận chăm sóc khách hàng sẽ liên hệ xác nhận đơn hàng của quý khách.<span class="mt-10 text-center">Hỗ trợ quý khách hàng nhanh chóng thông qua <b><a class="btn-link" href="https://zalo.me/0929123678">Zalo Thượng Yến (0929 123 678)</a></b>.</span>';
            }

            var validate_data = {
                name: name_el, 
                phone: phone_el,
                email: email_el,
                payment_method: payment_method,
                address: address_el,
                district: district_el,
                city: city_el,
                gift_checkbox: gift_checkbox_el,
                gift_message: gift_message_el,

            };

            var error = $('.error-notice');
            var flag = validate_form(validate_data);
            // validate cho phần chọn nhiều vị
            /*$('.slt_chon_vi').each(function(){
                
                var slt_val = $(this).val();
                if( slt_val=='multi' ){
                    var stt = $(this).attr('stt');
                    var txt_multi_el = $('.multi-option-row-'+stt+' .txt_multi');
                    var txt_multi_val = txt_multi_el.val().trim();                    
                    if( ! txt_multi_val ){
                        txt_multi_el.addClass('error');
                        txt_multi_el.focus();
                        flag = false;
                    } else {
                        txt_multi_el.removeClass('error');
                    }
                }                
            });*/            

            if(flag){            
                error.hide();                
                $.ajax({
                    type: "POST",
                    url: url_ajax,
                    data: {
                        action: 'create_order',
                        'name': name,
                        'phone': phone,
                        'email': email,
                        'customer_note': note,
                        'payment_method': 'cod',
                        'payment_method_title': payment_method_title,
                        'addresses': addresses,
                        'chon_vi': chon_vi,
                        'gift_checkbox': gift_checkbox,
                        'gift_message': gift_message
                    },
                    beforeSend: function(){
                        loading.show();
                    },
                    success: function(res){
                        if(res){
                            loading.hide();
                            ga('send', 'event', 'giỏ hàng', 'checkout', 'đặt hàng thành công', {'nonInteraction': 1});
                            var redirect = home_url + '/dat-hang-thanh-cong/?msc='+message_code;
                            window.location.href = redirect;
                        }
                    }
                });
            } else {            
                error.show();
                ga('send', 'event', 'giỏ hàng', 'checkout', 'đặt hàng thiếu thông tin', {'nonInteraction': 1});
            }
        });

        //btn_mua_nhanh
        $('body').on('click', '.btn_mua_nhanh', function(e){
            e.preventDefault();
            var product_id = $(this).data('product_id');
            var qty = $('.input_qty_single').val();
            var loading = $('img.loading-bar');   
            var error_phone = $('.error-phone-notice-mua-nhanh');            
            var phone = $('.input_phone_single');
            var address = $('.input_address_single');
            var error_address = $('.error-address-notice-mua-nhanh');
            var flag = true;
            phone_val = phone.val().trim();
            address_val = address.val().trim();
            if( ! phone_val ){
                error_phone.show();
                phone.focus();
                flag = false;
            } else {
                if( ! check_phone_number(phone_val) ){
                    error_phone.show();               
                    flag = false;
                } else {
                    error_phone.hide();
                }
            }

            if( ! address_val ){
                error_address.show();                
                flag = false;
            } else {
                error_address.hide();
            }

            if(flag){
                add_to_cart_muanhanh(product_id, qty, loading, phone_val, address_val);                
            }            
        });

        // dat hang nhanh
        $('body').on('click', '.btn_place_order_muanhanh', function(e){
            e.preventDefault();
            var loading = $(this).find('img');                           
            var phone = $(this).data('phone');

            $.ajax({
                type: "POST",
                url: url_ajax,
                data: {
                    action: 'create_order_muanhanh',
                    'phone': phone
                },
                beforeSend: function(){
                    loading.show();
                },
                success: function(res){
                    if(res){
                        loading.hide();
                        $('#orderModal').modal('hide');
                        $('#orderSuccessModal').modal('show'); 
                        load_content_cart();
                    }
                }
            });

        });

        // sidebar page menu mobile
        $('body').on('change', '#sidebar-page-menu', function(e){
            e.preventDefault();
            var url = $(this).val();
            window.location.href = url;
        });
        
        // select option procut category for mobile
        $('body').on('change', '#product_category_mobile', function(e){
            e.preventDefault();
            var url = $(this).val();
            if(url){
                //console.log(url);
                window.location.href = url;    
            }            
        });
        
        $('#carousel-testimonial, #carousel-123').carousel({
            interval: false
        });
        
        $(".wp-caption").removeAttr('style');

        $('.btn-tab-cbbq-trigger').on('click', function(e){
            e.preventDefault();
            $('.btn-tab-cbbq').trigger('click');            
            $('html, body').animate({
                scrollTop: $(".product-info-detail").offset().top - 100
            }, 500);
        });
        
        if (sessionStorage.getItem('advertOnce') !== 'true') {
            setTimeout(function(){ 
                $('.formcraft-css a').trigger('click');
            }, 5000);           
            sessionStorage.setItem('advertOnce', 'true');
        }

        // coupon
        $('body').on('click', '.voucher-box .btn-link', function(e){
            e.preventDefault();
            $(this).parent().hide();
            $('.voucher-form').show();
        });
        // apply coupon
        $('body').on('click', '.coupon_apply', function(e){
            e.preventDefault();
            var coupon_code = $('.coupon_code').val().trim();
            var coupon_check = $('.coupon_check');
            var coupon_error = $('.coupon_error');
            var coupon_success = $('.coupon_success');
            var cart_subtotal = $('.cart_subtotal');
            var cart_discount = $('.cart_discount');
            var cart_total = $('.cart_total');
            if(coupon_code==''){                
                coupon_error.find('span').text('Hãy nhập mã giảm giá').show();                
            } else {
                coupon_error.hide();
                $.ajax({
                    type: "POST",
                    url: url_ajax,
                    data: {
                        action: 'apply_coupon',
                        'coupon_code': coupon_code
                    },
                    beforeSend: function(){
                        coupon_error.hide();
                        coupon_success.hide();
                        coupon_check.show();
                    },
                    success: function(res) {
                        coupon_check.hide();                        
                        var obj = $.parseJSON(res); 
                        var status = obj.status;
                        var coupon_code = obj.coupon_code;
                        var coupon_amount = obj.coupon_amount;
                        var subtotal = obj.subtotal;
                        var total = obj.total;
                        var notice = obj.notice;
                        
                        if(status=='success'){
                            coupon_error.hide();
                            coupon_success.find('span').text(notice);
                            coupon_success.show();
                            cart_subtotal.find('em').text(subtotal);
                            cart_subtotal.show();
                            cart_discount.find('i').text(coupon_amount);
                            cart_discount.show();
                            cart_total.find('b').text(total);
                        }
                        if(status=='fail'){
                            coupon_success.hide();
                            if(!coupon_code){
                                cart_subtotal.hide();
                                cart_discount.hide();
                            }                            
                            coupon_error.find('span').text(notice);
                            coupon_error.show();
                            cart_total.find('b').text(total);
                        }
                    }
                });
            }
        });

        // load home_order_box        
        $(window).on('load', function(){
            load_count_item_cart();
            //load_home_order_box();
        });

        // su kien select chon vi trong gio hang        
        $('body').on('change', '.slt_chon_vi', function(e){            
            e.preventDefault();

            var val = $(this).val();
            var stt = $(this).attr('stt');            
            var multi_option = $('.multi-option-row-'+stt);            
            if(val=='multi'){
                multi_option.show();                
            } else {
                multi_option.hide();
            }
        });

        // su kien chon lai trong gio hang (khi gio hang co combo 6)
        $('body').on('click', '.btn_chon_lai', function(e){
            e.preventDefault();                        
            var product_id = $(this).data('product_id');
            var cart_body = $('.card .cart-body');
            var loading = cart_body.find('.loading');
            $.ajax({
                type: "POST",
                url: url_ajax,
                data: {
                    action: 'add_to_cart_chon_lai',
                    'product_id': product_id                    
                },
                beforeSend: function(){
                    loading.show();
                },
                success: function(res) {
                    loading.hide();
                    if (res) {
                        cart_body.html(res);   
                    }
                }
            });
        });
                
    }

    function load_home_order_box(){
        if( $('.content').hasClass('home-page') ){
            var loading = $('.loading');
            var order_box = $('.list_order_box');
            $.ajax({
                url: url_ajax,            
                type: 'POST',
                data : {
                    action : 'home_order_box'
                },
                beforeSend: function(){
                    loading.show();
                },
                success: function(res){
                    loading.hide();
                    order_box.html(res);
                }
            });
        }        
    }

    function add_to_cart(product_id, qty, loading){
        if( qty > 0){
            var redirect = home_url + '/gio-hang';
            $.ajax({
                type: "POST",
                url: url_ajax,
                data: {
                    action: 'add_to_cart',
                    'product_id': product_id,
                    'qty': qty
                },
                beforeSend: function(){
                    loading.show();
                },
                success: function(res) {
                    if(res){
                        window.location.href = redirect;
                    }
                }
            });
        } else {
            alert('Số lượng bạn nhập chưa chính xác!');
        }        
    }

    function add_to_cart_muanhanh(product_id, qty, loading, phone, address){
        if( qty > 0){
            $.ajax({
                type: "POST",
                url: url_ajax,
                data: {
                    action: 'add_to_cart_muanhanh',
                    'product_id': product_id,
                    'qty': qty,
                    'phone': phone,
                    'address': address
                },
                beforeSend: function(){
                    loading.show();
                },
                success: function(res) {
                    if (res) {
                        ga('send', 'event', 'chi tiết sản phẩm', 'click mua nhanh', phone, {'nonInteraction': 1});
                        var redirect = home_url + '/dat-hang-thanh-cong/?msc=Mw==';
                        window.location.href = redirect;
                        /*var muanhanh = true;
                        $('.cart-empty').hide();                        
                        loading.hide();                        
                        load_content_cart(muanhanh, phone);                        
                        $('.cart-button').trigger('click');     */                   
                    } else {
                        //$('.cart-empty').hide();
                    }
                }
            });
        } else {
            alert('Số lượng bạn nhập chưa chính xác!');
        }        
    }

    /*function load_content_cart(muanhanh=false, phone=''){ 
        
        // count item cart
        load_count_item_cart();

        $.ajax({
            type: "POST",
            url: url_ajax,
            data: {
                action: 'content_cart',
                'muanhanh': muanhanh,
                'phone': phone
            },
            beforeSend: function(){
                return 0;
            },
            success: function(res) {
                if (res) {
                    $('#orderModal .modal-content').html(res);                    
                    return 1;
                }
            }
        });
    }*/
    function load_content_cart(){         

        $.ajax({
            type: "POST",
            url: url_ajax,
            data: {
                action: 'content_cart',
            },
            beforeSend: function(){
                return 0;
            },
            success: function(res) {
                if (res) {
                    $('.cart-body').html(res);                    
                    return 1;
                }
            }
        });
    }

    function load_count_item_cart(){
        $.ajax({
            type: "POST",
            url: url_ajax,
            data: {
                action: 'count_item_cart'
            },
            success: function(res) {
                if (res) {
                    $('.cart-button .cart-count').html(res);                    
                }
            }
        });
    }

    function add_update_url_param(uri, paramKey, paramVal) {
        var re = new RegExp("([?&])" + paramKey + "=[^&#]*", "i");
        if (re.test(uri)) {
            uri = uri.replace(re, '$1' + paramKey + "=" + paramVal);
        } else {
            var separator = /\?/.test(uri) ? "&" : "?";
            uri = uri + separator + paramKey + "=" + paramVal;
        }
        return uri;
    }

    function validate_form(data){

        var flag = true;        
        var name = data['name'];
        var phone = data['phone'];
        var email =  data['email'];
        var payment_method = data['payment_method'];
        var address = data['address'];
        var district = data['district'];
        var city = data['city'];
        var gift_checkbox = data['gift_checkbox'];
        var gift_message = data['gift_message'];
        
        if( ! name.val().trim() ){
            //name.addClass('error');
            name.next().find('span').text('Vui lòng nhập đúng họ tên.');
            name.next().show();
            flag = false;
        } else {
            //name.removeClass('error');
            name.next().find('span').text('');
            name.next().hide();
        }

        phone_val = phone.val().trim();
        if( ! phone_val ){
            //phone.addClass('error');            
            phone.next().find('span').text('Vui lòng nhập đúng số điện thoại.');            
            phone.next().show();
            flag = false;
        } else {
            if( ! check_phone_number(phone_val) ){
                //phone.addClass('error');
                phone.next().find('span').text('Số điện thoại không hợp lệ');
                phone.next().show();
                flag = false;
            } else {
                //phone.removeClass('error');
                phone.next().find('span').text('');
                phone.next().hide();
            }
        }

        //check_email
        email_val = email.val().trim();
        if( email_val ){
            if( !check_email(email_val) ){
                //email.addClass('error');
                email.next().find('span').text('Email không hợp lệ');
                email.next().show();
                flag = false;
            } else {
                //email.removeClass('error');
                email.next().find('span').text('');
                email.next().hide();
            }            
        }

        if( payment_method=='cod' ){           

            var district_el = $('#selectDistrict');
            if( district.val()=='0' ){
                //district_el.addClass('error'); 
                district_el.next().find('span').text('Vui lòng chọn quận/ huyện');               
                district_el.next().show();
                flag = false;
            } else {
                //district_el.removeClass('error');
                district_el.next().find('span').text('');
                district_el.next().hide();
            }

            var city_el = $('#selectCity');
            if( city.val()=='0' ){
                //city_el.addClass('error');  
                city_el.next().find('span').text('Vui lòng chọn Tỉnh/ thành phố');
                city_el.next().show();
                flag = false;
            } else {
                //city_el.removeClass('error');
                city_el.next().find('span').text('');
                city_el.next().hide();
            }

            if( ! address.val().trim() ){
                //address.addClass('error'); 
                address.next().find('span').text('Vui lòng nhập Số nhà, tên đường, phường/ xã');               
                address.next().show();
                flag = false;
            } else {
                //address.removeClass('error');
                address.next().find('span').text('');
                address.next().hide();
            }
        }

        /*if( gift_checkbox.is(':checked') ){
            if(!gift_message.val().trim()){
                gift_message.next().find('span').text('Vui lòng ghi chú nội dung thiệp');
                gift_message.next().show();
                flag = false;
            } else {
                gift_message.next().find('span').text('');
                gift_message.next().hide();
            }
        }*/

        return flag;
    }

    function check_phone_number(phone) {
        
        var flag = false;        
        phone = phone.replace('(+84)', '0');
        phone = phone.replace('+84', '0');
        phone = phone.replace('0084', '0');
        phone = phone.replace(/ /g, '');
        if (phone != '') {
            var firstNumber = phone.substring(0, 2);
            if ((firstNumber == '09' || firstNumber == '08' || firstNumber == '03' || firstNumber == '07' || firstNumber == '05') && phone.length == 10) {
                if (phone.match(/^\d{10}/)) {
                    flag = true;
                }
            } /*else if (firstNumber == '01' && phone.length == 11) {
                if (phone.match(/^\d{11}/)) {
                    flag = true;
                }
            }*/
        }
        return flag;
    }

    function check_email(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function option_district_by_city_id(city_id){
        
        if( !city_id ) return;

        var list = '{"4":{"4":"T\u1eeb Li\u00eam","5":"Thanh Tr\u00ec","6":"S\u00f3c S\u01a1n","7":"Gia L\u00e2m","8":"\u0110\u00f4ng Anh","9":"Long Bi\u00ean","10":"Ho\u00e0ng Mai","11":"C\u1ea7u Gi\u1ea5y","12":"T\u00e2y H\u1ed3","13":"Thanh Xu\u00e2n","14":"Hai B\u00e0 Tr\u01b0ng","15":"\u0110\u1ed1ng \u0110a","16":"Ba \u0110\u00ecnh","17":"Ho\u00e0n Ki\u1ebfm","18":"M\u00ea Linh","19":"V\u00e2n \u0110i\u1ec1n","20":"Ba V\u00ec","21":"Ch\u01b0\u01a1ng M\u1ef9","22":"\u0110an Ph\u01b0\u1ee3ng","23":"H\u00e0 \u0110\u00f4ng","24":"Ho\u00e0i \u0110\u1ee9c","25":"M\u1ef9 \u0110\u1ee9c","26":"Ph\u00fa Xuy\u00ean","27":"Ph\u00fac Th\u1ecd","28":"Qu\u1ed1c Oai","29":"S\u01a1n T\u00e2y","30":"Th\u1ea1ch Th\u1ea5t","31":"Thanh Oai","32":"Th\u01b0\u1eddng T\u00edn","33":"\u1ee8ng H\u00f2a","34":"Nam T\u1eeb Li\u00eam"},"5":{"35":"Qu\u1eadn 1","36":"Qu\u1eadn 2","37":"Qu\u1eadn 3","38":"Qu\u1eadn 4","39":"Qu\u1eadn 5","40":"Qu\u1eadn 6","41":"Qu\u1eadn 7","42":"Qu\u1eadn 8","43":"Qu\u1eadn 9","44":"Qu\u1eadn 10","45":"Qu\u1eadn 11","46":"Qu\u1eadn 12","47":"Qu\u1eadn Ph\u00fa Nhu\u1eadn","48":"Qu\u1eadn B\u00ecnh Th\u1ea1nh","49":"Qu\u1eadn T\u00e2n B\u00ecnh","50":"Qu\u1eadn T\u00e2n Ph\u00fa","51":"Qu\u1eadn G\u00f2 V\u1ea5p","52":"Qu\u1eadn Th\u1ee7 \u0110\u1ee9c","53":"Qu\u1eadn B\u00ecnh T\u00e2n","54":"Huy\u1ec7n B\u00ecnh Ch\u00e1nh","55":"Huy\u1ec7n C\u1ee7 Chi","56":"Huy\u1ec7n Nh\u00e0 B\u00e8","57":"Huy\u1ec7n C\u1ea7n Gi\u1edd","58":"Huy\u1ec7n H\u00f3c M\u00f4n"},"6":{"66":"\u0110\u1ea3o Ho\u00e0ng Sa","67":"H\u1ea3i Ch\u00e2u","68":"H\u00f2a Vang","69":"Li\u00ean Chi\u1ec3u","70":"Ng\u0169 H\u00e0nh S\u01a1n","71":"S\u01a1n Tr\u00e0","72":"Thanh Kh\u00ea","73":"C\u1ea9m L\u1ec7"},"7":{"74":"Bi\u00ean H\u00f2a","75":"\u0110\u1ecbnh Qu\u00e1n","76":"Long Kh\u00e1nh","77":"Long Th\u00e0nh","78":"Nh\u01a1n Tr\u1ea1ch","79":"T\u00e2n Ph\u00fa","80":"Th\u1ed1ng Nh\u1ea5t","81":"V\u0129nh C\u1eedu","82":"Xu\u00e2n L\u1ed9c","83":"Tr\u1ea3ng Bom","84":"C\u1ea9m M\u1ef9"},"8":{"59":"B\u1ebfn C\u00e1t","60":"D\u1ea7u Ti\u1ebfng","61":"D\u0129 An","62":"T\u00e2n Uy\u00ean","63":"Th\u1ee7 D\u1ea7u M\u1ed9t","64":"Thu\u1eadn An","65":"Ph\u00fa Gi\u00e1o"},"9":{"85":"B\u1ebfn L\u1ee9c","86":"C\u1ea7n \u0110\u01b0\u1edbc","87":"C\u1ea7n Giu\u1ed9c","88":"Ch\u00e2u Th\u00e0nh","89":"\u0110\u1ee9c H\u00f2a","90":"\u0110\u1ee9c Hu\u1ec7","91":"M\u1ed9c H\u00f3a","92":"T\u00e2n An","93":"T\u00e2n H\u01b0ng","94":"T\u00e2n Th\u1ea1nh","95":"T\u00e2n Tr\u1ee5","96":"Th\u1ea1nh H\u00f3a","97":"Th\u1ee7 Th\u1eeba","98":"V\u0129nh H\u01b0ng","99":"Li\u00ean H\u01b0ng"},"10":{"100":"An Ph\u00fa","101":"Ch\u00e2u \u0110\u1ed1c","102":"Ch\u00e2u Ph\u00fa","103":"Ch\u00e2u Th\u00e0nh","104":"Ch\u1ee3 M\u1edbi","105":"Long Xuy\u00ean","106":"Ph\u00fa T\u00e2n","107":"T\u00e2n Ch\u00e2u","108":"Tho\u1ea1i S\u01a1n","109":"T\u1ecbnh Bi\u00ean","110":"Tri T\u00f4n"},"11":{"111":"B\u00e0 R\u1ecba","112":"Ch\u00e2u \u0110\u1ee9c","113":"C\u00f4n \u0110\u1ea3o","114":"Long \u0110\u1ea5t","115":"T\u00e2n Th\u00e0nh","116":"TP.V\u0169ng T\u00e0u","117":"Xuy\u00ean M\u1ed9c","118":"Long \u0110i\u1ec1n","119":"\u0110\u1ea5t \u0110\u1ecf"},"12":{"120":"B\u1eafc Giang","121":"Hi\u1ec7p H\u00f2a","122":"L\u1ea1ng Giang","123":"L\u1ee5c Nam","124":"L\u1ee5c Ng\u1ea1n","125":"S\u01a1n \u0110\u1ed9ng","126":"T\u00e2n Y\u00ean","127":"Vi\u1ec7t Y\u00ean","128":"Y\u00ean D\u0169ng","129":"Y\u00ean Th\u1ebf"},"13":{"130":"Ba B\u1ec3","131":"B\u1eafc K\u1ea1n","132":"B\u1ea1ch Th\u00f4ng ","133":"Ch\u1ee3 \u0110\u1ed3n","134":"Ch\u1ee3 M\u1edbi","135":"Na R\u00ec","136":"Ng\u00e2n S\u01a1n"},"14":{"137":"B\u1ea1c Li\u00eau","138":"Gi\u00e1 Rai","139":"H\u1ed3ng D\u00e2n","140":"V\u0129nh L\u1ee3i","141":"Ph\u01b0\u1edbc Long","142":"\u0110\u00f4ng H\u1ea3i","143":"H\u00f2a B\u00ecnh"},"15":{"144":"B\u1eafc Ninh","145":"Gia B\u00ecnh","146":"L\u01b0\u01a1ng T\u00e0i","147":"Qu\u1ebf V\u00f5","148":"Thu\u1eadn Th\u00e0nh","149":"Ti\u00ean Du","150":"T\u1eeb S\u01a1n","151":"Y\u00ean Phong"},"16":{"152":"Ba Tri","153":"B\u1ebfn Tre","154":"B\u00ecnh \u0110\u1ea1i","155":"Ch\u00e2u Th\u00e0nh","156":"Ch\u1ee3 L\u00e1ch","157":"Gi\u1ed3ng Tr\u00f4m","158":"M\u1ecf C\u00e0y","159":"Th\u1ea1nh Ph\u00fa"},"17":{"160":"An L\u00e3o","161":"An Nh\u01a1n","162":"Ho\u00e0i \u00c2n","163":"Ho\u00e0i Nh\u01a1n","164":"Ph\u00f9 C\u00e1t","165":"Ph\u00f9 M\u1ef9","166":"Qui Nh\u01a1n","167":"T\u00e2y S\u01a1n","168":"Tuy Ph\u01b0\u1edbc","169":"V\u00e2n Canh","170":"V\u0129nh Th\u1ea1nh"},"18":{"171":"B\u00ecnh Long","172":"Ph\u01b0\u1edbc Long","173":"B\u00f9 \u00d0\u0103ng","174":"Ch\u01a1n Th\u00e0nh","175":"\u0110\u1ed3ng Xo\u00e0i","176":"\u0110\u1ed3ng Ph\u00fa","177":"B\u00f9 \u0110\u1ed1p","178":"B\u00f9 Gia M\u1eadp","179":"H\u1edbn Qu\u1ea3n","180":"L\u1ed9c Ninh"},"19":{"181":"B\u1eafc B\u00ecnh","182":"\u0110\u1ee9c Linh","183":"H\u00e0m T\u00e2n","184":"H\u00e0m Thu\u1eadn B\u1eafc","185":"H\u00e0m Thu\u1eadn Nam","186":"Phan Thi\u1ebft","187":"Ph\u00fa Qu\u00ed","188":"T\u00e1nh Linh","189":"Tuy Phong","190":"La Gi"},"20":{"191":"TP. C\u00e0 Mau","192":"C\u00e1i N\u01b0\u1edbc","193":"\u0110\u1ea7m D\u01a1i","194":"Ng\u1ecdc Hi\u1ec3n","195":"Th\u1edbi B\u00ecnh","196":"Tr\u1ea7n V\u0103n Th\u1eddi","197":"U Minh","198":"N\u0103m C\u0103n","199":"Ph\u00fa T\u00e2n"},"21":{"200":"TP.C\u1ea7n Th\u01a1","201":"\u00d4 M\u00f4n","202":"Th\u1ed1t N\u1ed1t","203":"Ninh Ki\u1ec1u","204":"C\u00e1i R\u0103ng","205":"B\u00ecnh Th\u1ee7y","206":"Phong \u0110i\u1ec1n","207":"C\u1edd \u0110\u1ecf","208":"Th\u1edbi Lai","209":"V\u0129nh Th\u1ea1nh"},"22":{"210":"B\u1ea3o L\u1ea1c","211":"Cao B\u1eafng","212":"H\u1ea1 Lang","213":"H\u00e0 Qu\u1ea3ng","214":"H\u00f2a An","215":"Nguy\u00ean B\u00ecnh","216":"Qu\u1ea3ng H\u00f2a","217":"Th\u1ea1ch An","218":"Th\u00f4ng N\u00f4ng","219":"Tr\u00e0 L\u0129nh","220":"Tr\u00f9ng Kh\u00e1nh"},"23":{"221":"Bu\u00f4n \u0110\u00f4n","222":"Bu\u00f4n Ma Thu\u1ed9t","223":"C\u01b0 J\u00fat","224":"C\u01b0 M\'gar","225":"\u0110\u1eafk Mil","226":"\u0110\u1eafk N\u00f4ng","227":"\u0110\u1eafk R\'l\u1ea5p","228":"Ea H\'leo","229":"Ea Kra","230":"Ea S\u00fap","231":"Kr\u00f4ng A Na","232":"Kr\u00f4ng B\u00f4ng","233":"Kr\u00f4ng B\u00fak","234":"Kr\u00f4ng N\u0103ng","235":"Kr\u00f4ng N\u00f4","236":"Kr\u00f4ng P\u1eafc","237":"L\u1eafk","238":"M\'\u0110r\u1eaft"},"24":{"239":"Th\u1ecb x\u00e3 Gia Ngh\u0129a","240":"C\u01b0 J\u00fat","241":"\u0110\u1eafk Glong","242":"\u0110\u1eafk Mil","243":"\u0110\u1eafk R\'L\u1ea5p","244":"\u0110\u1eafk Song","245":"Kr\u00f4ng N\u00f4","246":"Tuy \u0110\u1ee9c"},"25":{"247":"TP.\u0110i\u1ec7n Bi\u00ean Ph\u1ee7","248":"Th\u1ecb x\u00e3 M\u01b0\u1eddng Lay","249":"H.\u0110i\u1ec7n Bi\u00ean","250":"H.\u0110i\u1ec7n Bi\u00ean \u0110\u00f4ng","251":"H.M\u01b0\u1eddng \u1ea2ng","252":"H.M\u01b0\u1eddng Ch\u00e0","253":"H.M\u01b0\u1eddng Nh\u00e9","254":"H.T\u1ee7a Ch\u00f9a","255":"H.Tu\u1ea7n Gi\u00e1o","256":"H.N\u1eadm P\u1ed3"},"26":{"257":"Cao L\u00e3nh","258":"Ch\u00e2u Th\u00e0nh","259":"H\u1ed3ng Ng\u1ef1","260":"Lai Vung","261":"L\u1ea5p V\u00f2","262":"Tam N\u00f4ng","263":"T\u00e2n H\u1ed3ng","264":"Thanh B\u00ecnh","265":"Th\u00e1p M\u01b0\u1eddi","266":"Sa \u0110\u00e9c"},"27":{"267":"An Kh\u00ea","268":"Ayun Pa ","269":"Ch\u01b0 P\u0103h","270":"Ch\u01b0 Pr\u00f4ng ","271":"Ch\u01b0 S\u00ea ","272":"\u0110\u1ee9c C\u01a1 ","273":"KBang ","274":"Kr\u00f4ng Chro","275":"Kr\u00f4ng Pa ","276":"La Grai ","277":"Mang Yang ","278":"Pleiku","279":"\u0110\u1eafk \u0110oa"},"28":{"280":"B\u1eafc M\u00ea","281":"B\u1eafc Quang","282":"\u0110\u1ed3ng V\u0103n","283":"H\u00e0 Giang","284":"Ho\u00e0ng Su Ph\u00ec","285":"M\u00e8o V\u1ea1t","286":"Qu\u1ea3n B\u1ea1","287":"V\u1ecb Xuy\u00ean","288":"X\u00edn M\u1ea7n","289":"Y\u00ean Minh"},"29":{"290":"B\u00ecnh L\u1ee5c","291":"Duy Ti\u00ean","292":"Kim B\u1ea3ng","293":"L\u00fd Nh\u00e2n","294":"Ph\u1ee7 L\u00fd","295":"Thanh Li\u00eam"},"30":{"296":"C\u1ea9m Xuy\u00ean","297":"Can L\u1ed9c","298":"\u0110\u1ee9c Th\u1ecd","299":"H\u00e0 T\u0129nh","300":"H\u1ed3ng L\u0129nh","301":"H\u01b0\u01a1ng Kh\u00ea","302":"H\u01b0\u01a1ng S\u01a1n","303":"K\u1ef3 Anh","304":"Nghi Xu\u00e2n","305":"Th\u1ea1ch H\u00e0","306":"V\u0169 Quang","307":"L\u1ed9c H\u00e0"},"31":{"308":"B\u00ecnh Giang","309":"C\u1ea9m Gi\u00e0ng","310":"Ch\u00ed Linh","311":"Gia L\u1ed9c","312":"H\u1ea3i D\u01b0\u01a1ng","313":"Kim Th\u00e0nh","314":"Nam S\u00e1ch","315":"Ninh Giang","316":"Kinh M\u00f4n","317":"Ninh Giang","318":"Thanh H\u00e0","319":"Thanh Mi\u1ec7n","320":"T\u1eeb K\u1ef3"},"32":{"321":"An L\u00e3o","322":"B\u1ea1ch Long V\u1ef9","323":"\u0110\u1ed3 S\u01a1n","324":"H\u1ed3ng B\u00e0ng","325":"Ki\u1ebfn An","326":"Ki\u1ebfn Th\u1ee5y","327":"L\u00ea Ch\u00e2n","328":"Ng\u00f4 Quy\u1ec1n","329":"Th\u1ee7y Nguy\u00ean","330":"Ti\u00ean L\u00e3ng","331":"V\u0129nh B\u1ea3o","332":"C\u00e1t H\u1ea3i","333":"D\u01b0\u01a1ng Kinh","334":"H\u1ea3i An","335":"Huy\u1ec7n An D\u01b0\u01a1ng","336":"An D\u01b0\u01a1ng"},"33":{"337":"Ch\u00e2u Th\u00e0nh","338":"Long M\u1ef9","339":"Ph\u1ee5ng Hi\u1ec7p","340":"V\u1ecb Thanh","341":"V\u1ecb Th\u1ee7y","342":"Ch\u00e2u Th\u00e0nh A","343":"Ng\u00e3 B\u1ea3y"},"34":{"344":"\u0110\u00e0 B\u1eafc","345":"H\u00f2a B\u00ecnh","346":"Kim B\u00f4i","347":"K\u1ef3 S\u01a1n","348":"L\u1ea1c S\u01a1n","349":"L\u1ea1c Th\u1ee7y","350":"L\u01b0\u01a1ng S\u01a1n","351":"Mai Ch\u00e2u","352":"T\u00e2n L\u1ea1c","353":"Y\u00ean Th\u1ee7y","354":"Cao Phong"},"35":{"355":"\u00c2n Thi","356":"TP.H\u01b0ng Y\u00ean","357":"Kho\u00e1i Ch\u00e2u","358":"Ti\u00ean L\u1eef","359":"V\u0103n Giang","360":"V\u0103n L\u00e2m","361":"Y\u00ean M\u1ef9","362":"\u00c2n Thi","363":"H\u01b0ng Y\u00ean","364":"Kho\u00e1i Ch\u00e2u","365":"Kim \u0110\u1ed9ng","366":"M\u1ef9 H\u00e0o","367":"Ph\u00f9 C\u1eeb"},"36":{"368":"Nha Trang","369":"Kh\u00e1nh V\u0129nh","370":"Di\u00ean Kh\u00e1nh","371":"Ninh H\u00f2a","372":"Kh\u00e1nh S\u01a1n","373":"Cam Ranh","374":"Tr\u01b0\u1eddng Sa","375":"V\u1ea1n Ninh","376":"Cam L\u00e2m"},"37":{"377":"An Bi\u00ean","378":"An Minh","379":"Ch\u00e2u Th\u00e0nh","380":"G\u00f2 Quao","381":"G\u1ed3ng Gi\u1ec1ng","382":"H\u00e0 Ti\u00ean","383":"H\u00f2n \u0110\u1ea5t","384":"Ki\u00ean H\u1ea3i","385":"Ph\u00fa Qu\u1ed1c","386":"R\u1ea1ch Gi\u00e1","387":"T\u00e2n Hi\u1ec7p","388":"V\u0129nh Thu\u1eadn"},"38":{"389":"\u0110\u1eafk Glei","390":"\u0110\u1eafk T\u00f4","391":"Kon Pl\u00f4ng","392":"Kon Tum","393":"Ng\u1ecdc H\u1ed3i","394":"Sa Th\u1ea7y","395":"\u0110\u1eafk Glei","396":"\u0110\u1eafk H\u00e0","397":"\u0110\u1eafk T\u00f4","398":"Kon Pl\u00f4ng","399":"Kon Tum","400":"Ng\u1ecdc H\u1ed3i","401":"Sa Th\u1ea7y"},"39":{"402":"\u0110i\u1ec7n Bi\u00ean","403":"\u0110i\u1ec7n Bi\u00ean \u0110\u00f4ng","404":"\u0110i\u1ec7n Bi\u00ean Ph\u1ee7","405":"Lai Ch\u00e2u","406":"M\u01b0\u1eddng Lay","407":"M\u01b0\u1eddng T\u00e8","408":"Phong Th\u1ed5","409":"S\u00ecn H\u1ed3","410":"T\u1ee7a Ch\u00f9a","411":"Tu\u1ea7n Gi\u00e1o"},"40":{"412":"B\u1ea3o L\u00e2m","413":"B\u1ea3o L\u1ed9c","414":"C\u00e1t Ti\u00ean","415":"\u0110\u00e0 L\u1ea1t","416":"\u0110\u1ea1 T\u1ebbh","417":"\u0110\u1ea1 Huoai","418":"Di Linh","419":"\u0110\u01a1n D\u01b0\u01a1ng","420":"\u0110\u1ee9c Tr\u1ecdng","421":"L\u1ea1c D\u01b0\u01a1ng","422":"L\u00e2m H\u00e0"},"41":{"423":"B\u1eafc S\u01a1n","424":"B\u00ecnh Gia","425":"Cao L\u0103ng","426":"Cao L\u1ed9c","427":"\u0110\u00ecnh L\u1eadp","428":"H\u1eefu L\u0169ng","429":"L\u1ea1ng S\u01a1n","430":"L\u1ed9c B\u00ecnh","431":"Tr\u00e0ng \u0110\u1ecbnh","432":"V\u0103n L\u00e3ng","433":"V\u0103n Quang"},"42":{"434":"B\u1eafc H\u00e0","435":"B\u1ea3o Th\u1eafng","436":"B\u1ea3o Y\u00ean","437":"B\u00e1t X\u00e1t","438":"Cam \u0110\u01b0\u1eddng","439":"L\u00e0o Cai","440":"M\u01b0\u1eddng Kh\u01b0\u01a1ng","441":"Sa Pa","442":"Than Uy\u00ean","443":"V\u0103n B\u00e0n","444":"Xi Ma Cai"},"43":{"445":"Giao Th\u1ee7y","446":"H\u1ea3i H\u1eadu","447":"M\u1ef9 L\u1ed9c","448":"Nam \u0110\u1ecbnh","449":"Nam Tr\u1ef1c","450":"Ngh\u0129a H\u01b0ng","451":"Tr\u1ef1c Ninh","452":"V\u1ee5 B\u1ea3n","453":"Xu\u00e2n Tr\u01b0\u1eddng","454":"\u00dd Y\u00ean"},"44":{"455":"Anh S\u01a1n","456":"Con Cu\u00f4ng","457":"C\u1eeda L\u00f2","458":"Di\u1ec5n Ch\u00e2u","459":"\u0110\u00f4 L\u01b0\u01a1ng","460":"H\u01b0ng Nguy\u00ean","461":"K\u1ef3 S\u01a1n","462":"Nam \u0110\u00e0n","463":"Nghi L\u1ed9c","464":"Ngh\u0129a \u0110\u00e0n","465":"Qu\u1ebf Phong","466":"Qu\u1ef3 Ch\u00e2u","467":"Qu\u1ef3 H\u1ee3p","468":"Qu\u1ef3nh L\u01b0u","469":"T\u00e2n K\u1ef3","470":"Thanh Ch\u01b0\u01a1ng","471":"T\u01b0\u01a1ng D\u01b0\u01a1ng","472":"Vinh","473":"Y\u00ean Th\u00e0nh","474":"Th\u00e1i H\u00f2a"},"45":{"475":"Hoa L\u01b0","476":"Kim S\u01a1n","477":"Nho Quan","478":"Ninh B\u00ecnh","479":"Tam \u0110i\u1ec7p","480":"Y\u00ean Kh\u00e1nh","481":"Y\u00ean M\u00f4","482":"Gia Vi\u1ec5n"},"46":{"483":"Ninh H\u1ea3i","484":"Ninh Ph\u01b0\u1edbc","485":"Ninh S\u01a1n","486":"Phan Rang - Th\u00e1p Ch\u00e0m","487":"B\u00e1c \u00c1i","488":"Thu\u1eadn B\u1eafc","489":"Thu\u1eadn Nam"},"47":{"490":"\u0110oan H\u00f9ng","491":"H\u1ea1 H\u00f2a","492":"L\u00e2m Thao","493":"Ph\u00f9 Ninh","494":"Ph\u00fa Th\u1ecd","495":"S\u00f4ng Thao","496":"Tam N\u00f4ng","497":"Thanh Ba","498":"Thanh S\u01a1n","499":"Thanh Th\u1ee7y","500":"Vi\u1ec7t Tr\u00ec","501":"Y\u00ean L\u1eadp","502":"C\u1ea9m Kh\u00ea","503":"T\u00e2n S\u01a1n"},"48":{"504":"\u0110\u1ed3ng Xu\u00e2n","505":"S\u01a1n H\u00f2a","506":"S\u00f4ng C\u1ea7u","507":"S\u00f4ng Hinh","508":"Tuy An","509":"Tuy H\u00f2a","510":"\u0110\u00f4ng H\u00f2a","511":"T\u00e2y H\u00f2a","512":"Ph\u00fa H\u00f2a"},"49":{"513":"B\u1ed1 Tr\u1ea1ch","514":"\u0110\u1ed3ng H\u1edbi","515":"L\u1ec7 Th\u1ee7y","516":"Qu\u1ea3ng Ninh","517":"Qu\u1ea3ng Tr\u1ea1ch","518":"Tuy\u00ean H\u00f3a","519":"Minh H\u00f3a"},"50":{"520":"\u0110\u1ea1i L\u1ed9c","521":"\u0110i\u1ec7n B\u00e0n","522":"Duy Xuy\u00ean","523":"Hi\u00ean","524":"Hi\u1ec7p \u0110\u1ee9c","525":"TP.H\u1ed9i An","526":"Nam Giang","527":"N\u00fai Th\u00e0nh","528":"Ph\u01b0\u1edbc S\u01a1n","529":"Qu\u1ebf S\u01a1n","530":"Tam K\u1ef3","531":"Th\u0103ng B\u00ecnh","532":"Ti\u00ean Ph\u01b0\u1edbc","533":"Tr\u00e0 My"},"51":{"534":"Ba T\u01a1","535":"B\u00ecnh S\u01a1n","536":"\u0110\u1ee9c Ph\u1ed5","537":"L\u00fd S\u01a1n","538":"Minh Long","539":"M\u1ed9 \u0110\u1ee9c","540":"Ngh\u0129a H\u00e0nh","541":"Qu\u1ea3ng Ng\u00e3i","542":"S\u01a1n H\u00e0","543":"S\u01a1n T\u00e2y","544":"S\u01a1n T\u1ecbnh","545":"Tr\u00e0 B\u1ed3ng","546":"T\u01b0 Ngh\u0129a"},"52":{"547":"Ba Ch\u1ebf","548":"B\u00ecnh Li\u00eau","549":"C\u1ea9m Ph\u1ea3","550":"C\u00f4 T\u00f4","551":"\u0110\u00f4ng Tri\u1ec1u","552":"TP.H\u1ea1 Long","553":"Ho\u00e0nh B\u1ed3","554":"M\u00f3ng C\u00e1i","555":"Qu\u1ea3ng H\u00e0","556":"Ti\u00ean Y\u00ean","557":"U\u00f4ng B\u00ed","558":"V\u00e2n \u0110\u1ed3n","559":"Y\u00ean H\u01b0ng"},"53":{"560":"Cam L\u1ed9","561":"\u0110a Kr\u00f4ng","562":"\u0110\u00f4ng H\u00e0","563":"Gio Linh","564":"H\u1ea3i L\u0103ng","565":"H\u01b0\u1edbng H\u00f3a","566":"Qu\u1ea3ng Tr\u1ecb","567":"Tri\u1ec7u Phong","568":"V\u0129nh Linh"},"54":{"569":"K\u1ebf S\u00e1ch","570":"Long Ph\u00fa","571":"M\u1ef9 T\u00fa","572":"M\u1ef9 Xuy\u00ean","573":"S\u00f3c Tr\u0103ng","574":"Thanh Tr\u1ecb","575":"V\u0129nh Ch\u00e2u"},"55":{"576":"B\u1eafc Y\u00ean","577":"Mai S\u01a1n","578":"M\u1ed9c Ch\u00e2u","579":"Mu\u1eddng La","580":"Ph\u00f9 Y\u00ean","581":"Qu\u1ef3nh Nhai","582":"S\u01a1n La","583":"S\u00f4ng M\u00e3","584":"Thu\u1eadn Ch\u00e2u","585":"Y\u00ean Ch\u00e2u","586":"S\u1ed1p C\u1ed9p"},"56":{"587":"B\u1ebfn C\u1ea7u","588":"Ch\u00e2u Th\u00e0nh","589":"D\u01b0\u01a1ng Minh Ch\u00e2u","590":"G\u00f2 D\u1ea7u","591":"H\u00f2a Th\u00e0nh","592":"T\u00e2n Bi\u00ean","593":"T\u00e2n Ch\u00e2u","594":"T\u00e2y Ninh","595":"Tr\u1ea3ng B\u00e0ng"},"57":{"596":"\u0110\u00f4ng H\u01b0ng","597":"H\u01b0ng H\u00e0","598":"Ki\u1ebfn X\u01b0\u01a1ng","599":"Qu\u1ef3nh Ph\u1ee5","600":"Th\u00e1i B\u00ecnh","601":"Th\u00e1i Th\u1ee5y","602":"Ti\u1ec1n H\u1ea3i","603":"V\u0169 Th\u01b0"},"58":{"604":"\u0110\u1ea1i T\u1eeb","605":"\u0110\u1ecbnh H\u00f3a","606":"\u0110\u1ed3ng H\u1ef7","607":"Ph\u1ed5 Y\u00ean","608":"Ph\u00fa B\u00ecnh","609":"Ph\u00fa L\u01b0\u01a1ng","610":"S\u00f4ng C\u00f4ng","611":"Th\u00e1i Nguy\u00ean","612":"V\u00f5 Nhai"},"59":{"613":"B\u00e1 Th\u01b0\u1edbc","614":"B\u1ec9m S\u01a1n","615":"C\u1ea9m Th\u1ee7y","616":"\u0110\u00f4ng S\u01a1n","617":"H\u00e0 Trung","618":"H\u1eadu L\u1ed9c","619":"Ho\u1eb1ng H\u00f3a","620":"Lang Ch\u00e1nh","621":"M\u01b0\u1eddng L\u00e1t","622":"Nga S\u01a1n","623":"Ng\u1ecdc L\u1eb7c","624":"Nh\u01b0 Thanh","625":"Nh\u01b0 Xu\u00e2n","626":"N\u00f4ng C\u1ed1ng","627":"Quan H\u00f3a","628":"Quan S\u01a1n","629":"Qu\u1ea3ng X\u01b0\u01a1ng","630":"S\u1ea7m S\u01a1n","631":"Th\u1ea1ch Th\u00e0nh","632":"Thanh H\u00f3a","633":"Th\u1ecd Xu\u00e2n","634":"Th\u01b0\u1eddng Xu\u00e2n","635":"T\u0129nh Gia","636":"Thi\u1ec7u H\u00f3a","637":"Tri\u1ec7u S\u01a1n","638":"V\u0129nh L\u1ed9c","639":"Y\u00ean \u0110\u1ecbnh"},"60":{"640":"A L\u01b0\u1edbi","641":"Hu\u1ebf","642":"H\u01b0\u01a1ng Th\u1ee7y","643":"H\u01b0\u01a1ng Tr\u00e0","644":"Nam \u0110\u00f4ng","645":"Phong \u0110i\u1ec1n","646":"Ph\u00fa L\u1ed9c","647":"Ph\u00fa Vang","648":"Qu\u1ea3ng \u0110i\u1ec1n"},"61":{"649":"C\u00e1i B\u00e8","650":"Cai L\u1eady","651":"Ch\u00e2u Th\u00e0nh","652":"Ch\u1ee3 G\u1ea1o","653":"G\u00f2 C\u00f4ng","654":"G\u00f2 C\u00f4ng \u0110\u00f4ng","655":"G\u00f2 C\u00f4ng T\u00e2y","656":"M\u1ef9 Tho","657":"T\u00e2n Ph\u01b0\u1edbc"},"62":{"658":"C\u00e0ng Long","659":"C\u1ea7u K\u00e8","660":"C\u1ea7u Ngang","661":"Ch\u00e2u Th\u00e0nh","662":"Duy\u00ean H\u1ea3i","663":"Ti\u1ec3u C\u1ea7n","664":"Tr\u00e0 C\u00fa","665":"Th\u00e0nh ph\u1ed1 Tr\u00e0 Vinh"},"63":{"666":"Chi\u00eam H\u00f3a","667":"H\u00e0m Y\u00ean","668":"Na Hang","669":"S\u01a1n D\u01b0\u01a1ng","670":"Tuy\u00ean Quang","671":"Y\u00ean S\u01a1n"},"64":{"672":"B\u00ecnh Minh","673":"Long H\u1ed3","674":"Mang Th\u00edt","675":"Tam B\u00ecnh","676":"Tr\u00e0 \u00d4n","677":"V\u0129nh Long","678":"V\u0169ng Li\u00eam"},"65":{"679":"B\u00ecnh Xuy\u00ean","680":"L\u1eadp Th\u1ea1ch","681":"Tam D\u01b0\u01a1ng","682":"V\u0129nh T\u01b0\u1eddng","683":"V\u0129nh Y\u00ean","684":"Y\u00ean L\u1ea1c","685":"Ph\u00fac Y\u00ean","686":"Tam \u0110\u1ea3o"},"66":{"687":"L\u1ee5c Y\u00ean","688":"M\u00f9 C\u0103ng Ch\u1ea3i","689":"Tr\u1ea1m T\u1ea5u","690":"Tr\u1ea5n Y\u00ean","691":"V\u0103n Ch\u1ea5n","692":"V\u0103n Y\u00ean","693":"Y\u00ean B\u00e1i","694":"Y\u00ean B\u00ecnh"}}';
        var district_option = '<option value="0">Chọn quận, huyện *</option>';
        list = $.parseJSON(list);
        $.each(list, function(_city_id,district_obj){
            if(_city_id==city_id){
                $.each(district_obj, function(district_id,district_name){                    
                    district_option+= '<option value="'+district_id+'">'+district_name+'</option>';
                });
            }
        });
        return district_option;
    }

    $(document).ready(function(){
        event();        
    });

})(jQuery);