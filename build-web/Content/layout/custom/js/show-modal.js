const _showdialog = function (modalId) {
    const elappendmodal = '<div class="opacity opacifa-active" id="opacity"><div>';
    const elindex = document.getElementById('index');
    const elunder = document.getElementById('under');
    if (!elunder && !elindex) { return }
    const idModalShow = document.getElementById(`${modalId}`);
    $(idModalShow).addClass("modalshowactive");
    if (elindex != null) {
        elindex.insertAdjacentHTML('beforeend', elappendmodal);
    }
    if (elunder != null) {
        elunder.insertAdjacentHTML('beforeend', elappendmodal);
    }
    $('.btn-close-dialog').on('click', function () {
        const idModalClose = $(this).parent().attr('id');
        const ModalisClose = document.getElementById(`${idModalClose}`);
        ModalisClose.classList.remove("modalshowactive");
        const elopacity = document.getElementById("opacity");
        if (elindex != null) {
            elindex.removeChild(elopacity);
        }
        if (elunder != null) {
            elunder.removeChild(elopacity);
        }
    });
}