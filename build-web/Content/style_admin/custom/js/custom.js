function GetSeNameString(text) {
	// Get Categories by language
	var request1 = $.ajax({
		url: "/Admin/AdminCommon/GetSeNameValue?text=" + text,
		type: "GET",
	});
	request1.done(function (data) {
		$('#custom-sename').text(data.SeName);
	});
	request1.fail(function (jqXHR, textStatus) {
		alert("Request1 failed: " + textStatus);
	});

}

function CheckAll() {
    $(".checkbox").attr("checked", true);
}

$(document).ready(function () {
    $('.check-all').click(function (e) {
        var table = $(e.target).closest('tr');
        $('td input:checkbox', table).prop('checked', this.checked);
    });
});