﻿function UpdateDashboard(guidId, time) {
    var timeStr = time.replace(/\//g, '-');
    $.ajax({
        url: '/Admin/BranchConsumptionAdmin/BranchConsumption?originalBranchValue=' + guidId + '&dateStr=' + timeStr,
        type: 'GET',
        cache: false,
        success: function (result) {
            $('#DashboardContainer').html(result);
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}