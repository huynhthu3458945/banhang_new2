﻿namespace MainProject.Web.Models
{
    /// <summary>
    /// Order Model
    /// </summary>
    public class OrderModel
    {
        public string BuyerName { get; set; }

        public string BuyerAddress { get; set; }

        public string BuyerEmail { get; set; }

        public string BuyerPhone { get; set; }

        public string OtherBuyerPhone { get; set; }

        public string Note { get; set; }

        public long DistrictSelectedValue { get; set; }
    }
}