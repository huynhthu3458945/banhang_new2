﻿$(function () {
    var selections = $('#OptionAnswers');
    $('#addAnswer').click(function () {
        var value = txtAnswer.val().trim();
        if (value != "" && !txtAnswer.hasClass('input-validation-error') && !txtAnswer.hasClass('text-label')) {
            $(document.createElement('option'))
                .attr('value', value)
                .text(value)
                .appendTo(selections);

            $('#OptionAnswers option').prop('selected', true);
            txtAnswer.val('');
            txtAnswer.trigger('blur');
        }
    });

    $('#removeAnswer').click(function () {
        $("#OptionAnswers option:selected").remove();
        $('#OptionAnswers option').prop('selected', true);
    });

    var txtAnswer = $('#txtAnswer');
    txtAnswer.focus(function () {
        if (this.value == $(this).attr('title')) {
            this.value = '';
            $(this).removeClass('text-label');
        }
    });

    txtAnswer.blur(function () {
        if (this.value == '') {
            this.value = $(this).attr('title');
            $(this).addClass('text-label');
        }
    });
    txtAnswer.trigger('blur');

    var btnAddDefaultAnswer = $('#poll-items-submit');
    btnAddDefaultAnswer.click(function () {
        $('#OptionAnswers option').prop('selected', true);
    });
});