﻿$(function () {
    var leftSelections = $('#OptionBranches');
    var branchRightSelections = $('#BrancheInfosSelected');
    var productRightSelections = $('#ProductsSelected');
    
    $('#addBranch').click(function () {
        $("#OptionBranches option:selected").each(function () {
            var optionVal = $(this).val();
            var optionText = $(this).text();
            var exists = false;
            $('#BrancheInfosSelected option').each(function () {
                if (this.value == optionVal) {
                    exists = true;
                }
            });

            if (!exists) {
                $(document.createElement('option'))
                .attr('value', optionVal)
                .text(optionText)
                .appendTo(branchRightSelections);
            }
        });
        $('#OptionBranches option').prop('selected', false);
    });

    $('#removeBranch').click(function () {
        $("#BrancheInfosSelected option:selected").remove();
        //$('#OptionBranchesSelected option').prop('selected', true);
    });
    

    $('#addProduct').click(function () {

        $("#OptionProducts option:selected").each(function () {
            var pOptionVal = $(this).val();
            var pOptionText = $(this).text();
            var pExists = false;
            $('#ProductsSelected option').each(function () {
                if (this.value == pOptionVal) {
                    pExists = true;
                }
            });

            if (!pExists) {
                $(document.createElement('option'))
                .attr('value', pOptionVal)
                .text(pOptionText)
                .appendTo(productRightSelections);
            }
        });
        $('#OptionProducts option').prop('selected', false);
    });

    $('#removeProduct').click(function () {
        $("#ProductsSelected option:selected").remove();
    });

    var btnAddDefaultAnswer = $('#event-submit');
    btnAddDefaultAnswer.click(function () {
        $('#BrancheInfosSelected option').prop('selected', true);
        $('#ProductsSelected option').prop('selected', true);
    });
    
});