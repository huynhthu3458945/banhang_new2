﻿using System.Linq;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Core.UserInfos;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Behaviors;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Root")]
    public class BehaviorAdminController : BaseController
    {
        //
        // GET: /Admin/BehaviorAdmin/

        private readonly UnitOfWork unitOfWork;

        private readonly int _itemInPerPage = 30;

        public BehaviorAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult Index(int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "BehaviorAdmin";
            if (page < 1) page = 1;
            var sql = unitOfWork.BehaviorRepository.FindAll().ToList();
            var count = sql.Count();
            var behaviors = sql.OrderByDescending(x => x.Id).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            var model = new IndexViewModel<Behavior>()
            {
                ListItems = behaviors,
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/BehaviorAdmin/Index?page={0}'")

            };

            return View(model);
        }

        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "BehaviorAdmin";
            var model = new BehaviorManageModel {
                BehaviorTypes = EnumHelper.ToSelectList(typeof(BehaviorTypeCollection))
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(BehaviorManageModel model)
        {
            TempData["CurrentMenu"] = "BehaviorAdmin";
            if (ModelState.IsValid)
            {
                var behavior = new Behavior();
                behavior.Type = model.BehaviorTypeSelected;
                BehaviorManageModel.ToEntity(model, ref behavior);
                unitOfWork.BehaviorRepository.Insert(behavior);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>Thành công!</strong>");
                return RedirectToAction("Index");
            }
            model.BehaviorTypes = EnumHelper.ToSelectList(typeof(BehaviorTypeCollection));
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "BehaviorAdmin";
            var behavior = unitOfWork.BehaviorRepository.FindUnique(d => d.Id == id);
            if (behavior == null)
            {
                TempData["message"] = string.Format("Không tìm thấy tác vụ cần sửa");
                return RedirectToAction("Index");
            }
            var model = new BehaviorManageModel {
                BehaviorTypes = EnumHelper.ToSelectList(typeof(BehaviorTypeCollection)),
                BehaviorTypeSelected = behavior.Type,
            };
            model.Id = behavior.Id;
            BehaviorManageModel.ToModel(model, ref behavior);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(BehaviorManageModel model)
        {
            TempData["CurrentMenu"] = "BehaviorAdmin";
            if (ModelState.IsValid)
            {
                var behavior = unitOfWork.BehaviorRepository.FindUnique(d => d.Id == model.Id);
                behavior.Type = model.BehaviorTypeSelected;
                BehaviorManageModel.ToEntity(model, ref behavior);
                unitOfWork.Save();

                TempData["message"] = string.Format("<span style='color:green'>{0} đã cập nhật thành công</span>",
                                                    behavior.Name);
                return RedirectToAction("Index");
            }
            model.BehaviorTypes = EnumHelper.ToSelectList(typeof(BehaviorTypeCollection));
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "BehaviorAdmin";
            var behavior = unitOfWork.BehaviorRepository.FindUnique(d => d.Id == id);
            if (behavior != null)
            {
                var behaviorName = behavior.Name;
                unitOfWork.BehaviorRepository.Delete(behavior);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được xóa</span>", behaviorName);
            }
            return RedirectToAction("Index");
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
