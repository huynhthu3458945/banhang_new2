﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Feedback;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin, Mod, FormManager")]
    public class FeedbackAdminController : BaseController
    {
        //
        // GET: /Admin/FeedbackAdmin/
        private readonly UnitOfWork unitOfWork;
        private readonly int itemInPerPage;

        public FeedbackAdminController()
        {
            unitOfWork = new UnitOfWork();
            itemInPerPage = 10;
        }

        [CustomViewAuthorize("FeedbackAdmin", "View")]
        public ActionResult Index(int languageId = 0, int page = 1)
        {
            TempData["CurrentMenu"] = "FeedbackAdmin";
            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "FeedbackAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "FeedbackAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "FeedbackAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            var sql = unitOfWork.FeedbackRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            var count = sql.Count();
            var feedbacks = sql.OrderBy(x => x.Id).Skip((page - 1) * itemInPerPage).Take(itemInPerPage).ToList();
            var FeedbacksAndLanguageVersions = new List<EntityWithLanguageVersionsModel<Feedback>>();
            foreach (var feedback in feedbacks)
            {
                var languageVersions =
                    unitOfWork.FeedbackRepository.Find(x => x.OriginalValue == feedback.OriginalValue && x.Language.Id != feedback.Language.Id).
                        ToList();
                FeedbacksAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<Feedback>() { Entity = feedback, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<Feedback>>()
            {
                ListItems = FeedbacksAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = languageId
                    },
                    BaseUrl = "/Admin/FeedbackAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, itemInPerPage, page, "href='/Admin/FeedbackAdmin/Index?languageId=" + languageId + "&page={0}'")

            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId);
            return View(model);
        }

        [CustomViewAuthorize("FeedbackAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "FeedbackAdmin";
            var FeedbackOriginal = id == 0 ? null : unitOfWork.FeedbackRepository.FindUnique(x => x.Id == id);
            var originalValue = FeedbackOriginal != null ? FeedbackOriginal.OriginalValue : Guid.NewGuid();
            if (FeedbackOriginal == null)
            {
                while (unitOfWork.FeedbackRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }
            var FeedbackViewModel = new FeedbackViewModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<Feedback>(originalValue, unitOfWork), 0),
                OriginalValue = originalValue
            };

            var imageFolder = "";
            if (FeedbackOriginal != null)
            {
                imageFolder = FeedbackOriginal.ImageFolder;
                FeedbackViewModel.ImageDefault = FeedbackOriginal.ImageDefault;
                FeedbackViewModel.Order = FeedbackOriginal.Order;
            }
            else
            {
                imageFolder = "/Upload/Feedback/" + Guid.NewGuid().ToString();
                while (Directory.Exists(Server.MapPath(imageFolder)))
                {
                    imageFolder = "/Upload/Feedback/" + Guid.NewGuid().ToString();
                }
            }
            FeedbackViewModel.ImageFolder = imageFolder;

            FeedbackViewModel.Languages.Insert(0, new SelectListItem{Text = "Vui lòng chọn ngôn ngữ", Value = ""});

            return View(FeedbackViewModel);
        }
        [HttpPost]
        public ActionResult Create(FeedbackViewModel FeedbackViewModel)
        {
            TempData["CurrentMenu"] = "FeedbackAdmin";
            if (ModelState.IsValid)
            {
                var Feedback = new Feedback()
                {
                    Language = unitOfWork.LanguageRepository.FindById(FeedbackViewModel.LanguageSelectedValue)
                };
                FeedbackViewModel.ToEntity(FeedbackViewModel, ref Feedback);
                unitOfWork.FeedbackRepository.Insert(Feedback);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Cảm nhận Anh/Chị <strong>{0}</strong> đã được thêm thành công!</span>", FeedbackViewModel.FullName);
                return RedirectToAction("Index", new { languageId = Feedback.Language.Id });
            }
            FeedbackViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<Feedback>(FeedbackViewModel.OriginalValue, unitOfWork), 0);
            FeedbackViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(FeedbackViewModel);
        }

        [CustomViewAuthorize("FeedbackAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "FeedbackAdmin";
            var Feedback = unitOfWork.FeedbackRepository.FindUnique(d => d.Id == id);
            if (Feedback == null)
            {
                TempData["message"] = string.Format("Không tìm thấy danh mục cần sửa");
                return RedirectToAction("Index");
            }
            var FeedbackViewModel = new FeedbackViewModel
            {
                LanguageSelectedValue = Feedback.Language.Id,
                Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), Feedback.Language.Id)
            };
            FeedbackViewModel.Id = Feedback.Id;
            FeedbackViewModel.ToModel(FeedbackViewModel, ref Feedback);
            return View(FeedbackViewModel);
        }
        [HttpPost]
        public ActionResult Edit(FeedbackViewModel FeedbackViewModel)
        {
            TempData["CurrentMenu"] = "FeedbackAdmin";
            if (ModelState.IsValid)
            {
                var Feedback = unitOfWork.FeedbackRepository.FindUnique(d => d.Id == FeedbackViewModel.Id);

                Feedback.Language = unitOfWork.LanguageRepository.FindById(FeedbackViewModel.LanguageSelectedValue);
                FeedbackViewModel.ToEntity(FeedbackViewModel, ref Feedback);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Cảm nhận của Anh/Chị {0} đã cập nhật thành công</span>",
                                                    Feedback.FullName);
                return RedirectToAction("Index", new { languageId = FeedbackViewModel.LanguageSelectedValue });
            }
            FeedbackViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<Feedback>(FeedbackViewModel.OriginalValue, unitOfWork), 0);
            FeedbackViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(FeedbackViewModel);
        }

        [CustomViewAuthorize("FeedbackAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "FeedbackAdmin";
            var Feedback = unitOfWork.FeedbackRepository.FindUnique(p => p.Id == id);
            if (Feedback == null)
            {
                TempData["message"] = string.Format("<strong style='color:red'>Cảm nhận không tồn tại</strong>");
                return RedirectToAction("Index");
            }
            var title = Feedback.FullName;
            var languageId = Feedback.Language.Id;
            try
            {
                //delete images folder
                if (Directory.Exists(Server.MapPath(Feedback.ImageFolder)))
                {
                    try
                    {
                        Directory.Delete(Server.MapPath(Feedback.ImageFolder), true);
                    }
                    catch{
                        TempData["message"] = string.Format("<strong style='color:red'>Xảy ra lỗi khi xóa ảnh đại diện, hãy thử lại!</strong>");
                    }
                }
                unitOfWork.FeedbackRepository.Delete(Feedback);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>Cảm nhận của Anh/Chị {0}  Đã xóa</strong>", title);
            }
            catch
            {
                TempData["message"] = string.Format("<strong style='color:red'>Xóa không thành công, hãy thử lại!</strong>");
            }
            return RedirectToAction("Index", new { languageId = languageId });

        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
