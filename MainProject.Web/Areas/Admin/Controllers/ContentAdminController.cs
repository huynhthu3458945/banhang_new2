﻿using System.Linq;
using System.Web.Mvc;
using MainProject.Core.UserInfos;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Contents;
using MainProject.Web.BaseControllers;
namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Root")]
    public class ContentAdminController : BaseController
    {
        //
        // GET: /Admin/ContentAdmin/

        private readonly UnitOfWork unitOfWork;

        private readonly int _itemInPerPage = 30;

        public ContentAdminController()
        {
            unitOfWork = new UnitOfWork();
        }
        
        public ActionResult Index(int contentGroupId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "ContentAdmin";
            
            var sql = unitOfWork.ContentRepository.FindAll();
            if (contentGroupId != 0)
            {
                sql = sql.Where(x => x.ContentGroup.Id == contentGroupId);
            }
            if (page < 1) page = 1;
            var count = sql.Count();
            var contents = sql.OrderByDescending(x => x.Id).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            var model = new IndexViewModel<Content>()
            {
                ListItems = contents,
                FilterViewModel = new FilterViewModel()
                {
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = ContentGroupHelper.BindSelectListItemContent(unitOfWork.ContentGroupRepository, contentGroupId),
                        FatherSelectedValue = contentGroupId
                    },
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/ContentAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/ContentAdmin/Index?contentGroupId=" + contentGroupId + "&page={0}'")

            };
            model.FilterViewModel.FatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "All", Value = "0", Selected = contentGroupId == 0 });
            return View(model);
        }
        
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "ContentAdmin";
            var model = new ContentManageModel
            {
                ContentGroups = ContentGroupHelper.BindSelectListItemContent(unitOfWork.ContentGroupRepository, unitOfWork.ContentGroupRepository.FindAll().FirstOrDefault().Id)

            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ContentManageModel model)
        {
            TempData["CurrentMenu"] = "ContentAdmin";
            if (ModelState.IsValid)
            {

                var content = new Content()
                {
                    ContentGroup = unitOfWork.ContentGroupRepository.FindById(model.ContentGroupSelectedValue)
                };
                ContentManageModel.ToEntity(model, ref content);
                unitOfWork.ContentRepository.Insert(content);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>Thành công!</strong>");
                return RedirectToAction("Index", new { contentGroupId = content.ContentGroup.Id });
            }
            model.ContentGroups = ContentGroupHelper.BindSelectListItemContent(unitOfWork.ContentGroupRepository, model.ContentGroupSelectedValue);
            return View(model);
        }
        
        public ActionResult Edit(long id)
        {
            TempData["CurrentMenu"] = "ContentAdmin";
            var content = unitOfWork.ContentRepository.FindUnique(d => d.Id == id);
            if (content == null)
            {
                TempData["message"] = string.Format("Không tìm thấy nội dung cần sửa");
                return RedirectToAction("Index");
            }
            var model = new ContentManageModel()
            {
                ContentGroupSelectedValue = content.ContentGroup.Id,
                ContentGroups = ContentGroupHelper.BindSelectListItemContent(unitOfWork.ContentGroupRepository, content.ContentGroup.Id)
            };
            model.Id = content.Id;
            ContentManageModel.ToModel(model, ref content);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ContentManageModel model)
        {
            TempData["CurrentMenu"] = "ContentAdmin";
            if (ModelState.IsValid)
            {
                var content = unitOfWork.ContentRepository.FindUnique(d => d.Id == model.Id);

                content.ContentGroup = unitOfWork.ContentGroupRepository.FindById(model.ContentGroupSelectedValue);

                ContentManageModel.ToEntity(model, ref content);
                unitOfWork.Save();

                TempData["message"] = string.Format("<span style='color:green'>{0} đã cập nhật thành công</span>",
                                                    content.Title);
                return RedirectToAction("Index", new { contentGroupId = content.ContentGroup.Id });
            }
            model.ContentGroups = ContentGroupHelper.BindSelectListItemContent(unitOfWork.ContentGroupRepository, model.ContentGroupSelectedValue);
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "ContentAdmin";
            var content = unitOfWork.ContentRepository.FindUnique(d => d.Id == id);
            var contentGroupId = content.ContentGroup.Id;
            if (content != null)
            {
                var title = content.Title;
                unitOfWork.ContentRepository.Delete(content);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được xóa</span>", title);
            }
            return RedirectToAction("Index", new { contentGroupId = contentGroupId });
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
