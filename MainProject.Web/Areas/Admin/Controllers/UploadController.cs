﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Net;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Web.Areas.Admin.Models.Upload;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class UploadController : BaseController
    {
        
        private readonly UnitOfWork unitOfWork;
        private string CurrentUser
        {
            get { return HttpContext.User.Identity.Name; }
        }

        public UploadController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult Images(string p)
        {
            if (string.IsNullOrEmpty(p)) return null;

            if (!Directory.Exists(Server.MapPath(p)))
            {
                Directory.CreateDirectory(Server.MapPath(p));
            }
            var images = new ImagesModel();
            //Read out files from the files directory
            var files = Directory.GetFiles(Server.MapPath(p));
            //Add them to the model

            foreach (var file in files)
            {
                var fileInfo = new FileInfo(file);
                images.Images.Add(new ImageInfo() { Link = p + "/" + fileInfo.Name, Name = fileInfo.Name });
            }
            ViewBag.Images = images;

            var model = new UploadImageModel()
            {
                FolderName = p
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult UploadImage(UploadImageModel model)
        {
            //Check if all simple data annotations are valid
            if (ModelState.IsValid)
            {
                //Prepare the needed variables
                Bitmap original = null;
                var name = "newimagefile";
                var errorField = string.Empty;

                if (model.IsUrl)
                {
                    errorField = "Url";
                    name = GetUrlFileName(model.Url);
                    original = GetImageFromUrl(model.Url);
                }
                else if (model.IsFlickr)
                {
                    errorField = "Flickr";
                    name = GetUrlFileName(model.Flickr);
                    original = GetImageFromUrl(model.Flickr);
                }
                else if (model.File != null) // model.IsFile
                {
                    errorField = "File";
                    name = Path.GetFileNameWithoutExtension(model.File.FileName);
                    original = Bitmap.FromStream(model.File.InputStream) as Bitmap;
                }


                //If we had success so far
                if (original != null)
                {
                    var img = CreateImage(original, model.X, model.Y, model.Width, model.Height);

                    //Demo purposes only - save image in the file system
                    //var fn = Server.MapPath(model.FolderName + "/" + name + Path.GetExtension(model.File.FileName));
                    var fn = "";
                    if (model.IsUrl)
                    {
                        fn = Server.MapPath(model.FolderName + "/" + name + ".jpg");

                    }
                    else if (model.File != null)
                    {
                        fn = Server.MapPath(model.FolderName + "/" + name + Path.GetExtension(model.File.FileName));
                    }
                    while (System.IO.File.Exists(fn))
                    {
                        if (name.Contains("-"))
                        {
                            int n;
                            var str = name.Substring(name.LastIndexOf("-") + 1);
                            if (int.TryParse(str, out n))
                            {
                                n++;
                                name = name.Substring(0, name.LastIndexOf("-") + 1) + n;
                            }
                            else
                            {
                                name = name + "-1";
                            }
                        }
                        else
                        {
                            name = name + "-1";
                        }
                        fn = Server.MapPath(model.FolderName + "/" + name + Path.GetExtension(model.File.FileName));
                    }
                    if (fn.EndsWith(".png"))
                    {
                        img.Save(fn, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    else if (fn.EndsWith(".jpg") || fn.EndsWith(".jpeg"))
                    {
                        img.Save(fn, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    else if (fn.EndsWith(".gif"))
                    {
                        img.Save(fn, System.Drawing.Imaging.ImageFormat.Gif);
                    }
                    else if (fn.EndsWith(".icon"))
                    {
                        img.Save(fn, System.Drawing.Imaging.ImageFormat.Icon);
                    }
                    else
                    {
                        img.Save(fn);
                    }

                    //Redirect to index
                    return RedirectToAction("Images", new { p = model.FolderName });
                }
                else //Otherwise we add an error and return to the (previous) view with the model data
                    ModelState.AddModelError(errorField, "Your upload did not seem valid. Please try again using only correct images!");
            }
            return View(model);
        }

        public ActionResult DeleteImage(string p)
        {
            var logHistory = new LogHistory()
            {
                ActionBy = CurrentUser,
                ActionType = ActionTypeCollection.Delete,
                CreatedDate = DateTime.Now,
                EntityType = EntityTypeCollection.Images,
                Comment = p
            };
            unitOfWork.LogHistoryRepository.Insert(logHistory);
            try
            {
                unitOfWork.Save();
            }
            catch (Exception ex)
            {
            }
            var message = "Not found!";
            var result = false;
            if (System.IO.File.Exists(Server.MapPath(p)))
            {
                try
                {
                    System.IO.File.Delete(Server.MapPath(p));
                    message = "Completed!";
                    result = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return RedirectToAction("Images", new { p = p.Substring(0, p.LastIndexOf("/")) });
        }

        /// <summary>
        /// Gets an image from the specified URL.
        /// </summary>
        /// <param name="url">The URL containing an image.</param>
        /// <returns>The image as a bitmap.</returns>
        Bitmap GetImageFromUrl(string url)
        {
            var buffer = 1024;
            Bitmap image = null;

            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                return image;

            using (var ms = new MemoryStream())
            {
                var req = WebRequest.Create(url);

                using (var resp = req.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        var bytes = new byte[buffer];
                        var n = 0;

                        while ((n = stream.Read(bytes, 0, buffer)) != 0)
                            ms.Write(bytes, 0, n);
                    }
                }

                image = Bitmap.FromStream(ms) as Bitmap;
            }

            return image;
        }

        /// <summary>
        /// Gets the filename that is placed under a certain URL.
        /// </summary>
        /// <param name="url">The URL which should be investigated for a file name.</param>
        /// <returns>The file name.</returns>
        string GetUrlFileName(string url)
        {
            var parts = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var last = parts[parts.Length - 1];
            return Path.GetFileNameWithoutExtension(last);
        }

        /// <summary>
        /// Creates a small image out of a larger image.
        /// </summary>
        /// <param name="original">The original image which should be cropped (will remain untouched).</param>
        /// <param name="x">The value where to start on the x axis.</param>
        /// <param name="y">The value where to start on the y axis.</param>
        /// <param name="width">The width of the final image.</param>
        /// <param name="height">The height of the final image.</param>
        /// <returns>The cropped image.</returns>
        Bitmap CreateImage(Bitmap original, int x, int y, int width, int height)
        {
            var img = new Bitmap(width, height);

            using (var g = Graphics.FromImage(img))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(original, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
            }

            return img;
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
