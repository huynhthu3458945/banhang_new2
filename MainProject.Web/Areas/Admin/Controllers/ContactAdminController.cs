﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models.Contact;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class ContactAdminController : BaseController
    {
        //
        // GET: /Admin/ContactContentAdmin/

        private readonly UnitOfWork unitOfWork;
        private readonly int ItemPages = 30;
        public ContactAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("ContactAdmin", "View")]
        public ActionResult Index(string name = null, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "ContactAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsDetail = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "ContactAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DetailTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "ContactAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);

            var sql = unitOfWork.ContactRepository.FindAll();
            if (!string.IsNullOrWhiteSpace(name))
            {
                sql = sql.Where(d => d.Name.Contains(name.Trim()));
            }
            int count = sql.Count();
            var model = new ContactViewModel
            {
                ListContactContent = sql.OrderByDescending(d => d.CreatedDate).Skip((page - 1) * ItemPages).Take(ItemPages).ToList(),
                PagingViewModel = new PagingModel(count, ItemPages, page, "href='/Admin/ContactAdmin/Index?name=" + name + "&page={0}'"),
                Name = name ?? ""
            };
            return View(model);
        }

        [CustomViewAuthorize("ContactAdmin", "Detail")]
        public ActionResult Detail(long id)
        {
            TempData["CurrentMenu"] = "Contact";
            var model = unitOfWork.ContactRepository.FindUnique(d => d.Id == id);
            if (model != null)
            {
                return View(model);
            }
            TempData["message"] = string.Format("<strong style='color:red'>Không tìm thấy đối tượng cần xem</strong>");
            return RedirectToAction("Index");
        }

        [CustomViewAuthorize("ContactAdmin", "Delete")]
        public ActionResult Delete(long id)
        {
            TempData["CurrentMenu"] = "ContactAdmin";
            var model = unitOfWork.ContactRepository.FindUnique(d => d.Id == id);
            if (model != null)
            {
                unitOfWork.ContactRepository.Delete(model);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>{0} đã được xóa</strong>", model.Name);
                return RedirectToAction("Index");
            }
            TempData["message"] = string.Format("<strong style='color:red'>Không tìm thấy đối tượng cần xóa</strong>");
            return RedirectToAction("Index");
        }

        public ActionResult Next(long id)
        {
            TempData["CurrentMenu"] = "ContactAdmin";
            try
            {
                var item =
                    unitOfWork.ContactRepository.FindAll().Where(
                        d => d.Id > id).ToList().First();
                return RedirectToAction("Detail", "ContactAdmin", new { @id = item.Id });
            }
            catch (Exception)
            {
                TempData["message"] = "<strong style='color:red'>Không còn kết quả để xem</strong>";
                return RedirectToAction("Detail", "ContactAdmin", new { id = id });
            }
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
