﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Products;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class ProductAdminController : BaseController
    {
        //
        // GET: /Admin/ProductAdmin/

        private readonly UnitOfWork unitOfWork;

        private readonly int pageItems = 30;

        public ProductAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("ProductAdmin", "View")]
        public ActionResult Index(int languageId = 0, long categoryId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "ProductAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "ProductAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "ProductAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "ProductAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            if (categoryId <= 0) categoryId = ConfigItemHelper.GetRootNewsId();
            var sql = unitOfWork.ProductRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            if (categoryId != ConfigItemHelper.GetRootNewsId())
            {
                sql = sql.Where(x => x.Category.Id == categoryId);
            }
            var count = sql.Count();
            var products = sql.OrderByDescending(x => x.Id).Skip((page - 1) * pageItems).Take(pageItems).ToList();
            var productAndLanguageVersions = new List<EntityWithLanguageVersionsModel<Product>>();
            foreach (var a in products)
            {
                var languageVersions =
                    unitOfWork.ProductRepository.Find(x => x.OriginalValue == a.OriginalValue && x.Language.Id != a.Language.Id).
                        ToList();
                productAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<Product>() { Entity = a, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<Product>>()
            {
                ListItems = productAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = languageId
                    },
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = CategoryHelper.BindSelectListItemProduct(unitOfWork.CategoryRepository, categoryId, languageId, 0, false),
                        FatherSelectedValue = categoryId
                    },
                    EntityType = EntityTypeCollection.Categories,
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/ProductAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, pageItems, page, "href='/Admin/ProductAdmin/Index?languageId=" + languageId + "&categoryId=" + categoryId + "&page={0}'")

            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId);
            model.FilterViewModel.FatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "All", Value = "0", Selected = categoryId == 0 });
            return View(model);
        }

        [CustomViewAuthorize("ProductAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "ProductAdmin";

            var productOriginal = id == 0 ? null : unitOfWork.ProductRepository.FindUnique(x => x.Id == id);
            var originalValue = productOriginal != null ? productOriginal.OriginalValue : Guid.NewGuid();
            if (productOriginal == null)
            {
                while (unitOfWork.ProductRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }
            var model = new ProductManageModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<Product>(originalValue, unitOfWork), 0),
                Categories = new List<SelectListItem> { },
                OriginalValue = originalValue
            };
            var imageFolder = "";
            if (productOriginal != null)
            {
                imageFolder = productOriginal.ImageFolder;
                model.ImageDefault = productOriginal.ImageDefault;
                model.ProductCode = productOriginal.ProductCode;
            }
            else
            {
                imageFolder = "/Upload/Products/" + Guid.NewGuid().ToString();
                while (Directory.Exists(Server.MapPath(imageFolder)))
                {
                    imageFolder = "/Upload/Products/" + Guid.NewGuid().ToString();
                }
            }
            model.ImageFolder = imageFolder;
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = "0"
            });
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ProductManageModel model)
        {
            TempData["CurrentMenu"] = "ProductAdmin";

            if (ModelState.IsValid)
            {
                var entity = new Product()
                {
                    Category = unitOfWork.CategoryRepository.FindById(model.CategorySelectedValue),
                    Language = unitOfWork.LanguageRepository.FindById(model.LanguageSelectedValue)
                };
                ProductManageModel.ToEntity(model, ref entity);
                unitOfWork.ProductRepository.Insert(entity);

                // Save to get entityId for Generate Sename
                unitOfWork.Save();

                var entityType = EntityTypeCollection.Products;
                entity.SeName = SeoHelper.VaidateSeNameAndSubmit(entityType, entity.Id, entity.Name);

                unitOfWork.Save();

                TempData["message"] = string.Format("<span style='color:green'>Sản phẩm <strong>{0}</strong> đã được thêm thành công!</span>", model.Name);
                return RedirectToAction("Index", new { languageId = entity.Language.Id, categoryId = entity.Category.Id });
            }
            model.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<Product>(model.OriginalValue, unitOfWork), 0);
            model.Categories = new List<SelectListItem> { };
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = "0"
            });
            return View(model);
        }

        [CustomViewAuthorize("ProductAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "ProductAdmin";

            var product = unitOfWork.ProductRepository.FindUnique(d => d.Id == id);
            if (product == null)
            {
                TempData["message"] = string.Format("Không tìm thấy sản phẩm cần sửa");
                return RedirectToAction("Index");
            }
            var model = new ProductManageModel
            {
                LanguageSelectedValue = product.Language.Id,
                Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), product.Language.Id),
                CategorySelectedValue = product.Category.Id,
                Categories = CategoryHelper.BindSelectListItemProduct(unitOfWork.CategoryRepository, product.Category.Id, product.Language.Id, 0, false)
            };
            model.Id = product.Id;

            ProductManageModel.ToModel(model, ref product);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ProductManageModel model)
        {
            TempData["CurrentMenu"] = "Product";

            if (ModelState.IsValid)
            {
                var entity = unitOfWork.ProductRepository.FindUnique(d => d.Id == model.Id);

                entity.Category = unitOfWork.CategoryRepository.FindById(model.CategorySelectedValue);
                entity.Language = unitOfWork.LanguageRepository.FindById(model.LanguageSelectedValue);              

                ProductManageModel.ToEntity(model, ref entity);

                // save to update Category of Product to Database for VaidateSeNameAndSubmit()
                unitOfWork.Save();

                entity.SeName = SeoHelper.VaidateSeNameAndSubmit(EntityTypeCollection.Products, entity.Id, entity.Name);
                unitOfWork.Save();

                TempData["message"] = string.Format("<span style='color:green'>{0} đã cập nhật thành công</span>",
                                                    entity.Name);
                return RedirectToAction("Index", new { languageId = model.LanguageSelectedValue, categoryId = entity.Category.Id });
            }
            model.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<Product>(model.OriginalValue, unitOfWork), 0);
            model.Categories = new List<SelectListItem> { };
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = "0"
            });
            return View(model);
        }

        [CustomViewAuthorize("ProductAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "ProductAdmin";

            var entity = unitOfWork.ProductRepository.FindUnique(d => d.Id == id);
            var currentCategoryId = entity.Category.Id;
            var currentLanguageId = entity.Language.Id;
            var name = entity.Name;
            if (entity != null)
            {

                // Delete images of Product
                string urlImg = entity.ImageDefault;
                if (!string.IsNullOrWhiteSpace(urlImg))
                {
                    if (Directory.Exists(Server.MapPath(urlImg)))
                    {
                        System.IO.File.Delete(urlImg);
                    }
                }

                // Delete product
                // Delete UrlRecord
                SeoHelper.DeleteUrlData(EntityTypeCollection.Products, entity.Id);

                unitOfWork.ProductRepository.Delete(entity);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được xóa</span>", name);
            }
            Complete:
            return RedirectToAction("Index", new { languageId = currentLanguageId, categoryId = currentCategoryId });
        }

    }
}
