﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Article;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class ArticleAdminController : BaseController
    {
        private UnitOfWork unitOfWork;

        private string CurrentUser
        {
            get { return HttpContext.User.Identity.Name; }
        }
        private int pageItems = ConfigItemHelper.GetItemsCountPerPage();

        public ArticleAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("ArticleAdmin", "View")]
        public ActionResult Index(int cul = 0, long fa = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "ArticleAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "ArticleAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "ArticleAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "ArticleAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            if (cul == 0) cul = unitOfWork.LanguageRepository.FindAll().FirstOrDefault().Id;
            if (fa <= 0) fa = ConfigItemHelper.GetRootNewsId();
            if (page < 1) page = 1;
            var sql = unitOfWork.ArticleRepository.FindAll();
            if (cul != 0)
            {
                sql = sql.Where(x => x.Language.Id == cul);
            }
            if (fa != ConfigItemHelper.GetRootNewsId())
            {
                sql = sql.Where(x => x.Category.Id == fa);
            }
            var count = sql.Count();
            var articles = sql.OrderByDescending(x => x.Id).Skip((page - 1) * pageItems).Take(pageItems).ToList();

            var articleAndLanguageVersions = new List<EntityWithLanguageVersionsModel<Article>>();
            foreach (var a in articles)
            {
                var languageVersions =
                    unitOfWork.ArticleRepository.Find(x => x.OriginalValue == a.OriginalValue && x.Language.Id != a.Language.Id).
                        ToList();
                articleAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<Article>() { Entity = a, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<Article>>()
            {
                ListItems = articleAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = cul
                    },
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = CategoryHelper.BindSelectListItemArticle(unitOfWork.CategoryRepository, fa, cul, 0, false),
                        FatherSelectedValue = fa
                    },
                    EntityType = EntityTypeCollection.Categories,
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/ArticleAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, pageItems, page, "href='/Admin/ArticleAdmin/Index?cul=" + cul + "&fa=" + fa + "&page={0}'")

            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), cul);
            model.FilterViewModel.FatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "All", Value = "0", Selected = fa == 0 });
            return View(model);
        }

        // id !=0 -> add language version for article 
        [CustomViewAuthorize("ArticleAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "ArticleAdmin";
            var articleOriginal = id == 0 ? null : unitOfWork.ArticleRepository.FindUnique(x => x.Id == id);
            var originalValue = articleOriginal != null ? articleOriginal.OriginalValue : Guid.NewGuid();
            if (articleOriginal == null)
            {
                while (unitOfWork.ArticleRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }

            var model = new ArticleViewModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<Article>(originalValue, unitOfWork), 0),
                Categories = new List<SelectListItem> { },
                OriginalValue = originalValue
            };

            var imageFolder = "";
            if (articleOriginal != null)
            {
                imageFolder = articleOriginal.ImageFolder;
                model.ImageDefault = articleOriginal.ImageDefault;
            }
            else
            {
                imageFolder = "/Upload/News/" + Guid.NewGuid().ToString();
                while (Directory.Exists(Server.MapPath(imageFolder)))
                {
                    imageFolder = "/Upload/News/" + Guid.NewGuid().ToString();
                }
            }
            model.ImageFolder = imageFolder;
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Chọn ngôn ngữ",
                Value = "0"
            });

            var logHistory = new LogHistory()
            {
                ActionBy = CurrentUser,
                ActionType = ActionTypeCollection.Temp,
                CreatedDate = DateTime.Now,
                EntityType = EntityTypeCollection.Articles,
                Comment = imageFolder
            };
            unitOfWork.LogHistoryRepository.Insert(logHistory);
            try
            {
                unitOfWork.Save();
                model.LogHistoryId = logHistory.Id;
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ArticleViewModel model)
        {
            TempData["CurrentMenu"] = "ArticleAdmin";
            if (ModelState.IsValid)
            {
                var entity = new Article()
                                 {
                                     Category =
                                         unitOfWork.CategoryRepository.FindById(
                                             model.CategorySelectedValue),
                                        Language =
                                         unitOfWork.LanguageRepository.FindById(
                                             model.LanguageSelectedValue),
                                        UpdateDate = DateTime.Now,
                                     CreateDate = DateTime.Now,
                                 };
                ArticleViewModel.ToEntity(model, ref entity);
                try
                {
                    unitOfWork.ArticleRepository.Insert(entity);

                    // save to get entityId for Generate Sename
                    unitOfWork.Save();

                    var entityType = EntityTypeCollection.Articles;
                    entity.SeName = SeoHelper.VaidateSeNameAndSubmit(entityType, entity.Id, entity.Title);

                    // save articleVersion 
                    var logHistory = unitOfWork.LogHistoryRepository.FindUnique(x => x.Id == model.LogHistoryId);
                        //change action type of log history to check that article is createnew completed
                        if (logHistory != null)
                        {
                            logHistory.EntityId = entity.Id;
                            logHistory.ActionType = ActionTypeCollection.Create;
                        }
                    unitOfWork.Save();
                    TempData["message"] = string.Format("<strong style='color:green'>Thêm thành công!</strong>");
                    return RedirectToAction("Index", new { cul = entity.Language.Id, fa = entity.Category.Id });
                }
                catch (Exception ex)
                {
                    TempData["message"] = string.Format("<strong style='color:green'>Lỗi xảy ra!</strong>");
                    return RedirectToAction("Index");
                }

            }
            model.Categories = CategoryHelper.BindSelectListItem(unitOfWork.CategoryRepository, model.CategorySelectedValue, model.LanguageSelectedValue);
            model.Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<Article>(model.OriginalValue, unitOfWork), model.LanguageSelectedValue);
            return View(model);
        }

        [CustomViewAuthorize("ArticleAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "ArticleAdmin";
            var article = unitOfWork.ArticleRepository.FindUnique(x => x.Id == id);
            if (article == null) return RedirectToAction("Index");

            var model = new ArticleViewModel(article);
            model.Categories = CategoryHelper.BindSelectListItemArticle(unitOfWork.CategoryRepository, article.Category.Id, article.Language.Id, 0, false);
            model.CategorySelectedValue = article.Category.Id;
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ArticleViewModel model)
        {
            TempData["CurrentMenu"] = "ArticleAdmin";
            var article = unitOfWork.ArticleRepository.FindUnique(x => x.Id == model.Id);
            if (ModelState.IsValid)
            {
                if (article == null) return RedirectToAction("Index");
                ArticleViewModel.ToEntity(model, ref article);
                article.UpdateDate = DateTime.Now;
                article.Category = unitOfWork.CategoryRepository.FindById(model.CategorySelectedValue);

                try
                {
                    article.SeName = SeoHelper.VaidateSeNameAndSubmit(EntityTypeCollection.Articles, article.Id, article.Title);
                    unitOfWork.Save();
                    TempData["message"] = string.Format("<strong style='color:green'>Cập nhật thành công!</strong>");
                }
                    catch (Exception ex)
                {
                    TempData["message"] = string.Format("<strong style='color:red'>Lỗi xảy ra! {0}</strong>", ex.Message);
                }
            return RedirectToAction("Index", new { fa = article.Category.Id });
            }
            model.Categories = CategoryHelper.BindSelectListItem(unitOfWork.CategoryRepository, model.CategorySelectedValue, article.Language.Id, 0, false);
            return View(model);
        }

        [CustomViewAuthorize("ArticleAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "ArticleAdmin";
            var article = unitOfWork.ArticleRepository.FindUnique(x => x.Id == id);
            if (article == null) return RedirectToAction("Index");
            try
            {
                //delete images folder
                if (Directory.Exists(Server.MapPath(article.ImageFolder)))
                {
                    try
                    {
                        Directory.Delete(Server.MapPath(article.ImageFolder), true);
                    }
                    catch (Exception ex) { }
                }

                SeoHelper.DeleteUrlData(EntityTypeCollection.Articles, article.Id);
                unitOfWork.ArticleRepository.Delete(id);
                unitOfWork.Save();

                TempData["message"] = string.Format("<strong style='color:green'>Xóa thành công!</strong>");
            }
            catch (Exception ex)
            {
                TempData["message"] = string.Format("<strong style='color:red'>Xóa không thành công, hãy thử lại!</strong>");
            }
            return RedirectToAction("Index");
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
