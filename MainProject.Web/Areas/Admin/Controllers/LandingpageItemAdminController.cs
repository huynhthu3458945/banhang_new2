﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.LandingpageItem;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class LandingpageItemAdminController : BaseController
    {
        //
        // GET: /Admin/LandingpageItemAdmin/

        private readonly UnitOfWork unitOfWork;

        private readonly int pageItems = 30;

        public LandingpageItemAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("LandingpageItemAdmin", "View")]
        public ActionResult Index(int languageId = 0, int landingpageGroupId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "LandingpageItemAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "LandingpageItemAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "LandingpageItemAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "LandingpageItemAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            var sql = unitOfWork.LandingpageItemRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            if (landingpageGroupId != 0)
            {
                sql = sql.Where(x => x.LandingpageGroup.Id == landingpageGroupId);
            }
            var count = sql.Count();
            var landingpageItems = sql.OrderByDescending(x => x.Id).Skip((page - 1) * pageItems).Take(pageItems).ToList();
            var landingpageItemAndLanguageVersions = new List<EntityWithLanguageVersionsModel<LandingpageItem>>();
            foreach (var landingpageItem in landingpageItems)
            {
                var languageVersions =
                    unitOfWork.LandingpageItemRepository.Find(x => x.OriginalValue == landingpageItem.OriginalValue && x.Language.Id != landingpageItem.Language.Id).
                        ToList();
                landingpageItemAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<LandingpageItem>() { Entity = landingpageItem, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<LandingpageItem>>()
            {
                ListItems = landingpageItemAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = languageId
                    },
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = LandingpageGroupHelper.BuildSelectListItems(unitOfWork.LandingpageGroupRepository, languageId, landingpageGroupId),
                        FatherSelectedValue = landingpageGroupId
                    },
                    EntityType = EntityTypeCollection.Categories,
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/LandingpageItemAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, pageItems, page, "href='/Admin/LandingpageItemAdmin/Index?languageId=" + languageId + "&landingpageGroupId=" + landingpageGroupId + "&page={0}'")

            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId);
            model.FilterViewModel.FatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "All", Value = "0", Selected = landingpageGroupId == 0 });
            return View(model);
        }

        [CustomViewAuthorize("LandingpageItemAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "LandingpageItemAdmin";

            var landingpageItemOriginal = id == 0 ? null : unitOfWork.LandingpageItemRepository.FindUnique(x => x.Id == id);
            var originalValue = landingpageItemOriginal != null ? landingpageItemOriginal.OriginalValue : Guid.NewGuid();
            if (landingpageItemOriginal == null)
            {
                while (unitOfWork.LandingpageItemRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }
            var landingpageItemViewModel = new LandingpageItemViewModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<LandingpageItem>(originalValue, unitOfWork), 0),
                OriginalValue = originalValue,
                LanguageGroups = new List<SelectListItem> { },
            };
            var imageFolder = "";
            if (landingpageItemOriginal != null)
            {
                imageFolder = landingpageItemOriginal.ImageFolder;
                landingpageItemViewModel.ImageDefault = landingpageItemOriginal.ImageDefault;
            }
            else
            {
                imageFolder = "/Upload/LandingpageItem/" + Guid.NewGuid().ToString();
                while (Directory.Exists(Server.MapPath(imageFolder)))
                {
                    imageFolder = "/Upload/LandingpageItem/" + Guid.NewGuid().ToString();
                }
            }
            landingpageItemViewModel.ImageFolder = imageFolder;
            landingpageItemViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(landingpageItemViewModel);
        }

        [HttpPost]
        public ActionResult Create(LandingpageItemViewModel landingpageItemViewModel)
        {
            TempData["CurrentMenu"] = "LandingpageItemAdmin";

            if (ModelState.IsValid)
            {
                var landingpageItem = new LandingpageItem()
                {
                    LandingpageGroup = unitOfWork.LandingpageGroupRepository.FindById(landingpageItemViewModel.LanguageGroupSelectedValue),
                    Language = unitOfWork.LanguageRepository.FindById(landingpageItemViewModel.LanguageSelectedValue)
                };
                LandingpageItemViewModel.ToEntity(landingpageItemViewModel, ref landingpageItem);
                unitOfWork.LandingpageItemRepository.Insert(landingpageItem);

                unitOfWork.Save();

                TempData["message"] = string.Format("<span style='color:green'>Bài viết <strong>{0}</strong> đã được thêm thành công!</span>", landingpageItemViewModel.Title);
                return RedirectToAction("Index", new { languageId = landingpageItem.Language.Id, landingpageGroupId = landingpageItem.LandingpageGroup.Id });
            }
            landingpageItemViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<LandingpageItem>(landingpageItemViewModel.OriginalValue, unitOfWork), 0);
            landingpageItemViewModel.LanguageGroups = new List<SelectListItem> { };
            landingpageItemViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(landingpageItemViewModel);
        }

        [CustomViewAuthorize("LandingpageItemAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "LandingpageItemAdmin";

            var landingpageItem = unitOfWork.LandingpageItemRepository.FindUnique(d => d.Id == id);
            if (landingpageItem == null)
            {
                TempData["message"] = string.Format("Không tìm thấy bài viết cần sửa");
                return RedirectToAction("Index");
            }
            var landingpageItemViewModel = new LandingpageItemViewModel
            {
                LanguageSelectedValue = landingpageItem.Language.Id,
                Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), landingpageItem.Language.Id),
                LanguageGroupSelectedValue = landingpageItem.LandingpageGroup.Id,
                LanguageGroups = LandingpageGroupHelper.BuildSelectListItems(unitOfWork.LandingpageGroupRepository, landingpageItem.Language.Id, landingpageItem.LandingpageGroup.Id)
            };
            landingpageItemViewModel.Id = landingpageItem.Id;

            LandingpageItemViewModel.ToModel(landingpageItemViewModel, ref landingpageItem);
            return View(landingpageItemViewModel);
        }

        [HttpPost]
        public ActionResult Edit(LandingpageItemViewModel landingpageItemViewModel)
        {
            TempData["CurrentMenu"] = "Product";

            if (ModelState.IsValid)
            {
                var landingpageItem = unitOfWork.LandingpageItemRepository.FindUnique(d => d.Id == landingpageItemViewModel.Id);

                landingpageItem.LandingpageGroup = unitOfWork.LandingpageGroupRepository.FindById(landingpageItemViewModel.LanguageGroupSelectedValue);
                landingpageItem.Language = unitOfWork.LanguageRepository.FindById(landingpageItemViewModel.LanguageSelectedValue);              

                LandingpageItemViewModel.ToEntity(landingpageItemViewModel, ref landingpageItem);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã cập nhật thành công</span>",
                                                    landingpageItem.Title);
                return RedirectToAction("Index", new { languageId = landingpageItemViewModel.LanguageSelectedValue, landingpageGroupId = landingpageItem.LandingpageGroup.Id });
            }
            landingpageItemViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<LandingpageItem>(landingpageItemViewModel.OriginalValue, unitOfWork), 0);
            landingpageItemViewModel.LanguageGroups = new List<SelectListItem> { };
            landingpageItemViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(landingpageItemViewModel);
        }

        [CustomViewAuthorize("LandingpageItemAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "LandingpageItemAdmin";

            var landingpageItem = unitOfWork.LandingpageItemRepository.FindUnique(d => d.Id == id);
            var currentlandingpageGroupId = landingpageItem.LandingpageGroup.Id;
            var currentLanguageId = landingpageItem.Language.Id;
            var title = landingpageItem.Title;
            if (landingpageItem != null)
            {

                // Delete images of LandingpageItem
                string urlImg = landingpageItem.ImageDefault;
                if (!string.IsNullOrWhiteSpace(urlImg))
                {
                    if (Directory.Exists(Server.MapPath(urlImg)))
                    {
                        System.IO.File.Delete(urlImg);
                    }
                }

                // Delete LandingpageItem

                unitOfWork.LandingpageItemRepository.Delete(landingpageItem);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được xóa</span>", title);
            }
            return RedirectToAction("Index", new { languageId = currentLanguageId, landingpageGroupId = currentlandingpageGroupId });
        }

    }
}
