﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Menu;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class MenuAdminController : BaseController
    {
        private UnitOfWork unitOfWork;

        private string currentUser
        {
            get { return HttpContext.User.Identity.Name; }
        }
        private int pageItems = ConfigItemHelper.GetItemsCountPerPage();

        public MenuAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("MenuAdmin", "View")]
        public ActionResult Index(int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "MenuAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "MenuAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "MenuAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "MenuAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            if (page < 1) page = 1;
            var sql = unitOfWork.MenuRepository.FindAll().ToList();
            var count = sql.Count();
            var menus = sql.OrderByDescending(x => x.Id).Skip((page - 1) * pageItems).Take(pageItems).ToList();
            var indexViewModel = new IndexViewModel<Menu>()
            {
                ListItems = menus,
                PagingViewModel = new PagingModel(count, pageItems, page, "href='/Admin/MenuAdmin/Index?page={0}'")

            };

            return View(indexViewModel);
        }

        [CustomViewAuthorize("MenuAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "MenuAdmin";
            var menuViewModel = new MenuViewModel
            {
                MenuTypes = EnumHelper.ToSelectList(typeof(MenuTypeCollection))
            };
            menuViewModel.MenuTypes.Insert(0,new SelectListItem { Text = "Chọn loại menu", Value = "" });
            return View(menuViewModel);
        }

        [HttpPost]
        public ActionResult Create(MenuViewModel menuViewModel)
        {
            TempData["CurrentMenu"] = "MenuAdmin";
            if (ModelState.IsValid)
            {
                var menu = new Menu();
                menu.MenuType = menuViewModel.MenuTypeSelected;
                MenuViewModel.ToEntity(menuViewModel, ref menu);
                unitOfWork.MenuRepository.Insert(menu);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>Thành công!</strong>");
                return RedirectToAction("Index");
            }
            menuViewModel.MenuTypes = EnumHelper.ToSelectList(typeof(MenuTypeCollection));
            menuViewModel.MenuTypes.Insert(0, new SelectListItem { Text = "Chọn loại menu", Value = "" });
            return View(menuViewModel);
        }

        [CustomViewAuthorize("MenuAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "MenuAdmin";
            var menu = unitOfWork.MenuRepository.FindUnique(d => d.Id == id);
            if (menu == null)
            {
                TempData["message"] = string.Format("Không tìm thấy menu cần sửa");
                return RedirectToAction("Index");
            }
            var model = new MenuViewModel
            {
                MenuTypes = EnumHelper.ToSelectList(typeof(MenuTypeCollection)),
                MenuTypeSelected = menu.MenuType,
            };
            model.Id = menu.Id;
            MenuViewModel.ToModel(model, ref menu);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(MenuViewModel model)
        {
            TempData["CurrentMenu"] = "MenuAdmin";
            if (ModelState.IsValid)
            {
                var menu = unitOfWork.MenuRepository.FindUnique(d => d.Id == model.Id);
                menu.MenuType = model.MenuTypeSelected;
                MenuViewModel.ToEntity(model, ref menu);
                unitOfWork.Save();

                TempData["message"] = string.Format("<span style='color:green'>{0} đã cập nhật thành công</span>",
                                                    menu.Title);
                return RedirectToAction("Index");
            }
            model.MenuTypes = EnumHelper.ToSelectList(typeof(MenuTypeCollection));
            return View(model);
        }

        [CustomViewAuthorize("MenuAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "MenuAdmin";
            var menu = unitOfWork.MenuRepository.FindUnique(d => d.Id == id);
            if (menu != null)
            {
                var menuTitle = menu.Title;
                unitOfWork.MenuRepository.Delete(menu);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được xóa</span>", menuTitle);
            }
            return RedirectToAction("Index");
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
