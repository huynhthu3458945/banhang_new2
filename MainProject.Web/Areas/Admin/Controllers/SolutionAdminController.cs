﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Solution;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class SolutionAdminController : BaseController
    {
        //
        // GET: /Admin/SolutionAdmin/
        private readonly UnitOfWork unitOfWork;
        private readonly int itemInPerPage;

        public SolutionAdminController()
        {
            unitOfWork = new UnitOfWork();
            itemInPerPage = 10;
        }

        [CustomViewAuthorize("SolutionAdmin", "View")]
        public ActionResult Index(int languageId = 0, int page = 1)
        {
            TempData["CurrentMenu"] = "SolutionAdmin";
            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "SolutionAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "SolutionAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "SolutionAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            var sql = unitOfWork.SolutionRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            var count = sql.Count();
            var solutions = sql.OrderByDescending(x => x.Id).Skip((page - 1) * itemInPerPage).Take(itemInPerPage).ToList();
            var solutionsAndLanguageVersions = new List<EntityWithLanguageVersionsModel<Solution>>();
            foreach (var a in solutions)
            {
                var languageVersions =
                    unitOfWork.SolutionRepository.Find(x => x.OriginalValue == a.OriginalValue && x.Language.Id != a.Language.Id).
                        ToList();
                solutionsAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<Solution>() { Entity = a, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<Solution>>()
            {
                ListItems = solutionsAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = languageId
                    },
                    BaseUrl = "/Admin/SolutionAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, itemInPerPage, page, "href='/Admin/SolutionAdmin/Index?languageId=" + languageId + "&page={0}'")

            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId);
            return View(model);
        }

        [CustomViewAuthorize("SolutionAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "SolutionAdmin";
            var solutionOriginal = id == 0 ? null : unitOfWork.SolutionRepository.FindUnique(x => x.Id == id);
            var originalValue = solutionOriginal != null ? solutionOriginal.OriginalValue : Guid.NewGuid();
            if (solutionOriginal == null)
            {
                while (unitOfWork.SolutionRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }
            var solutionViewModel = new SolutionViewModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<Solution>(originalValue, unitOfWork), 0),
                OriginalValue = originalValue
            };

            if (solutionOriginal != null)
            {
                solutionViewModel.Order = solutionOriginal.Order;
            }
            solutionViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(solutionViewModel);
        }
        [HttpPost]
        public ActionResult Create(SolutionViewModel solutionViewModel)
        {
            TempData["CurrentMenu"] = "SolutionAdmin";
            if (ModelState.IsValid)
            {
                var solution = new Solution()
                {
                    Language = unitOfWork.LanguageRepository.FindById(solutionViewModel.LanguageSelectedValue)
                };
                SolutionViewModel.ToEntity(solutionViewModel, ref solution);
                unitOfWork.SolutionRepository.Insert(solution);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Giải pháp <strong>{0}</strong> đã được thêm thành công!</span>", solutionViewModel.Title);
                return RedirectToAction("Index", new { languageId = solution.Language.Id });
            }
            solutionViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<Solution>(solutionViewModel.OriginalValue, unitOfWork), 0);
            solutionViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(solutionViewModel);
        }

        [CustomViewAuthorize("SolutionAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "SolutionAdmin";
            var solution = unitOfWork.SolutionRepository.FindUnique(d => d.Id == id);
            if (solution == null)
            {
                TempData["message"] = string.Format("Không tìm thấy giải pháp cần sửa");
                return RedirectToAction("Index");
            }
            var solutionViewModel = new SolutionViewModel
            {
                LanguageSelectedValue = solution.Language.Id,
                Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), solution.Language.Id),
            };
            solutionViewModel.Id = solution.Id;
            SolutionViewModel.ToModel(solutionViewModel, ref solution);
            return View(solutionViewModel);
        }
        [HttpPost]
        public ActionResult Edit(SolutionViewModel solutionViewModel)
        {
            TempData["CurrentMenu"] = "SolutionAdmin";
            if (ModelState.IsValid)
            {
                var solution = unitOfWork.SolutionRepository.FindUnique(d => d.Id == solutionViewModel.Id);

                solution.Language = unitOfWork.LanguageRepository.FindById(solutionViewModel.LanguageSelectedValue);
                SolutionViewModel.ToEntity(solutionViewModel, ref solution);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Giải pháp {0} đã cập nhật thành công</span>",
                                                    solution.Title);
                return RedirectToAction("Index", new { languageId = solutionViewModel.LanguageSelectedValue });
            }
            solutionViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<Solution>(solutionViewModel.OriginalValue, unitOfWork), 0);
            solutionViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(solutionViewModel);
        }

        [CustomViewAuthorize("SolutionAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "SolutionAdmin";
            var solution = unitOfWork.SolutionRepository.FindUnique(p => p.Id == id);
            if (solution == null)
            {
                TempData["message"] = string.Format("<strong style='color:red'>Giải pháp không tồn tại</strong>");
                return RedirectToAction("Index");
            }
            var title = solution.Title;
            var languageId = solution.Language.Id;
            unitOfWork.SolutionRepository.Delete(solution);
            unitOfWork.Save();
            TempData["message"] = string.Format("<strong style='color:green'>{0}  Đã xóa</strong>", title);
            return RedirectToAction("Index", new { languageId = languageId });

        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
