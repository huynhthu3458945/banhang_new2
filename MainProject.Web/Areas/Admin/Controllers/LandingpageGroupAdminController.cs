﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.LandingpageGroup;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin, Mod, FormManager")]
    public class LandingpageGroupAdminController : BaseController
    {
        //
        // GET: /Admin/LandingpageGroupAdmin/
        private readonly UnitOfWork unitOfWork;
        private readonly int itemInPerPage;

        public LandingpageGroupAdminController()
        {
            unitOfWork = new UnitOfWork();
            itemInPerPage = 10;
        }

        [CustomViewAuthorize("LandingpageGroupAdmin", "View")]
        public ActionResult Index(int languageId = 0, int page = 1)
        {
            TempData["CurrentMenu"] = "LandingpageGroupAdmin";
            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "LandingpageGroupAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "LandingpageGroupAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "LandingpageGroupAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            var sql = unitOfWork.LandingpageGroupRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            var count = sql.Count();
            var landingpageGroups = sql.OrderBy(x => x.Id).Skip((page - 1) * itemInPerPage).Take(itemInPerPage).ToList();
            var landingpageGroupsAndLanguageVersions = new List<EntityWithLanguageVersionsModel<LandingpageGroup>>();
            foreach (var landingpageGroup in landingpageGroups)
            {
                var languageVersions =
                    unitOfWork.LandingpageGroupRepository.Find(x => x.OriginalValue == landingpageGroup.OriginalValue && x.Language.Id != landingpageGroup.Language.Id).
                        ToList();
                landingpageGroupsAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<LandingpageGroup>() { Entity = landingpageGroup, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<LandingpageGroup>>()
            {
                ListItems = landingpageGroupsAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = languageId
                    },
                    BaseUrl = "/Admin/LandingpageGroupAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, itemInPerPage, page, "href='/Admin/LandingpageGroupAdmin/Index?languageId=" + languageId + "&page={0}'")

            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId);
            return View(model);
        }

        [CustomViewAuthorize("LandingpageGroupAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "LandingpageGroupAdmin";
            var landingpageGroupOriginal = id == 0 ? null : unitOfWork.LandingpageGroupRepository.FindUnique(x => x.Id == id);
            var originalValue = landingpageGroupOriginal != null ? landingpageGroupOriginal.OriginalValue : Guid.NewGuid();
            if (landingpageGroupOriginal == null)
            {
                while (unitOfWork.LandingpageGroupRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }
            var landingpageGroupViewModel = new LandingpageGroupViewModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<LandingpageGroup>(originalValue, unitOfWork), 0),
                OriginalValue = originalValue,
                PositionTypes = EnumHelper.ToSelectList(typeof(PositionTypeCollection))
            };

            var imageFolder = "";
            if (landingpageGroupOriginal != null)
            {
                imageFolder = landingpageGroupOriginal.ImageFolder;
                landingpageGroupViewModel.ImageDefault = landingpageGroupOriginal.ImageDefault;
            }
            else
            {
                imageFolder = "/Upload/LandingpageGroup/" + Guid.NewGuid().ToString();
                while (Directory.Exists(Server.MapPath(imageFolder)))
                {
                    imageFolder = "/Upload/LandingpageGroup/" + Guid.NewGuid().ToString();
                }
            }
            landingpageGroupViewModel.ImageFolder = imageFolder;

            landingpageGroupViewModel.Languages.Insert(0, new SelectListItem{Text = "Vui lòng chọn ngôn ngữ", Value = ""});
            landingpageGroupViewModel.PositionTypes.Insert(0, new SelectListItem { Text = "Chọn loại menu", Value = "" });

            return View(landingpageGroupViewModel);
        }
        [HttpPost]
        public ActionResult Create(LandingpageGroupViewModel landingpageGroupViewModel)
        {
            TempData["CurrentMenu"] = "LandingpageGroupAdmin";
            if (ModelState.IsValid)
            {
                var landingpageGroup = new LandingpageGroup()
                {
                    Language = unitOfWork.LanguageRepository.FindById(landingpageGroupViewModel.LanguageSelectedValue)
                };
                LandingpageGroupViewModel.ToEntity(landingpageGroupViewModel, ref landingpageGroup);
                unitOfWork.LandingpageGroupRepository.Insert(landingpageGroup);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Danh mục <strong>{0}</strong> đã được thêm thành công!</span>", landingpageGroupViewModel.Title);
                return RedirectToAction("Index", new { languageId = landingpageGroup.Language.Id });
            }
            landingpageGroupViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<LandingpageGroup>(landingpageGroupViewModel.OriginalValue, unitOfWork), 0);
            landingpageGroupViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(landingpageGroupViewModel);
        }

        [CustomViewAuthorize("LandingpageGroupAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "LandingpageGroupAdmin";
            var landingpageGroup = unitOfWork.LandingpageGroupRepository.FindUnique(d => d.Id == id);
            if (landingpageGroup == null)
            {
                TempData["message"] = string.Format("Không tìm thấy danh mục cần sửa");
                return RedirectToAction("Index");
            }
            var landingpageGroupViewModel = new LandingpageGroupViewModel
            {
                LanguageSelectedValue = landingpageGroup.Language.Id,
                Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), landingpageGroup.Language.Id),
                PositionTypes = EnumHelper.ToSelectList(typeof(PositionTypeCollection)),
                PositionTypeSelectedValue = landingpageGroup.PositionType,
            };
            landingpageGroupViewModel.Id = landingpageGroup.Id;
            LandingpageGroupViewModel.ToModel(landingpageGroupViewModel, ref landingpageGroup);
            return View(landingpageGroupViewModel);
        }
        [HttpPost]
        public ActionResult Edit(LandingpageGroupViewModel landingpageGroupViewModel)
        {
            TempData["CurrentMenu"] = "LandingpageGroupAdmin";
            if (ModelState.IsValid)
            {
                var landingpageGroup = unitOfWork.LandingpageGroupRepository.FindUnique(d => d.Id == landingpageGroupViewModel.Id);

                landingpageGroup.Language = unitOfWork.LanguageRepository.FindById(landingpageGroupViewModel.LanguageSelectedValue);
                landingpageGroup.PositionType = landingpageGroupViewModel.PositionTypeSelectedValue;
                LandingpageGroupViewModel.ToEntity(landingpageGroupViewModel, ref landingpageGroup);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Nhân viên {0} đã cập nhật thành công</span>",
                                                    landingpageGroup.Title);
                return RedirectToAction("Index", new { languageId = landingpageGroupViewModel.LanguageSelectedValue });
            }
            landingpageGroupViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<LandingpageGroup>(landingpageGroupViewModel.OriginalValue, unitOfWork), 0);
            landingpageGroupViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            landingpageGroupViewModel.PositionTypes = EnumHelper.ToSelectList(typeof(PositionTypeCollection));
            return View(landingpageGroupViewModel);
        }

        [CustomViewAuthorize("LandingpageGroupAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "LandingpageGroupAdmin";
            var landingpageGroup = unitOfWork.LandingpageGroupRepository.FindUnique(p => p.Id == id);
            if (landingpageGroup == null)
            {
                TempData["message"] = string.Format("<strong style='color:red'>Danh mục không tồn tại</strong>");
                return RedirectToAction("Index");
            }
            var title = landingpageGroup.Title;
            var languageId = landingpageGroup.Language.Id;
            try
            {
                //delete images folder
                if (Directory.Exists(Server.MapPath(landingpageGroup.ImageFolder)))
                {
                    try
                    {
                        Directory.Delete(Server.MapPath(landingpageGroup.ImageFolder), true);
                    }
                    catch{
                        TempData["message"] = string.Format("<strong style='color:red'>Xảy ra lỗi khi xóa ảnh đại diện, hãy thử lại!</strong>");
                    }
                }
                unitOfWork.LandingpageGroupRepository.Delete(landingpageGroup);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>{0}  Đã xóa</strong>", title);
            }
            catch
            {
                TempData["message"] = string.Format("<strong style='color:red'>Xóa không thành công, hãy thử lại!</strong>");
            }
            return RedirectToAction("Index", new { languageId = languageId });

        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
