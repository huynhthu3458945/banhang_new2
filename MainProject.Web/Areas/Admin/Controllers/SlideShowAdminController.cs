﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.ActionFilters;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models.SlideShows;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [FilterPermission(EntityManageType = EntityManageTypeCollection.ManageSlide)]
    public class SlideShowAdminController : BaseController
    {
        //
        // GET: /Admin/SlideShowAdmin/

        private UnitOfWork unitOfWork;
        private int PageItems = 15;

        public SlideShowAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("SlideShowAdmin", "View")]
        public ActionResult Index(int languageId = 0, long categoryId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "SlideShowAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "SlideShowAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "SlideShowAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "SlideShowAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            if (categoryId <= 0) categoryId = ConfigItemHelper.GetRootNewsId();
            var sql = unitOfWork.SlideShowRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            if (categoryId != ConfigItemHelper.GetRootNewsId())
            {
                sql = sql.Where(x => x.Category.Id == categoryId);
            }
            int count = sql.Count();
            var slideShows = sql.OrderBy(d => d.Id).Skip((page - 1) * PageItems).Take(PageItems).ToList();
            var slideShowsAndLanguageVersions = new List<EntityWithLanguageVersionsModel<SlideShow>>();
            foreach (var a in slideShows)
            {
                var languageVersions =
                    unitOfWork.SlideShowRepository.Find(x => x.OriginalValue == a.OriginalValue && x.Language.Id != a.Language.Id).
                        ToList();
                slideShowsAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<SlideShow>() { Entity = a, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<SlideShow>>()
            {
                ListItems = slideShowsAndLanguageVersions,
                PagingViewModel = new PagingModel(count, PageItems, page, "href='/Admin/SlideShowAdmin/Index?languageId=" + languageId + "&page={0}'"),
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = languageId
                    },
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = CategoryHelper.BindSelectListItemSlideShow(unitOfWork.CategoryRepository, categoryId, languageId, 0, false),
                        FatherSelectedValue = categoryId
                    },
                    EntityType = EntityTypeCollection.Categories,
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/slideShowAdmin/Index?"
                }
            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId);
            model.FilterViewModel.FatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "All", Value = "0", Selected = categoryId == 0 });
            return View(model);
        }

        [CustomViewAuthorize("SlideShowAdmin", "Create")]
        public ActionResult Create(int languageId = 0)
        {
            TempData["CurrentMenu"] = "SlideShowAdmin";
            var model = new SlideShowManageModel { LanguageSelectedValue = languageId };
            model.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(),
                                                                              languageId);
            var originalValue = Guid.NewGuid();
            while (unitOfWork.SlideShowRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
            {
                originalValue = Guid.NewGuid();
            }
            model.OriginalValue = originalValue;
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            model.Categories = new List<SelectListItem> { };
            var imageFolder = "/Upload/SlideShow/" + Guid.NewGuid().ToString();
            while (Directory.Exists(Server.MapPath(imageFolder)))
            {
                imageFolder = "/Upload/SlideShow/" + Guid.NewGuid().ToString();
            }
            model.ImageFolder = imageFolder;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(SlideShowManageModel slideShowManageModel)
        {
            TempData["CurrentMenu"] = "SlideShowAdmin";
            if (ModelState.IsValid)
            {
                var entity = new SlideShow
                {
                    Category = unitOfWork.CategoryRepository.FindById(slideShowManageModel.CategorySelectedValue),
                    Language = unitOfWork.LanguageRepository.FindById(slideShowManageModel.LanguageSelectedValue)
                };
                slideShowManageModel.ToEntity(slideShowManageModel, ref entity);
                unitOfWork.SlideShowRepository.Insert(entity);
                unitOfWork.Save();
                return RedirectToAction("Index", "SlideShowAdmin",
                                        new { languageId = slideShowManageModel.LanguageSelectedValue, categoryId = slideShowManageModel.CategorySelectedValue });
            }
            slideShowManageModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(),
                                                                         slideShowManageModel.LanguageSelectedValue);
            slideShowManageModel.Categories = new List<SelectListItem> { };
            slideShowManageModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = "0"
            });
            return View(slideShowManageModel);
        }

        [CustomViewAuthorize("SlideShowAdmin", "Edit")]
        public ActionResult Edit(long id)
        {
            TempData["CurrentMenu"] = "SlideShowAdmin";
            var entity = unitOfWork.SlideShowRepository.FindUnique(d => d.Id == id);
            if (entity != null)
            {
                var model = new SlideShowManageModel
                {
                    LanguageSelectedValue = entity.Language.Id,
                    Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), entity.Language.Id),
                    CategorySelectedValue = entity.Category.Id,
                    Categories = CategoryHelper.BindSelectListItemSlideShow(unitOfWork.CategoryRepository, entity.Category.Id, entity.Language.Id, 0, false)

                };
                model.ToModel(model, ref entity);
                return View(model);
            }
            TempData["message"] = string.Format("<span style='color:red'>Không tìm thấy đối tượng cần chỉnh sửa</span>");
            return View("Index");
        }

        [HttpPost]
        public ActionResult Edit(SlideShowManageModel model)
        {
            TempData["CurrentMenu"] = "SlideShowAdmin";
            if (ModelState.IsValid)
            {
                var entity = unitOfWork.SlideShowRepository.FindUnique(d => d.Id == model.Id);
                entity.Category = unitOfWork.CategoryRepository.FindById(model.CategorySelectedValue);
                model.ToEntity(model, ref entity);
                entity.Language = unitOfWork.LanguageRepository.FindUnique(d => d.Id == model.LanguageSelectedValue);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã cập nhật thành công</span>", entity.Name);
                return RedirectToAction("Index", "SlideShowAdmin",
                                        new { languageId = model.LanguageSelectedValue });
            }
            model.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(),
                                                                         model.LanguageSelectedValue);
            return View(model);
        }

        [CustomViewAuthorize("SlideShowAdmin", "Create")]
        public ActionResult AddVersion(long id)
        {
            TempData["CurrentMenu"] = "SlideShowAdmin";
            var slideShowVer = unitOfWork.SlideShowRepository.FindUnique(d => d.Id == id);
            if (slideShowVer != null)
            {
                var sqlLang =
                unitOfWork.SlideShowRepository.FindAll().Where(d => d.OriginalValue == slideShowVer.OriginalValue).Select(d => d.Language.Id).ToList();
                var model = new SlideShowManageModel { LanguageSelectedValue = 0 };
                model.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().Where(d => !sqlLang.Contains(d.Id)).ToList(),
                                                                              0);
                model.Categories = new List<SelectListItem> { };
                model.Languages.Insert(0, new SelectListItem
                {
                    Text = "Vui lòng chọn ngôn ngữ",
                    Value = ""
                });
                model.OriginalValue = slideShowVer.OriginalValue;

                var imageFolder = "";
                if (slideShowVer != null)
                {
                    imageFolder = slideShowVer.ImageFolder;
                    model.Image = slideShowVer.Image;
                }
                else
                {
                    imageFolder = "/Upload/SlideShow/" + Guid.NewGuid().ToString();
                    while (Directory.Exists(Server.MapPath(imageFolder)))
                    {
                        imageFolder = "/Upload/SlideShow/" + Guid.NewGuid().ToString();
                    }
                }
                model.ImageFolder = imageFolder;
                return View(model);
            }
            TempData["message"] =
                string.Format("<span style='color:red'>Không tìm thấy đối tượng cần add version</span>");
            return View("Index");
        }

        [HttpPost]
        public ActionResult AddVersion(SlideShowManageModel model)
        {
            TempData["CurrentMenu"] = "SlideShowAdmin";
            if (ModelState.IsValid)
            {
                var entity = new SlideShow
                {
                    Language = unitOfWork.LanguageRepository.FindById(model.LanguageSelectedValue),
                    Category = unitOfWork.CategoryRepository.FindById(model.CategorySelectedValue)
                };
                model.ToEntity(model, ref entity);
                unitOfWork.SlideShowRepository.Insert(entity);
                unitOfWork.Save();
                return RedirectToAction("Index", "SlideShowAdmin",
                                        new { languageId = model.LanguageSelectedValue, categoryId = model.CategorySelectedValue });
            }
            model.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(),
                                                                         model.LanguageSelectedValue);
            return View(model);
        }

        [CustomViewAuthorize("SlideShowAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "SlideShowAdmin";
            var entity = unitOfWork.SlideShowRepository.FindUnique(d => d.Id == id);

            if (entity != null)
            {
                unitOfWork.SlideShowRepository.Delete(entity);
                unitOfWork.Save();
                try
                {
                    //delete images folder
                    if (Directory.Exists(Server.MapPath(entity.ImageFolder)))
                    {
                        try
                        {
                            Directory.Delete(Server.MapPath(entity.ImageFolder), true);
                        }
                        catch (Exception ex) { }
                    }
                }
                catch (Exception ex)
                {
                    TempData["message"] = string.Format("<strong style='color:red'>Xảy ra lỗi trong quá trình xóa. Xin vui lòng thử lại!</strong>");
                }
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được xóa</span>", entity.Name);
                return RedirectToAction("Index");
            }
            TempData["message"] = string.Format("<span style='color:red'>Không tìm thấy đối tượng cần xóa</span>");
            return View("Index");
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
