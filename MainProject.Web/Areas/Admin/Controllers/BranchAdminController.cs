﻿using System.Web.Security;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Branch;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class BranchAdminController : BaseController
    {
        //
        // GET: /Admin/BranchAdmin/

        private UnitOfWork unitOfWork;

        private readonly int _itemInPerPage = 30;

        public BranchAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("BranchAdmin", "View")]
        public ActionResult Index(int cityId = 0, int districtId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "BranchAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "BranchAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "BranchAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "BranchAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);

            var sql = unitOfWork.BranchRepository.FindAll();
            if (cityId != 0)
            {
                sql = sql.Where(x => x.Region.Parent.Id == cityId);
            }
            if (districtId != 0)
            {
                sql = sql.Where(x => x.Region.Id == districtId);
            }
            var count = sql.Count();
            var branches = sql.OrderByDescending(x => x.Id).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();

            var indexViewModel = new IndexViewModel<Branch>()
            {
                ListItems = branches,
                FilterViewModel = new FilterViewModel()
                {
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = CityHelper.BindSelectListItemBranch(unitOfWork.RegionRepository, cityId),
                        FatherSelectedValue = cityId
                    },
                    OtherFatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = DistrictHelper.GetDistrictByCity(unitOfWork.RegionRepository, cityId, districtId),
                        FatherSelectedValue = districtId
                    },
                    EntityType = EntityTypeCollection.Branches,
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/BranchAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/BranchAdmin/Index?cityId=" + cityId + "&districtId=" + districtId + "&page={0}'")

            };

            indexViewModel.FilterViewModel.FatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "All", Value = "0", Selected = cityId == 0 });
            indexViewModel.FilterViewModel.OtherFatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "All", Value = "0", Selected = districtId == 0 });
            return View(indexViewModel);
        }

        [CustomViewAuthorize("BranchAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "BranchAdmin";
            var model = new BranchViewModel
            {
                Cities = CityHelper.BindSelectListItemBranch(unitOfWork.RegionRepository, 0),
                Districts = new List<SelectListItem> { }
            };
            model.Cities.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn thành phố",
                Value = "0"
            });
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(BranchViewModel branchViewModel)
        {
            TempData["CurrentMenu"] = "BranchAdmin";
            if (ModelState.IsValid)
            {
                var branch = new Branch()
                {
                    Region = unitOfWork.RegionRepository.FindById(branchViewModel.DistrictSelectedValue)
                };
                BranchViewModel.ToEntity(branchViewModel, ref branch);
                unitOfWork.BranchRepository.Insert(branch);
                unitOfWork.Save();

                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "<strong>{0}</strong> đã được thêm thành công."
                + "</div>", branch.Name);
                
                return RedirectToAction("Index");
            }
            branchViewModel.Cities = CityHelper.BindSelectListItemBranch(unitOfWork.RegionRepository, branchViewModel.CitySelectedValue);
            branchViewModel.Districts = DistrictHelper.BindSelectListItemBranch(unitOfWork.RegionRepository, branchViewModel.DistrictSelectedValue);
            return View(branchViewModel);
        }

        [CustomViewAuthorize("BranchAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "BranchAdmin";
            var branch = unitOfWork.BranchRepository.FindUnique(d => d.Id == id);
            if (branch == null)
            {
                TempData["message"] = string.Format("<div class=\"alert alert-danger alert-dismissable\">"
                   + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                   + "Không tìm thấy bệnh viện cần sửa."
                   + "</div>");

                return RedirectToAction("Index");
            }
            var branchViewModel = new BranchViewModel
            {
                CitySelectedValue = branch.Region.Parent.Id,
                DistrictSelectedValue = branch.Region.Id,
                Cities = CityHelper.BindSelectListItemBranch(unitOfWork.RegionRepository, branch.Region.Parent.Id),
                Districts = DistrictHelper.BindSelectListItemBranch(unitOfWork.RegionRepository, branch.Region.Id)
            };
            branchViewModel.Id = branch.Id;
            BranchViewModel.ToManageModel(branchViewModel, ref branch);
            return View(branchViewModel);
        }

        [HttpPost]
        public ActionResult Edit(BranchViewModel branchViewModel)
        {
            TempData["CurrentMenu"] = "BranchAdmin";
            if (ModelState.IsValid)
            {
                var branch = unitOfWork.BranchRepository.FindUnique(d => d.Id == branchViewModel.Id);

                branch.Region = unitOfWork.RegionRepository.FindById(branchViewModel.DistrictSelectedValue);

                BranchViewModel.ToEntity(branchViewModel, ref branch);
                unitOfWork.Save();

                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "<strong>{0}</strong> đã được cập nhật thành công."
                + "</div>", branch.Name);

                return RedirectToAction("Index", new { cityId = branchViewModel.CitySelectedValue, districtId = branchViewModel.DistrictSelectedValue });
            }
            return View(branchViewModel);
        }

        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "BranchAdmin";
            var branch = unitOfWork.BranchRepository.FindUnique(d => d.Id == id);
            var name = branch.Name;
            var cityId = branch.Region.Parent.Id;
            var districtId = branch.Region.Id;
            if (branch != null)
            {
                unitOfWork.BranchRepository.Delete(branch);
                unitOfWork.Save();

                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                   + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                   + "<strong>{0}</strong> đã được xóa."
                   + "</div>", name);
            }
        Complete:
            return RedirectToAction("Index", new { cityId = cityId, districtId = districtId });
        }


    }
}
