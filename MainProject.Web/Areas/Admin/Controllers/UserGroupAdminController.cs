﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core.Enums;
using MainProject.Core.UserInfos;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.UserGroupManage;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Root")]
    public class UserGroupAdminController : BaseMembershipController
    {
        private readonly UnitOfWork unitOfWork;

        private readonly int _itemInPerPage = 10;

        public UserGroupAdminController()
        {
            unitOfWork = new UnitOfWork();
        }
       
        public ActionResult Index(int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "UserGroupAdmin";

            if (page < 1) page = 1;
            var sql = unitOfWork.RoleRepository.FindAll();
            var count = sql.Count();
            var groups = sql.OrderByDescending(x=>x.RoleId).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            var model = new IndexViewModel<Role>()
            {
                ListItems = groups,
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/UserGroupAdmin/Index?page={0}'")

            };

            return View(model);
        }

        public ActionResult Create()
        {
            TempData["CurrentMenu"] = "UserGroupAdmin";

            // Get ContentGroup Objects for Create, Edit pages
            ViewBag.ContentBehaviorGroupObjects = ContentGroupHelper.GetContentBehaviorGroupObjects(unitOfWork.ContentGroupRepository, unitOfWork.ContentRepository, unitOfWork.ContentBehaviorRepository);
            
            var model = new UserGroupManageModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(UserGroupManageModel model)
        {
            TempData["CurrentMenu"] = "UserGroupAdmin";
            var checkedRole = unitOfWork.RoleRepository.FindUnique(x=>x.RoleName == model.RoleName);
            if (checkedRole == null)
            {
                // Insert role
                var role = new Role {
                    RoleName = model.RoleName,
                    RoleDescription = model.RoleDescription
                };
                unitOfWork.RoleRepository.Insert(role);

                // Insert RoleContentBehaviors
                if (model.ViewContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.ViewContentIds);
                }
                if (model.CreateContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.CreateContentIds);
                }
                if (model.EditContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.EditContentIds);
                }
                if (model.DeleteContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.DeleteContentIds);
                }
                if (model.DetailContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.DetailContentIds);
                }
                unitOfWork.Save();
                TempData["message"] = "<strong style='color: green'>Nhóm Tài khoản đã được tạo và gán quyền!</strong>";
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Tên nhóm tài khoản đã tồn tại, xin chọn tên khác");
            }
            // Get ContentGroup Objects for Create, Edit pages
            ViewBag.ContentBehaviorGroupObjects = ContentGroupHelper.GetContentBehaviorGroupObjects(unitOfWork.ContentGroupRepository, unitOfWork.ContentRepository, unitOfWork.ContentBehaviorRepository);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "UserGroupAdmin";

            var role = unitOfWork.RoleRepository.FindById(id);

            var model = new UserGroupManageModel { 
                RoleId = role.RoleId,
                RoleName = role.RoleName,
                RoleDescription = role.RoleDescription,
                ViewContentIds = unitOfWork.RoleContentBehaviorRepository.FindAll().Where(x=>x.Role.RoleId == role.RoleId && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.ViewTemplate).Select(x=>x.ContentBehavior.Id).ToList(),
                CreateContentIds = unitOfWork.RoleContentBehaviorRepository.FindAll().Where(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate).Select(x => x.ContentBehavior.Id).ToList(),
                EditContentIds = unitOfWork.RoleContentBehaviorRepository.FindAll().Where(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate).Select(x => x.ContentBehavior.Id).ToList(),
                DeleteContentIds = unitOfWork.RoleContentBehaviorRepository.FindAll().Where(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate).Select(x => x.ContentBehavior.Id).ToList(),
                DetailContentIds = unitOfWork.RoleContentBehaviorRepository.FindAll().Where(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DetailTemplate).Select(x => x.ContentBehavior.Id).ToList(),
                ContentIds = ContentGroupHelper.GetCheckAllIds(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role)
            };


            // Get ContentGroup Objects for Create, Edit pages
            ViewBag.ContentGroupObjects = ContentGroupHelper.GetContentBehaviorGroupObjects(unitOfWork.ContentGroupRepository, unitOfWork.ContentRepository, unitOfWork.ContentBehaviorRepository);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(UserGroupManageModel model)
        {
            TempData["CurrentMenu"] = "UserGroupAdmin";
            try
            {

                var role = unitOfWork.RoleRepository.FindById(model.RoleId);
                role.RoleName = model.RoleName;
                role.RoleDescription = model.RoleDescription;
                //_roleRepository.SaveChanges();

                // Remove previous RoleContentBehaviors
                ContentGroupHelper.DeleteRoleContentBehaviorsByRoleId(unitOfWork.RoleContentBehaviorRepository, role);

                // Insert new RoleContentBehaviors
                if (model.ViewContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.ViewContentIds);
                }
                if (model.CreateContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.CreateContentIds);
                }
                if (model.EditContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.EditContentIds);
                }
                if (model.DeleteContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.DeleteContentIds);
                }
                if (model.DetailContentIds != null)
                {
                    ContentGroupHelper.InsertRoleContentBehaviors(unitOfWork.RoleContentBehaviorRepository, unitOfWork.ContentBehaviorRepository, role, model.DetailContentIds);
                }
                unitOfWork.Save();
                TempData["message"] = "<strong style='color: green'>Nhóm Tài khoản đã được tạo và gán quyền!</strong>";
                return RedirectToAction("Index");
            }
            catch
            {
                ModelState.AddModelError("", "Xảy ra lỗi trong quá trình cập nhật dữ liệu!");

                // Get ContentGroup Objects for Create, Edit pages
                ViewBag.ContentBehaviorGroupObjects = ContentGroupHelper.GetContentBehaviorGroupObjects(unitOfWork.ContentGroupRepository, unitOfWork.ContentRepository, unitOfWork.ContentBehaviorRepository);
                return View(model);
            }
        }
        
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "UserGroupAdmin";
            var currentRole = unitOfWork.RoleRepository.FindById(id);
            if (Roles.GetUsersInRole(currentRole.RoleName).ToList().Count > 0)
            {
                TempData["message"] = "<strong style='color:red'>Vui lòng xóa tài khoản tồn tại thuộc nhóm tài khoản!</strong>";
                return RedirectToAction("Index");
            }
            else
            {
                var roleName = currentRole.RoleName;

                // Delete RoleContentBehaviors
                var roleCBs = unitOfWork.RoleContentBehaviorRepository.FindAll().Where(x => x.Role.RoleId == currentRole.RoleId).ToList();
                if(roleCBs.Count > 0)
                {
                    foreach (var item in roleCBs)
                    {
                        unitOfWork.RoleContentBehaviorRepository.Delete(item);
                        //_roleCBRepository.SaveChanges();
                    }
                }

                // Delete Role
                unitOfWork.RoleRepository.Delete(currentRole);
                //_roleRepository.SaveChanges();
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>{0} đã được xóa</strong>", roleName);
                return RedirectToAction("Index");
            }
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
