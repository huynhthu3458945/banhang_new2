﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.IO;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.ActionFilters;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models.Order;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [FilterPermission(EntityManageType = EntityManageTypeCollection.ManageOrders)]
    public class OrderAdminController : BaseController
    {
        private UnitOfWork unitOfWork;

        private string CurrentUser
        {
            get { return HttpContext.User.Identity.Name; }
        }
        private readonly int _itemInPerPage;

        public OrderAdminController()
        {
            unitOfWork = new UnitOfWork();
            _itemInPerPage = 30;
        }

        [CustomViewAuthorize("OrderAdmin", "View")]
        public ActionResult Index(IndexManageModel indexManageModel, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "OrderAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsDetail = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "OrderAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DetailTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "OrderAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);

            // Paging
            var sql = unitOfWork.OrderRepository.FindAll();
            if (indexManageModel.BuyerName != null)
            {
                sql = sql.Where(x => x.BuyerName.Contains(indexManageModel.BuyerName));
            }
            if (indexManageModel.OrderStatusSelected != 0)
            {
                sql = sql.Where(x => x.OrderStatus == indexManageModel.OrderStatusSelected);
            }

            if (indexManageModel.StartDate != null)
            {
                var startDateTemp = DateTime.Parse(indexManageModel.StartDate);
                sql = sql.Where(d => d.OrderTime >= startDateTemp);
            }

            if (indexManageModel.EndDate != null)
            {
                var endDateTemp = DateTime.Parse(indexManageModel.EndDate).AddDays(1);
                sql = sql.Where(d => d.OrderTime <= endDateTemp);
            }

            TempData["ExportItems"] = sql.OrderByDescending(x => x.Id).ToList();

            if (page < 1) page = 1;
            var count = sql.Count();
            var list = sql.OrderByDescending(x => x.Id).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            // End Paging

            var model = new IndexManageModel
            {
                //OrderStatusSelected = OrderStatusCollection.JustCreate,
                OrderStatusItems = Enum.GetValues(typeof(OrderStatusCollection)).Cast<OrderStatusCollection>()
                .Select(c => new SelectListItem
                {
                    Value = c.ToString(),
                    Text = c.GetDescription()
                }).ToList(),
                Orders = list,
                PagingModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/OrderAdmin/Index?BuyerName=" + indexManageModel.BuyerName
                                             + "&OrderStatusSelected=" + indexManageModel.OrderStatusSelected
                                             + "&StartDate=" + indexManageModel.StartDate
                                             + "&EndDate=" + indexManageModel.EndDate + "&page=" + "{0}'")
            };
            model.OrderStatusItems.Insert(0, new SelectListItem { Text = "All", Value = "0", Selected = model.OrderStatusSelected == 0 });
            return View(model);
        }

        [CustomViewAuthorize("OrderAdmin", "Detail")]
        public ActionResult Detail(long id)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "OrderAdmin";

            ViewData["message"] = TempData["message"];

            var order = unitOfWork.OrderRepository.FindById(id);
            var orderItems = unitOfWork.OrderItemRepository.Find(x => x.Order.Id == order.Id).OrderByDescending(x => x.Id).ToList();

            var model = new DetailManageModel
            {
                Id = order.Id,
                BuyerAddress = order.BuyerAddress,
                BuyerName = order.BuyerName,
                BuyerEmail = order.BuyerEmail,
                BuyerPhone = order.BuyerPhone,
                Amount = order.Amount,
                OrderType = order.OrderType.Title,
                OrderTime = order.OrderTime,
                OrderStatus = order.OrderStatus.GetDescription(),
                OrderStatusSelected = order.OrderStatus,
                OrderStatusItems = Enum.GetValues(typeof(OrderStatusCollection)).Cast<OrderStatusCollection>()
                    .Select(c => new SelectListItem
                    {
                        Value = c.ToString(),
                        Text = c.GetDescription(),
                        Selected = c == order.OrderStatus
                    }).ToList(),
                OrderItems = orderItems,
                OrderLogItems =
                unitOfWork.OrderLogRepository.Find(c => c.Order.Id == id)
                    .OrderByDescending(c => c.ActionTime)
                    .ToList()
                    .Select(c => new OrderLogItemModel
                    {
                        Id = c.Id,
                        Notes = c.Notes,
                        OrderLogType = c.OrderLogType.GetDescription(),
                        ActionBy = c.ActionBy,
                        ActionTime = c.ActionTime,
                        NewValue = c.NewValue,
                        OldValue = c.OldValue
                    }).ToList()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Detail(DetailManageModel model)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "OrderAdmin";

            if (ModelState.IsValid)
            {
                var item = unitOfWork.OrderRepository.FindById(model.Id);
                if (item != null)
                {
                    if (item.OrderStatus != model.OrderStatusSelected)
                    {
                        OrderHelper.UpdateOrderItemLogs(unitOfWork.OrderLogRepository, model, item);
                        OrderHelper.UpdateOrder(model, item);
                        unitOfWork.Save();
                    }

                    TempData["message"] = "Thao tác đã được cập nhật thành công!";
                    return RedirectToAction("Detail", new { id = model.Id });
                }
            }
            else
            {
                TempData["message"] = "Có xãy ra lỗi, vui lòng kiểm tra lại!";
                return RedirectToAction("Detail", new { id = model.Id });
            }

            return null;
        }

        public ActionResult ExportExcel()
        {
            TempData["CurrentMenu"] = "Player";

            var orders = (List<Order>)TempData["ExportItems"];

            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Danh sách đơn hàng");
                //---------------
                //Create Headers and format them
                ws.Cells[1, 1].Value = "STT";
                ws.Cells[1, 2].Value = "Tên khách hàng";
                ws.Cells[1, 3].Value = "Điện thoại";
                ws.Cells[1, 4].Value = "Email";
                ws.Cells[1, 5].Value = "Địa chỉ";
                ws.Cells[1, 6].Value = "Thời gian";
                ws.Cells[1, 7].Value = "Tình trạng đơn hàng";
                ws.Cells["A1:G1"].Style.Font.Bold = true;
                ws.Cells["A1:G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells["A1:G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                ws.Cells["A1:G1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ws.Cells["A1:G1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ws.Cells["A1:G1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                ws.Cells["A1:G1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                for (int i = 0; i < orders.Count; i++)
                {
                    var item = orders[i];
                    ws.Cells[i + 2, 1].Value = i + 1;
                    ws.Cells[i + 2, 2].Value = item.BuyerName;
                    ws.Cells[i + 2, 3].Value = item.BuyerPhone;
                    ws.Cells[i + 2, 4].Value = item.BuyerEmail;
                    ws.Cells[i + 2, 5].Value = item.BuyerAddress;
                    ws.Cells[i + 2, 6].Value = item.OrderTime.ToString("dd/MM/yyyy HH:mm");
                    ws.Cells[i + 2, 7].Value = item.OrderStatus.GetDescription();
                }
                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                var fileName = "Danh sách đơn hàng";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }
            return RedirectToAction("Index");
        }

        [CustomViewAuthorize("OrderAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "OrderAdmin";
            var order = unitOfWork.OrderRepository.FindById(id);
            if (order != null)
            {
                var orderItems = unitOfWork.OrderItemRepository.Find(x => x.Order.Id == order.Id).ToList();
                if (orderItems.Count() > 0)
                {
                    var orderLogs = unitOfWork.OrderLogRepository.Find(u => u.Order.Id == id).ToList();
                    if (orderLogs.Count() > 0)
                    {
                        foreach (var item in orderLogs)
                        {
                            unitOfWork.OrderLogRepository.Delete(item);
                        }
                        //unitOfWork.Save();
                    }

                    foreach (var item2 in orderItems)
                    {
                        unitOfWork.OrderItemRepository.Delete(item2);
                    }

                    unitOfWork.OrderRepository.Delete(order);
                    unitOfWork.Save();
                    TempData["message"] = string.Format("<strong style='color:green'>Xóa thành công!</strong>");
                }
            }
            else
            {
                TempData["message"] = string.Format("<strong style='color:red'>Đơn hàng này không tồn tại!</strong>");
            }
            return RedirectToAction("Index");
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
