﻿using System.Web.Security;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Districts;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class DistrictAdminController : BaseController
    {
        //
        // GET: /Admin/DistrictAdmin/

        private UnitOfWork unitOfWork;

        private readonly int _itemInPerPage = 30;

        public DistrictAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("DistrictAdmin", "View")]
        public ActionResult Index( int cityId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "DistrictAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "DistrictAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "DistrictAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "DistrictAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);

            var sql = unitOfWork.RegionRepository.FindAll();
            if (cityId != 0)
            {
                sql = sql.Where(x => x.Parent.Id == cityId);
            }
            else
            {
                sql = sql.Where(x => x.Parent != null);
            }
            var count = sql.Count();
            var districts = sql.OrderByDescending(x => x.Id).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            var indexViewModel = new IndexViewModel<Region>()
            {
                ListItems = districts,
                FilterViewModel = new FilterViewModel()
                {
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = CityHelper.BindSelectListItemDistrict(unitOfWork.RegionRepository, cityId, 0),
                        FatherSelectedValue = cityId
                    },
                    EntityType = EntityTypeCollection.Regions,
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/DistrictAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/DistrictAdmin/Index?cityId=" + cityId + "&page={0}'")

            };
            indexViewModel.FilterViewModel.FatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "All", Value = "0", Selected = cityId == 0 });
            return View(indexViewModel);
        }

        [CustomViewAuthorize("DistrictAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "DistrictAdmin";
            var districtViewModel = new DistrictViewModel();
            districtViewModel.Parents = CityHelper.BindSelectListItemDistrict(unitOfWork.RegionRepository, unitOfWork.RegionRepository.Find(c => c.IsPublished && c.Parent == null).FirstOrDefault().Id, 0);
            return View(districtViewModel);
        }

        [HttpPost]
        public ActionResult Create(DistrictViewModel districtViewModel)
        {
            TempData["CurrentMenu"] = "DistrictAdmin";
            if (ModelState.IsValid)
            {
                var district = new Region()
                {
                    Parent = unitOfWork.RegionRepository.FindById(districtViewModel.ParentSelectedValue)
                };
                DistrictViewModel.ToEntity(districtViewModel, ref district);
                unitOfWork.RegionRepository.Insert(district);
                unitOfWork.Save();

                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "<strong>{0}</strong> đã được thêm thành công."
                + "</div>", district.Name);

                return RedirectToAction("Index");
            }
            districtViewModel.Parents = CityHelper.BindSelectListItemDistrict(unitOfWork.RegionRepository, districtViewModel.ParentSelectedValue, 0);
            return View(districtViewModel);
        }

        [CustomViewAuthorize("DistrictAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "DistrictAdmin";
            var district = unitOfWork.RegionRepository.FindUnique(d => d.Id == id);
            if (district == null)
            {
                TempData["message"] = string.Format("<div class=\"alert alert-danger alert-dismissable\">"
                   + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                   + "Không tìm thấy quận/huyện phố cần sửa."
               + "</div>");

                return RedirectToAction("Index");
            }
            var districtViewModel = new DistrictViewModel
            {
                ParentSelectedValue = district.Parent.Id,
                Parents = CityHelper.BindSelectListItemDistrict(unitOfWork.RegionRepository, district.Parent.Id, 0)
            };
            districtViewModel.Id = district.Id;
            DistrictViewModel.ToManageModel(districtViewModel, ref district);
            return View(districtViewModel);
        }

        [HttpPost]
        public ActionResult Edit(DistrictViewModel districtViewModel)
        {
            TempData["CurrentMenu"] = "DistrictAdmin";
            if (ModelState.IsValid)
            {
                var district = unitOfWork.RegionRepository.FindUnique(d => d.Id == districtViewModel.Id);

                district.Parent = unitOfWork.RegionRepository.FindById(districtViewModel.ParentSelectedValue);

                DistrictViewModel.ToEntity(districtViewModel, ref district);
                unitOfWork.Save();

                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "<strong>{0}</strong> đã được cập nhật thành công."
                + "</div>", district.Name);
                
                return RedirectToAction("Index", new {cityId = districtViewModel.ParentSelectedValue });
            }
            return View(districtViewModel);
        }

        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "DistrictAdmin";
            var district = unitOfWork.RegionRepository.FindUnique(d => d.Id == id);
            var cityId = district.Parent.Id;
            var name = district.Name;
            if (district != null)
            {
                unitOfWork.RegionRepository.Delete(district);
                unitOfWork.Save();

                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "<strong>{0}</strong> đã được xóa."
                + "</div>", name);

            }
        Complete:
            return RedirectToAction("Index", new {cityId = cityId });
        }


    }
}