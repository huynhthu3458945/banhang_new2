﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Data;
using MainProject.Web.Areas.Admin.Models.UserManage;
using MainProject.Web.BaseControllers;
using WebMatrix.WebData;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Root")]
    public class UserAdminController : BaseMembershipController
    {
        private readonly UnitOfWork unitOfWork;
      
        public UserAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult Index()
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "UserAdmin";

            var userProfiles = DbContext.UserProfiles.ToList();
            var list = (from u in userProfiles
                        select new[] { u.UserName, u.Email, Convert.ToString(u.IsActive) }).ToList();
            return View(list);
        }

        public ActionResult Create()
        {
            TempData["CurrentMenu"] = "User";
            var model = new UserManageCreateViewModel {ListRoles = Roles.GetAllRoles().ToList()};
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(UserManageCreateViewModel userManageViewModel)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var userId = MembershipProvider.GetUserId(userManageViewModel.UserName);
            if (userId < 0)
            {
                var userEmail = unitOfWork.UserProfileRepository.FindUnique(x => x.Email == userManageViewModel.Email);

                if (userEmail == null)
                {
                    if (CheckPassword(userManageViewModel.Password))
                    {
                        WebSecurity.CreateUserAndAccount(userManageViewModel.UserName, userManageViewModel.Password,
                                                         new {userManageViewModel.Email ,userManageViewModel.IsActive});

                        if (userManageViewModel.ListRolesCheck.Count > 0)
                        {
                            Roles.AddUserToRoles(userManageViewModel.UserName, userManageViewModel.ListRolesCheck.ToArray());
                            TempData["message"] = "<strong style='color: green'>Tài khoản đã được tạo và gán quyền!</strong>";
                        }
                        else
                        {
                            TempData["message"] = "<strong style='color: orange'>Tài khoản đã được tạo nhưng chưa gán quyền!</strong>";
                        }
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Mật khẩu phải chứa ít nhất 1 ký hiệu đặc biệt và 1 số, xin chọn mật khẩu khác");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Email đã có, xin chọn Email khác");
                }
            }
            else
            {
                ModelState.AddModelError("", "Tên tài khoản đã có, xin chọn tên khác");
            }
            userManageViewModel.ListRoles = Roles.GetAllRoles().ToList();
            return View(userManageViewModel);
        }

        public ActionResult CreateUser()
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var model = new UserManageCreateViewModel { ListRoles = Roles.GetAllRoles().ToList() };
            return View(model);
        }
        [HttpPost]
        public ActionResult CreateUser(UserManageCreateViewModel userManageViewModel)
        {
            TempData["CurrentMenu"] = "User";
            userManageViewModel.UserName = userManageViewModel.Email;
            var userId = MembershipProvider.GetUserId(userManageViewModel.UserName);
            if (userId < 0)
            {
                var userEmail = unitOfWork.UserProfileRepository.FindUnique(x => x.Email == userManageViewModel.Email);
                if (userEmail == null)
                {
                    if (CheckPassword(userManageViewModel.Password))
                    {
                        WebSecurity.CreateUserAndAccount(userManageViewModel.UserName, userManageViewModel.Password,
                                                         new { userManageViewModel.Email, userManageViewModel.IsActive });

                        if (userManageViewModel.ListRolesCheck.Count > 0)
                        {
                            Roles.AddUserToRoles(userManageViewModel.UserName, userManageViewModel.ListRolesCheck.ToArray());
                            TempData["message"] = "<strong style='color: green'>Tài khoản đã được tạo và gán quyền!</strong>";
                        }
                        else
                        {
                            TempData["message"] = "<strong style='color: orange'>Tài khoản đã được tạo nhưng chưa gán quyền!</strong>";
                        }
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Mật khẩu phải chứa ít nhất 1 ký hiệu đặc biệt và 1 số, xin chọn mật khẩu khác");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Email đã có, xin chọn Email khác");
                }
            }
            else
            {
                ModelState.AddModelError("", "Email không hợp lệ");
            }
            userManageViewModel.ListRoles = Roles.GetAllRoles().ToList();
            return View(userManageViewModel);
        }

        // Change Password
        public ActionResult EditPassword(string username)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var user = Membership.GetUser(username);
            if (user != null)
            {
                var userProfile =
                    DbContext.UserProfiles.FirstOrDefault(
                        c => c.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                var userItem = new UserManageEditViewModel
                {
                    Email = userProfile != null ? userProfile.Email : string.Empty,
                    UserName = user.UserName,
                    IsActive = userProfile == null || userProfile.IsActive,
                    ListRoles = Roles.GetAllRoles().ToList(),
                    ListRolesCheck = Roles.GetRolesForUser(username).ToList()
                };
                return View(userItem);
            }

            TempData["message"] = "<strong style='color: red'>Tài khoản không tồn tại!</strong>";
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult EditPassword(UserManageEditViewModel userItem)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var currentUser = Membership.GetUser(userItem.UserName);
            if (currentUser != null)
            {
                if (userItem.OldPassword != null)
                {
                    if (userItem.NewPassword != null)
                    {
                        if (CheckPassword(userItem.NewPassword))
                        {
                            if (currentUser.ChangePassword(userItem.OldPassword, userItem.NewPassword))
                            {
                                TempData["message"] = "<strong style='color: green'>Đổi mật khẩu thành công.</strong>";
                                return View(userItem);
                            }
                            else
                            {
                                TempData["valOldPassword"] = "<strong style='color: red'>Mật khẩu cũ không đúng</strong>";
                                return View(userItem);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Mật khẩu phải chứa ít nhất 1 ký hiệu đặc biệt và 1 số, xin chọn mật khẩu khác");
                            return View(userItem);
                        }
                    }
                    else
                    {
                        TempData["valNewPassword"] = "<strong style='color: red'>Mật khẩu mới không được bỏ trống</strong>";
                        TempData["valRePassword"] = "<strong style='color: red'>Mật khẩu nhập lại không được bỏ trống</strong>";
                        return View(userItem);
                    }
                }
                else
                {
                    TempData["valOldPassword"] = "<strong style='color: red'>Vui lòng nhập mật khẩu cũ</strong>";
                    return View(userItem);
                }
                return RedirectToAction("Index");
            }

            TempData["message"] = "<strong style='color: red'>Tài khoản không tồn tại! Cập nhật thất bại</strong>";
            return RedirectToAction("Index");
        }

        //Change Email
        public ActionResult EditEmail(string username)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var user = Membership.GetUser(username);
            if (user != null)
            {
                var userProfile =
                    DbContext.UserProfiles.FirstOrDefault(
                        c => c.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                var userItem = new UserManageEditViewModel
                {
                    Email = userProfile != null ? userProfile.Email : string.Empty,
                    UserName = user.UserName,
                    IsActive = userProfile == null || userProfile.IsActive,
                    ListRoles = Roles.GetAllRoles().ToList(),
                    ListRolesCheck = Roles.GetRolesForUser(username).ToList()
                };
                return View(userItem);
            }

            TempData["message"] = "<strong style='color: red'>Tài khoản không tồn tại!</strong>";
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult EditEmail(UserManageEditViewModel userItem)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var currentUser = Membership.GetUser(userItem.UserName);
            if (currentUser != null)
            {
                
                var usermodel = unitOfWork.UserProfileRepository.FindById(Convert.ToInt32(currentUser.ProviderUserKey));
                //var userEmail = _userInfoRepository.FindAll().FirstOrDefault(d => d.Email != userItem.Email && d.UserName == userItem.UserName);
                var userEmail = unitOfWork.UserProfileRepository.FindAll().FirstOrDefault(d => d.Email == userItem.Email && d.UserName != userItem.UserName);
                if (userEmail != null)
                {
                    ModelState.AddModelError("", "Email này đã trùng với 1 user khác, nhập email khác");
                    return View(userItem);
                }
                else
                {
                    usermodel.Email = userItem.Email;
                }
                unitOfWork.Save();
                TempData["message"] = "<strong style='color: green'>Đổi email thành công.</strong>";
                return View(userItem);
                //return RedirectToAction("Index");
            }

            TempData["message"] = "<strong style='color: red'>Tài khoản không tồn tại! Cập nhật thất bại</strong>";
            return RedirectToAction("Index");
        }

        //Change Role
        public ActionResult EditRole(string username)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var user = Membership.GetUser(username);
            if (user != null)
            {
                var userProfile =
                    DbContext.UserProfiles.FirstOrDefault(
                        c => c.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                var userItem = new UserManageEditViewModel
                {
                    Email = userProfile != null ? userProfile.Email : string.Empty,
                    UserName = user.UserName,
                    IsActive = userProfile == null || userProfile.IsActive,
                    ListRoles = Roles.GetAllRoles().ToList(),
                    ListRolesCheck = Roles.GetRolesForUser(username).ToList()
                };
                return View(userItem);
            }

            TempData["message"] = "<strong style='color: red'>Tài khoản không tồn tại!</strong>";
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult EditRole(UserManageEditViewModel userItem)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var currentUser = Membership.GetUser(userItem.UserName);
            var usermodel = unitOfWork.UserProfileRepository.FindById(Convert.ToInt32(currentUser.ProviderUserKey));
            userItem.ListRoles = Roles.GetAllRoles().ToList();
            if (currentUser != null)
            {

                var arrayRoles = Roles.GetRolesForUser(userItem.UserName);
                if (arrayRoles != null && arrayRoles.Any())
                {
                    Roles.RemoveUserFromRoles(userItem.UserName, arrayRoles);
                }
                Roles.AddUserToRoles(userItem.UserName, userItem.ListRolesCheck.ToArray());
                usermodel.IsActive = userItem.IsActive;
                unitOfWork.Save();
                TempData["message"] = "<strong style='color: green'>Đổi quyền truy cập thành công.</strong>";
                return View(userItem);
            }

            TempData["message"] = "<strong style='color: red'>Tài khoản không tồn tại! Cập nhật thất bại</strong>";
            return RedirectToAction("Index");
        }
        public ActionResult Edit(string username)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            var user = Membership.GetUser(username);
            if (user != null)
            {
                var userProfile =
                    DbContext.UserProfiles.FirstOrDefault(
                        c => c.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                var userItem = new UserManageEditViewModel
                                   {
                                       Email = userProfile != null ? userProfile.Email : string.Empty,
                                       UserName = user.UserName,
                                       IsActive = userProfile == null || userProfile.IsActive,
                                       ListRoles = Roles.GetAllRoles().ToList(),
                                       ListRolesCheck = Roles.GetRolesForUser(username).ToList()
                                   };
                return View(userItem);
            }

            TempData["message"] = "<strong style='color: red'>Tài khoản không tồn tại!</strong>";
            return RedirectToAction("Index");
        }
        public ActionResult Delete(string username)
        {
            TempData["CurrentMenu"] = "UserAdmin";
            if(User.Identity.Name.Equals(username))
            {
                TempData["message"] = "<strong style='color:red'>Bạn không thể xóa tài khoản của bạn khi đang đăng nhập</strong>";
                return RedirectToAction("Index");
            }
            var currentUser = Membership.GetUser(username);
            if(currentUser!=null)
            {
                var item = unitOfWork.PermissionRefRepository.FindUnique(c => c.UserName.ToLower() == username.ToLower());
                if (item != null)
                {
                    unitOfWork.PermissionRefRepository.Delete(item);
                    unitOfWork.Save();
                }
                if (Roles.GetRolesForUser(username).Count() > 0)
                {
                    Roles.RemoveUserFromRoles(username, Roles.GetRolesForUser(username));
                }
                MembershipProvider.DeleteAccount(username);
                MembershipProvider.DeleteUser(username,true);
            }
            else
            {
                TempData["message"] = "<strong style='color:red'>Không tìm thấy user cần xóa</strong>";
            }
            TempData["message"] = string.Format("<strong style='color:green'>{0} đã được xóa</strong>",username);
            return RedirectToAction("Index");
        }

        public ActionResult ForgetPass()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgetPass(string _string)
        {
            return View();
        }

        public static bool CheckPassword(string password)
        {
            if(CheckNumberCharacterPassword(password)&&CheckSpecialCharacterPassword(password))
            {
                return true;
            }
            return false;
        }

        public static bool CheckSpecialCharacterPassword(string password)
        {
            var specialCharacter = new string[8];
            specialCharacter[0] = "!";
            specialCharacter[1] = "@";
            specialCharacter[2] = "#";
            specialCharacter[3] = "$";
            specialCharacter[4] = "%";
            specialCharacter[5] = "^";
            specialCharacter[6] = "&";
            specialCharacter[7] = "*";
            for (int i = 0; i < specialCharacter.Length; i++)
            {
                if (password.Contains(specialCharacter[i]))
                    return true;
            }
            return false;
        }

        public static bool CheckNumberCharacterPassword(string password)
        {
            var numberCharacter = new string[10];
            numberCharacter[0] = "1";
            numberCharacter[1] = "2";
            numberCharacter[2] = "3";
            numberCharacter[3] = "4";
            numberCharacter[4] = "5";
            numberCharacter[5] = "6";
            numberCharacter[6] = "7";
            numberCharacter[7] = "8";
            numberCharacter[8] = "9";
            numberCharacter[9] = "0";
            for (int i = 0; i < numberCharacter.Length; i++)
            {
                if (password.Contains(numberCharacter[i]))
                    return true;
            }
            return false;
        }

        public PartialViewResult GetAllRolesOfUser(string username)
        {
            var roles = Roles.GetRolesForUser(username);
            string rolesString="";
            if(roles!=null)
            {
                for (int i = 0; i < roles.Count(); i++)
                {
                    rolesString += roles[i].ToString() + ",";
                }
                if(!string.IsNullOrWhiteSpace(rolesString))
                {
                    TempData["rolesString"] = rolesString.Substring(0, rolesString.Length - 1);
                }
            }
            return PartialView();
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
