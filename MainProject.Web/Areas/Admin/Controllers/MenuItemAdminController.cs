﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.MenuItem;
using MainProject.Web.BaseControllers;
using System.IO;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class MenuItemAdminController : BaseController
    {
        private UnitOfWork unitOfWork;

        private string CurrentUser
        {
            get { return HttpContext.User.Identity.Name; }
        }
        private int PageItems = ConfigItemHelper.GetItemsCountPerPage();

        public MenuItemAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("MenuItemAdmin", "View")]
        public ActionResult Index(int languageId = 0, int menuId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "MenuItemAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "MenuItemAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "MenuItemAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "MenuItemAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            if (languageId == 0) languageId = unitOfWork.LanguageRepository.FindAll().FirstOrDefault().Id;
            if (page < 1) page = 1;
            var sql = unitOfWork.MenuItemRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            if (menuId != 0)
            {
                sql = sql.Where(x => x.Menu.Id == menuId);
            }
            var count = sql.Count();
            var menuItems = sql.OrderByDescending(x => x.Id).Skip((page - 1) * PageItems).Take(PageItems).ToList();

            var menuItemsAndLanguageVersions = new List<EntityWithLanguageVersionsModel<MenuItem>>();
            foreach (var a in menuItems)
            {
                var languageVersions =
                    unitOfWork.MenuItemRepository.Find(x => x.OriginalValue == a.OriginalValue && x.Language.Id != a.Language.Id).
                        ToList();
                menuItemsAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<MenuItem>() { Entity = a, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<MenuItem>>()
            {
                ListItems = menuItemsAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId)
                    },
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = MenuHelper.BuildSelectListItemsMenuItem(unitOfWork.MenuRepository, menuId)
                    },
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/MenuItemAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, PageItems, page, "href='/Admin/MenuItemAdmin/Index?languageId=" + languageId + "&menuId=" + menuId + "&page={0}'")

            };
            model.FilterViewModel.FatherSelectModel.Fathers.Insert(0, new SelectListItem() { Text = "Tất cả", Value = "0", Selected = menuId == 0 });
            return View(model);
        }

        // id !=0 -> add language version for article 
        [CustomViewAuthorize("MenuItemAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "MenuItemAdmin";
            var menuItemOriginal = id == 0 ? null : unitOfWork.MenuItemRepository.FindUnique(x => x.Id == id);
            var originalValue = menuItemOriginal != null ? menuItemOriginal.OriginalValue : Guid.NewGuid();
            if (menuItemOriginal == null)
            {
                while (unitOfWork.MenuItemRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }

            var menuItemViewModel = new MenuItemViewModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<MenuItem>(originalValue, unitOfWork),
                                                            unitOfWork.LanguageRepository.FindAll().FirstOrDefault().Id),
                OriginalValue = originalValue,
                Menus = MenuHelper.BuildSelectListItemsMenuItem(unitOfWork.MenuRepository, 0),
                Parents = MenuItemHelper.BuildSelectListItemsByLanguageMenu(unitOfWork.MenuItemRepository, -1, -1),
                LinkTargetTypes = EnumHelper.ToSelectList(typeof(LinkTargetTypeCollection)),
            };

            if (menuItemOriginal != null)
            {
                menuItemViewModel.Order = menuItemOriginal.Order;
            }
            menuItemViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Chọn ngôn ngữ",
                Value = ""
            });
            menuItemViewModel.Menus.Insert(0, new SelectListItem
            {
                Text = "Chọn loại menu",
                Value = ""
            });

            try
            {
                unitOfWork.Save();
            }
            catch
            {
                return RedirectToAction("Index");
            }
            return View(menuItemViewModel);
        }

        [HttpPost]
        public ActionResult Create(MenuItemViewModel menuItemViewModel)
        {
            TempData["CurrentMenu"] = "MenuItemAdmin";
            if (ModelState.IsValid)
            {
                var menuItem = new MenuItem()
                {
                    Parent = unitOfWork.MenuItemRepository.FindById(menuItemViewModel.ParentSelectedValue),
                    Language = unitOfWork.LanguageRepository.FindById(menuItemViewModel.LanguageSelectedValue),
                    Menu = unitOfWork.MenuRepository.FindById(menuItemViewModel.MenuSelectedValue)
                };
                MenuItemViewModel.ToEntity(menuItemViewModel, ref menuItem);
                try
                {
                    unitOfWork.MenuItemRepository.Insert(menuItem);
                    unitOfWork.Save();
                    TempData["message"] = string.Format("<strong style='color:green'>Thêm thành công!</strong>");
                    return RedirectToAction("Index", new { languageId = menuItem.Language.Id, menuId = menuItem.Menu.Id });
                }
                catch (Exception ex)
                {
                    TempData["message"] = string.Format("<strong style='color:green'>Lỗi xảy ra!</strong>");
                    return RedirectToAction("Index");
                }

            }
            menuItemViewModel.Menus = MenuHelper.BuildSelectListItemsMenuItem(unitOfWork.MenuRepository, menuItemViewModel.MenuSelectedValue);
            menuItemViewModel.Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<MenuItem>(menuItemViewModel.OriginalValue, unitOfWork), menuItemViewModel.LanguageSelectedValue);
            menuItemViewModel.Parents = MenuItemHelper.BuildSelectListItemsByLanguageMenu(unitOfWork.MenuItemRepository, menuItemViewModel.LanguageSelectedValue, menuItemViewModel.MenuSelectedValue);
            return View(menuItemViewModel);
        }

        [CustomViewAuthorize("MenuItemAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "MenuItemAdmin";
            var article = unitOfWork.MenuItemRepository.FindUnique(x => x.Id == id);
            if (article == null) return RedirectToAction("Index");

            var model = new MenuItemViewModel();
            MenuItemViewModel.ToModel(model, ref article);
            model.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(),
                                                           article.Language.Id);
            model.OriginalValue = article.OriginalValue;
            model.Menus = MenuHelper.BuildSelectListItemsMenuItem(unitOfWork.MenuRepository, 0);
            model.Parents = MenuItemHelper.BuildSelectListItemsByLanguageMenu(unitOfWork.MenuItemRepository, -1, -1);
            model.LinkTargetTypes = EnumHelper.ToSelectList(typeof(LinkTargetTypeCollection));
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(MenuItemViewModel model)
        {
            TempData["CurrentMenu"] = "MenuItemAdmin";
            var menuItem = unitOfWork.MenuItemRepository.FindUnique(x => x.Id == model.Id);
            if (ModelState.IsValid)
            {
                if (menuItem == null) return RedirectToAction("Index");
                MenuItemViewModel.ToEntity(model, ref menuItem);
                try
                {
                    unitOfWork.MenuItemRepository.Update(menuItem);
                    unitOfWork.Save();
                    TempData["message"] = string.Format("<strong style='color:green'>Cập nhật thành công!</strong>");
                }
                catch (Exception ex)
                {
                    TempData["message"] = string.Format("<strong style='color:red'>Lỗi xảy ra! {0}</strong>", ex.Message);
                }
                return RedirectToAction("Index", new { languageId = menuItem.Language.Id });
            }
            return View(model);
        }

        [CustomViewAuthorize("MenuItemAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "MenuItemAdmin";
            var article = unitOfWork.MenuItemRepository.FindUnique(x => x.Id == id);
            if (article == null) return RedirectToAction("Index");
            SeoHelper.DeleteUrlData(EntityTypeCollection.Articles, article.Id);
            unitOfWork.MenuItemRepository.Delete(id);
            unitOfWork.Save();
            try
            {
                ////delete images folder
                //if (Directory.Exists(Server.MapPath(article.ImageFolder)))
                //{
                //    try
                //    {
                //        Directory.Delete(Server.MapPath(article.ImageFolder), true);
                //    }
                //    catch (Exception ex) { }
                //}
                TempData["message"] = string.Format("<strong style='color:green'>Xóa thành công!</strong>");
            }
            catch (Exception ex)
            {
                TempData["message"] = string.Format("<strong style='color:red'>Xóa không thành công, hãy thử lại!</strong>");
            }
            return RedirectToAction("Index");
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
