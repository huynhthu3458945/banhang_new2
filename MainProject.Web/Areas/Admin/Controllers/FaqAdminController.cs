﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;
using MainProject.Web.Areas.Admin.Models.Faq;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class FaqAdminController : BaseController
    {
        //
        // GET: /Admin/SolutionAdmin/
        private readonly UnitOfWork unitOfWork;
        private readonly int itemInPerPage;

        public FaqAdminController()
        {
            unitOfWork = new UnitOfWork();
            itemInPerPage = 10;
        }

        [CustomViewAuthorize("FaqAdmin", "View")]
        public ActionResult Index(int languageId = 0, int page = 1)
        {
            TempData["CurrentMenu"] = "FaqAdmin";
            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "FaqAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "FaqAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "FaqAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);


            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            var sql = unitOfWork.FaqRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            var count = sql.Count();
            var faqs = sql.OrderByDescending(x => x.Id).Skip((page - 1) * itemInPerPage).Take(itemInPerPage).ToList();
            var faqsAndLanguageVersions = new List<EntityWithLanguageVersionsModel<Faq>>();
            foreach (var a in faqs)
            {
                var languageVersions =
                    unitOfWork.FaqRepository.Find(x => x.OriginalValue == a.OriginalValue && x.Language.Id != a.Language.Id).
                        ToList();
                faqsAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<Faq>() { Entity = a, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<Faq>>()
            {
                ListItems = faqsAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = languageId
                    },
                    BaseUrl = "/Admin/FaqAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, itemInPerPage, page, "href='/Admin/FaqAdmin/Index?languageId=" + languageId + "&page={0}'")

            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId);
            return View(model);
        }

        [CustomViewAuthorize("FaqAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "FaqAdmin";
            var faqOriginal = id == 0 ? null : unitOfWork.FaqRepository.FindUnique(x => x.Id == id);
            var originalValue = faqOriginal != null ? faqOriginal.OriginalValue : Guid.NewGuid();
            if (faqOriginal == null)
            {
                while (unitOfWork.FaqRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }
            var solutionViewModel = new FaqViewModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<Solution>(originalValue, unitOfWork), 0),
                OriginalValue = originalValue
            };

            if (faqOriginal != null)
            {
                solutionViewModel.Order = faqOriginal.Order;
            }
            solutionViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(solutionViewModel);
        }
        [HttpPost]
        public ActionResult Create(FaqViewModel faqViewModel)
        {
            TempData["CurrentMenu"] = "FaqAdmin";
            if (ModelState.IsValid)
            {
                var faq = new Faq()
                {
                    Language = unitOfWork.LanguageRepository.FindById(faqViewModel.LanguageSelectedValue)
                };
                FaqViewModel.ToEntity(faqViewModel, ref faq);
                unitOfWork.FaqRepository.Insert(faq);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Câu hỏi <strong>{0}</strong> đã được thêm thành công!</span>", faqViewModel.Question);
                return RedirectToAction("Index", new { languageId = faq.Language.Id });
            }
            faqViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<Solution>(faqViewModel.OriginalValue, unitOfWork), 0);
            faqViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(faqViewModel);
        }

        [CustomViewAuthorize("FaqAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "FaqAdmin";
            var faq = unitOfWork.FaqRepository.FindUnique(d => d.Id == id);
            if (faq == null)
            {
                TempData["message"] = string.Format("Không tìm thấy câu hỏi cần sửa");
                return RedirectToAction("Index");
            }
            var faqViewModel = new FaqViewModel
            {
                LanguageSelectedValue = faq.Language.Id,
                Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), faq.Language.Id),
            };
            faqViewModel.Id = faq.Id;
            FaqViewModel.ToModel(faqViewModel, ref faq);
            return View(faqViewModel);
        }
        [HttpPost]
        public ActionResult Edit(FaqViewModel faqViewModel)
        {
            TempData["CurrentMenu"] = "FaqAdmin";
            if (ModelState.IsValid)
            {
                var faq = unitOfWork.FaqRepository.FindUnique(d => d.Id == faqViewModel.Id);

                faq.Language = unitOfWork.LanguageRepository.FindById(faqViewModel.LanguageSelectedValue);
                FaqViewModel.ToEntity(faqViewModel, ref faq);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Câu hỏi {0} đã cập nhật thành công</span>",
                                                    faq.Question);
                return RedirectToAction("Index", new { languageId = faqViewModel.LanguageSelectedValue });
            }
            faqViewModel.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<Solution>(faqViewModel.OriginalValue, unitOfWork), 0);
            faqViewModel.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(faqViewModel);
        }

        [CustomViewAuthorize("FaqAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "FaqAdmin";
            var solution = unitOfWork.FaqRepository.FindUnique(p => p.Id == id);
            if (solution == null)
            {
                TempData["message"] = string.Format("<strong style='color:red'>câu hỏi không tồn tại</strong>");
                return RedirectToAction("Index");
            }
            var title = solution.Question;
            var languageId = solution.Language.Id;
            unitOfWork.FaqRepository.Delete(solution);
            unitOfWork.Save();
            TempData["message"] = string.Format("<strong style='color:green'>{0}  Đã xóa</strong>", title);
            return RedirectToAction("Index", new { languageId = languageId });

        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
