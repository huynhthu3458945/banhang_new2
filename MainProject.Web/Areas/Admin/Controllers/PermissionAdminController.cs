﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.ActionFilters;
using MainProject.Framework.Constant;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models.Permission;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [FilterPermission(EntityManageType = EntityManageTypeCollection.ManagePermissions)]
    public class PermissionAdminController : BaseMembershipController
    {
        private UnitOfWork unitOfWork;

        public PermissionAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        //public ActionResult Index()
        //{
        //    var model = new AddUserPermissionModel
        //                    {
        //                        Parents = new List<SelectListItem>()
        //                    };
        //    model.LanguageSelectedValue = int.Parse(model.Languages.First().Value);
        //    model.UserNames = new List<SelectListItem>();

        //    var userNames = Roles.GetUsersInRole(RoleName.Mod);
        //    foreach (var userName in userNames)
        //    {
        //        model.UserNames.Add(new SelectListItem
        //                                {
        //                                    Text = userName,
        //                                    Value = userName
        //                                });
        //    }

        //    return View(model);
        //}
        public ActionResult Index()
        {
            var model = new AddUserPermissionModel
            {
                Parents = new List<SelectListItem>()
            };
            model.LanguageSelectedValue = int.Parse(model.Languages.First().Value);
            model.UserNames = new List<SelectListItem>();

            var userNames = Roles.GetUsersInRole(RoleName.RootUser);
            foreach (var userName in userNames)
            {
                model.UserNames.Add(new SelectListItem
                {
                    Text = userName,
                    Value = userName
                });
            }

            return View(model);
        }

        public ActionResult GetItems()
        {
            var model = new List<UserPermissionItemModel>();
            var items = unitOfWork.PermissionRefRepository.FindAll().OrderBy(c => c.UserName).ToList();
            foreach (var permissionRef in items)
            {
                var modelItem = new UserPermissionItemModel
                                    {
                                        UserName = permissionRef.UserName,
                                        IsEditor = permissionRef.IsEditor,
                                        IsManager = permissionRef.IsManager,
                                        EntityId = permissionRef.EntityId,
                                        FeatureName =
                                            permissionRef.EntityId > 0
                                                ? (unitOfWork.CategoryRepository.FindById(permissionRef.EntityId) != null
                                                       ? unitOfWork.CategoryRepository.FindById(permissionRef.EntityId).Title
                                                       : "")
                                                : ((EntityManageTypeCollection) permissionRef.EntityId).GetDescription()
                                    };
                
                model.Add(modelItem);
            }

            return View("~/Areas/Admin/Views/PermissionAdmin/_UserPermissionList.cshtml", model);
        }

        public JsonResult AddPermissionToUser(PermissionManageModel model)
        {
            var item =
                unitOfWork.PermissionRefRepository.FindUnique(
                    c => c.UserName.ToLower() == model.UserName.ToLower()  && c.EntityId == model.EntityTypeValue);
            
            var isSaveSuccess = false;
            if (item == null)
            {
                if (model.EntityTypeValue > 0)
                {
                    var category = unitOfWork.CategoryRepository.FindUnique(c => c.Id == model.EntityTypeValue);
                    if (category != null)
                    {
                        unitOfWork.PermissionRefRepository.Insert(new PermissionRef
                                                            {
                                                                IsEditor = false,
                                                                IsManager = false,
                                                                UserName = model.UserName,
                                                                EntityId = model.EntityTypeValue
                                                            });
                        unitOfWork.Save();
                        isSaveSuccess = true;
                    }
                }
                else
                {
                    unitOfWork.PermissionRefRepository.Insert(new PermissionRef
                                                        {
                                                            IsEditor = false,
                                                            IsManager = false,
                                                            UserName = model.UserName,
                                                            EntityId = model.EntityTypeValue
                                                        });
                    unitOfWork.Save();
                    isSaveSuccess = true;
                }
            }
            return Json(new { Result = isSaveSuccess }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdatePermissionToUser(PermissionManageModel model)
        {
            var item =
                unitOfWork.PermissionRefRepository.FindUnique(
                    c => c.UserName.ToLower() == model.UserName.ToLower() && c.EntityId == model.EntityTypeValue);

            var isSaveSuccess = false;
            if (item != null)
            {
                item.IsEditor = model.IsEditor;
                item.IsManager = model.IsManager;

                unitOfWork.Save();
                isSaveSuccess = true;
            }
            return Json(new { Result = isSaveSuccess }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemovePermissionToUser(PermissionManageModel model)
        {
            var item =
                unitOfWork.PermissionRefRepository.FindUnique(
                    c => c.UserName.ToLower() == model.UserName.ToLower() && c.EntityId == model.EntityTypeValue);

            var isSaveSuccess = false;
            if (item != null)
            {
                unitOfWork.PermissionRefRepository.Delete(item);

                unitOfWork.Save();
                isSaveSuccess = true;
            }
            return Json(new { Result = isSaveSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
