﻿using System;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.ActionFilters;
using MainProject.Web.Areas.Admin.Models.Setting;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [FilterPermission(EntityManageType = EntityManageTypeCollection.ManageSettings)]
    public class SettingAdmController : BaseController
    {
        //
        // GET: /Admin/SettingAdm/
        private UnitOfWork unitOfWork;

        public SettingAdmController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult Index()
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "Setting";
            return View(unitOfWork.SettingRepository.FindAll().ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(SettingManagerViewModel setting)
        {
            TempData["CurrentMenu"] = "Setting";
            if(ModelState.IsValid)
            {
                //setting.Key = StringHelper.GetSeName(setting.Key);
                if (SettingHelper.CheckDuplicateKey(unitOfWork.SettingRepository,setting.Key))
                {
                    var entity = new Setting {Key = setting.Key, Value = setting.Value};
                    unitOfWork.SettingRepository.Insert(entity);
                    unitOfWork.Save();
                    TempData["message"] = string.Format("<strong style='color:green'>Đã thêm đối tượng {0}</strong>", entity.Key);
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Key đã trùng, xin nhập key khác!");
                }
            }
            return View(setting);
        }

        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "Setting";
            var entity = unitOfWork.SettingRepository.FindUnique(d => d.Id == id);
            if(entity != null)
            {
                var setting = new SettingManagerViewModel {Id = entity.Id, Key = entity.Key, Value = entity.Value};
                return View(setting);
            }
            else
            {
                TempData["message"] = "<strong style='color:red'>Không tìm thấy đổi tượng cần sửa</strong>";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult Edit(SettingManagerViewModel setting)
        {
            TempData["CurrentMenu"] = "Setting";
            if(ModelState.IsValid)
            {
                if (SettingHelper.CheckDuplicateKey(unitOfWork.SettingRepository, setting.Key,setting.Id))
                {
                    var newmodel = unitOfWork.SettingRepository.FindUnique(d => d.Id == setting.Id);
                    if(newmodel!=null)
                    {
                        newmodel.Key = setting.Key;
                        newmodel.Value = setting.Value;
                        unitOfWork.Save();
                        TempData["message"] =
                            string.Format("<strong style='color:green'>Cập nhật {0} thành công</strong>", newmodel.Key);
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", "Không tìm thấy dữ liệu cần sửa");
                }
                else
                {
                    ModelState.AddModelError("", "Key đã trùng, xin nhập key khác!");
                }
            }
            return View(setting);
        }

        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "Setting";
            var setting = unitOfWork.SettingRepository.FindUnique(d => d.Id == id);
            if (setting == null)
            {
                TempData["message"] = "<strong style='color:red'>Không tìm thấy đổi tượng cần xóa</strong>";
                return RedirectToAction("Index");
            }
            unitOfWork.SettingRepository.Delete(setting.Id);
            try
            {
                TempData["message"] = "<strong style='color:green'>Đã xóa đối tượng</strong>";
                unitOfWork.Save();
            }
            catch(Exception ex)
            {
                TempData["message"] = string.Format("<strong style='color:red'>Xảy ra lỗi trong quá trình xóa. Xin vui lòng thử lại!</strong>");
            }
            return RedirectToAction("Index");
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
