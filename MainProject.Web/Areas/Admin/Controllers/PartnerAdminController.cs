﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Partner;
using MainProject.Web.BaseControllers;


namespace MainProject.Web.Areas.Admin.Controllers
{
    public class PartnerAdminController : BaseController
    {
        //
        // GET: /Admin/PartnerAdmin/
        private readonly UnitOfWork unitOfWork;
        private readonly int PageItems = 20;
        public PartnerAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("PartnerAdmin", "View")]
        public ActionResult Index(int page = 1, PartnerTypeCollection type = PartnerTypeCollection.Partner)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "PartnerAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "PartnerAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "PartnerAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "PartnerAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);



            var sql = unitOfWork.PartnerRepository.FindAll();
            if (type != 0)
            {
                sql = sql.Where(x => x.Type == type);
            }
            var count = sql.Count();
            var partners = sql.OrderByDescending(p => p.Id).Skip((page - 1) * PageItems).Take(PageItems).ToList();
            var indexViewModel = new IndexViewModel<Partner>()
            {
                ListItems = partners,
                PagingViewModel = new PagingModel(count, PageItems, page, "href='/Admin/PartnerAdmin/Index?page={0}'"),
                FilterViewModel = new FilterViewModel()
                {
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = EnumHelper.ToSelectList(typeof(PartnerTypeCollection)),
                        FatherSelectedStringValue = type.ToString()
                    },
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/PartnerAdmin/Index?"
                }
            };
            return View(indexViewModel);
        }

        [CustomViewAuthorize("PartnerAdmin", "Create")]
        public ActionResult Create()
        {
            TempData["CurrentMenu"] = "PartnerAdmin";
            var partnerViewModel = new PartnerViewModel();
            var imageFolder = "/Upload/Popup/" + Guid.NewGuid().ToString();
            while (Directory.Exists(Server.MapPath(imageFolder)))
            {
                imageFolder = "/Upload/Popup/" + Guid.NewGuid().ToString();
            }
            partnerViewModel.ImageFolder = imageFolder;
            partnerViewModel.PartnerTypeSelecteds = EnumHelper.ToSelectList(typeof(PartnerTypeCollection));
            return View(partnerViewModel);
        }
        [HttpPost]
        public ActionResult Create(PartnerViewModel partnerViewModel)
        {
            TempData["CurrentMenu"] = "PartnerAdmin";
            if (ModelState.IsValid)
            {
                var partner = new Partner();
                PartnerViewModel.ToEntity(partnerViewModel, ref partner);
                unitOfWork.PartnerRepository.Insert(partner);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>Thành công!</strong>");
                return RedirectToAction("Index");
            }
            return View(partnerViewModel);
        }

        [CustomViewAuthorize("PartnerAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "PartnerAdmin";
            var partner = unitOfWork.PartnerRepository.FindUnique(p => p.Id == id);
            if (partner == null)
            {
                TempData["message"] = string.Format("<strong style='color:red'>Đối tác không tồn tại</strong>");
                return RedirectToAction("Index");
            }
            var partnerViewModel = new PartnerViewModel();
            PartnerViewModel.ToModel(partnerViewModel, ref partner);
            partnerViewModel.PartnerTypeSelecteds = EnumHelper.ToSelectList(typeof(PartnerTypeCollection));
            return View(partnerViewModel);
        }
        [HttpPost]
        public ActionResult Edit(PartnerViewModel partnerViewModel)
        {
            TempData["CurrentMenu"] = "PartnerAdmin";
            if (ModelState.IsValid)
            {
                var partner = unitOfWork.PartnerRepository.FindUnique(p => p.Id == partnerViewModel.Id);
                PartnerViewModel.ToEntity(partnerViewModel, ref partner);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>{0} Đã cập nhật thành công</strong>", partner.Name);
                return RedirectToAction("Index");
            }
            return View(partnerViewModel);
        }

        [CustomViewAuthorize("PartnerAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "PartnerAdmin";
            var partner = unitOfWork.PartnerRepository.FindUnique(p => p.Id == id);
            if (partner == null)
            {
                TempData["message"] = string.Format("<strong style='color:red'>Đối tác không tồn tại</strong>");
                return RedirectToAction("Index");
            }
            var partnerName = partner.Name;
            string ImageDefault = partner.ImageDefault;
            if (!string.IsNullOrWhiteSpace(ImageDefault))
            {
                if (Directory.Exists(Server.MapPath(ImageDefault)))
                {
                    System.IO.File.Delete(ImageDefault);
                }
            }
            unitOfWork.PartnerRepository.Delete(partner);
            unitOfWork.Save();
            TempData["message"] = string.Format("<strong style='color:green'>{0}  Đã xóa</strong>", partnerName);
            return RedirectToAction("Index");

        }

    }
}
