﻿using System;
using System.Web;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.DownloadFile;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;


namespace MainProject.Web.Areas.Admin.Controllers
{
    public class DownloadFileAdminController : BaseController
    {
        //
        // GET: /Admin/DownloadFileAdmin/
        private readonly UnitOfWork unitOfWork;
        private readonly int PageItems = 10;
        public DownloadFileAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("DownloadFileAdmin", "View")]
        public ActionResult Index(int page =1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "DownloadFileAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "DownloadFileAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "DownloadFileAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "DownloadFileAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);



            var sql = unitOfWork.DownloadFileRepository.FindAll();
            var count = sql.Count();
            var files = sql.OrderByDescending(p => p.Id).Skip((page - 1) * PageItems).Take(PageItems).ToList();
            var indexViewModel = new IndexViewModel<DownloadFile>()
            {
                ListItems = files,
                PagingViewModel = new PagingModel(count, PageItems, page, "href='/Admin/DownloadFileAdmin/Index?page={0}'")
            };
            return View(indexViewModel);
        }

        [CustomViewAuthorize("DownloadFileAdmin", "Create")]
        public ActionResult Create()
        {
            TempData["CurrentMenu"] = "DownloadFileAdmin";
            var downloadFileViewModel = new DownloadFileViewModel();
            var imageFolder = "/Upload/Popup/" + Guid.NewGuid().ToString();
            while (Directory.Exists(Server.MapPath(imageFolder)))
            {
                imageFolder = "/Upload/Popup/" + Guid.NewGuid().ToString();
            }
            downloadFileViewModel.ImageFolder = imageFolder;

            return View(downloadFileViewModel);
        }
        [HttpPost]
        public ActionResult Create(DownloadFileViewModel downloadFileViewModel, HttpPostedFileBase documentFile)
        {
            TempData["CurrentMenu"] = "DownloadFileAdmin";
            if (ModelState.IsValid)
            {

                if (documentFile != null)
                {
                    if (!Directory.Exists(Server.MapPath("/Upload/FileDownload")))
                    {
                        Directory.CreateDirectory(Server.MapPath("/Upload/FileDownload"));
                    }
                    //save file
                    var fileExtention = Path.GetExtension(documentFile.FileName);
                    var fileName = FileHelper.GetFileName("/Upload/FileDownload/",
                                                          Path.GetFileNameWithoutExtension(documentFile.FileName),
                                                          fileExtention);
                    var filepath = "/Upload/FileDownload/" + fileName + fileExtention;

                    try
                    {
                        documentFile.SaveAs(Server.MapPath(filepath));
                    }
                    catch
                    {
                        TempData["message"] = string.Format("<strong style='color:red'>Xảy ra lỗi upload file!</strong>");
                        return RedirectToAction("Index");
                    }
                    downloadFileViewModel.FilePath = filepath;
                }

                var downloadFile = new DownloadFile();
                DownloadFileViewModel.ToEntity(downloadFileViewModel, ref downloadFile);
                unitOfWork.DownloadFileRepository.Insert(downloadFile);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>Thành công!</strong>");
                return RedirectToAction("Index");
            }
            return View(downloadFileViewModel);
        }

        [CustomViewAuthorize("DownloadFileAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "DownloadFileAdmin";
            var downloadFile = unitOfWork.DownloadFileRepository.FindUnique(p => p.Id == id);
            if (downloadFile == null)
            {
                TempData["message"] = string.Format("<strong style='color:red'>File download không tồn tại</strong>");
                return RedirectToAction("Index");
            }
            var downloadFileViewModel = new DownloadFileViewModel();
            DownloadFileViewModel.ToModel(downloadFileViewModel, ref downloadFile);
            return View(downloadFileViewModel);
        }
        [HttpPost]
        public ActionResult Edit(DownloadFileViewModel downloadFileViewModel, HttpPostedFileBase documentFile)
        {
            TempData["CurrentMenu"] = "DownloadFileAdmin";
            if (ModelState.IsValid)
            {
                var downloadFile = unitOfWork.DownloadFileRepository.FindUnique(p => p.Id == downloadFileViewModel.Id);

                string filepath = downloadFile.FilePath;
                if (documentFile != null && documentFile.ContentLength > 0)
                {
                    if (documentFile.ContentLength > ConfigItemHelper.GetMaxFileUploadSize())
                    {
                        TempData["message"] = string.Format("<strong style='color:red'>File size vượt quá {0} Mb!</strong>", ConfigItemHelper.GetMaxFileUploadSize()/(1024 * 1024));
                        return RedirectToAction("Index");
                    }
                    if (!Directory.Exists(Server.MapPath("/Upload/FileDownloadle")))
                    {
                        Directory.CreateDirectory(Server.MapPath("/Upload/FileDownloadle"));
                    }
                    //save file
                    var fileExtention = Path.GetExtension(documentFile.FileName);
                    var fileName = FileHelper.GetFileName("/Upload/FileDownloadle/",
                                                          Path.GetFileNameWithoutExtension(documentFile.FileName),
                                                          fileExtention);
                    filepath = "/Upload/FileDownloadle/" + fileName + fileExtention;

                    try
                    {
                        documentFile.SaveAs(Server.MapPath(filepath));
                        if (System.IO.File.Exists(Server.MapPath(downloadFile.FilePath)))
                        {
                            System.IO.File.Delete(Server.MapPath(downloadFile.FilePath));
                        }
                    }
                    catch
                    {
                        TempData["message"] = string.Format("<strong style='color:red'>Xảy ra lỗi upload file!</strong>");
                        return RedirectToAction("Index");
                    }
                }
                downloadFileViewModel.FilePath = filepath;

                DownloadFileViewModel.ToEntity(downloadFileViewModel, ref downloadFile);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>{0} Đã cập nhật thành công</strong>", downloadFile.Title);
                return RedirectToAction("Index");
            }
            return View(downloadFileViewModel);
        }

        [CustomViewAuthorize("DownloadFileAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "DownloadFileAdmin";
            var downloadFile = unitOfWork.DownloadFileRepository.FindUnique(p => p.Id == id);
            if (downloadFile == null)
            {
                TempData["message"] = string.Format("<strong style='color:red'>File download không tồn tại</strong>");
                return RedirectToAction("Index");
            }
            var downloadFileTitle = downloadFile.Title;

            if (downloadFileTitle != null)
            {
                // delete image folder
                if (Directory.Exists(Server.MapPath(downloadFile.ImageFolder)))
                {
                    Directory.Delete(Server.MapPath(downloadFile.ImageFolder), true);
                }

                // delete file path
                string filePath = downloadFile.FilePath;
                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    if (Directory.Exists(Server.MapPath(filePath)))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
            }
            unitOfWork.DownloadFileRepository.Delete(downloadFile);
            unitOfWork.Save();
            TempData["message"] = string.Format("<strong style='color:green'>{0}  Đã xóa</strong>", downloadFileTitle);
            return RedirectToAction("Index");

        }

        public FileResult Download(int id)
        {
            var downloadFile = unitOfWork.DownloadFileRepository.FindUnique(x => x.Id == id);
            if (downloadFile != null)
            {
                if (System.IO.File.Exists(Server.MapPath(downloadFile.FilePath)))
                {
                    var fileName = Path.GetFileName(Server.MapPath(downloadFile.FilePath));
                    return File(Server.MapPath(downloadFile.FilePath), "application/octet-stream", fileName);
                }
            }
            return null;
        }

    }
}
