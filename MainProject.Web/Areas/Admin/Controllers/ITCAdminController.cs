﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Data;
using MainProject.Framework.Constant;
using MainProject.Web.Areas.Admin.Models.UserManage;
using MainProject.Web.BaseControllers;
using WebMatrix.WebData;
using MainProject.Framework.Helper;
using MainProject.Core.UserInfos;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class ITCAdminController : BaseController
    {
        private readonly UnitOfWork unitOfWork;

        public ITCAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult ForgetPass()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgetPass(ForgetPass forgetPass)
        {
            string password = string.Empty;
            var userProfile = new UserProfile();
            if (forgetPass.Email == null && forgetPass.UserName == null)
            {
                return View(forgetPass);
            }
            if (forgetPass.Email != null)
            {
                if (IsValidEmail(forgetPass.Email))
                {
                    userProfile = unitOfWork.UserProfileRepository.FindAll().First(d => d.Email == forgetPass.Email);
                    var user =
                        Membership.GetUser(unitOfWork.UserProfileRepository.FindAll().First(d => d.Email == forgetPass.Email).UserName);
                    if (user == null)
                    {
                        ModelState.AddModelError("UserName", "Tên đăng nhập không tồn tại!");
                        return View(forgetPass);
                    }

                    var token = WebSecurity.GeneratePasswordResetToken(user.UserName);
                    password = StringHelper.GetString();
                    WebSecurity.ResetPassword(token, password);
                }
                else
                {
                    ModelState.AddModelError("", "Email không hợp lệ!");
                    return View(forgetPass);
                }
            }
            if (forgetPass.UserName != null)
            {
                var user = Membership.GetUser(forgetPass.UserName);
                if (user == null)
                {
                    ModelState.AddModelError("UserName", "Tên đăng nhập không tồn tại!");
                    return View(forgetPass);
                }
                else
                {
                    userProfile = unitOfWork.UserProfileRepository.FindAll().First(d => d.UserName == forgetPass.UserName);
                }
                var token = WebSecurity.GeneratePasswordResetToken(user.UserName);
                password = StringHelper.GetString();
                WebSecurity.ResetPassword(token, password);
            }
            if (!string.IsNullOrEmpty(password))
            {
                var listEmail = new List<string>();
                listEmail.Add(userProfile.Email);
                MailHelper.ResetPassUserByEmail(listEmail, "Lấy lại password", password, null);
                TempData["message"] = string.Format("<span style='color:green'>Vào email để lấy mật khẩu</span>");
                return RedirectToAction("Logon", "ITCAdmin");
            }
            TempData["message"] = string.Format("<span style='color:red'>Xảy ra vấn đề với mật khẩu của bạn,nói admin cấp lại user mới</span>");
            return RedirectToAction("Logon", "ITCAdmin");
        }
        // Validate Email
        public static bool IsValidEmail(string InputEmail)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(InputEmail);
            if (match.Success)
                return true;
            else
                return false;
        }
        [AllowAnonymous]
        public ActionResult Logon()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Logon(UserLogon model, string returnUrl)
        {
            var user = unitOfWork.UserProfileRepository.FindUnique(d=>d.UserName==model.UserName);
            if (user != null)
            {
                if (user.IsActive)
                {
                    if (ModelState.IsValid &&
                        WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
                    {
                        return RedirectToLocal(returnUrl);
                    }
                    // If we got this far, something failed, redisplay form
                    ModelState.AddModelError("", "Tên tài khoản hoặc mật khẩu không đúng.");
                }
                ModelState.AddModelError("", "Tài khoản không được kích hoạt.");
            }
            ModelState.AddModelError("", "Tài khoản không tồn tại.");
            return View(model);
        }

        [AllowAnonymous]
        public string ResetPasswordForDefaultAccount(string key = "")
        {
            if (string.IsNullOrEmpty(key))
            {
                var str = DateTime.Now.ToFileTimeUtc().ToString();
                var strKey = "*" + str.Substring(str.Length - 4) + "!";
                Session["resetKey"] = strKey;

                return str;
            }
            var value = Session["resetKey"];
            if (value != null)
            {
                if (key.Equals((string)value))
                {
                    var user = Membership.GetUser(StringConstant.DefaultAdministrator);
                    if (user == null)
                    {
                        WebSecurity.CreateUserAndAccount(StringConstant.DefaultAdministrator, "@123456",
                                                         new {Email = "admin@web4g.com", IsActive = true});
                        Roles.AddUserToRole(StringConstant.DefaultAdministrator, RoleName.RootUser);

                        return "created!";
                    }
                    else
                    {
                        if (!Roles.IsUserInRole(StringConstant.DefaultAdministrator, RoleName.RootUser))
                        {
                            Roles.AddUserToRole(StringConstant.DefaultAdministrator, RoleName.RootUser);
                            return "assigned!";
                        }
                        else
                        {
                            var token = WebSecurity.GeneratePasswordResetToken(StringConstant.DefaultAdministrator);
                            WebSecurity.ResetPassword(token, "@123456");
                            return "finished!";
                        }
                    }
                }
            }
            return "nothing";
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "ITCAdmin");
            }
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Logon", "ITCAdmin", new { area = "Admin" });
        }

        public ActionResult Ckfinder()
        {
            return View();
        }

        public ActionResult CkfinderTemp()
        {
            return View();
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
