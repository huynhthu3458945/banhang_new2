﻿using System.Linq;
using System.Web.Mvc;
using MainProject.Core.UserInfos;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.ContentGroups;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Root")]
    public class ContentGroupAdminController : BaseController
    {
        //
        // GET: /Admin/ContentGroupAdmin/

        private readonly UnitOfWork unitOfWork;

        private readonly int _itemInPerPage = 30;

        public ContentGroupAdminController()
        {
            unitOfWork = new UnitOfWork();
        }
        
        public ActionResult Index(int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "ContentGroupAdmin";

            if (page < 1) page = 1;
            var sql = unitOfWork.ContentGroupRepository.FindAll().ToList();
            var count = sql.Count();
            var groups = sql.OrderBy(x => x.Order).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            var model = new IndexViewModel<ContentGroup>()
            {
                ListItems = groups,
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/ContentGroupAdmin/Index?page={0}'")

            };

            return View(model);
        }
        
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "ContentGroupAdmin";
            var model = new ContentGroupManageModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ContentGroupManageModel model)
        {
            TempData["CurrentMenu"] = "ContentGroupAdmin";
            if (ModelState.IsValid)
            {
                var group = new ContentGroup();
                ContentGroupManageModel.ToEntity(model, ref group);
                unitOfWork.ContentGroupRepository.Insert(group);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>Thành công!</strong>");
                return RedirectToAction("Index");
            }
            return View(model);
        }
        
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "ContentGroupAdmin";
            var group = unitOfWork.ContentGroupRepository.FindUnique(d => d.Id == id);
            if (group == null)
            {
                TempData["message"] = string.Format("Không tìm thấy nhóm nội dung cần sửa");
                return RedirectToAction("Index");
            }
            var model = new ContentGroupManageModel();
            model.Id = group.Id;
            ContentGroupManageModel.ToModel(model, ref group);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ContentGroupManageModel model)
        {
            TempData["CurrentMenu"] = "ContentGroupAdmin";
            if (ModelState.IsValid)
            {
                var group = unitOfWork.ContentGroupRepository.FindUnique(d => d.Id == model.Id);

                ContentGroupManageModel.ToEntity(model, ref group);
                unitOfWork.Save();

                TempData["message"] = string.Format("<span style='color:green'>{0} đã cập nhật thành công</span>",
                                                    group.GroupName);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "ContentGroupAdmin";
            var group = unitOfWork.ContentGroupRepository.FindUnique(d => d.Id == id);
            if (group != null)
            {
                var groupName = group.GroupName;
                unitOfWork.ContentGroupRepository.Delete(group);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được xóa</span>", groupName);
            }
            return RedirectToAction("Index");
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
