﻿using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.ActionFilters;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models.StringResource;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [FilterPermission(EntityManageType = EntityManageTypeCollection.ManageResource)]
    public class ResourceAdminController : BaseController
    {
        private readonly UnitOfWork unitOfWork;

        private int PageItems
        {
            get { return ConfigItemHelper.GetItemsCountPerPage(); }
        }

        public ResourceAdminController()
        {
            unitOfWork = new UnitOfWork();
            
        }

        public ActionResult Index(int languageId = 0, string filter=null,int page = 1)
        {
            TempData["CurrentMenu"] = "Resource";
            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            var sql = unitOfWork.StringResourceValueRepository.FindAll().Where(d => d.Language.Id == languageId);
            if (!string.IsNullOrWhiteSpace(filter))
            {
                sql = sql.Where(d => d.Value.Contains(filter.Trim()));
            }
            int count = sql.Count();
            var model = new StringResourceViewModel2
                            {
                                Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId),
                                LanguageSelectedValue = languageId,
                                ListStringResource = sql.OrderByDescending(x => x.Id).Skip((page - 1) * PageItems).Take(PageItems).ToList(),
                                PagingViewModel = new PagingModel(count, PageItems, page, "href='/Admin/ResourceAdmin/Index?page={0}'"),
                                Filter = filter
                            };
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "Resource";
            var resource = unitOfWork.StringResourceValueRepository.FindUnique(x => x.Id == id);
            if (resource != null)
            {
                return View(resource);
            }
            TempData["message"] = string.Format("<span style='color:red'>Không tìm thấy đối tượng cần sửa</span>");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(StringResourceValue stringResourceValue)
        {
            TempData["CurrentMenu"] = "Resource";
            if(ModelState.IsValid)
            {
                var model = unitOfWork.StringResourceValueRepository.FindUnique(d => d.Id == stringResourceValue.Id);
                model.Value = stringResourceValue.Value;
                ResourceHelper.UpdateResourceOnCache(model.Key.Name, model.Language.LanguageKey, model.Value);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được cập nhật</span>",stringResourceValue.Key.Name);
                return RedirectToAction("Index", new { languageId = model.Language.Id });
            }
            return View(stringResourceValue);
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
