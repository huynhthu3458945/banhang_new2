﻿using System.Linq;
using System.Web.Mvc;
using MainProject.Core.UserInfos;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.ContentBehaviors;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Root")]
    public class ContentBehaviorAdminController : BaseController
    {
        //
        // GET: /Admin/ContentBehaviorAdmin/

        private readonly UnitOfWork unitOfWork;

        private readonly int _itemInPerPage = 30;

        public ContentBehaviorAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult Index(long contentId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "ContentBehaviorAdmin";

            if (page < 1) page = 1;
            var sql = unitOfWork.ContentBehaviorRepository.FindAll();
            if (contentId != 0)
            {
                sql = sql.Where(x => x.Content.Id == contentId);
            }
            var count = sql.Count();
            var contentBehaviors = sql.OrderByDescending(x => x.Id).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            var model = new IndexViewModel<ContentBehavior>()
            {
                ListItems = contentBehaviors,
                FilterViewModel = new FilterViewModel()
                {
                    FatherSelectModel = new FatherSelectModel()
                    {
                        Fathers = ContentHelper.BindSelectListItemContentBehavior(unitOfWork.ContentRepository, contentId),
                        FatherSelectedValue = contentId
                    },
                    HasFatherSelect = true,
                    BaseUrl = "/Admin/ContentBehaviorAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/ContentBehaviorAdmin/Index?contentId=" + contentId + "&page={0}'")

            };

            return View(model);
        }
        
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "ContentBehaviorAdmin";
            var model = new ContentBehaviorManageModel
            {
                Contents = ContentHelper.BindSelectListItemContentBehavior(unitOfWork.ContentRepository, unitOfWork.ContentRepository.FindAll().FirstOrDefault().Id),
                Behaviors = BehaviorHelper.BindSelectListItemContentBehavior(unitOfWork.BehaviorRepository, unitOfWork.BehaviorRepository.FindAll().FirstOrDefault().Id)
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ContentBehaviorManageModel model)
        {
            TempData["CurrentMenu"] = "ContentBehaviorAdmin";
            if (ModelState.IsValid)
            {
                var cb = new ContentBehavior()
                {
                    Content = unitOfWork.ContentRepository.FindById(model.ContentSelectedValue),
                    Behavior = unitOfWork.BehaviorRepository.FindById(model.BehaviorSelectedValue)
                };
                unitOfWork.ContentBehaviorRepository.Insert(cb);
                unitOfWork.Save();
                TempData["message"] = string.Format("<strong style='color:green'>Thành công!</strong>");
                return RedirectToAction("Index", new { contentId = cb.Content.Id });
            }
            model.Contents = ContentHelper.BindSelectListItemContentBehavior(unitOfWork.ContentRepository, model.ContentSelectedValue);
            model.Behaviors = BehaviorHelper.BindSelectListItemContentBehavior(unitOfWork.BehaviorRepository, model.BehaviorSelectedValue);
            return View(model);
        }
        
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "ContentBehaviorAdmin";
            var cb = unitOfWork.ContentBehaviorRepository.FindUnique(d => d.Id == id);
            if (cb == null)
            {
                TempData["message"] = string.Format("Không tìm thấy đối tượng cần sửa");
                return RedirectToAction("Index");
            }
            var model = new ContentBehaviorManageModel()
            {
                ContentSelectedValue = cb.Content.Id,
                Contents = ContentHelper.BindSelectListItemContentBehavior(unitOfWork.ContentRepository, cb.Content.Id),
                BehaviorSelectedValue = cb.Behavior.Id,
                Behaviors = BehaviorHelper.BindSelectListItemContentBehavior(unitOfWork.BehaviorRepository, cb.Behavior.Id)
            };
            model.Id = cb.Id;
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ContentBehaviorManageModel model)
        {
            TempData["CurrentMenu"] = "ContentBehaviorAdmin";
            if (ModelState.IsValid)
            {
                var cb = unitOfWork.ContentBehaviorRepository.FindUnique(d => d.Id == model.Id);

                cb.Content = unitOfWork.ContentRepository.FindById(model.ContentSelectedValue);
                cb.Behavior = unitOfWork.BehaviorRepository.FindById(model.BehaviorSelectedValue);
                unitOfWork.Save();

                TempData["message"] = string.Format("<span style='color:green'>{0} đã cập nhật thành công</span>",
                                                    cb.Content.Title);
                return RedirectToAction("Index", new { contentId = cb.Content.Id });
            }
            model.Contents = ContentHelper.BindSelectListItemContentBehavior(unitOfWork.ContentRepository, model.ContentSelectedValue);
            model.Behaviors = BehaviorHelper.BindSelectListItemContentBehavior(unitOfWork.BehaviorRepository, model.BehaviorSelectedValue);
            return View(model);
        }
        
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "ContentBehaviorAdmin";
            var cb = unitOfWork.ContentBehaviorRepository.FindUnique(d => d.Id == id);
            var contentId = cb.Content.Id;
            if (cb != null)
            {
                var contentTitle = cb.Content.Title;
                unitOfWork.ContentBehaviorRepository.Delete(cb);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>{0} đã được xóa</span>", contentTitle);
            }
            return RedirectToAction("Index", new { contentId = contentId });
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
