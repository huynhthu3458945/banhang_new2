﻿using System.Web.Security;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Models;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.City;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class CityAdminController : BaseController
    {
        //
        // GET: /Admin/CityAdmin/

        private UnitOfWork unitOfWork;

        private readonly int _itemInPerPage = 30;

        public CityAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("CityAdmin", "View")]
        public ActionResult Index(int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "CityAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "CityAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "CityAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "CityAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);

            var sql = unitOfWork.RegionRepository.FindAll();

            var count = sql.Count();
            var cities = sql.OrderByDescending(p => p.Id).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            var indexViewModel = new IndexViewModel<Region>()
            {
                ListItems = cities,
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/CityAdmin/Index?page={0}'")
            };
            return View(indexViewModel);
        }

        [CustomViewAuthorize("CityAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "CityAdmin";
            var model = new CityViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CityViewModel cityViewModel)
        {
            TempData["CurrentMenu"] = "CityAdmin";
            if (ModelState.IsValid)
            {
                var city = new Region();
                CityViewModel.ToEntity(cityViewModel, ref city);
                unitOfWork.RegionRepository.Insert(city);
                unitOfWork.Save();

                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "<strong>{0}</strong> đã được thêm thành công."
                + "</div>", city.Name);

                return RedirectToAction("Index");
            }
            return View(cityViewModel);
        }

        [CustomViewAuthorize("CityAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "CityAdmin";
            var city = unitOfWork.RegionRepository.FindUnique(d => d.Id == id);
            if (city == null)
            {
                TempData["message"] = string.Format("<div class=\"alert alert-danger alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "Không tìm thấy tỉnh/thành phố cần sửa."
                + "</div>");

                return RedirectToAction("Index");
            }
            var cityViewModel = new CityViewModel();
            cityViewModel.Id = city.Id;
            CityViewModel.ToManageModel(cityViewModel, ref city);
            return View(cityViewModel);
        }

        [HttpPost]
        public ActionResult Edit(CityViewModel cityViewModel)
        {
            TempData["CurrentMenu"] = "CityAdmin";
            if (ModelState.IsValid)
            {
                var city = unitOfWork.RegionRepository.FindUnique(d => d.Id == cityViewModel.Id);

                CityViewModel.ToEntity(cityViewModel, ref city);
                unitOfWork.Save();

                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "<strong>{0}</strong> đã được cập nhật thành công."
                + "</div>", city.Name);

                return RedirectToAction("Index");
            }
            return View(cityViewModel);
        }

        [CustomViewAuthorize("CityAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "CityAdmin";
            var city = unitOfWork.RegionRepository.FindUnique(d => d.Id == id);
            var cityName = city.Name;
            if (city != null)
            {
                unitOfWork.RegionRepository.Delete(city);
                unitOfWork.Save();
                TempData["message"] = string.Format("<div class=\"alert alert-success alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                    + "<strong>{0}</strong> đã được xóa."
                + "</div>", cityName);
            }
        Complete:
            return RedirectToAction("Index");
        }


    }
}
