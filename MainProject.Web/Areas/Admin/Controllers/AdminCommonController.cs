﻿using System.Web.Mvc;
using System.Linq;
using System.Web.Security;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class AdminCommonController : BaseController
    {
        private UnitOfWork unitOfWork;

        public AdminCommonController()
        {
            unitOfWork = new UnitOfWork();
        }
        public ActionResult AdminMenu()
        {
            var systemRole = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRole);
            var contentGroupObjs = ContentGroupHelper.GetContentGroupObjectsByRoleId(unitOfWork.ContentGroupRepository, unitOfWork.ContentBehaviorRepository
                                                                                        , unitOfWork.RoleContentBehaviorRepository, role);
            return View(contentGroupObjs);
        }
        public JsonResult GetCategoryByLanague(int id, long cateId = 0)
        {
            if (cateId == 0) cateId = ConfigItemHelper.GetRootNewsId();
            var cate = unitOfWork.CategoryRepository.FindUnique(c => c.Id == cateId);
            var temp = CategoryHelper.BindSelectListItem(unitOfWork.CategoryRepository, cate != null && cate.Parent != null ? cate.Parent.Id : cateId, id, cateId, false, EntityTypeCollection.Categories);

            return Json(new { Data = temp }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCategoryArticleByLanguage(int id, long cateId = 0)
        {
            if (cateId == 0) cateId = ConfigItemHelper.GetRootNewsId();
            var cate = unitOfWork.CategoryRepository.FindUnique(c => c.Id == cateId);
            var temp = CategoryHelper.BindSelectListItemArticle(unitOfWork.CategoryRepository, cate != null && cate.Parent != null ? cate.Parent.Id : cateId, id, cateId, false, EntityTypeCollection.Categories);

            return Json(new { Data = temp }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCategoryProductByLanguage(int id, long cateId = 0)
        {
            if (cateId == 0) cateId = ConfigItemHelper.GetRootNewsId();
            var cate = unitOfWork.CategoryRepository.FindUnique(c => c.Id == cateId);
            var temp = CategoryHelper.BindSelectListItemProduct(unitOfWork.CategoryRepository, cate != null && cate.Parent != null ? cate.Parent.Id : cateId, id, cateId, false, EntityTypeCollection.Categories);

            return Json(new { Data = temp }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCategorySlideShowByLanguage(int id, long cateId = 0)
        {
            if (cateId == 0) cateId = ConfigItemHelper.GetRootNewsId();
            var cate = unitOfWork.CategoryRepository.FindUnique(c => c.Id == cateId);
            var temp = CategoryHelper.BindSelectListItemSlideShow(unitOfWork.CategoryRepository, cate != null && cate.Parent != null ? cate.Parent.Id : cateId, id, cateId, false, EntityTypeCollection.Categories);

            return Json(new { Data = temp }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetParentByLanguageMenu(int languageId, int menuId)
        {
            var temp = MenuItemHelper.BuildSelectListItemsByLanguageMenu(unitOfWork.MenuItemRepository, languageId,menuId);

            return Json(new { Data = temp }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLandingpageGroupByLanguage(int languageId)
        {
            var temp = LandingpageGroupHelper.BuildSelectListItems(unitOfWork.LandingpageGroupRepository, languageId);

            return Json(new { Data = temp }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDistrictByCity(int id)
        {
            var temp = DistrictHelper.GetDistrictByCity(unitOfWork.RegionRepository, id, 0);

            return Json(new { Data = temp }, JsonRequestBehavior.AllowGet);
        }
    }
}
