﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MainProject.Framework.Helper;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class StyleAdminController : Controller
    {
        //
        // GET: /Admin/StyleAdmin/

        public ActionResult Index()
        {
            var filePath = Server.MapPath(ConfigItemHelper.GetSytleFilePath());
            var text = System.IO.File.ReadAllText(filePath);
            ViewBag.Content = text;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string contentStyle)
        {
            var filePath = Server.MapPath(ConfigItemHelper.GetSytleFilePath());
            System.IO.File.WriteAllText(filePath, contentStyle);
            ViewBag.Content = contentStyle;
            ViewBag.Message = "Cập nhật thành công!";
            return View();
        }
    }
}
