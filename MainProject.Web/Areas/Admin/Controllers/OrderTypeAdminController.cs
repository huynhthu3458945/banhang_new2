﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.OrderTypes;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    public class OrderTypeAdminController : BaseController
    {
        //
        // GET: /Admin/OrderTypeAdmin/

        private readonly UnitOfWork unitOfWork;

        private string CurrentUser
        {
            get { return HttpContext.User.Identity.Name; }
        }

        private readonly int _itemInPerPage;

        public OrderTypeAdminController()
        {
            unitOfWork = new UnitOfWork();
            _itemInPerPage = 30;
        }

        [CustomViewAuthorize("OrderTypeAdmin", "View")]
        public ActionResult Index(int languageId = 0, int page = 1)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "OrderTypeAdmin";

            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "OrderTypeAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            ViewBag.IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "OrderTypeAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            ViewBag.IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "OrderTypeAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);

            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            var sql = unitOfWork.OrderTypeRepository.FindAll();
            if (languageId != 0)
            {
                sql = sql.Where(x => x.Language.Id == languageId);
            }
            var count = sql.Count();
            var orderTypes = sql.OrderByDescending(x => x.Id).Skip((page - 1) * _itemInPerPage).Take(_itemInPerPage).ToList();
            var orderTypesAndLanguageVersions = new List<EntityWithLanguageVersionsModel<OrderType>>();
            foreach (var a in orderTypes)
            {
                var languageVersions =
                    unitOfWork.OrderTypeRepository.Find(x => x.OriginalValue == a.OriginalValue && x.Language.Id != a.Language.Id).
                        ToList();
                orderTypesAndLanguageVersions.Add(new EntityWithLanguageVersionsModel<OrderType>() { Entity = a, LanguageVersions = languageVersions });
            }
            var model = new IndexViewModel<EntityWithLanguageVersionsModel<OrderType>>()
            {
                ListItems = orderTypesAndLanguageVersions,
                FilterViewModel = new FilterViewModel()
                {
                    LanguageViewModel = new LanguageSelectModel()
                    {
                        LanguageSelectedValue = languageId
                    },
                    BaseUrl = "/Admin/OrderTypeAdmin/Index?"
                },
                PagingViewModel = new PagingModel(count, _itemInPerPage, page, "href='/Admin/OrderTypeAdmin/Index?languageId=" + languageId + "&page={0}'")

            };
            model.FilterViewModel.LanguageViewModel.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), languageId);
            return View(model);
        }

        [CustomViewAuthorize("OrderTypeAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "OrderTypeAdmin";

            var orderTypeOriginal = id == 0 ? null : unitOfWork.OrderTypeRepository.FindUnique(x => x.Id == id);
            var originalValue = orderTypeOriginal != null ? orderTypeOriginal.OriginalValue : Guid.NewGuid();
            if (orderTypeOriginal == null)
            {
                while (unitOfWork.OrderTypeRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }
            var model = new OrderTypeManageModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<OrderType>(originalValue, unitOfWork), 0),
                OriginalValue = originalValue
            };
            if (orderTypeOriginal != null)
            {
                model.Order = orderTypeOriginal.Order;
            }
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(OrderTypeManageModel model)
        {
            TempData["CurrentMenu"] = "OrderTypeAdmin";

            if (ModelState.IsValid)
            {
                var entity = new OrderType()
                {
                    Language = unitOfWork.LanguageRepository.FindById(model.LanguageSelectedValue)
                };
                OrderTypeManageModel.ToEntity(model, ref entity);
                unitOfWork.OrderTypeRepository.Insert(entity);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Hình thức <strong>{0}</strong> đã được thêm thành công!</span>", model.Title);
                return RedirectToAction("Index", new { languageId = entity.Language.Id });
            }
            model.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<OrderType>(model.OriginalValue, unitOfWork), 0);
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(model);
        }

        [CustomViewAuthorize("OrderTypeAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "OrderTypeAdmin";

            var entity = unitOfWork.OrderTypeRepository.FindUnique(d => d.Id == id);
            if (entity == null)
            {
                TempData["message"] = string.Format("Không tìm thấy hình thức cần sửa");
                return RedirectToAction("Index");
            }
            var model = new OrderTypeManageModel
            {
                LanguageSelectedValue = entity.Language.Id,
                Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), entity.Language.Id),
            };
            model.Id = entity.Id;
            OrderTypeManageModel.ToManageModel(model, ref entity);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(OrderTypeManageModel model)
        {
            TempData["CurrentMenu"] = "OrderTypeAdmin";

            if (ModelState.IsValid)
            {
                var entity = unitOfWork.OrderTypeRepository.FindUnique(d => d.Id == model.Id);

                entity.Language = unitOfWork.LanguageRepository.FindById(model.LanguageSelectedValue);
                OrderTypeManageModel.ToEntity(model, ref entity);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Hình thức {0} đã cập nhật thành công</span>",
                                                    entity.Title);
                return RedirectToAction("Index", new { languageId = model.LanguageSelectedValue });
            }
            model.Languages =
                LanguageHelper.BindLanguage(
                    LanguageHelper.FindAvailableLanguagesToBind<OrderType>(model.OriginalValue, unitOfWork), 0);
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Vui lòng chọn ngôn ngữ",
                Value = ""
            });
            return View(model);
        }

        [CustomViewAuthorize("OrderTypeAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "OrderTypeAdmin";

            var entity = unitOfWork.OrderTypeRepository.FindUnique(d => d.Id == id);
            var title = entity.Title;
            var currentLanguageId = entity.Language.Id;
            if (entity != null)
            {
                unitOfWork.OrderTypeRepository.Delete(entity);
                unitOfWork.Save();
                TempData["message"] = string.Format("<span style='color:green'>Hình thức {0} đã được xóa</span>", title);
            }
            return RedirectToAction("Index", new { languageId = currentLanguageId });
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
