﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.ActionFilters;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Helpers;
using MainProject.Web.Areas.Admin.Models;
using MainProject.Web.Areas.Admin.Models.Category;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Areas.Admin.Controllers
{
    [FilterPermission(EntityManageType = EntityManageTypeCollection.ManageCategories)]
    public class CategoryAdminController : BaseController
    {
        private UnitOfWork unitOfWork;

        public CategoryAdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        [CustomViewAuthorize("CategoryAdmin", "View")]
        public ActionResult Index(int languageId = 0)
        {
            //set current active menu of admin page
            TempData["CurrentMenu"] = "CategoryAdmin";
            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            ViewBag.IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "CategoryAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);

            languageId = languageId != 0 ? languageId : unitOfWork.LanguageRepository.FindAll().First().Id;
            var model = new LanguageSelectModel { LanguageSelectedValue = languageId };
            model.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(), model.LanguageSelectedValue);
            return View(model);
        }

        public JsonResult GetCategoryByLanague(int id, long cateId = 0)
        {
            if (cateId == 0) cateId = ConfigItemHelper.GetRootNewsId();
            var cate =  unitOfWork.CategoryRepository.FindUnique(c => c.Id == cateId);
            var temp = CategoryHelper.BindSelectListItem(unitOfWork.CategoryRepository, cate != null && cate.Parent != null ? cate.Parent.Id : ConfigItemHelper.GetRootNewsId(), id, cateId, true, EntityTypeCollection.Categories);
            return Json(new { Data = temp }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SortFollowLanguage(int languageId = 0)
        {
            // Set authorize behaviors on Index page
            var systemRoleName = Roles.GetRolesForUser().FirstOrDefault();
            var role = unitOfWork.RoleRepository.FindUnique(x => x.RoleName == systemRoleName);
            var IsCreate = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "CategoryAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.CreateTemplate);
            var IsEdit = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "CategoryAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.EditTemplate);
            var IsDelete = unitOfWork.RoleContentBehaviorRepository.FindUnique(x => x.Role.RoleId == role.RoleId && x.ContentBehavior.Content.ControllerName == "CategoryAdmin"
                                                            && x.ContentBehavior.Behavior.Type == BehaviorTypeCollection.DeleteTemplate);
            ViewBag.IsCreate = IsCreate;

            if (languageId == 0)
            {
                languageId = unitOfWork.LanguageRepository.FindAll().First().Id;
            }
            var model = CategoryHelper.BuildCategoryModel(unitOfWork.CategoryRepository, languageId, EntityTypeCollection.Categories);
            string strBuilder = "<table class='table table-bordered table-striped'><tr><th>STT</th><th>Tiêu đề</th><th>Thứ tự</th><th>Hiển thị</th><th>Ngôn ngữ</th><th>Tác vụ</th></tr>";
            var i = 1;
            foreach (var item in model)
            {
                if (i % 2 == 0)
                {
                    strBuilder += "<tr class='even'>";
                }
                else
                {
                    strBuilder += "<tr class='odd'>";
                }
                var cat = unitOfWork.CategoryRepository.FindUnique(x => x.Id == item.Id);
                var _tdLanguage = "";
                if (IsCreate != null)
                {
                    var languageVers =
                        unitOfWork.CategoryRepository.Find(x => x.OriginalValue == cat.OriginalValue && x.Language.Id != cat.Language.Id).
                            ToList();

                    _tdLanguage = string.Format("<span class='version'><img alt='{0}' src='{1}'/ title='{2}'></span>", cat.Language.LanguageName, cat.Language.Image, cat.Language.LanguageName);
                    foreach (var lang in languageVers)
                    {
                        _tdLanguage += string.Format("<a href='/Admin/CategoryAdmin/Edit?id={0}' class='version' title='Phiên bản'><img alt='{1}' src='{2}' title='{3}'/></a>",
                                                     lang.Id, lang.Language.LanguageName, lang.Language.Image, lang.Language.LanguageName);
                    }
                    _tdLanguage +=
                        string.Format(
                            "<a href='/Admin/CategoryAdmin/Create?id={0}' class='.btn.btn-app' title='Thêm ngôn ngữ'><i class='fa fa-plus'></i></a>", item.Id);
                }
                var _isPublished = item.IsPublished ? "<input checked class='check-box' disabled='disabled' type='checkbox'>"
                                                    : "<input class='check-box' disabled='disabled' type='checkbox'>";

                // Checking authorize behaviors (Edit and Delete) on Index page
                if (IsEdit != null && IsDelete != null)
                {
                    strBuilder +=
                        string.Format(
                            //"<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td class='containIcon'><a class='.btn.btn-app' href='/Admin/CategoryAdmin/Edit/{5}' title='Sửa'><i class='fa fa-edit'></i></a><a class='.btn.btn-app' title='Xóa' onclick='return onlickDelete()' href='/Admin/CategoryAdmin/Delete/{5}'><i class='fa fa-trash-o'></i></a></td>",
                            "<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td class='containIcon'><a class='btn btn-info' href='/Admin/CategoryAdmin/Edit/{5}' title='Sửa'><i class='fa fa-edit'></i></a><span id='category-delete' class='btn btn-danger delete-btn' data-toggle='modal' data-target='#categorymodel-delete-confirmation' data-id='{5}'><i class='fa fa-trash-o'></i></span></td>",
                            i, item.Title, item.Order, _isPublished, _tdLanguage, item.Id);
                }
                if (IsEdit != null && IsDelete == null)
                {
                    strBuilder +=
                        string.Format(
                            "<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td class='containIcon'><a class='btn btn-info' href='/Admin/CategoryAdmin/Edit/{5}' title='Sửa'><i class='fa fa-edit'></i></a></td>",
                            i, item.Title, item.Order, _isPublished, _tdLanguage, item.Id);
                }
                if (IsEdit == null && IsDelete != null)
                {
                    strBuilder +=
                        string.Format(
                            "<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td class='containIcon'><span id='category-delete' class='btn btn-danger delete-btn' data-toggle='modal' data-target='#categorymodel-delete-confirmation' data-id='{5}'><i class='fa fa-trash-o'></i></span></td>",
                            i, item.Title, item.Order, _isPublished, _tdLanguage, item.Id);
                }
                if (IsEdit == null && IsDelete == null)
                {
                    strBuilder +=
                        string.Format(
                            "<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td class='containIcon'></td>",
                            i, item.Title, item.Order, _isPublished, _tdLanguage);
                }

                i++;
            }
            strBuilder += "</tr></table>";
            return Json(new { Data = strBuilder }, JsonRequestBehavior.AllowGet);
        }

        [CustomViewAuthorize("CategoryAdmin", "Create")]
        public ActionResult Create(int id = 0)
        {
            TempData["CurrentMenu"] = "CategoryAdmin";
            var original = id == 0 ? null : unitOfWork.CategoryRepository.FindUnique(x => x.Id == id);
            var originalValue = original != null ? original.OriginalValue : Guid.NewGuid();
            if (original == null)
            {
                while (unitOfWork.CategoryRepository.FindUnique(x => x.OriginalValue == originalValue) != null)
                {
                    originalValue = Guid.NewGuid();
                }
            }
            var model = new CategoryManageViewModel
            {
                Languages = LanguageHelper.BindLanguage(LanguageHelper.FindAvailableLanguagesToBind<Category>(originalValue, unitOfWork), 0),
                Parents = new List<SelectListItem>
                                              {
                                                  new SelectListItem
                                                      {
                                                          Value = "",
                                                          Text = "Chọn ngôn ngữ"
                                                      }
                                              },
                OriginalValue = originalValue,
                DisplayTemplates = EnumHelper.ToSelectList(typeof(DisplayTemplateCollection))
            };
            var imageFolder = "";
            if (original != null)
            {
                imageFolder = original.ImageFolder;
                model.ImageDefault = original.ImageDefault;
                model.ImageDefault2 = original.ImageDefault2;
            }
            else
            {
                imageFolder = "/Upload/Categoties/" + Guid.NewGuid().ToString();
                while (Directory.Exists(Server.MapPath(imageFolder)))
                {
                    imageFolder = "/Upload/Categoties/" + Guid.NewGuid().ToString();
                }
            }
            model.ImageFolder = imageFolder;
            model.Languages.Insert(0, new SelectListItem
            {
                Text = "Chọn ngôn ngữ",
                Value = ""
            });
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CategoryManageViewModel model)
        {
            TempData["CurrentMenu"] = "CategoryAdmin";
            if (ModelState.IsValid)
            {
                var category = new Category
                                   {
                                        IsSystem = false,
                                        OriginalValue = model.OriginalValue,
                                        Language = unitOfWork.LanguageRepository.FindById(model.LanguageSelectedValue),
                                        Order = model.Order,
                                        Parent = unitOfWork.CategoryRepository.FindById(model.ParentSelectedValue),
                                        PrivateArea = model.PrivateArea,
                                        SeName = model.Title.ToLower(),
                                        Title = model.Title,
                                        Description = model.Description,
                                        ImageDefault = model.ImageDefault,
                                        ImageFolder = model.ImageFolder,
                                        IsPublished = model.IsPublished,
                                        IsShowOnMenu = model.IsShowOnMenu,
                                        ExternalLink = model.ExternalLink,
                                        BannerLink = model.BannerLink,
                                        MetaTitle = model.MetaTitle,
                                        MetaDescription = model.MetaDescription,
                                        MetaKeywords = model.MetaKeywords,
                                        MetaImage = model.MetaImage,
                                        ImageDefault2 = model.ImageDefault2,
                                        IsHotProductCategory = model.IsHotProductCategory,
                                        Benefit = model.Benefit,
                                        Why = model.Why,
                                        LinkYouTuBe = model.LinkYouTuBe,
                                        ImageIcon = model.ImageIcon
                                   };
                category.EntityType = EntityTypeCollection.Categories;
                category.DisplayTemplate = model.DisplayTemplateSelected;
                unitOfWork.CategoryRepository.Insert(category);

                // save to get entityId for Generate Sename
                unitOfWork.Save();

                category.SeName = SeoHelper.VaidateSeNameAndSubmit(EntityTypeCollection.Categories, category.Id, category.Title);
                unitOfWork.Save();

                TempData["message"] = string.Format("<strong style='color:green'>Tạo mới thành công!</strong>", model.Title);
                return RedirectToAction("Index");
            }
            model.DisplayTemplates = EnumHelper.ToSelectList(typeof (DisplayTemplateCollection));
            model.Parents =
                CategoryHelper.BindSelectListItem(
                    unitOfWork.CategoryRepository, model.ParentSelectedValue, model.LanguageSelectedValue);
            model.Languages = LanguageHelper.BindLanguage(unitOfWork.LanguageRepository.FindAll().ToList(),
                                                                               model.LanguageSelectedValue);
            return View(model);
        }

        [CustomViewAuthorize("CategoryAdmin", "Edit")]
        public ActionResult Edit(int id)
        {
            TempData["CurrentMenu"] = "CategoryAdmin";
            var category = unitOfWork.CategoryRepository.FindById(id);
            var model = new CategoryManageViewModel
                            {
                                Id = category.Id,
                                Title = category.Title,
                                Description = category.Description,
                                ImageDefault = category.ImageDefault,
                                ImageFolder = category.ImageFolder,
                                Order = category.Order,
                                PrivateArea = category.PrivateArea,
                                ParentSelectedValue = Convert.ToInt32(category.Parent.Id),
                                LanguageSelectedValue = Convert.ToInt32(category.Language.Id),
                                Parents = CategoryHelper.BindSelectListItem(unitOfWork.CategoryRepository, category.Parent.Id, category.Language.Id, id),
                                DisplayTemplates = EnumHelper.ToSelectList(typeof(DisplayTemplateCollection)),
                                DisplayTemplateSelected = category.DisplayTemplate,
                                IsPublished = category.IsPublished,
                                ExternalLink = category.ExternalLink,
                                IsShowOnMenu = category.IsShowOnMenu,
                                BannerLink = category.BannerLink,
                                MetaTitle = category.MetaTitle,
                                MetaDescription = category.MetaDescription,
                                MetaKeywords = category.MetaKeywords,
                                MetaImage = category.MetaImage,
                                ImageDefault2 = category.ImageDefault2,
                                IsHotProductCategory = category.IsHotProductCategory,
                                Benefit = category.Benefit,
                                Why = category.Why,
                                LinkYouTuBe = category.LinkYouTuBe,
                                ImageIcon = category.ImageIcon
                            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(CategoryManageViewModel model)
        {
            TempData["CurrentMenu"] = "CategoryAdmin";
            if (ModelState.IsValid)
            {
                var category = unitOfWork.CategoryRepository.FindById(model.Id);
                category.Order = model.Order;
                var cat = unitOfWork.CategoryRepository.FindById(model.ParentSelectedValue);
                category.Parent = cat;

                // save to update Category Parent of Category to Database for VaidateSeNameAndSubmit()
                unitOfWork.Save();

                category.EntityType = category.Parent.EntityType;
                category.Title = model.Title;
                category.Description = model.Description;
                category.ImageDefault = model.ImageDefault;
                category.ImageFolder = model.ImageFolder;
                category.PrivateArea = model.PrivateArea;
                category.SeName = SeoHelper.VaidateSeNameAndSubmit(category.EntityType, category.Id, category.Title);
                category.DisplayTemplate = model.DisplayTemplateSelected;
                category.IsPublished = model.IsPublished;
                category.IsShowOnMenu = model.IsShowOnMenu;
                category.ExternalLink = model.ExternalLink;
                category.BannerLink = model.BannerLink;
                category.MetaTitle = model.MetaTitle;
                category.MetaKeywords = model.MetaKeywords;
                category.MetaDescription = model.MetaDescription;
                category.MetaImage = model.MetaImage;
                category.ImageDefault2 = model.ImageDefault2;
                category.IsHotProductCategory = model.IsHotProductCategory;
                category.ImageIcon = model.ImageIcon;
                category.Benefit = model.Benefit;
                category.Why = model.Why;
                category.LinkYouTuBe = model.LinkYouTuBe;
                unitOfWork.Save();

                TempData["message"] = string.Format("<strong style='color:green'>Cập nhật thành công!</strong>", model.Title);
                return RedirectToAction("Index");
            }
            model.DisplayTemplates = EnumHelper.ToSelectList(typeof (DisplayTemplateCollection));
            model.Parents =
                CategoryHelper.BindSelectListItem(
                    unitOfWork.CategoryRepository, model.ParentSelectedValue, model.LanguageSelectedValue, model.Id);

            return View(model);
        }

        [CustomViewAuthorize("CategoryAdmin", "Delete")]
        public ActionResult Delete(int id)
        {
            TempData["CurrentMenu"] = "CategoryAdmin";
            var model = unitOfWork.CategoryRepository.FindById(id);
            if (model != null)
            {
                if (!unitOfWork.ArticleRepository.Find(d => d.Category.Id == model.Id).Any())
                {
                    if (!unitOfWork.CategoryRepository.Find(d => d.Parent != null && d.Parent.Id == id).Any())
                    {
                        //delete images folder
                        if (Directory.Exists(Server.MapPath(model.ImageFolder)))
                        {
                            try
                            {
                                Directory.Delete(Server.MapPath(model.ImageFolder), true);
                            }
                            catch (Exception ex) { }
                        }
                        try
                        {
                            SeoHelper.DeleteUrlData(EntityTypeCollection.Categories, model.Id);
                            unitOfWork.CategoryRepository.Delete(model);
                            unitOfWork.Save();
                            TempData["message"] = string.Format("<strong style='color:green'>Xóa thành công</strong>");
                            return RedirectToAction("Index");
                        }
                        catch
                        {
                            TempData["message"] = string.Format("<strong style='color:red'>Xảy ra lỗi trong quá trình xóa dữ liệu</strong>");
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        TempData["message"] = string.Format("<strong style='color:red'>{0} có nhóm con, không thể xóa</strong>", model.Title);
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    TempData["message"] = string.Format("<strong style='color:red'>{0} có bài viết con, không thể xóa</strong>", model.Title);
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");

        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}