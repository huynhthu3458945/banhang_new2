﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.OrderTypes
{
    public class OrderTypeManageModel:BaseLanguageModel
    {
        public OrderTypeManageModel()
        {
            Order = 0;
        }
        public int Id { get; set; }
        public string Title { get; set; }

        [AllowHtml]
        public string Content { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }

        public static void ToEntity(OrderTypeManageModel model, ref OrderType entity)
        {
            entity.Title = model.Title;
            entity.Content = model.Content;
            entity.IsPublished = model.IsPublished;
            entity.Order = model.Order;
            entity.OriginalValue = model.OriginalValue;
        }

        public static void ToManageModel(OrderTypeManageModel model, ref Core.OrderType entity)
        {
            model.Title = entity.Title;
            model.Content = entity.Content;
            model.IsPublished = entity.IsPublished;
            model.Order = entity.Order;
            model.OriginalValue = entity.OriginalValue;
        }
    }
}