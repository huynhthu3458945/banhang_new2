﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.UserInfos;

namespace MainProject.Web.Areas.Admin.Models.ContentGroups
{
    public class ContentGroupManageModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Nhập tên nhóm")]
        public string GroupName { get; set; }

        public int Order { get; set; }

        public string IconClass { get; set; }


        public static void ToEntity(ContentGroupManageModel model, ref ContentGroup entity)
        {
            entity.GroupName = model.GroupName;
            entity.Order = model.Order;
            entity.IconClass = model.IconClass;
        }

        public static void ToModel(ContentGroupManageModel model, ref ContentGroup entity)
        {
            model.GroupName = entity.GroupName;
            model.Order = entity.Order;
            model.IconClass = entity.IconClass;
        }
    }
}