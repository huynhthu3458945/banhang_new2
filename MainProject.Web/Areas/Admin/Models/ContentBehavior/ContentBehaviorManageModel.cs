﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.UserInfos;

namespace MainProject.Web.Areas.Admin.Models.ContentBehaviors
{
    public class ContentBehaviorManageModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn nội dung!")]
        public long ContentSelectedValue { get; set; }

        public List<SelectListItem> Contents { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn tác vụ!")]
        public int BehaviorSelectedValue { get; set; }

        public List<SelectListItem> Behaviors { get; set; }
    }
}