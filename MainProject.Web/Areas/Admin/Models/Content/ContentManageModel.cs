﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.UserInfos;

namespace MainProject.Web.Areas.Admin.Models.Contents
{
    public class ContentManageModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Nhập tiêu đề")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Nhập tên controller")]
        public string ControllerName { get; set; }
        public int Order { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn nhóm nội dung!")]
        public int ContentGroupSelectedValue { get; set; }

        public List<SelectListItem> ContentGroups { get; set; }


        public static void ToEntity(ContentManageModel model, ref Content entity)
        {
            entity.Title = model.Title;
            entity.ControllerName = model.ControllerName;
            entity.Order = model.Order;
        }

        public static void ToModel(ContentManageModel model, ref Content entity)
        {
            model.Title = entity.Title;
            model.ControllerName = entity.ControllerName;
            model.Order = entity.Order;
        }
    }
}