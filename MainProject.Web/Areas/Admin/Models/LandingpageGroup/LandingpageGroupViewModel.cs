﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.LandingpageGroup
{
    public class LandingpageGroupViewModel : BaseLanguageModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        public string ImageDefault { get; set; }

        public string ImageFolder { get; set; }

        public bool IsPublished { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn vị trí hiển thị!")]
        public PositionTypeCollection PositionTypeSelectedValue { get; set; }
        public List<SelectListItem> PositionTypes { get; set; }

        public static void ToModel(LandingpageGroupViewModel model, ref Core.LandingpageGroup entity)
        {
            model.Title = entity.Title;
            model.Description = entity.Description;
            model.ImageFolder = entity.ImageFolder;
            model.ImageDefault = entity.ImageDefault;
            model.OriginalValue = entity.OriginalValue;
            model.PositionTypeSelectedValue = entity.PositionType;
            model.IsPublished = entity.IsPublished;
        }
        public static void ToEntity(LandingpageGroupViewModel model, ref Core.LandingpageGroup entity)
        {
            entity.Title = model.Title;
            entity.Description = model.Description;
            entity.ImageFolder = model.ImageFolder;
            entity.ImageDefault = model.ImageDefault;
            entity.OriginalValue = model.OriginalValue;
            entity.PositionType = model.PositionTypeSelectedValue;
            entity.IsPublished = model.IsPublished;
        }
    }
}