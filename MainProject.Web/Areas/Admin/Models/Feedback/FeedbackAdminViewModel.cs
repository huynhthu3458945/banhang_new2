﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.Feedback
{
    public class FeedbackViewModel : BaseLanguageModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Vui lòng nhập nội dung!")]
        public string Description { get; set; }

        public string ImageDefault { get; set; }

        public string ImageFolder { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập chức vụ!")]
        public string Location { get; set; }

        public bool IsPublished { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }
        public int Order { get; set; }
        public static void ToModel(FeedbackViewModel model, ref Core.Feedback entity)
        {
            model.FullName = entity.FullName;
            model.Description = entity.Description;
            model.ImageFolder = entity.ImageFolder;
            model.ImageDefault = entity.ImageDefault;
            model.OriginalValue = entity.OriginalValue;
            model.Order = entity.Order;
            model.IsPublished = entity.IsPublished;
            model.Location = entity.Location;
        }
        public static void ToEntity(FeedbackViewModel model, ref Core.Feedback entity)
        {
            entity.FullName = model.FullName;
            entity.Description = model.Description;
            entity.ImageFolder = model.ImageFolder;
            entity.ImageDefault = model.ImageDefault;
            entity.OriginalValue = model.OriginalValue;
            entity.Order = model.Order;
            entity.IsPublished = model.IsPublished;
            entity.Location = model.Location;
        }
    }
}