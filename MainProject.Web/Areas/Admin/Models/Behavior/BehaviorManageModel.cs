﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.UserInfos;
using MainProject.Core.Enums;

namespace MainProject.Web.Areas.Admin.Models.Behaviors
{
    public class BehaviorManageModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Nhập tên tác vụ")]
        public string Name { get; set; }

        public BehaviorTypeCollection BehaviorTypeSelected { get; set; }

        public List<SelectListItem> BehaviorTypes { get; set; }


        public static void ToEntity(BehaviorManageModel model, ref Behavior entity)
        {
            entity.Name = model.Name;
        }

        public static void ToModel(BehaviorManageModel model, ref Behavior entity)
        {
            model.Name = entity.Name;
        }
    }
}