﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.MenuItem
{
    public class MenuItemViewModel : BaseLanguageModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public LinkTargetTypeCollection LinkTargetTypeSelected { get; set; }
        public List<SelectListItem> LinkTargetTypes { get; set; }

        public bool IsPublished { get; set; }

        public int Order { get; set; }

        public string IconClass { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn loại menu!")]
        public int MenuSelectedValue { get; set; }
        public List<SelectListItem> Menus { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn menu cha!")]
        public int ParentSelectedValue { get; set; }
        public List<SelectListItem> Parents { get; set; }

        public static void ToModel(MenuItemViewModel model, ref Core.MenuItem entity)
        {
            model.Title = entity.Title;
            model.Url = entity.Url;
            model.Order = entity.Order;
            model.IsPublished = entity.IsPublished;
            model.IconClass = entity.IconClass;
            model.OriginalValue = entity.OriginalValue;
            model.LinkTargetTypeSelected = entity.LinkTargetType;
        }
        public static void ToEntity(MenuItemViewModel model, ref Core.MenuItem entity)
        {
            entity.Title = model.Title;
            entity.Url = model.Url;
            entity.Order = model.Order;
            entity.IsPublished = model.IsPublished;
            entity.IconClass = model.IconClass;
            entity.OriginalValue = model.OriginalValue;
            entity.LinkTargetType = model.LinkTargetTypeSelected;
        }
    }
}