﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.LandingpageItem
{
    public class LandingpageItemViewModel : BaseLanguageModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        [AllowHtml]
        public string Content { get; set; }

        public string LinkYoube { get; set; }
        public string ImageDefault { get; set; }

        public string ImageFolder { get; set; }

        public string Url { get; set; }

        public string ImageProduct { get; set; }
        public string Size { get; set; }
        public string Material { get; set; }
        public string MassShip { get; set; }
        public string MassFood { get; set; }
        public string Logo { get; set; }
        public bool IsPublished { get; set; }

        public int Order { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn danh mục!")]
        public int LanguageGroupSelectedValue { get; set; }
        public List<SelectListItem> LanguageGroups { get; set; }

        public static void ToModel(LandingpageItemViewModel model, ref Core.LandingpageItem entity)
        {
            model.Title = entity.Title;
            model.Description = entity.Description;
            model.Url = entity.Url;
            model.Order = entity.Order;
            model.IsPublished = entity.IsPublished;
            model.ImageDefault = entity.ImageDefault;
            model.ImageFolder = entity.ImageFolder;
            model.OriginalValue = entity.OriginalValue;
            model.Content = entity.Content;
            model.LinkYoube = entity.LinkYoube;
            model.ImageProduct = entity.ImageProduct;
            model.Size = entity.Size;
            model.Material = entity.Material;
            model.MassShip = entity.MassShip;
            model.MassFood = entity.MassFood;
            model.Logo = entity.Logo;
        }
        public static void ToEntity(LandingpageItemViewModel model, ref Core.LandingpageItem entity)
        {
            entity.Title = model.Title;
            entity.Description = model.Description;
            entity.Url = model.Url;
            entity.Order = model.Order;
            entity.IsPublished = model.IsPublished;
            entity.ImageDefault = model.ImageDefault;
            entity.ImageFolder = model.ImageFolder;
            entity.OriginalValue = model.OriginalValue;
            entity.Content = model.Content;
            entity.LinkYoube = model.LinkYoube;
            entity.ImageProduct = model.ImageProduct;
            entity.Size = model.Size;
            entity.Material = model.Material;
            entity.MassShip = model.MassShip;
            entity.MassFood = model.MassFood;
            entity.Logo = model.Logo;
        }
    }
}