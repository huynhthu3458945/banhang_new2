﻿using MainProject.Web.Areas.Admin.Models.BaseModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainProject.Web.Areas.Admin.Models.Faq
{
    public class FaqViewModel : BaseLanguageModel
    {
        public FaqViewModel()
        {
            Order = 0;
        }
        public int Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }
        public FilterViewModel FilterViewModel { get; set; }

        public static void ToEntity(FaqViewModel model, ref Core.Faq entity)
        {
            entity.Question = model.Question;
            entity.Answer = model.Answer;
            entity.Order = model.Order;
            entity.IsPublished = model.IsPublished;
            entity.OriginalValue = model.OriginalValue;
        }

        public static void ToModel(FaqViewModel model, ref Core.Faq entity)
        {
            model.Question = entity.Question;
            model.Answer = entity.Answer;
            model.Order = entity.Order;
            model.IsPublished = entity.IsPublished;
            model.OriginalValue = entity.OriginalValue;
        }
    }
}