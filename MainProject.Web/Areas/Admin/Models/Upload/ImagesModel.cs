﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainProject.Web.Areas.Admin.Models.Upload
{
    public class ImagesModel
    {
        public ImagesModel()
        {
            Images = new List<ImageInfo>();
        }

        public List<ImageInfo> Images { get; set; }
    }
    public class ImageInfo
    {
        public string Name { get; set; }
        public string Link { get; set; }
    }
}