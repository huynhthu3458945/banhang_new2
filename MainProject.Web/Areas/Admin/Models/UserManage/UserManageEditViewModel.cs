﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MainProject.Web.Areas.Admin.Models.UserManage
{
    public class UserManageEditViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập tên tài khoản!")]
        [RegularExpression("([0-9a-zA-Z]+)", ErrorMessage = "Tài khoản không được dùng ký tự đặc biệt!")]
        public string UserName { get; set; }

        public string OldPassword { get; set; }

        [StringLength(50, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu từ 6 đến 50 ký tự!")]
        public string NewPassword { get; set; }

        [Compare("NewPassword", ErrorMessage = "Mật khẩu nhập lại không chính xác.")]
        public string RePassword { get; set; }


        [StringLength(100, ErrorMessage = "Độ dài Email không quá 100 ký tự!")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email không hợp lệ.")]
        [Required(ErrorMessage = "Vui lòng điền Email!")]
        public string Email { get; set; }

        [StringLength(100, MinimumLength = 5, ErrorMessage = "Độ dài họ tên không quá 100 ký tự!")]
        public string FullName { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu!")]
        public string Password { get; set; }

        public List<string> ListRoles { get; set; }

        public List<string> ListRolesCheck { get; set; }
    }
}