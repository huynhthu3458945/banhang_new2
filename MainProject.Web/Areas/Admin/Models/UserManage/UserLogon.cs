﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Web.Areas.Admin.Models.UserManage
{
    public class UserLogon
    {
        [Required(ErrorMessage = "Vui lòng nhập tên tài khoản!")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu!")]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
    public class ForgetPass
    {

        public string UserName { get; set; }

        public string Email { get; set; }
    }
}