﻿namespace MainProject.Web.Areas.Admin.Models.UserManage
{
    public class ArrayUsersRoles
    {
        public string[] RolesArray { get; set; }

        public string RolesSelected { get; set; }
    }
}