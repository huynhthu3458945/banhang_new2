﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MainProject.Web.Areas.Admin.Models.Branch
{
    public class BranchViewModel
    {
        public BranchViewModel()
        {
            Order = 0;
        }
        public int Id { get; set; }

        [Required(ErrorMessage = "Nhập tên chi nhánh!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Nhập tên đường!")]
        public string Street { get; set; }
        [Required(ErrorMessage = "Nhập số điện thoại!")]
        public string Phone { get; set; }
        public string Fax { get; set; }
        public bool IsPublished { get; set; }
        public bool IsOnFooter { get; set; }
        [Required(ErrorMessage = "Nhập tọa độ Lng")]
        public double Lng { get; set; }
        [Required(ErrorMessage = "Nhập tọa độ Lat")]
        public double Lat { get; set; }
        public int Order { get; set; }
        public string Url { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn tỉnh/thành phố!")]
        public int CitySelectedValue { get; set; }
        public List<SelectListItem> Cities { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn quận/huyện!")]
        public int DistrictSelectedValue { get; set; }
        public List<SelectListItem> Districts { get; set; }



        public static void ToEntity(BranchViewModel model, ref Core.Branch entity)
        {
            entity.Name = model.Name;
            entity.Street = model.Street;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
            entity.IsPublished = model.IsPublished;
            entity.IsOnFooter = model.IsOnFooter;
            entity.Order = model.Order;
            entity.Lng = model.Lng;
            entity.Lat = model.Lat;
            entity.Url = model.Url;
        }

        public static void ToManageModel(BranchViewModel model, ref Core.Branch entity)
        {
            model.Name = entity.Name;
            model.Street = entity.Street;
            model.Phone = entity.Phone;
            model.Fax = entity.Fax;
            model.IsPublished = entity.IsPublished;
            model.IsOnFooter = entity.IsOnFooter;
            model.Order = entity.Order;
            model.Lng = entity.Lng;
            model.Lat = entity.Lat;
            model.Url = entity.Url;
        }
    }
}