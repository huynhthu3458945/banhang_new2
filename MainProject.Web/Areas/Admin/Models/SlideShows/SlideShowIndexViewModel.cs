﻿using MainProject.Core;
using MainProject.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainProject.Web.Areas.Admin.Models.SlideShows
{
    public class SlideShowIndexViewModel
    {
        public List<SlideShow> ListItems { get; set; }

        public PagingModel PagingViewModel { get; set; }

        public List<SelectListItem> Categories { get; set; }
    }
}