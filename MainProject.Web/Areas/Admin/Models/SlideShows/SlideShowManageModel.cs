﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.SlideShows
{
    public class SlideShowManageModel:BaseLanguageModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Nhập tên")]
        public string Name { get; set; }

        public string Image { get; set; }

        public string ImageMobile { get; set; }

        public string Description { get; set; }

        public string Link { get; set; }
        public int Order { get; set; }
        public bool IsPublished { get; set; }

        [Required(ErrorMessage = "Thư mục hình ảnh chưa đc khởi tạo")]
        public string ImageFolder { get; set; }

        public bool IsHomePage { get; set; }

        public string VideoId { get; set; }
        public bool IsVideo { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn danh mục!")]
        public long CategorySelectedValue { get; set; }
        public List<SelectListItem> Categories { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }


        public void ToEntity(SlideShowManageModel model,ref SlideShow entity)
        {
            entity.Name = model.Name;
            entity.Description = model.Description;
            entity.ImageFolder = model.ImageFolder;
            entity.Image = model.Image;
            entity.ImageMobile = model.ImageMobile;
            entity.IsPublished = model.IsPublished;
            entity.Order = model.Order;
            entity.Link = model.Link;
            entity.IsHomePage = model.IsHomePage;
            entity.VideoId = model.VideoId;
            entity.IsVideo = model.IsVideo;
            entity.OriginalValue = model.OriginalValue;
        }

        public void ToModel(SlideShowManageModel model,ref SlideShow entity)
        {
            model.Id = entity.Id;
            model.Name = entity.Name;
            model.Description = entity.Description;
            model.ImageFolder = entity.ImageFolder;
            model.Image = entity.Image;
            model.ImageMobile = entity.ImageMobile;
            model.IsPublished = entity.IsPublished;
            model.Order = entity.Order;
            model.Link = entity.Link;
            model.IsHomePage = entity.IsHomePage;
            model.VideoId = entity.VideoId;
            model.IsVideo = entity.IsVideo;
            model.OriginalValue = entity.OriginalValue;
        }
    }
}