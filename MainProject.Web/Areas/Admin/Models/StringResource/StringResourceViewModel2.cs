﻿using System.Collections.Generic;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Framework.Models;

namespace MainProject.Web.Areas.Admin.Models.StringResource
{
    public class StringResourceViewModel2
    {
        public List<StringResourceValue> ListStringResource { get; set; }

        public int LanguageSelectedValue { get; set; }

        public List<SelectListItem> Languages { get; set; }

        public PagingModel PagingViewModel { get; set; }

        public string Filter { get; set; }
    }
}