﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MainProject.Core;

namespace MainProject.Web.Areas.Admin.Models.StringResource
{
    public class StringResourceViewModel
    {
        public StringResourceKey ResourceKey { get; set; }
        public IList<StringResourceValue> ResourceValues { get; set; } 
    }
}