﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainProject.Web.Areas.Admin.Models
{
    public class PagingViewModel
    {
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public string BaseUrl { get; set; }
        
    }
}