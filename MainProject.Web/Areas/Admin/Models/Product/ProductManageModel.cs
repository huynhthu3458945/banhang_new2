﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.Products
{
    public class ProductManageModel:BaseModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string SeName { get; set; }
        public string ShortDescription { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [AllowHtml]
        public string Preserve { get; set; }
        public string ImageDefault { get; set; }
        public string ImageFolder { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập số thứ tự!")]
        public int Order { get; set; }
        public bool IsPublished { get; set; }
        public bool IsPriceContact { get; set; }
        public decimal Price { get; set; }
        public bool IsPromotion { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập phần trăm khuyến mãi!")]
        public int PromotionPercent { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập giá khuyến mãi!")]
        public decimal PromotionPrice { get; set; }
        public string ProductCode { get; set; }
        public bool IsVideo { get; set; }
        public string VideoId { get; set; }
        public bool IsLandingpage { get; set; }
        [AllowHtml]
        public string SalesCommitment { get; set; }
        [AllowHtml]
        public string DetailInfo { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn danh mục!")]
        public long CategorySelectedValue { get; set; }
        public List<SelectListItem> Categories { get; set; }

        public static void ToModel(ProductManageModel model, ref Core.Product entity)
        {
            model.Name = entity.Name;
            model.ShortDescription = entity.ShortDescription;
            model.Description = entity.Description;
            model.Preserve = entity.Preserve;
            model.ImageDefault = entity.ImageDefault;
            model.ImageFolder = entity.ImageFolder;
            model.IsPublished = entity.IsPublished;
            model.Order = entity.Order;
            model.IsPriceContact = entity.IsPriceContact;
            model.Price = entity.Price;
            model.IsPromotion = entity.IsPromotion;
            model.PromotionPrice = entity.PromotionPrice;
            model.PromotionPercent = entity.PromotionPercent;
            model.ProductCode = entity.ProductCode;
            model.OriginalValue = entity.OriginalValue;
            model.IsVideo = entity.IsVideo;
            model.VideoId = entity.VideoId;
            model.SalesCommitment = entity.SalesCommitment;
            model.DetailInfo = entity.DetailInfo;
            model.IsLandingpage = entity.IsLandingpage;
            
        }
        public static void ToEntity(ProductManageModel model, ref Core.Product entity)
        {
            entity.Name = model.Name;
            entity.ShortDescription = model.ShortDescription;
            entity.Description = model.Description;
            entity.Preserve = model.Preserve;
            entity.ImageDefault = model.ImageDefault;
            entity.ImageFolder = model.ImageFolder;
            entity.IsPublished = model.IsPublished;
            entity.Order = model.Order;
            entity.IsPriceContact = model.IsPriceContact;
            entity.Price = model.Price;
            entity.IsPromotion = model.IsPromotion;
            entity.PromotionPercent = model.PromotionPercent;
            entity.PromotionPrice = model.PromotionPrice;
            entity.ProductCode = model.ProductCode;
            entity.OriginalValue = model.OriginalValue;
            entity.IsVideo = model.IsVideo;
            entity.VideoId = model.VideoId;
            entity.SalesCommitment = model.SalesCommitment;
            entity.DetailInfo = model.DetailInfo;
            entity.IsLandingpage = model.IsLandingpage;
        }
    }
}