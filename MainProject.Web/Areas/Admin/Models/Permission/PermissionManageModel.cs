﻿namespace MainProject.Web.Areas.Admin.Models.Permission
{
    public class PermissionManageModel
    {
        public string UserName { get; set; }

        public int LanguageId { get; set; }

        public long EntityTypeValue { get; set; }

        public bool IsEditor { get; set; }

        public bool IsManager { get; set; }
    }
}