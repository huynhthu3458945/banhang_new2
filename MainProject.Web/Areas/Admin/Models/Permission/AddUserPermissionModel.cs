﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MainProject.Web.Areas.Admin.Models.Permission
{
    public class AddUserPermissionModel
    {
        public string UserNameSelected { get; set; }

        public List<SelectListItem> UserNames { get; set; }

        public int ParentSelectedValue { get; set; }

        public List<SelectListItem> Parents { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }

        public List<SelectListItem> Languages { get; set; }

    }
}