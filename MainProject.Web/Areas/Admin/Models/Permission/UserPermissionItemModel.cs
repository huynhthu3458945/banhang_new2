﻿namespace MainProject.Web.Areas.Admin.Models.Permission
{
    public class UserPermissionItemModel
    {
        public string UserName { get; set; }

        public long EntityId { get; set; }

        public string Language { get; set; }

        public string FeatureName { get; set; }

        public bool IsEditor { get; set; }

        public bool IsManager { get; set; }
    }
}