﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Framework.Models;

namespace MainProject.Web.Areas.Admin.Models
{
    public class IndexViewModel<T> where T : class
    {
        public IList<T> ListItems { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public PagingModel PagingViewModel { get; set; }
        
    }
}