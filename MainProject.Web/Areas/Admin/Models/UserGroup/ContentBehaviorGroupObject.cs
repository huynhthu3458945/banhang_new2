﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.UserInfos;

namespace MainProject.Web.Areas.Admin.Models.UserGroupManage
{
    public class ContentBehaviorGroupObject
    {
        public ContentGroup ContentGroup { get; set; }
        public List<ContentBehaviorObject> ContentBehaviorObjects { get; set; }
    }
}