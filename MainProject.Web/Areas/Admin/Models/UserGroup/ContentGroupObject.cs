﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.UserInfos;

namespace MainProject.Web.Areas.Admin.Models.UserGroupManage
{
    public class ContentGroupObject
    {
        public ContentGroup ContentGroup { get; set; }
        public List<Content> Contents { get; set; }
    }
}