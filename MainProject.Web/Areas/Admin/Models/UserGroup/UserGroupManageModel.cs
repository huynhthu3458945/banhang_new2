﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.UserInfos;

namespace MainProject.Web.Areas.Admin.Models.UserGroupManage
{
    public class UserGroupManageModel
    {
        // Role properties
        public int RoleId { get; set; }

        [StringLength(50, MinimumLength = 5, ErrorMessage = "Độ dài tài khoản không quá 50 ký tự!")]
        [Required(ErrorMessage = "Vui lòng nhập tên nhóm tài khoản!")]
        [RegularExpression("([0-9a-zA-Z]+)", ErrorMessage = "Tên nhóm tài khoản không được có ký tự đặc biệt!")]
        public string RoleName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mô tả cho nhóm tài khoản!")]
        public string RoleDescription { get; set; }

        // List of Ids of ContentBehaviors in View, Create, Edit, Delete
        public List<long> ViewContentIds { get; set; }

        public List<long> CreateContentIds { get; set; }

        public List<long> EditContentIds { get; set; }

        public List<long> DeleteContentIds { get; set; }

        public List<long> DetailContentIds { get; set; }

        // Check all button
        public List<long> ContentIds { get; set; }
    }
}