﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.UserInfos;

namespace MainProject.Web.Areas.Admin.Models.UserGroupManage
{
    public class ContentBehaviorObject
    {
        public Content Content { get; set; }
        public ContentBehavior ViewContentBehavior { get; set; }
        public ContentBehavior CreateContentBehavior { get; set; }
        public ContentBehavior EditContentBehavior { get; set; }
        public ContentBehavior DeleteContentBehavior { get; set; }
        public ContentBehavior DetailContentBehavior { get; set; }
    }
}