﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.Enums;

namespace MainProject.Web.Areas.Admin.Models.Menu
{
    public class MenuViewModel 
    {
        public int Id { get; set; }

        public string Title { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn loại menu!")]
        public MenuTypeCollection MenuTypeSelected { get; set; }
        public List<SelectListItem> MenuTypes { get; set; }

        public int Order { get; set; }

        public static void ToModel(MenuViewModel model, ref Core.Menu entity)
        {
            model.Title = entity.Title;
            model.Order = entity.Order;
            model.MenuTypeSelected = entity.MenuType;
        }
        public static void ToEntity(MenuViewModel model, ref Core.Menu entity)
        {
            entity.Title = model.Title;
            entity.Order = model.Order;
            entity.MenuType = model.MenuTypeSelected;
        }
    }
}