﻿using System.Collections.Generic;

namespace MainProject.Web.Areas.Admin.Models
{

    public class EntityWithLanguageVersionsModel<T> where T:class
    {
        public T Entity { get; set; }
        public IList<T> LanguageVersions { get; set; }
    }
}