﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MainProject.Web.Areas.Admin.Models
{
    public class FatherSelectModel
    {
        public long FatherSelectedValue { get; set; }

        public string FatherSelectedStringValue { get; set; }

        public List<SelectListItem> Fathers { get; set; }
    }
}