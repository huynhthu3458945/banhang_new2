﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.Solution
{
    public class SolutionViewModel : BaseLanguageModel
    {
        public SolutionViewModel()
        {
            Order = 0;
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }

        public static void ToEntity(SolutionViewModel model, ref Core.Solution entity)
        {
            entity.Title = model.Title;
            entity.Link = model.Link;
            entity.IsPublished = model.IsPublished;
            entity.Order = model.Order;
            entity.OriginalValue = model.OriginalValue;
        }

        public static void ToModel(SolutionViewModel model, ref Core.Solution entity)
        {
            model.Title = entity.Title;
            model.Link = entity.Link;
            model.IsPublished = entity.IsPublished;
            model.Order = entity.Order;
            model.OriginalValue = entity.OriginalValue;
        }
    }
}