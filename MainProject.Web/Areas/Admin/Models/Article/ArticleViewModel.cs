﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.Article
{
    public class ArticleViewModel:BaseModel
    {
        public long Id { get; set; }
        
        public string Title { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        [AllowHtml]
        public string Body { get; set; }

        public string ImageDefault { get; set; }

        public string BigImage { get; set; }

        public string ImageFolder { get; set; }

        public bool IsPublished { get; set; }
        public bool IsHot { get; set; }
        public bool IsIntroduction { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn danh mục!")]
        public long CategorySelectedValue { get; set; }

        public List<SelectListItem> Categories { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }

        public List<SelectListItem> Languages { get; set; }

        public int Order { get; set; }

        public long LogHistoryId { get; set; }

        public DateTime CreateDate { get; set; }

        public ArticleViewModel() { }

        public ArticleViewModel(Core.Article entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Description = entity.Description;
            Body = entity.Body;
            ImageDefault = entity.ImageDefault;
            BigImage = entity.BigImage;
            ImageFolder = entity.ImageFolder;
            IsPublished = entity.IsPublished;
            Order = entity.Order;
            MetaDescription = entity.MetaDescription;
            MetaKeywords = entity.MetaKeywords;
            MetaTitle = entity.MetaTitle;
            OriginalValue = entity.OriginalValue;
            IsHot = entity.IsHot;
            IsIntroduction = entity.IsIntroduction;
        }

        public static void ToEntity(ArticleViewModel model, ref Core.Article entity)
        {
            entity.Title = model.Title;
            entity.Body = model.Body;
            entity.Description = model.Description;
            entity.ImageDefault = model.ImageDefault;
            entity.BigImage = model.BigImage;
            entity.ImageFolder = model.ImageFolder;
            entity.IsPublished = model.IsPublished;
            entity.Order = model.Order;
            entity.MetaDescription = model.MetaDescription;
            entity.MetaKeywords = model.MetaKeywords;
            entity.MetaTitle = model.MetaTitle;
            entity.OriginalValue = model.OriginalValue;
            entity.IsHot = model.IsHot;
            entity.IsIntroduction = model.IsIntroduction;
            if (string.IsNullOrEmpty(model.MetaImage) && !string.IsNullOrEmpty(model.ImageDefault))
            {
                entity.MetaImage = model.ImageDefault;
            }
            else
            {
                entity.MetaImage = model.MetaImage;
            }
        }

    }

}