﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MainProject.Web.Areas.Admin.Models
{
    public class LanguageSelectModel
    {
        public int LanguageSelectedValue { get; set; }

        public List<SelectListItem> Languages { get; set; }
    }
}