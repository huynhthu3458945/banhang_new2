﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Framework.Models;
using MainProject.Core.Enums;

namespace MainProject.Web.Areas.Admin.Models.Order
{
    public class DetailManageModel
    {
        public long Id { get; set; }

        public string BuyerName { get; set; }

        public string BuyerEmail { get; set; }

        public string BuyerPhone { get; set; }

        public string BuyerAddress { get; set; }

        public DateTime OrderTime { get; set; }

        public decimal Amount { get; set; }

        public string OrderStatus { get; set; }

        public string OrderType { get; set; }

        public OrderStatusCollection OrderStatusSelected { get; set; }

        public List<SelectListItem> OrderStatusItems { get; set; }

        public List<OrderItem> OrderItems { get; set; }

        public List<OrderLogItemModel> OrderLogItems { get; set; }

    }

    public class OrderLogItemModel
    {
        public long Id { get; set; }

        public string ActionBy { get; set; }

        public DateTime ActionTime { get; set; }

        public string OrderLogType { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public string Notes { get; set; }
    }

}