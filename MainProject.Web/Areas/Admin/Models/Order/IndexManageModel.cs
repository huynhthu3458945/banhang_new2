﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Framework.Models;

namespace MainProject.Web.Areas.Admin.Models.Order
{
    public class IndexManageModel
    {
        public string BuyerName { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public OrderStatusCollection OrderStatusSelected { get; set; }

        public List<SelectListItem> OrderStatusItems { get; set; }

        public List<Core.Order> Orders { get; set; }

        public PagingModel PagingModel { get; set; }

    }

}