﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MainProject.Web.Areas.Admin.Models.Setting
{
    public class SettingManagerViewModel
    {
        public SettingManagerViewModel(){}
        public SettingManagerViewModel(Core.Setting setting)
        {
            Id = setting.Id;
            Key = setting.Key;
            Value = setting.Value;
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Nhập key")]
        [RegularExpression("([0-9a-zA-Z]+)", ErrorMessage = "Vui lòng không nhập ký tự đặc biệt!")]
        public string Key { get; set; }

        [Required(ErrorMessage = "Nhập value")]
        [AllowHtml]
        public string Value { get; set; }
    }
}