﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MainProject.Core.Enums;
using MainProject.Framework.Models;

namespace MainProject.Web.Areas.Admin.Models
{
    public class FilterViewModel
    {
        public EntityTypeCollection EntityType { get; set; }
        public LanguageSelectModel LanguageViewModel { get; set; }
        public FatherSelectModel FatherSelectModel { get; set; }
        public FatherSelectModel OtherFatherSelectModel { get; set; }
        public bool HasFatherSelect { get; set; }
        public string BaseUrl { get; set; }
        public string Text { get; set; }

        public FilterViewModel() { }

        public FilterViewModel(string baseUrl, LanguageSelectModel languageSelectModel)
        {
            LanguageViewModel = languageSelectModel;
            BaseUrl = baseUrl;
        }

        public FilterViewModel(string baseUrl, LanguageSelectModel languageSelectModel, FatherSelectModel fatherSelectModel)
        {
            LanguageViewModel = languageSelectModel;
            BaseUrl = baseUrl;
            HasFatherSelect = true;
        }
        public FilterViewModel(string baseUrl, LanguageSelectModel languageSelectModel, FatherSelectModel fatherSelectModel, FatherSelectModel otherSelectModel)
        {
            LanguageViewModel = languageSelectModel;
            BaseUrl = baseUrl;
            HasFatherSelect = true;
        }
    }
}