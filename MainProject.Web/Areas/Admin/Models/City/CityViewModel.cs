﻿using System;
using System.ComponentModel.DataAnnotations;
using MainProject.Core.Enums;
using MainProject.Core;

namespace MainProject.Web.Areas.Admin.Models.City
{
    public class CityViewModel
    {
        public CityViewModel()
        {
            Order = 0;
        }
        public int Id { get; set; }

        [Required(ErrorMessage = "Nhập tên tỉnh/thành phố!")]
        public string Name { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }
        public Guid OriginalValue { get; set; }
        [Required(ErrorMessage = "Nhập tọa độ Lng")]
        public double Lng { get; set; }
        [Required(ErrorMessage = "Nhập tọa độ Lat")]
        public double Lat { get; set; }

        public RegionTypeCollection RegionType { get; set; }

        public Region Parent { get; set; }

        public static void ToEntity(CityViewModel model, ref Core.Region entity)
        {
            entity.Name = model.Name;
            entity.IsPublished = model.IsPublished;
            entity.Order = model.Order;
            entity.Lng = model.Lng;
            entity.Lat = model.Lat;
            entity.RegionType = RegionTypeCollection.City;
            entity.Parent = null;
        }

        public static void ToManageModel(CityViewModel model, ref Core.Region entity)
        {
            model.Name = entity.Name;
            model.IsPublished = entity.IsPublished;
            model.Order = entity.Order;
            model.Lng = entity.Lng;
            model.Lat = entity.Lat;
            model.RegionType = entity.RegionType;
            model.Parent = entity.Parent;
        }
    }
}