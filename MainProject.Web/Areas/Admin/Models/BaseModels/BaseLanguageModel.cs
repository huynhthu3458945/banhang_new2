﻿using System;

namespace MainProject.Web.Areas.Admin.Models.BaseModels
{
    public class BaseLanguageModel
    {
        public Guid OriginalValue { get; set; }
    }
}