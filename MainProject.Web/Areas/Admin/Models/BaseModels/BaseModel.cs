﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Web.Areas.Admin.Models.BaseModels
{
    public class BaseModel:BaseLanguageModel
    {
        [StringLength(500)]
        public string MetaTitle { get; set; }

        [StringLength(500)]
        public string MetaDescription { get; set; }

        [StringLength(500)]
        public string MetaKeywords { get; set; }

        public string MetaImage { get; set; }
    }
}