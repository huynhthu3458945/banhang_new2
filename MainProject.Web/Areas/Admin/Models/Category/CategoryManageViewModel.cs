﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Web.Areas.Admin.Models.BaseModels;

namespace MainProject.Web.Areas.Admin.Models.Category
{
    public class CategoryManageViewModel: BaseModel
    {
        public long Id { get; set; }

        [StringLength(55,ErrorMessage = "Độ dài tên danh mục không được quá 55 ký tự!")]
        [Required(ErrorMessage = "Vui lòng điền tên danh mục!")]
        public string Title { get; set; }

        [AllowHtml]
        public string Description { get; set; }
        [AllowHtml]
        public string Benefit { get; set; }
        [AllowHtml]
        public string Why { get; set; }

        public string ImageDefault { get; set; }

        public string ImageDefault2 { get; set; }

        public string ImageFolder { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Vui lòng chỉ nhập số!")]
        public int Order { get; set; }

        public bool IsPublished { get; set; }

        public bool IsShowOnMenu { get; set; }

        public int ParentSelectedValue { get; set; }

        public List<SelectListItem> Parents { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngôn ngữ!")]
        public int LanguageSelectedValue { get; set; }
        public List<SelectListItem> Languages { get; set; }

        public DisplayTemplateCollection DisplayTemplateSelected { get; set; }

        public List<SelectListItem> DisplayTemplates { get; set; }

        public bool PrivateArea { get; set; }

        public string ExternalLink { get; set; }

        public string BannerLink { get; set; }

        public bool IsHotProductCategory { get; set; }

        public string ImageIcon { get; set; }
        public string LinkYouTuBe { get; set; }
    }
}