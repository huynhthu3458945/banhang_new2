﻿namespace MainProject.Web.Areas.Admin.Models.Category
{
    public class CategoryItemViewModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Lanaguage { get; set; }

        public int Order { get; set; }

        public bool IsPublished { get; set; }
    }
}