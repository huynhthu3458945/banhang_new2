﻿
namespace MainProject.Web.Areas.Admin.Models.DownloadFile
{
    public class DownloadFileViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageDefault { get; set; }
        public string ImageFolder { get; set; }
        public string FilePath { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }

        public static void ToModel(DownloadFileViewModel model, ref Core.DownloadFile entity)
        {
            model.Id = entity.Id;
            model.Title = entity.Title;
            model.Description = entity.Description;
            model.ImageDefault = entity.ImageDefault;
            model.ImageFolder = entity.ImageFolder;
            model.FilePath = entity.FilePath;
            model.IsPublished = entity.IsPublished;
            model.Order = entity.Order;
        }
        public static void ToEntity(DownloadFileViewModel model, ref Core.DownloadFile entity)
        {
            entity.Title = model.Title;
            entity.Description = model.Description;
            entity.ImageDefault = model.ImageDefault;
            entity.ImageFolder = model.ImageFolder;
            entity.FilePath = model.FilePath;
            entity.IsPublished = model.IsPublished;
            entity.Order = model.Order;
        }
    }
}