﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MainProject.Core.Enums;

namespace MainProject.Web.Areas.Admin.Models.Districts
{
    public class DistrictViewModel
    {
        public DistrictViewModel()
        {
            Order = 0;
        }
        public int Id { get; set; }

        [Required(ErrorMessage = "Nhập tên quận/huyện!")]
        public string Name { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }
        public Guid OriginalValue { get; set; }
        [Required(ErrorMessage = "Nhập tọa độ Lng")]
        public double Lng { get; set; }
        [Required(ErrorMessage = "Nhập tọa độ Lat")]
        public double Lat { get; set; }

        public RegionTypeCollection RegionType { get; set; }

        public int ParentSelectedValue { get; set; }
        public List<SelectListItem> Parents { get; set; }

        public static void ToEntity(DistrictViewModel model, ref Core.Region entity)
        {
            entity.Name = model.Name;
            entity.IsPublished = model.IsPublished;
            entity.Order = model.Order;
            entity.Lng = model.Lng;
            entity.Lat = model.Lat;
            entity.RegionType = RegionTypeCollection.District;
        }

        public static void ToManageModel(DistrictViewModel model, ref Core.Region entity)
        {
            model.Name = entity.Name;
            model.IsPublished = entity.IsPublished;
            model.Order = entity.Order;
            model.Lng = entity.Lng;
            model.Lat = entity.Lat;
            model.RegionType = entity.RegionType;
        }
    }
}