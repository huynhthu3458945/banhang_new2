﻿
using MainProject.Core.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MainProject.Web.Areas.Admin.Models.Partner
{
    public class PartnerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageDefault { get; set; }
        public string ImageFolder { get; set; }
        public string ImageAlt { get; set; }
        public bool IsPublished { get; set; }
        public int Order { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn Loại")]
        public PartnerTypeCollection PartnerTypeSelected { get; set; }
        public List<SelectListItem> PartnerTypeSelecteds { get; set; }

        public static void ToModel(PartnerViewModel model, ref Core.Partner entity)
        {
            model.Name = entity.Name;
            model.ImageDefault = entity.ImageDefault;
            model.ImageFolder = entity.ImageFolder;
            model.ImageAlt = entity.ImageAlt;
            model.IsPublished = entity.IsPublished;
            model.Order = entity.Order;
            model.PartnerTypeSelected = entity.Type;
        }
        public static void ToEntity(PartnerViewModel model, ref Core.Partner entity)
        {
            entity.Name = model.Name;
            entity.ImageDefault = model.ImageDefault;
            entity.ImageFolder = model.ImageFolder;
            entity.IsPublished = model.IsPublished;
            entity.ImageAlt = model.ImageAlt;
            entity.Order = model.Order;
            entity.Type = model.PartnerTypeSelected;
        }
    }
}