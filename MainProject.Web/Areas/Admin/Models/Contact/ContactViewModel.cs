﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MainProject.Framework.Models;

namespace MainProject.Web.Areas.Admin.Models.Contact
{
    public class ContactViewModel
    {
        public List<MainProject.Core.Contacts> ListContactContent { get; set; }
        public PagingModel PagingViewModel { get; set; }
        public string Name { get; set; }
    }
    public class ContactEditModel
    {
        public long Id { get; set; }
        public string Note { get; set; }
    }
}