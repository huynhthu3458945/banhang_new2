﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class CityHelper
    {
        public static List<SelectListItem> BindSelectListItemDistrict(GenericRepository<Region> regionRepository, int selectedValue, EntityTypeCollection entityType = EntityTypeCollection.Regions)
        {
            var model = new List<SelectListItem>();
            IQueryable<Region> sql;
            sql = regionRepository.Find(c => c.IsPublished && c.Parent == null);
            var roots = sql.ToList();
            foreach (var city in roots)
            {
                var selectListItem = new SelectListItem
                {
                    Selected = city.Id == selectedValue,
                    Text = city.Name,
                    Value = city.Id.ToString()
                };
                model.Add(selectListItem);
            }
            return model;
        }
        public static List<SelectListItem> BindSelectListItemBranch(GenericRepository<Region> regionRepository, int selectedValue, EntityTypeCollection entityType = EntityTypeCollection.Regions)
        {
            var model = new List<SelectListItem>();
            IQueryable<Region> sql;
            sql = regionRepository.Find(c =>c.IsPublished && c.Parent == null);
            var roots = sql.ToList();
            foreach (var city in roots)
            {
                var selectListItem = new SelectListItem
                {
                    Selected = city.Id == selectedValue,
                    Text = city.Name,
                    Value = city.Id.ToString()
                };
                model.Add(selectListItem);
            }
            return model;
        }
    }
}