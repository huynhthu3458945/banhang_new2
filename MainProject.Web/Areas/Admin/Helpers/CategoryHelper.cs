﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models.Category;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class CategoryHelper
    {
        #region selectedlist item
        public static List<SelectListItem> BindSelectListItem(GenericRepository<Category> categoryRepository, long selectedValue, int languageValue,
            long categoryIdExpect = 0, bool isIncludeRootCategory = true, EntityTypeCollection entityType = EntityTypeCollection.Categories)
        {
            long rootId = (int)entityType == 12 ? -1 : 0;
            switch (entityType)
            {
                case EntityTypeCollection.Categories:
                    rootId = ConfigItemHelper.GetRootNewsId();
                    break;
                case EntityTypeCollection.AttachmentCategories:
                    rootId = ConfigItemHelper.GetRootAttachmentId();
                    break;
            }
            var model = new List<SelectListItem>();
            if (isIncludeRootCategory)
            {
                model.Add(new SelectListItem
                              {
                                  Value = rootId.ToString(),
                                  Text = "Danh mục gốc!",
                                  Selected = rootId == selectedValue
                              });
            }

            IQueryable<Category> sql;
            sql = rootId == -1
                      ? categoryRepository.Find(c => c.Parent.Parent == null && c.Language.Id == languageValue)
                      : categoryRepository.Find(c => c.Parent.Id == rootId && c.Language.Id == languageValue);
            var roots = sql.ToList();
            foreach (var category in roots)
            {
                if (category.Id == categoryIdExpect) continue;
                var selectListItem = new SelectListItem
                {
                    Selected = category.Id == selectedValue,
                    Text = category.Title,
                    Value = category.Id.ToString()
                };
                model.Add(selectListItem);
                model.AddRange(BuildSelectListItemsOfChild(categoryRepository, selectedValue, category.Id,
                                                           selectListItem.Text, categoryIdExpect));
            }

            return model;
        }
        private static IList<SelectListItem> BuildSelectListItemsOfChild(GenericRepository<Category> categoryRepository, long selectedValue, long categoryId, string prefix, long categoryIdExpect)
        {
            var listResult = new List<SelectListItem>();
            foreach (var item in categoryRepository.Find(c => c.Parent.Id == categoryId).ToList())
            {
                if (item.Id == categoryIdExpect) break;
                var selectListItem = new SelectListItem
                {
                    Selected = item.Id == selectedValue,
                    Text = prefix + " >> " + item.Title,
                    Value = item.Id.ToString()
                };
                listResult.Add(selectListItem);
                listResult.AddRange(BuildSelectListItemsOfChild(categoryRepository, selectedValue, item.Id, selectListItem.Text, categoryIdExpect));
            }

            return listResult;
        }

        public static List<SelectListItem> BindSelectListItemArticle(GenericRepository<Category> categoryRepository, long selectedValue, int languageValue,
            long categoryIdExpect = 0, bool isIncludeRootCategory = true, EntityTypeCollection entityType = EntityTypeCollection.Categories)
        {
            long rootId = (int)entityType == 12 ? -1 : 0;
            switch (entityType)
            {
                case EntityTypeCollection.Categories:
                    rootId = ConfigItemHelper.GetRootNewsId();
                    break;
                case EntityTypeCollection.AttachmentCategories:
                    rootId = ConfigItemHelper.GetRootAttachmentId();
                    break;
            }
            var model = new List<SelectListItem>();
            if (isIncludeRootCategory)
            {
                model.Add(new SelectListItem
                {
                    Value = rootId.ToString(),
                    Text = "Danh mục gốc!",
                    Selected = rootId == selectedValue
                });
            }
            IQueryable<Category> sql;
            sql = rootId == -1
                      ? categoryRepository.Find(c => c.Parent.Parent == null && c.Language.Id == languageValue)
                      : categoryRepository.Find(c => c.Parent.Id == rootId && c.Language.Id == languageValue);
            var roots = sql.Where(x => x.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate
                                    || x.DisplayTemplate == DisplayTemplateCollection.Child1AdviseTemplate
                                    || x.DisplayTemplate == DisplayTemplateCollection.PurchaseTemplate
                                    || x.DisplayTemplate == DisplayTemplateCollection.ExchangeTemplate
                                    || x.DisplayTemplate == DisplayTemplateCollection.PayTemplate
                                    || x.DisplayTemplate == DisplayTemplateCollection.SecurityTemplate
                                    ).ToList();
            foreach (var category in roots)
            {
                if (category.Id == categoryIdExpect) continue;
                var selectListItem = new SelectListItem
                {
                    Selected = category.Id == selectedValue,
                    Text = category.Title,
                    Value = category.Id.ToString()
                };
                model.Add(selectListItem);
                model.AddRange(BuildSelectListItemsOfChild(categoryRepository, selectedValue, category.Id,
                                                           selectListItem.Text, categoryIdExpect));
            }
            return model;
        }
        public static List<SelectListItem> BindSelectListItemProduct(GenericRepository<Category> categoryRepository, long selectedValue, int languageValue,
            long categoryIdExpect = 0, bool isIncludeRootCategory = true, EntityTypeCollection entityType = EntityTypeCollection.Categories)
        {
            long rootId = (int)entityType == 12 ? -1 : 0;
            switch (entityType)
            {
                case EntityTypeCollection.Categories:
                    rootId = ConfigItemHelper.GetRootNewsId();
                    break;
                case EntityTypeCollection.AttachmentCategories:
                    rootId = ConfigItemHelper.GetRootAttachmentId();
                    break;
            }
            var model = new List<SelectListItem>();
            if (isIncludeRootCategory)
            {
                model.Add(new SelectListItem
                {
                    Value = rootId.ToString(),
                    Text = "Danh mục gốc!",
                    Selected = rootId == selectedValue
                });
            }
            IQueryable<Category> sql;
            sql = rootId == -1
                      ? categoryRepository.Find(c => c.Parent.Parent == null && c.Language.Id == languageValue && (c.DisplayTemplate == DisplayTemplateCollection.ProductTemplate || c.DisplayTemplate == DisplayTemplateCollection.YenChungTemplate))
                      : categoryRepository.Find(c => c.Parent.Id == rootId && c.Language.Id == languageValue && (c.DisplayTemplate == DisplayTemplateCollection.ProductTemplate || c.DisplayTemplate == DisplayTemplateCollection.YenChungTemplate));
            var roots = sql.ToList();
            foreach (var category in roots)
            {
                if (category.Id == categoryIdExpect) continue;
                var selectListItem = new SelectListItem
                {
                    Selected = category.Id == selectedValue,
                    Text = category.Title,
                    Value = category.Id.ToString()
                };
                model.Add(selectListItem);
                model.AddRange(BuildSelectListItemsOfChild(categoryRepository, selectedValue, category.Id,
                                                           selectListItem.Text, categoryIdExpect));
            }
            return model;
        }
        public static List<SelectListItem> BindSelectListItemSlideShow(GenericRepository<Category> categoryRepository, long selectedValue, int languageValue,
            long categoryIdExpect = 0, bool isIncludeRootCategory = true, EntityTypeCollection entityType = EntityTypeCollection.Categories)
        {
            long rootId = (int)entityType == 12 ? -1 : 0;
            switch (entityType)
            {
                case EntityTypeCollection.Categories:
                    rootId = ConfigItemHelper.GetRootNewsId();
                    break;
                case EntityTypeCollection.AttachmentCategories:
                    rootId = ConfigItemHelper.GetRootAttachmentId();
                    break;
            }
            var model = new List<SelectListItem>();
            if (isIncludeRootCategory)
            {
                model.Add(new SelectListItem
                {
                    Value = rootId.ToString(),
                    Text = "Danh mục gốc!",
                    Selected = rootId == selectedValue
                });
            }
            IQueryable<Category> sql;
            sql = rootId == -1
                      ? categoryRepository.Find(c => c.Parent.Parent == null && c.Language.Id == languageValue)
                      : categoryRepository.Find(c => c.Parent.Id == rootId && c.Language.Id == languageValue );
            var roots = sql.Where(x => x.IsPublished).OrderBy(x => x.Order).ToList();
            foreach (var category in roots)
            {
                if (category.Id == categoryIdExpect) continue;
                var selectListItem = new SelectListItem
                {
                    Selected = category.Id == selectedValue,
                    Text = category.Title,
                    Value = category.Id.ToString()
                };
                model.Add(selectListItem);
            }
            return model;
        }
        #endregion

        #region Build categories tree for all category
        public static List<CategoryItemViewModel> BuildCategoryModel(GenericRepository<Category> categoryRepository, int languageSelectedValue, EntityTypeCollection entityType)
        {
            long rootId = 0;
            switch (entityType)
            {
                case EntityTypeCollection.Categories:
                    rootId = ConfigItemHelper.GetRootNewsId();
                    break;
                case EntityTypeCollection.AttachmentCategories:
                    rootId = ConfigItemHelper.GetRootAttachmentId();
                    break;
               
            }
            var rootCategories =
                categoryRepository.Find(c => c.Parent.Id == rootId && c.Language.Id == languageSelectedValue).OrderBy(c => c.Order).ToList();

            var listResult = new List<CategoryItemViewModel>();
            foreach (var category in rootCategories)
            {
                var item = BuildCategoryModel(category, "");
                listResult.Add(item);
                listResult.AddRange(BuildChildCategoryModels(categoryRepository, item));
            }

            return listResult;
        }

        private static CategoryItemViewModel BuildCategoryModel(Category category, string parentPrefix)
        {
            var model = new CategoryItemViewModel
                            {
                                Id = category.Id,
                                Title = string.IsNullOrEmpty(parentPrefix) ? category.Title : parentPrefix + " >> " + category.Title,
                                Order = category.Order,
                                IsPublished = category.IsPublished
                            };
            return model;
        }

        private static List<CategoryItemViewModel> BuildChildCategoryModels(GenericRepository<Category> categoryRepository, CategoryItemViewModel categoryItemViewModel)
        {
            var listResult = new List<CategoryItemViewModel>();

            foreach (var item in categoryRepository.Find(c => c.Parent.Id == categoryItemViewModel.Id).OrderBy(c => c.Order).ToList())
            {
                var childItem = BuildCategoryModel(item, categoryItemViewModel.Title);
                listResult.Add(childItem);
                listResult.AddRange(BuildChildCategoryModels(categoryRepository, childItem));
            }
            return listResult;
        }
        #endregion

        public static List<long> BuildFamilyCategoryIds(GenericRepository<Category> categoryRepository, long categoryId)
        {
            var list = new List<long> {categoryId};

            var childrens = categoryRepository.Find(c => c.Parent.Id == categoryId).ToList();
            foreach (var children in childrens)
            {
                list.AddRange(BuildFamilyCategoryIds(categoryRepository, children.Id));
            }

            return list.Distinct().ToList();
        }
    }
}