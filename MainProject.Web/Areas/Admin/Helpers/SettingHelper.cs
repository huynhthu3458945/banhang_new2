﻿using MainProject.Core;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class SettingHelper
    {
        public static bool CheckDuplicateKey(GenericRepository<Setting> repository,string key)
        {
            var entity = repository.FindUnique(d => d.Key == key);
            if (entity != null)
            {
                return false;
            }
            return true;
        }

        public static bool CheckDuplicateKey(GenericRepository<Setting> repository, string key, int id)
        {
            var entity = repository.FindUnique(d => d.Key == key && d.Id != id);
            if (entity == null)
            {
                return true;
            }
            return false;
        }
    }
}