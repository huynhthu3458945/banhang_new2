﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Framework.Helper;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class PermissionHelper
    {
        public static List<SelectListItem> GetFunctionPermissionRows()
        {
            var items = new List<SelectListItem>
                            {
                                new SelectListItem
                                    {
                                        Text = EntityManageTypeCollection.ManagePolls.GetDescription(),
                                        Value = ((int) EntityManageTypeCollection.ManagePolls).ToString()
                                    },
                                new SelectListItem
                                    {
                                        Text = EntityManageTypeCollection.ManageComments.GetDescription(),
                                        Value = ((int) EntityManageTypeCollection.ManageComments).ToString()
                                    }
                            };

            return items;
        }
    }
}