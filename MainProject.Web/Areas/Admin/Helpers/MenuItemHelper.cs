﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class MenuItemHelper
    {
        public static List<SelectListItem> BuildSelectListItemsByLanguageMenu(GenericRepository<MenuItem> menuItemRepository, int languageId, int menuId)
        {
            var selectListItems = new List<SelectListItem>();
            selectListItems.Insert(0, new SelectListItem
            {
                Text = "Menu gốc",
                Value = "0"
            });
            var menuItemsParent = menuItemRepository.FindAll();
            switch(languageId)
            {
                case -1: // In case: No language selected yet
                    return selectListItems;
                case 0: // In case: Select all languages
                    break;
                default: // In case: Select any language
                    menuItemsParent = menuItemsParent.Where(x => x.Language.Id == languageId);
                    break;
            }
            switch (menuId)
            {
                case -1: // In case: No menu selected yet
                    return selectListItems;
                case 0: // In case: Select all menus
                    break;
                default: // In case: Select any menu
                    menuItemsParent = menuItemsParent.Where(x => x.Menu.Id == menuId);
                    break;
            }
            selectListItems.AddRange(BuildMenuItems(menuItemRepository, menuItemsParent.Where(x => x.Parent == null).OrderBy(x => x.Order).ToList(), new List<SelectListItem>()));
            return selectListItems;
        }

        private static List<SelectListItem> BuildMenuItems(GenericRepository<MenuItem> menuItemRepository, List<MenuItem> menuItems, List<SelectListItem> selectListItems)
        {
            foreach (var menuItem in menuItems)
            {
                selectListItems.Add(new SelectListItem
                {
                    Text = BuildStringMenuItems(menuItemRepository, menuItem),
                    Value = menuItem.Id.ToString()
                });
                if (menuItemRepository.FindUnique(x => x.Parent.Id == menuItem.Id) != null)
                {
                    var subMenuItems = menuItemRepository.FindAll().Where(x => x.Parent.Id == menuItem.Id).OrderBy(x => x.Order).ToList();
                    BuildMenuItems(menuItemRepository, subMenuItems, selectListItems);
                }
            }
            return selectListItems;
        }

        private static string BuildStringMenuItems(GenericRepository<MenuItem> menuItemRepository, MenuItem menuItem)
        {
            if (menuItem.Parent != null)
            {
                return BuildStringMenuItems(menuItemRepository, menuItem.Parent) + ">>" + menuItem.Title;
            }
            return menuItem.Title;
        }
    }
}