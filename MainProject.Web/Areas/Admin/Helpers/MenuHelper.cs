﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class MenuHelper
    {
        public static List<SelectListItem> BuildSelectListItemsMenuItem(GenericRepository<Menu> menuRepository, int selectedValue)
        {
            var selectListItemsMenuItem = new List<SelectListItem>();
            
            var menus = menuRepository.FindAll().OrderBy(x => x.Order).ToList();
            foreach (var menu in menus)
            {
                selectListItemsMenuItem.Add(new SelectListItem
                    {
                        Text = menu.Title,
                        Value = menu.Id.ToString(),
                        Selected = menu.Id == selectedValue
                });
            }
            return selectListItemsMenuItem;
        }
    }
}