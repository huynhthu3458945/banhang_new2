﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class DistrictHelper
    {
        public static List<SelectListItem> BindSelectListItemBranch(GenericRepository<Region> regionRepository, int selectedValue, EntityTypeCollection entityType = EntityTypeCollection.Branches)
        {
            var model = new List<SelectListItem>();
            IQueryable<Region> sql;
            sql = regionRepository.Find(c => c.IsPublished && c.Parent != null);
            var roots = sql.ToList();
            foreach (var district in roots)
            {
                var selectListItem = new SelectListItem
                {
                    Selected = district.Id == selectedValue,
                    Text = district.Name,
                    Value = district.Id.ToString()
                };
                model.Add(selectListItem);
            }
            return model;
        }

        public static List<SelectListItem> GetDistrictByCity(GenericRepository<Region> regionRepository,int cityId, int selectedValue)
        {
            var model = new List<SelectListItem>();
            IQueryable<Region> sql;
            sql = regionRepository.Find(c => c.Parent.Id == cityId && c.IsPublished && c.Parent != null);
            var roots = sql.ToList();
            foreach (var district in roots)
            {
                var selectListItem = new SelectListItem
                {
                    Selected = district.Id == selectedValue,
                    Text = district.Name,
                    Value = district.Id.ToString()
                };
                model.Add(selectListItem);
            }
            return model;
        }
    }
}