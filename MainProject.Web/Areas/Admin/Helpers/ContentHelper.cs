﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core.UserInfos;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class ContentHelper
    {
        public static List<SelectListItem> BindSelectListItemContentBehavior(GenericRepository<Content> repository, long selectedValue)
        {
            var model = new List<SelectListItem>();
            IQueryable<Content> sql;
            sql = repository.FindAll().OrderByDescending(x=>x.Id);
            var contents = sql.ToList();
            foreach (var item in contents)
            {
                var selectListItem = new SelectListItem
                {
                    Selected = item.Id == selectedValue,
                    Text = item.Title,
                    Value = item.Id.ToString()
                };
                model.Add(selectListItem);
            }
            return model;
        }
    }
}