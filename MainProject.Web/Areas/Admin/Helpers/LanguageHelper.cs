﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using MainProject.Core;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class LanguageHelper
    {
        public static IList<Language> FindAvailableLanguagesToBind<T>(Guid originalValue, UnitOfWork unitOfWork) where T : class
        {
            var list = unitOfWork.LanguageRepository.FindAll().ToList();
            var languagesUsed = new List<Language>();
            if (typeof(T) == typeof(Article))
            {
                languagesUsed =
                    unitOfWork.ArticleRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(Category))
            {
                languagesUsed =
                    unitOfWork.CategoryRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(Product))
            {
                languagesUsed =
                     unitOfWork.ProductRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(SlideShow))
            {
                languagesUsed =
                     unitOfWork.SlideShowRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(OrderType))
            {
                languagesUsed =
                     unitOfWork.OrderTypeRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(MenuItem))
            {
                languagesUsed =
                     unitOfWork.MenuItemRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(Solution))
            {
                languagesUsed =
                     unitOfWork.SolutionRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(LandingpageGroup))
            {
                languagesUsed =
                     unitOfWork.LandingpageGroupRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(LandingpageItem))
            {
                languagesUsed =
                     unitOfWork.LandingpageItemRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            else if (typeof(T) == typeof(Feedback))
            {
                languagesUsed =
                     unitOfWork.FeedbackRepository.FindAll().Where(x => x.OriginalValue == originalValue).Select(x => x.Language).Distinct().
                        ToList();
            }
            return list.Except(languagesUsed).ToList();
        }

        public static List<SelectListItem> BindLanguage(IList<Language> languages, int selectedvalue)
        {
            return languages.Select(
                d => new SelectListItem
                {
                    Text = d.LanguageName,
                    Value = d.Id.ToString(),
                    Selected = selectedvalue == d.Id
                }).ToList();
        }
    }
}