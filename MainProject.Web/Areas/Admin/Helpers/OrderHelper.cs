﻿using System.Collections.Generic;
using System;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Web.Areas.Admin.Models.Order;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class OrderHelper
    {
        public static void UpdateOrderItemLogs(GenericRepository<OrderLog> orderLogRepository, DetailManageModel model, Order order)
        {
            var orderLog = new OrderLog
            {
                ActionBy = System.Web.HttpContext.Current.User.Identity.Name,
                ActionTime = DateTime.Now,
                NewValue = model.OrderStatusSelected.GetDescription(),
                OldValue = order.OrderStatus.GetDescription(),
                Order = order,
                OrderLogType = OrderLogTypeCollection.ChangeStatus
            };
            orderLogRepository.Insert(orderLog);
        }

        public static void UpdateOrder(DetailManageModel model, Order order)
        {
            order.OrderStatus = model.OrderStatusSelected;
        }
    }
}