﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core.UserInfos;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Web.Areas.Admin.Models.UserGroupManage;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class ContentGroupHelper
    {
        //public static string admin = "Admin";
        public static List<SelectListItem> BindSelectListItemContent(GenericRepository<ContentGroup> repository, int selectedValue)
        {
            var model = new List<SelectListItem>();
            IQueryable<ContentGroup> sql;
            sql = repository.FindAll().OrderByDescending(x=>x.Id);
            var groups = sql.ToList();
            foreach (var item in groups)
            {
                var selectListItem = new SelectListItem
                {
                    Selected = item.Id == selectedValue,
                    Text = item.GroupName,
                    Value = item.Id.ToString()
                };
                model.Add(selectListItem);
            }
            return model;
        }

        // Get ContentBehaviorGroupObjects
        public static List<ContentBehaviorGroupObject> GetContentBehaviorGroupObjects(GenericRepository<ContentGroup> groupRepository, GenericRepository<Content> contentRepository
                                                                    , GenericRepository<ContentBehavior> contentBehaviorRepository)
        {
            //Initial List of ContentBehaviorGroupObject
            var list1 = new List<ContentBehaviorGroupObject>();

            // Get all GroupContents
            var groups = groupRepository.FindAll().OrderBy(x => x.Order).ToList();

            // Get ContentBehaviors of group
            foreach (var gp in groups)
            {
                // List of ContentBehaviorObject
                var list2 = new List<ContentBehaviorObject>();
                var contents = contentRepository.FindAll().Where(x => x.ContentGroup.Id == gp.Id).OrderBy(x => x.Order).ToList();
                foreach (var ct in contents)
                {
                    var viewContentBehavior = contentBehaviorRepository.FindUnique(x => x.Content.Id == ct.Id && x.Behavior.Type
                                                == BehaviorTypeCollection.ViewTemplate);
                    var createContentBehavior = contentBehaviorRepository.FindUnique(x => x.Content.Id == ct.Id && x.Behavior.Type
                                                == BehaviorTypeCollection.CreateTemplate);
                    var editContentBehavior = contentBehaviorRepository.FindUnique(x => x.Content.Id == ct.Id && x.Behavior.Type
                                                == BehaviorTypeCollection.EditTemplate);
                    var deleteContentBehavior = contentBehaviorRepository.FindUnique(x => x.Content.Id == ct.Id && x.Behavior.Type
                                                == BehaviorTypeCollection.DeleteTemplate);
                    var detailContentBehavior = contentBehaviorRepository.FindUnique(x => x.Content.Id == ct.Id && x.Behavior.Type
                                                == BehaviorTypeCollection.DetailTemplate);
                    var obj2 = new ContentBehaviorObject
                    {
                        Content = ct,
                        ViewContentBehavior = viewContentBehavior,
                        CreateContentBehavior = createContentBehavior,
                        EditContentBehavior = editContentBehavior,
                        DeleteContentBehavior = deleteContentBehavior,
                        DetailContentBehavior = detailContentBehavior
                    };
                    list2.Add(obj2);
                }

                var obj1 = new ContentBehaviorGroupObject
                {
                    ContentGroup = gp,
                    ContentBehaviorObjects = list2
                };
                list1.Add(obj1);
            }
            return list1;
        }


        // Get ContentGroupObjects By RoleId
        public static List<ContentGroupObject> GetContentGroupObjectsByRoleId(GenericRepository<ContentGroup> groupRepository, GenericRepository<ContentBehavior> contentBehaviorRepository
                                                                            , GenericRepository<RoleContentBehavior> roleCBRepository, Role role)
        {
            // Initial List of ContentGroupObject
            var list1 = new List<ContentGroupObject>();

            // Get all GroupContents
            var groups = groupRepository.FindAll().OrderBy(x => x.Order).ToList();

            // Get current RoleContentBehaviors by RoleId
            var roleCBs = roleCBRepository.FindAll().Where(x => x.Role.RoleId == role.RoleId).ToList();

            // Initial List of Content
            var list2 = new List<Content>();

            // Get distinct Contents list of current RoleContentBehaviors
            foreach (var item in roleCBs)
            {
                if (list2.Contains(item.ContentBehavior.Content))
                {
                    continue;
                }
                else
                {
                    list2.Add(item.ContentBehavior.Content);
                }
            }

            // Get contents of group
            foreach (var gp in groups)
            {
                var contents = list2.Where(x => x.ContentGroup.Id == gp.Id).OrderBy(x => x.Order).ToList();

                var obj = new ContentGroupObject
                {
                    ContentGroup = gp,
                    Contents = contents
                };
                list1.Add(obj);
            }
            return list1;
        }
        public static void InsertRoleContentBehaviors(GenericRepository<RoleContentBehavior> roleCBRepository, GenericRepository<ContentBehavior> contentBehaviorRespository, Role role, List<long> contentBehaviorIds)
        {
            foreach (var id in contentBehaviorIds)
            {
                var cb = contentBehaviorRespository.FindById(id);
                var roleCB = new RoleContentBehavior { 
                    Role = role,
                    ContentBehavior = cb
                };
                roleCBRepository.Insert(roleCB);
                //roleCBRepository.SaveChanges();
            }
        }
        public static List<long> GetCheckAllIds(GenericRepository<RoleContentBehavior> roleCBRepository, GenericRepository<ContentBehavior> contentBehaviorRespository, Role role)
        {
            var checkAllIds = new List<long>();
            var contentIds = new List<long>();
            var roleCBs = roleCBRepository.FindAll().Where(x => x.Role.RoleId == role.RoleId).ToList();
            foreach (var item in roleCBs)
            {
                if (contentIds.Contains(item.ContentBehavior.Content.Id))
                {
                    continue;
                }
                else
                {
                    contentIds.Add(item.ContentBehavior.Content.Id);
                }
            }
            foreach (var contentId in contentIds)
            {
                // get RoleContentBehaviors by ContentId
                var list1 = roleCBRepository.FindAll().Where(x => x.ContentBehavior.Content.Id == contentId && x.Role.RoleId == role.RoleId).ToList();

                // get ContentBehaviors by ContentId
                var list2 = contentBehaviorRespository.FindAll().Where(x => x.Content.Id == contentId).ToList();

                // Checking if list1 equal list2 => add to checkAllIds List
                if (list1.Count == list2.Count)
                {
                    checkAllIds.Add(contentId);
                }
            }
            return checkAllIds;
        }
        public static void DeleteRoleContentBehaviorsByRoleId(GenericRepository<RoleContentBehavior> roleCBRepository, Role role)
        {
            var roleCBs = roleCBRepository.FindAll().Where(x => x.Role.RoleId == role.RoleId).ToList();
            foreach (var item in roleCBs)
            {
                roleCBRepository.Delete(item);
                //roleCBRepository.SaveChanges();
            }
        }
    }
}