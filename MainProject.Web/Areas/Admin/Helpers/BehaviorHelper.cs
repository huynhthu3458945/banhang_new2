﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core.UserInfos;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class BehaviorHelper
    {
        public static List<SelectListItem> BindSelectListItemContentBehavior(GenericRepository<Behavior> repository, int selectedValue)
        {
            var model = new List<SelectListItem>();
            IQueryable<Behavior> sql;
            sql = repository.FindAll();
            var behaviors = sql.ToList();
            foreach (var item in behaviors)
            {
                var selectListItem = new SelectListItem
                {
                    Selected = item.Id == selectedValue,
                    Text = item.Name,
                    Value = item.Id.ToString()
                };
                model.Add(selectListItem);
            }
            return model;
        }
        public static List<Behavior> GetBehaviorsByContent(GenericRepository<ContentBehavior> repository, long contentId)
        {
            return repository.FindAll().Where(x => x.Content.Id == contentId).Select(x => x.Behavior)
                                .OrderByDescending(x => x.Id).ToList();
        }
    }
}