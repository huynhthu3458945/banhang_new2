﻿using System.Web;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public class FileHelper
    {
        public static string GetFileName(string folderName, string name, string fileExtension)
        {
            var fn = HttpContext.Current.Server.MapPath(folderName + "/" + name + fileExtension);
            while (System.IO.File.Exists(fn))
            {
                if (name.Contains("-"))
                {
                    int n;
                    var str = name.Substring(name.LastIndexOf("-") + 1);
                    if (int.TryParse(str, out n))
                    {
                        n++;
                        name = name.Substring(0, name.LastIndexOf("-") + 1) + n;
                    }
                    else
                    {
                        name = name + "-1";
                    }
                }
                else
                {
                    name = name + "-1";
                }
                fn = HttpContext.Current.Server.MapPath(folderName + "/" + name + fileExtension);
            }
            return name;
        }
    }
}