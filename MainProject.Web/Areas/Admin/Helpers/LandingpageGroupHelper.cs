﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MainProject.Core;
using MainProject.Data;

namespace MainProject.Web.Areas.Admin.Helpers
{
    public static class LandingpageGroupHelper
    {
        public static List<SelectListItem> BuildSelectListItems(GenericRepository<LandingpageGroup> landingpageGroupRepository, int languageId, int selectedValue = 0)
        {
            var selectListItems = new List<SelectListItem>();
            var landingpageGroups = landingpageGroupRepository.FindAll();
            switch(languageId)
            {
                case -1: // In case: No language selected yet
                    return selectListItems;
                case 0: // In case: Select all languages
                    break;
                default: // In case: Select any language
                    landingpageGroups = landingpageGroups.Where(x => x.Language.Id == languageId);
                    break;
            }
            foreach (var landingpageGroup in landingpageGroups.OrderBy(x => x.Id).ToList())
            {
                selectListItems.Add(new SelectListItem
                {
                    Text = landingpageGroup.Title,
                    Value = landingpageGroup.Id.ToString(),
                    Selected = landingpageGroup.Id == selectedValue
                });
            }
            return selectListItems;
        }
    }
}