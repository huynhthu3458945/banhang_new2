var player;
function onYouTubeIframeAPIReady() {
	const elVideo = document.getElementById("video_banner");
	
	if(!elVideo){return;}
    var idVdeo = $('#video_banner').attr('data-idVideo');
    player = new YT.Player('video_banner', {
        videoId: idVdeo,
        playerVars: {
        	'autoplay': 1,
        	'loop': 0,
          'color': 'white',
          'controls':1,
          'mute': 1,
          'rel' : 0,
          'showinfo': 0,
        }
    });
}

const _setClickEventForPVideoBtn = (function() {
  var elPVideoBtn = $('.btn-pVideo');
  if(!elPVideoBtn) {return;}
  $('.btn-pVideo').on('click', function () {
      player.playVideo();
  });
  $('.view_youtube').click(function(){
    var url = $(this).attr('data-idVideo');
    player.cueVideoById(url);
    player.playVideo();
  })
})();

// end run video

const common = (function(){

	var iframe;    	

	const _generateSlider = function(){

		const elIndexBanner = document.getElementById('banner');
	
		if(!elIndexBanner){return;}
		
		$(elIndexBanner).slick({
	        infinite: true,
	        autoplay: true,
	        autoplaySpeed:3000,
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        arrows: false,
	        dots: true,
	        prevArrow: '<span class="ic-Arrow-left"></span>',
        	nextArrow: '<span class="ic-Arrow-right"></span>',
	    }).on('afterChange', function(event, slick, currentSlide, nextSlide){
	        if(currentSlide == 0){
	            // player.playVideo();
	            $(elIndexBanner).slick('slickPause');
	        }
	    });
	}; //end_generateSlider

	const _generateFacebookFanpage = function(){
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=704558949717927&autoLogAppEvents=1';
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	}; //end_generateFacebookFanpage
  const _generateIntrocude = function(){
    const elslideIntro = document.getElementById('slideintroduce');
    if(!elslideIntro) { return }
    $(elslideIntro).slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed:3000,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      prevArrow: '<span class="ic-Arrow-left"></span>',
      nextArrow: '<span class="ic-Arrow-right"></span>', 
    })

  }
  // slide tin tức
  const _generateNewslist = function(){
    const elnewslist = document.getElementById('newslist');
    if(!elnewslist) { return }
    $(elnewslist).slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed:3000,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      prevArrow: '<span class="ic-Arrow-left"></span>',
      nextArrow: '<span class="ic-Arrow-right"></span>', 
      responsive: [
        {
          breakpoint: 641,
          settings: {
            slidesToShow: 2,
          },
        },{
          breakpoint: 420,
          settings: {
            slidesToShow: 1,
          },
        }
       ]
    })

  }
  const _generateProductRelation = function(){
    const elproductrelationship= document.getElementById('product_relation_ship');
    if(!elproductrelationship) { return }
    $(elproductrelationship).slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed:3000,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      prevArrow: '<span class="ic-Arrow-left"></span>',
      nextArrow: '<span class="ic-Arrow-right"></span>', 
      responsive: [
        {
          breakpoint: 641,
          settings: {
            slidesToShow: 2,
          },
        },{
          breakpoint: 420,
          settings: {
            slidesToShow: 1,
          },
        }
       ]
    })

  }
  // slide đối tác
  const _generatelistpartner = function(){
    const ellistpartner= document.getElementById('list_partner');
    if(!ellistpartner) { return }
    $(ellistpartner).slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed:2000,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      prevArrow: '<span class="ic-Arrow-left"></span>',
      nextArrow: '<span class="ic-Arrow-right"></span>', 
      responsive: [
        {
          breakpoint: 641,
          settings: {
            slidesToShow: 2,
          },
        }
       ]
    })

  }
  const _filterCate = function(){
    const elfiltercate= document.getElementById('filter_cate');
    if(!elfiltercate) { return }
    $(elfiltercate).slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed:3000,
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      prevArrow: '<span class="ic-Arrow-left"></span>',
      nextArrow: '<span class="ic-Arrow-right"></span>', 
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
          },
        },{
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
          },
        },{
          breakpoint: 420,
          settings: {
            slidesToShow: 2,
          },
        }
       ]
     })
  }
  const _activefilterCate = function(){
    $('.item_cate').click(function(){
      $('.item_cate').removeClass('active');
      $(this).addClass('active');
    })
  }
  const _generateSlideProductDetail = function(){
    const thumnailimagesetail = document.getElementById('thumnail-images-detail');
    if(!thumnailimagesetail) { return }
    const elmainimagesdetail = document.getElementById('main-images-detail');
    if(!elmainimagesdetail) { return }
    $(thumnailimagesetail).slick({
      vertical: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: true,
      prevArrow: '<span class="ic-Arrow-left"></span>',
      nextArrow: '<span class="ic-Arrow-right"></span>',
      slidesToShow: 3,
      slidesToScroll: 1,
      verticalSwiping: true,
      autoplay: true,
      focusOnSelect: true,
      asNavFor: elmainimagesdetail, 
      responsive: [
        {
          breakpoint: 768,
          settings: {
            verticalSwiping: false,
            vertical: false,
          },
        }
      ]
    });
    $(elmainimagesdetail).slick({
      infinite: true,
      autoplay: false,
      autoplaySpeed:2000,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      fade: true,
      asNavFor: thumnailimagesetail, 
    })

  }
// end_validateCustomerInfoForm

  const _showMenuMoblie = function(){    
    $('.mobile-toggle').click(function(){
        $('.mobile-toggle .button_toggle .line2').toggleClass('line_hidden');
        $('.mobile-toggle .button_toggle .line:nth-child(1)').toggleClass('line-close-1');
        $('.mobile-toggle .button_toggle .line:nth-child(3)').toggleClass('line-close-3');
        $('.navigationmenu').toggleClass('nav_showmenu');
    });

    const $objMenuFirst = $('.list_menu >li >.arrow_menu_first'),
      $objElArrowMenuChild = $('.col_sub_menu  li >p');
      $objElArrowMenuChildLast = $('.list_sub_second>li>.arrow_menu_first');
      $backtomenu = document.getElementById('back_to_page');   
      if(!$backtomenu) { return }

    $objMenuFirst.click(function(e){   
        // $objElArrowMenuParent.removeClass('arrow_mn');
        const elappendmodal = '<div class="opacity opacifa-active" id="opacity"><div>';
        if($(this).parent().find('.subcatemenu,.subcatemenu_nochild').hasClass('subcatemenu_block')){
            $(this).parent().find('.subcatemenu,.subcatemenu_nochild').removeClass('subcatemenu_block');
            $(this).removeClass('arrow_mn');
            e.preventDefault();
        }else{
            $objMenuFirst.parent().find('.subcatemenu,.subcatemenu_nochild').removeClass('subcatemenu_block');
            $(this).parent().find('.subcatemenu,.subcatemenu_nochild').first().addClass('subcatemenu_block');
            $objMenuFirst.removeClass('arrow_mn');
            $(this).addClass('arrow_mn');
            $('.main_header').append('<div class="opacity opacifa-active" id="opacity"><div>');
            e.preventDefault();

        }
    });
    $backtomenu = document.getElementById('back_to_page');   
    if(!$backtomenu) { return }
    $('.back_to_page').click(function(){
      $('.subcatemenu,.subcatemenu_nochild').removeClass('subcatemenu_block');
      $('#opacity').remove();
    });  

    $objElArrowMenuChild.click(function(e){
        // $objElArrowMenuChild.removeClass('arrow_mn');
        if($(this).parent().parent().find('.list_sub_second').hasClass('list_sub_second_show')){
            $(this).parent().parent().find('.list_sub_second').removeClass('list_sub_second_show');
            $(this).removeClass('arrow_mn');
        }else{            
            $objElArrowMenuChild.parent().parent().find('.list_sub_second').removeClass('list_sub_second_show');
            $(this).parent().parent().find('.list_sub_second').addClass('list_sub_second_show');
            $objElArrowMenuChild.removeClass('arrow_mn');
            $(this).addClass('arrow_mn');
        }
    });
    $objElArrowMenuChildLast.click(function(e){
        if($(this).parent().find('ul').hasClass('menu_last_show')){
            $(this).parent().find('ul').removeClass('menu_last_show');
            $(this).removeClass('arrow_mn');
        }else{            
            $objElArrowMenuChildLast.parent().find('ul').removeClass('menu_last_show');
            $(this).parent().find('ul').addClass('menu_last_show');
            $objElArrowMenuChildLast.removeClass('arrow_mn');
            $(this).addClass('arrow_mn');
        }
    });
  };
  const _ArrayIntroSlider = function(){
    const objArrayIntro = document.getElementById('img_intro');   
    if(!objArrayIntro) { return }               
    $(objArrayIntro).slick({
        infinite: true,
        autoplay: false,
        autoplaySpeed:3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        prevArrow: '<span class="icon-Arrow-left"></span>',
        nextArrow: '<span class="icon-Arrow-right"></span>',
    });
  }
  const _ArrayMainProduct = function(){
    const objMainProduct = document.getElementById('main_pd');
    if(!objMainProduct) { return } 
      $(objMainProduct).slick({
      infinite: true,
      focusOnSelect: true,
      autoplay: true,
      autoplaySpeed:3000,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      prevArrow: '<span class="icon-Arrow-left"></span>',
        nextArrow: '<span class="icon-Arrow-right"></span>',
        responsive: [
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 425,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      }
      ]
  });
  }
  const _checkHederMb = function(){
    var widthScrem = $(window).width();    
    $(window).scroll(function(){
      if(widthScrem > 640){
        if ($(this).scrollTop() > 80) {
            $('header').addClass('header_scroll');
        } else {
            $('header').removeClass('header_scroll');
        }
      }
      
      else{
        $('header').removeClass('header_scroll');
      }
    });    
    
    $(window).resize(function() {
      widthScrem = $(window).width();
    });
  }
  
  const _toToppage = function(){
    const objToTop = document.getElementsByClassName('to_toppage');
    if(!objToTop) return false;
    $(objToTop).on('click',function(){
        $('html, body').animate({scrollTop : 0},500);
    });
  }
  
  const _modalShowPopup = function () {
    $('.dataModal').on('click', function (e) {
      const dataModal = $(this).attr('data-id');
      const idModalShow = document.getElementById(`${dataModal}`);
      $(idModalShow).addClass("modalshowactive")
      $('.opacity').remove();
      $('#index').append('<div class="opacity opacifa-active" id="opacity"><div>');
      e.preventDefault();
    })
    $('.closepopup').on('click', function () {
      const idModalClose = $(this).parents('.modalshowactive').attr('id');
      const ModalisClose = document.getElementById(`${idModalClose}`);
      ModalisClose.classList.remove("modalshowactive");
      $('.opacity').remove();
    });
  }

  const _mathCart = function(){
    const getBtnplus = document.querySelectorAll('.plus');
    if(!getBtnplus) { return }
    const getBtnminus = document.querySelectorAll('.minus');
    if(!getBtnminus) { return }
    $(getBtnplus).click(function(){
      var count = $(this).parent().find('input').val();
      count ++;
      $(this).parent().find('input').attr('value',count);
    })
    $(getBtnminus).click(function(){
       // var count = 1;
      var count = $(this).parent().find('input').val();
      if(count > 1){
        count --;
        $(this).parent().find('input').attr('value',count);
      }
    }) 
  }
  const _changeNumberByKey = function(){
    const getTxtChange = document.querySelectorAll('.txt_change');
    if(!getTxtChange) { return}
    $(getTxtChange).keypress(function(event){
      var key = window.event ? event.keyCode : event.which;
      if (event.keyCode === 8 || event.keyCode === 46) {
          return true;
      } else if ( key < 48 || key > 57 ) {
          return false;
      } else {
          return true;
      }
    });
    $(getTxtChange).change(function(){
      var pattern = /[^0-9]/igm ;
      var valCurrent = $(this).val();
      if(pattern.test(valCurrent)){
        $(this).val('1') ;
      }
    })
  }
  // const _showdialog = function(){
    // const elappendmodal = '<div class="opacity opacifa-active" id="opacity"><div>';
    // const elindex = document.getElementById('index');
    // const elunder = document.getElementById('under');
    // if(!elunder && !elindex) { return }
    // $('.target-modal').on('click',function(){
      // const dataModal = $(this).attr('data-id');
      // const idModalShow = document.getElementById(`${dataModal}`);
      // $(idModalShow).addClass("modalshowactive");
      // elindex.insertAdjacentHTML('beforeend',elappendmodal);
      // elunder.insertAdjacentHTML('beforeend',elappendmodal);
    // })
    // $('.btn-close-dialog').on('click',function(){
      // const idModalClose = $(this).parent().attr('id');
      // const ModalisClose = document.getElementById(`${idModalClose}`);
      // ModalisClose.classList.remove("modalshowactive");
      // const elopacity = document.getElementById("opacity");
      // elindex.removeChild(elopacity);
    // });
  // }
  const _showPayment = function(){
    const elpaymentradio = document.getElementsByClassName('payment__payment-radio');
    $('.payment__payment-radio').click(function(){
      if($(this).is(":checked")){
        $('.payment_type').removeClass('payment_type_active');
        $(this).parent().find('.payment_type').addClass('payment_type_active');
      }else{
        return;
      }
    })
  }
	return {
		init: function() {
			_generateSlider();
			_generateFacebookFanpage();
      _showMenuMoblie();
      _toToppage();
     // _showdialog();
      _checkHederMb();
      // itc
      _modalShowPopup();
      _generateIntrocude();
      _generateNewslist();
      _generateSlideProductDetail();
      _generateProductRelation();
      _generatelistpartner();
      _filterCate();
      _mathCart();
      _changeNumberByKey();
      _activefilterCate();
      _showPayment();
		}
	};
})();

common.init();

const setGoogleMap = (function(){

  const _checkRungoogleMap = function() {
    const elContentMap = document.getElementById('content_map');
    if(!elContentMap) { return }
    const intLatPoint = Number(elContentMap.dataset['lat']),
          intLngPoint = Number(elContentMap.dataset['lng']),
          intZoom = Number(elContentMap.dataset['zoom']); 
    // google_map
      var markers = [];
      function initialize() {
        
        var mapOptions = {
            zoom: intZoom,
            center: new google.maps.LatLng(intLatPoint, intLngPoint),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [ { "featureType": "all", "elementType": "labels", "stylers": [ { "visibility": "on" } ] }, { "featureType": "administrative.locality", "elementType": "all", "stylers": [ { "visibility": "off" }, { "color": "#000" } ] }, { "featureType": "landscape", "elementType": "all", "stylers": [ { "color": "#f6f6f6" } ] }, { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color" : "#efebe2" } ] }, { "featureType": "poi.attraction", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.business", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.government", "elementType": "geometry.fill", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.medical", "elementType": "geometry.fill", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [ { "color": "#AEC93D" } ] }, { "featureType": "poi.place_of_worship", "elementType": "all", "stylers": [ { "color": "#000" } ] }, { "featureType": "poi.school", "elementType": "geometry", "stylers": [ { "color": "#7CBC73" } ] }, { "featureType": "poi.sports_complex", "elementType": "all", "stylers": [ { "color": "#000" } ] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#ffffff" } ] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [ { "color": "#ffffff" } ] }, { "featureType": "road.arterial", "elementType": "geometry.stroke", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road.local", "elementType": "geometry.fill", "stylers": [ { "color": "#fbfbfb" } ] }, { "featureType": "road.local", "elementType": "geometry.stroke", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "water", "elementType": "all", "stylers": [ { "color": "#337ab7" } ] } ]
        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);   
        var marker, i;
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                map: map,
                icon: locations[i].icon
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    map.setCenter(marker.getPosition());
                    infowindow.setContent(
                    '<div class="locations[i]" style="width:300px" ><div class="logomap"><img src="' +locations[i].logo +'" /></div><div class="ct_map"><h4>'+ locations[i].name
                        +'</h4><p><strong class="bold">Địa chỉ:</strong>' + locations[i].address
                        + '</p><p><strong class="bold">Điện thoại:</strong> ' + locations[i].phone
                        + '</p></div><div class="clearfix"></div></div>'
                    );
                    infowindow.open(map, marker);
                }
            })(marker, i));

            markers.push(marker);
        }
      }
      google.maps.event.addDomListener(window, 'load', initialize);
      function myClick(id){
          google.maps.event.trigger(markers[id], 'click');
      }

      const $elAddressPoint = $('.address_map');

      $elAddressPoint.on('click',function() {
        $('html,body').animate({
        scrollTop: $("#map").offset().top - 170},
        'slow');
        myClick($(this).index());
        return false;
      })
      // end_google_map
  };

  return {
    init() {
      _checkRungoogleMap();
    }
  }
})();

setGoogleMap.init();