function LoadHotPrdoducts(productCategoryId) {
    $.ajax({
        method: "POST",
        url: "/LoadHotProducts",
        beforeSend: function () { $('#loading_img').show(); },
        complete: function () { $('#loading_img').hide(); },
        data: { productCategoryId: productCategoryId }
    }).done(function (data) {
        $('#product_content').html(data.str);
    });
}

function SetCurrentLanguage(culture) {
    $.ajax({
        method: "POST",
        url: "/ShoppingCart/UpdateLanguage",
        data: { culture: culture }
    }).then(function (data) {
        console.log(data)
        location.reload()
    });
}