﻿using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using MainProject.Data;
using MainProject.Framework.Constant;
using MainProject.Framework.Helper;
using MainProject.Service.ServiceLayer;

namespace MainProject.Web.BaseControllers
{
    public class BaseController : Controller
    {
        //private ContactService service;
        private MainDbContext _dbContext;

        public MainDbContext DbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = DalHelper.InvokeDbContext();
                }
                return _dbContext;
            }
        }

        public BaseController()
        {
            //service = new ContactService();

            ViewBag.IsRootUser = true;
            if (!Roles.GetRolesForUser().Contains(RoleName.RootUser))
            {
                ViewBag.IsRootUser = false;
            }

            //// PlaceHolder model
            //ViewBag.PlaceHolderModel = service.GetPlaceholderData();
        }
    }
}
