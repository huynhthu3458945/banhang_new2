﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MainProject.Core.Enums;
using MainProject.Framework.Constant;
using MainProject.Framework.ActionResults;
using MainProject.Data;
using MainProject.Framework.Helper;

namespace MainProject.Web.BaseControllers.Ultities
{
    public class CommonController : BaseController
    {
        #region Fields

        private readonly UnitOfWork unitOfWork;

        #endregion


        #region Constructors

        public CommonController()
        {
            unitOfWork = new UnitOfWork();
        }

        #endregion

        public ActionResult ChangeCulture(string culture, string url)
        {
            var newCurrentLanguage = DbContext.Languages.FirstOrDefault(c => c.LanguageKey.Equals(culture, StringComparison.OrdinalIgnoreCase));
            if (newCurrentLanguage == null)
            {
                return Redirect("/");
            }
            CultureHelper.SaveCurrentLanguage(newCurrentLanguage.LanguageKey);

            var routerModel = UrlRecordHelper.GetUrlRecordFromUrl(WebUtility.UrlDecode(url));
            if (routerModel.HasResult)
            {
                var urlRecord = routerModel.UrlRecord;
                switch ((EntityTypeCollection)Enum.Parse(typeof(EntityTypeCollection), urlRecord.EntityType.ToString()))
                {
                    case EntityTypeCollection.Articles:
                        var currentArticle = DbContext.Articles.FirstOrDefault(c => c.Id == urlRecord.EntityId);
                        if (currentArticle != null)
                        {
                            var newArticle =
                                DbContext.Articles.FirstOrDefault(
                                    c =>
                                    c.OriginalValue == currentArticle.OriginalValue &&
                                    c.Language.Id == newCurrentLanguage.Id);
                            if (newArticle != null)
                            {
                                var newUrl = newArticle.GetUrl();
                                return Redirect(newUrl);
                            }
                        }
                        break;
                    case EntityTypeCollection.AttachmentCategories:
                    case EntityTypeCollection.Categories:
                        var currentCategory = DbContext.Categories.FirstOrDefault(c => c.Id == urlRecord.EntityId);
                        if (currentCategory != null)
                        {
                            var newCategory =
                                DbContext.Categories.FirstOrDefault(
                                    c =>
                                    c.OriginalValue == currentCategory.OriginalValue &&
                                    c.Language.Id == newCurrentLanguage.Id);
                            if (newCategory != null)
                            {
                                var newUrl = newCategory.GetPrefixUrl() + newCategory.SeName;
                                return Redirect(newUrl);
                            }
                        }
                        break;
                }
            }
            return Redirect("/");
        }

        public ActionResult Redirect()
        {
            var request = Request.Url.PathAndQuery;

            if (request.Equals("/_common-settings/AddDb", StringComparison.OrdinalIgnoreCase))
            {
                return
                    new MVCTransferResult(
                        new
                        {
                            controller = "Service",
                            action = "AddDb"
                        });
            }
            if (request.Equals("/_common-settings/ReGenerateDb", StringComparison.OrdinalIgnoreCase))
            {
                return
                    new MVCTransferResult(
                        new
                        {
                            controller = "Service",
                            action = "ReGenerateDb"
                        });
            }
            if (request.StartsWith("/_common-settings/", StringComparison.OrdinalIgnoreCase))
            {
                if (request.StartsWith("/_common-settings/ChangeCulture", StringComparison.OrdinalIgnoreCase))
                {
                    var parameters = request.Substring(request.IndexOf("?") + 1).Split('&');
                    if (parameters.Count() == 2 || parameters.Count() == 3)
                    {
                        var isValid = true;
                        var culture = string.Empty;
                        var newUrl = string.Empty;
                        foreach (var parameter in parameters)
                        {
                            var dictionary = parameter.Split('=');
                            if (dictionary.Count() == 2)
                            {
                                if (dictionary[0].Equals("page")) break;
                                if (!dictionary[0].Equals("culture") && !dictionary[0].Equals("url"))
                                {
                                    isValid = false;
                                }
                                else
                                {
                                    if (dictionary[0].Equals("culture")) culture = dictionary[1];
                                    if (dictionary[0].Equals("url")) newUrl = dictionary[1];
                                }
                            }
                            else if (dictionary.Count() == 3)
                            {
                                var subDictionary = parameter.Split('?');
                                newUrl = subDictionary[0];
                            }
                        }
                        if (isValid)
                        {
                            return
                                new MVCTransferResult(
                                    new { controller = "Common", action = "ChangeCulture", culture, url = newUrl });
                        }
                    }
                }
            }
            else
            {
                // Case: current request is UrlRecord
                var paramValues = request.Split('/').ToList();

                if (paramValues.Count > 0)
                {
                    var lastParamValue = paramValues[paramValues.Count - 1];
                    if (!lastParamValue.Contains("."))
                    {
                        var dbContext = DalHelper.InitDbContext();
                        DalHelper.SaveDbContextToRequest(dbContext);

                        var routerModel = UrlRecordHelper.GetUrlRecordFromUrl(request);

                        if (routerModel.HasResult)
                        {
                            var urlRecord = routerModel.UrlRecord;

                            var controllerName = string.Empty;
                            var actionName = string.Empty;
                            switch (
                                (EntityTypeCollection)
                                Enum.Parse(typeof(EntityTypeCollection), urlRecord.EntityType.ToString()))
                            {
                                case EntityTypeCollection.Articles:
                                    controllerName = "Common";
                                    actionName = "RedirectArticle";
                                    break;
                                case EntityTypeCollection.Categories:
                                    controllerName = "Common";
                                    actionName = "RedirectCategory";
                                    break;
                                case EntityTypeCollection.Tags:
                                    controllerName = "Home";
                                    actionName = "Tag";
                                    break;
                                case EntityTypeCollection.Products:
                                    controllerName = "Product";
                                    actionName = "Detail";
                                    break;
                                default:
                                    break;
                            }
                            if (!string.IsNullOrEmpty(controllerName) && !string.IsNullOrEmpty(actionName))
                            {
                                return
                                    new MVCTransferResult(
                                        new
                                        {
                                            controller = controllerName,
                                            action = actionName,
                                            id = urlRecord.EntityId,
                                            page = routerModel.PageIndex,
                                            breadcumUrl = urlRecord.Url
                                        });
                            }
                        }
                    }
                }
            }

            return new MVCTransferResult(new { controller = "home", action = "index" });
        }


        public ActionResult RedirectArticle(long id, string breadcumUrl, int page)
        {
            // Get current article 
            var currentArticle = unitOfWork.ArticleRepository.FindById(id);

            if (currentArticle != null)
            {
                switch (currentArticle.Category.DisplayTemplate)
                {
                    case DisplayTemplateCollection.Child1AdviseTemplate:
                        return
                        new MVCTransferResult(
                            new
                            {
                                controller = "News",
                                action = "Detail",
                                newsArticleId = currentArticle.Id
                            });
                        // Tụi tư vấn con cấp 1 xài chung 1 cái này luôn
                        //case DisplayTemplateCollection.SolutionTemplate:
                        //    return
                        //   new MVCTransferResult(
                        //       new
                        //       {
                        //           controller = "Solution",
                        //           action = "Detail",
                        //           solutionArticleId = currentArticle.Id
                        //       });
                        //case DisplayTemplateCollection.ProjectTemplate:
                        //    return
                        //   new MVCTransferResult(
                        //       new
                        //       {
                        //           controller = "Project",
                        //           action = "Detail",
                        //           projectArticleId = currentArticle.Id
                        //       });
                }

            }
            return View(StringConstant.ViewForRedirectToHomePage);
        }
        public ActionResult RedirectCategory(int id, string breadcumUrl, int page = 1)
        {
            //// Get current category
            var category = unitOfWork.CategoryRepository.FindUnique(x => x.Id == id);
            if (category != null)
            {
                switch (category.DisplayTemplate)
                {
                    // Introduction
                    case DisplayTemplateCollection.IntroductionTemplate:
                        goto IntroductionCategoryView;
                        break;

                    // Quà tặng doanh nghiệp
                    case DisplayTemplateCollection.BusinessGiftsTemplate:
                        goto BusinessGiftsView;
                        break;

                    // Ngự thiện
                    case DisplayTemplateCollection.NguThienTemplate:
                        goto NguThienView;
                        break;

                    // Faq
                    case DisplayTemplateCollection.FaqTemplate:
                        goto FaqCategoryView;
                        break;

                    // Mua hàng
                    case DisplayTemplateCollection.PurchaseTemplate:
                        goto PurchaseCategoryView;
                        break;

                    // Đổi trả
                    case DisplayTemplateCollection.ExchangeTemplate:
                        goto ExchangeCategoryView;
                        break;
                    // Thanh toán
                    case DisplayTemplateCollection.PayTemplate:
                        goto PayCategoryView;
                        break;

                    // Bảo mật
                    case DisplayTemplateCollection.SecurityTemplate:
                        goto SecurityCategoryView;
                        break;

                    // Product
                    case DisplayTemplateCollection.ProductTemplate:
                        goto ProductCategoryView;
                        break;
                    // product child
                    case DisplayTemplateCollection.Child1ProductTemplate:
                        goto Child1ProductCategoryView;
                        break; 
                    // Yen chung
                    case DisplayTemplateCollection.YenChungTemplate:
                        goto Child1ProductCategoryView;
                        break;
                    //case DisplayTemplateCollection.Child2ProductTemplate:
                    //    goto Child2ProductCategoryView;
                    //    break;
                    //case DisplayTemplateCollection.Child3ProductTemplate:
                    //    goto Child3ProductCategoryView;
                    //    break;

                    // Contact
                    case DisplayTemplateCollection.ContactTemplate:
                        goto ContactCategoryView;
                        break;

                    case DisplayTemplateCollection.Child1AdviseTemplate:
                        goto Child1AdviseView;
                        break;

                    //// Download file
                    //case DisplayTemplateCollection.DownloadpageTemplate:
                    //    goto DownloadCategoryView;
                    //    break;

                    default:
                        goto Home;
                        break;
                }

            // Introduction
            IntroductionCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Introduction",
                           action = "Index",
                           introductionCategoryId = category.Id
                       });
                // BusinessGiftsView
                BusinessGiftsView:
                return
                    new MVCTransferResult(
                        new
                        {
                            controller = "News",
                            action = "Quatang",
                            categoryId = category.Id
                        });
            NguThienView:
                return
                    new MVCTransferResult(
                        new
                        {
                            controller = "News",
                            action = "NguThien",
                            categoryId = category.Id
                        });

            // product
            ProductCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Product",
                           action = "Index",
                           productCategoryId = category.Id,
                           page = page
                       });
            // product child
            Child1ProductCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Product",
                           action = "Child1",
                           child1CategoryId = category.Id
                       });

            // Faq
            FaqCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Faq",
                           action = "Index",
                           faqCategoryId = category.Id
                       });
            // Mua hàng
            PurchaseCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Purchase",
                           action = "Index"
                       });
            // Đổi trả
            ExchangeCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Exchange",
                           action = "Index"
                       });
            // Thanh toán
            PayCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Pay",
                           action = "Index"
                       });
            // Bảo mật
            SecurityCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Security",
                           action = "Index"
                       });

            // Project
            ProjectCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Project",
                           action = "Index",
                           projectCategoryId = category.Id,
                           page = page
                       });

            // News
            Child1AdviseView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "News",
                           action = "Index",
                           newsCategoryId = category.Id,
                           page = page
                       });

            
            Child2ProductCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Product",
                           action = "Child2",
                           child2CategoryId = category.Id,
                           page = page
                       });
            Child3ProductCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Product",
                           action = "Child3",
                           child3CategoryId = category.Id,
                           page = page
                       });

            //Contact
            ContactCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Contact",
                           contactCategoryId = category.Id,
                           action = "Index"
                       });

            // Project
            DownloadCategoryView:
                return
                   new MVCTransferResult(
                       new
                       {
                           controller = "Download",
                           action = "Index",
                           downloadCategoryId = category.Id,
                           page = page
                       });

            Home:
                RedirectToAction("Index");
            }
            return View(StringConstant.ViewForRedirectToHomePage);
        }

    }
}
