﻿
namespace MainProject.Web.BaseControllers.Ultities
{
    public class ServiceController : BaseController
    {
        public string ReGenerateDb()
        {
            var message = Framework.DatabaseFramework.ReGenerateDb.Init();
            
            if (!string.IsNullOrEmpty(message))
                return message;
            return "Re generate database successfully!";
        }

        public string AddDb()
        {
            return Framework.DatabaseFramework.AddDb.Init();
        }
    }
}
