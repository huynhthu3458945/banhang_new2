﻿using System.Web;
using System.Web.Optimization;

namespace MainProject.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/bundle/PageMainCss").Include(
            "~/Content/css/main.css",
            "~/Content/css/a3_lazy_load.min.css",
            "~/Content/layout/resources/vendor/slick/slick.css",
            "~/Content/layout/resources/vendor/slick/slick-theme.css",
            "~/Content/layout/resources/css/style.css",
            "~/Content/layout/resources/css/responsive.css"));
            bundles.Add(new StyleBundle("~/bundle/PageMainJS").Include(
           "~/Content/js/jquery.js",                   
           "~/Content/js/jquery-migrate.min.js"));
            bundles.Add(new StyleBundle("~/bundle/PageJS").Include(
          "~/Content/js/scripts.js",      
          "~/Content/js/mediaelement-and-player.min.js",
          "~/Content/js/mediaelement-migrate.min.js",
          "~/Content/js/tether.min.js",
          "~/Content/js/bootstrap.min.js",
          "~/Content/js/jquery.toast.js",
          "~/Content/js/ekko-lightbox.min.js",
          "~/Content/js/placeholderTypewriter.js",
          "~/Content/js/main.js",
          "~/Content/js/jquery.lazyloadxt.extra.min.js",
          "~/Content/js/jquery.lazyloadxt.srcset.min.js",
          "~/Content/js/wp-embed.min.js",
          "~/Content/layout/custom/js/load-hot-products.js",
          "~/Content/js/jquery.toast.js",
          "~/Content/layout/resources/js/common.js"));
            BundleTable.EnableOptimizations = true;
        }

    }
}