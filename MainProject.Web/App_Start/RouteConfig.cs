﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MainProject.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            RouteTable.Routes.IgnoreRoute("{resource}.css/{*pathInfo}");
            RouteTable.Routes.IgnoreRoute("{resource}.js/{*pathInfo}");
            RouteTable.Routes.IgnoreRoute("{folder}/{*pathInfo}", new { folder = "upload" });

          routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "WebSetting",
                url: "_home-route-settings/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ShoppingCart",
                url: "ShoppingCart/{action}/{id}",
                defaults: new { controller = "ShoppingCart", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Giỏ hàng",
                url: "gio-hang",
                defaults: new { controller = "ShoppingCart", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Order",
                url: "Order/{action}/{id}",
                defaults: new { controller = "Order", action = "Index", id = UrlParameter.Optional }
            );
            //routes.MapRoute(
            //    name: "Search",
            //    url: "Search",
            //    defaults: new { controller = "Search", action = "Index", id = UrlParameter.Optional }
            //);
            routes.MapRoute(
                name: "Handler",
                url: "{*url}",
                defaults: new { controller = "Common", action = "Redirect" }
            );
        }
    }
}