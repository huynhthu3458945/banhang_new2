﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MainProject.Framework.Helper;
using WebMatrix.WebData;
using System.Web.WebPages;

namespace MainProject.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;

            DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode("Mobile")
            {
                ContextCondition = (context => context.GetOverriddenUserAgent().IndexOf("tablet", StringComparison.OrdinalIgnoreCase) >= 0 ||
                context.GetOverriddenUserAgent().IndexOf("iphone", StringComparison.OrdinalIgnoreCase) >= 0 ||
                context.GetOverriddenUserAgent().IndexOf("Linux", StringComparison.OrdinalIgnoreCase) >= 0 ||
                context.GetOverriddenUserAgent().IndexOf("Android", StringComparison.OrdinalIgnoreCase) >= 0 ||
                context.GetOverriddenUserAgent().IndexOf("BlackBerry", StringComparison.OrdinalIgnoreCase) >= 0 ||
                context.GetOverriddenUserAgent().IndexOf("BB10", StringComparison.OrdinalIgnoreCase) >= 0 ||
                context.GetOverriddenUserAgent().IndexOf("Opera", StringComparison.OrdinalIgnoreCase) >= 0 ||
                context.GetOverriddenUserAgent().IndexOf("Windows Phone", StringComparison.OrdinalIgnoreCase) >= 0)
            });
            AreaRegistration.RegisterAllAreas();
            
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            //QuartzTask.StartScheduler();

            try
            {
                WebSecurity.InitializeDatabaseConnection("DefaultConnection",
                        "UserProfile", "UserId", "UserName", autoCreateTables: true);
            }
            catch (Exception) { }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //if (HttpContext.Current.Request.IsSecureConnection.Equals(false))
            //{
            //    Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
            //}
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            DalHelper.ReleaseDbContextOnRequest();
        }
    }
}