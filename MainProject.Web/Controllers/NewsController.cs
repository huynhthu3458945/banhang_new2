﻿using System.Web.Mvc;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;

namespace MainProject.Web.Controllers
{
    public class NewsController : BaseController
    {
        /// <summary>
        /// News controller
        /// </summary>

        #region Fields

        private readonly NewsService newsService;

        #endregion

        #region Constructors

        public NewsController()
        {
            newsService = new NewsService();
        }

        #endregion
        
        public ActionResult Index(long newsCategoryId, int page)
        {
            var indexViewModel = newsService.GetDataOfIndexPage(newsCategoryId, page);

            //Set page title
            ViewBag.Title = indexViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = indexViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = indexViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = indexViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.MetaImage;
            ViewBag.Type = indexViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.Url;

            return View(indexViewModel);
        }

        public ActionResult Detail(long newsArticleId)
        {
            var model = newsService.GetDataOfDetailPage(newsArticleId);

            //Set page title
            ViewBag.Title = model.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = model.SEOModel.MetaTitle;
            ViewBag.MetaDescription = model.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = model.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.MetaImage;
            ViewBag.Type = model.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.Url;

            return View(model);
        }
        public ActionResult QuaTang(long categoryId)
        {
            var indexViewModel = newsService.GetQuaTang(categoryId);

            //Set page title
            ViewBag.Title = indexViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = indexViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = indexViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = indexViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.MetaImage;
            ViewBag.Type = indexViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.Url;

            return View(indexViewModel);
        }
        public ActionResult NguThien(long categoryId)
        {
        //    var indexViewModel = newsService.GetDataOfIndexPage(categoryId);

        //    //Set page title
        //    ViewBag.Title = indexViewModel.SEOModel.Title;

        //    //Set Meta data
        //    ViewBag.MetaTitle = indexViewModel.SEOModel.MetaTitle;
        //    ViewBag.MetaDescription = indexViewModel.SEOModel.MetaDescription;
        //    ViewBag.MetaKeywords = indexViewModel.SEOModel.MetaKeywords;
        //    ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.MetaImage;
        //    ViewBag.Type = indexViewModel.SEOModel.Type;
        //    ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.Url;

            return View();
        }

    }
}
