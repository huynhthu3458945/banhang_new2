﻿using System.Web.Mvc;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;

namespace MainProject.Web.Controllers
{
    public class SearchController : BaseController
    {
        /// <summary>
        /// Search controller
        /// </summary>

        #region Fields

        private readonly SearchService searchService;

        #endregion

        #region Constructors

        public SearchController()
        {
            searchService = new SearchService();
        }

        #endregion
        [Route("Search")]
        public ActionResult Index(string text = "", int page = 1)
        {

            var model = searchService.Search(text, page);

            // Set title of current page
            ViewBag.Title = "Tìm kiếm";

            return View(model);
        }
    }
}
