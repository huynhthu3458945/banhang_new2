﻿using System.Web.Mvc;
using System.IO;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;

namespace MainProject.Web.Controllers
{
    public class DownloadController : BaseController
    {
        /// <summary>
        /// Download controller
        /// </summary>

        #region Fields

        private readonly DownloadService downloadService;

        #endregion

        #region Constructors

        public DownloadController()
        {
            downloadService = new DownloadService();
        }

        #endregion
        
        public ActionResult Index(long downloadCategoryId, int page)
        {
            var indexViewModel = downloadService.GetDataOfIndexPage(downloadCategoryId, page);

            //Set page title
            ViewBag.Title = indexViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = indexViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = indexViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = indexViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.MetaImage;
            ViewBag.Type = indexViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.Url;

            return View(indexViewModel);
        }

        [Route("DownloadFile/{downloadFileId}")]
        public FileResult DownloadFile(int downloadFileId)
        {
            var productModel = downloadService.GetProductModel(downloadFileId);
            if (productModel != null)
            {
                if (System.IO.File.Exists(Server.MapPath(productModel.FilePath)))
                {
                    var fileName = Path.GetFileName(Server.MapPath(productModel.FilePath));
                    return File(Server.MapPath(productModel.FilePath), "application/octet-stream", fileName);
                }
            }
            return null;
        }
    }
}
