﻿using System.Web.Mvc;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;

namespace MainProject.Web.Controllers
{
    public class ProductController : BaseController
    {
        /// <summary>
        /// Product controller
        /// </summary>

        #region Fields

        private readonly ProductService productService;

        #endregion

        #region Constructors

        public ProductController()
        {
            productService = new ProductService();
        }

        #endregion

        public ActionResult index(long productCategoryId, int page)
        {
            var child2ViewModel = productService.GetDataOfIndexPage(productCategoryId, page);

            //Set page title
            ViewBag.Title = child2ViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = child2ViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = child2ViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = child2ViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + child2ViewModel.SEOModel.MetaImage;
            ViewBag.Type = child2ViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + child2ViewModel.SEOModel.Url;

            return View(child2ViewModel);
        }

        public ActionResult Child1(long child1CategoryId)
        {
            var child1ViewModel = productService.GetDataOfChild1Page(child1CategoryId);

            //Set page title
            ViewBag.Title = child1ViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = child1ViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = child1ViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = child1ViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + child1ViewModel.SEOModel.MetaImage;
            ViewBag.Type = child1ViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + child1ViewModel.SEOModel.Url;

            return View(child1ViewModel);
        }
        
        public ActionResult Child2(long child2CategoryId, int page)
        {
            var child2ViewModel = productService.GetDataOfChild2Page(child2CategoryId, page);

            //Set page title
            ViewBag.Title = child2ViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = child2ViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = child2ViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = child2ViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + child2ViewModel.SEOModel.MetaImage;
            ViewBag.Type = child2ViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + child2ViewModel.SEOModel.Url;

            return View(child2ViewModel);
        }
        
        public ActionResult Child3(long child3CategoryId, int page)
        {
            var child3ViewModel = productService.GetDataOfChild3Page(child3CategoryId, page);

            //Set page title
            ViewBag.Title = child3ViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = child3ViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = child3ViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = child3ViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + child3ViewModel.SEOModel.MetaImage;
            ViewBag.Type = child3ViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + child3ViewModel.SEOModel.Url;

            return View(child3ViewModel);
        }
        
        public ActionResult Detail(long id)
        {
            var detailViewModel = productService.GetDataOfDetailPage(id);

            //Set page title
            ViewBag.Title = detailViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = detailViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = detailViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = detailViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + detailViewModel.SEOModel.MetaImage;
            ViewBag.Type = detailViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + detailViewModel.SEOModel.Url;

            return View(detailViewModel);
        }
    }
}
