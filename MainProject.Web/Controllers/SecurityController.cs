﻿using MainProject.Service.ServiceLayer;
using MainProject.Web.BaseControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainProject.Web.Controllers
{
    public class SecurityController : BaseController
    {
        #region Fields

        private readonly SecurityService securityService;

        #endregion

        #region Constructors

        public SecurityController()
        {
            securityService = new SecurityService();
        }
        #endregion
        public ActionResult Index()
        {
            var indexModel = securityService.GetDataOfIndexPage();

            //Set page title
            ViewBag.Title = indexModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = indexModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = indexModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = indexModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + indexModel.SEOModel.MetaImage;
            ViewBag.Type = indexModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + indexModel.SEOModel.Url;

            return View(indexModel);
        }
    }
}