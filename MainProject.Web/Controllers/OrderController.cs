﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Framework.Helper;
using MainProject.Framework.ActionResults;
using MainProject.Framework.Constant;
using MainProject.Data;
using MainProject.Web.BaseControllers;
using MainProject.Service.Models.ViewModel.ShopppingCart;
using MainProject.Service.Models.ViewModel.Order;
using MainProject.Service.ServiceLayer;
using MainProject.Web.Models;

namespace MainProject.Web.Controllers
{
    public class OrderController : BaseController
    {
        /// <summary>
        /// Order controller
        /// </summary>

        #region Fields

        private readonly UnitOfWork unitOfWork;
        private readonly OrderService orderService;

        #endregion

        #region Constructors

        public OrderController()
        {
            unitOfWork = new UnitOfWork();
            orderService = new OrderService();
        }

        #endregion

        public ActionResult Index(string message = "")
        {
            if (Session[SessionKey.ShoppingCartItems] != null)
            {

                if (message == "Order Invalid")
                {
                    TempData["message"] = "<script>_showdialog('checkout_invalid')</script>";
                }
                var model = new OrderFormModel();

                // Get ordertypes 
                ViewBag.OrderTypes = orderService.GetOrderTypes();

                return View(model);
            }
            return Redirect("/");
        }
        [HttpPost]
        public ActionResult Checkout(OrderFormModel model)
        {
            if (ModelState.IsValid)
            {
                orderService.SubmitOrder(model, (List<ShoppingCartItem>)Session[SessionKey.ShoppingCartItems]);
                Session[SessionKey.ShoppingCartItems] = null;
                return Json(new { result = true });
            }
            else
            {
                return Json(new { result = false });               
            }
        }

        //[HttpPost]
        //public ActionResult Confirm(long orderId)
        //{
        //    var order = _orderRepository.FindId(orderId);

        //    if(order != null)
        //    {

        //        var model = new CheckoutViewModel
        //        {
        //            Order = order,
        //            ShoppingCartItems = (List<ShoppingCartItem>)Session[SessionKey.ShoppingCartItems]
        //        };
        //        // Send Email
        //        var listEmail = new List<string>();
        //        listEmail.Add(model.Order.BuyerEmail);
        //        var orderItems = _orderItemRepository.Find(x => x.Order.Id == model.Order.Id).ToList();
        //        MailHelper.SendOrderInfo(listEmail, model.Order, orderItems, SettingHelper.GetValueSetting("OrderMailBody"));
        //        // Remove Session
        //        Session[SessionKey.ShoppingCartItems] = null;
        //        return new MVCTransferResult(
        //                    new
        //                    {
        //                        controller = "Home",
        //                        action = "Index",
        //                        message = "Order Success"
        //                    });
        //    }
        //    else
        //    {
        //        return new MVCTransferResult(
        //            new
        //            {
        //                controller = "Order",
        //                action = "Index",
        //                message = "Order Invalid"
        //            });
        //    }
        //}

        //[HttpPost]
        //public ActionResult Checkout(OrderFormModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        decimal total = 0;
        //        foreach (var item in (List<ShoppingCartItem>)Session["ShoppingCartItems"])
        //        {
        //            total += item.ItemAmount;
        //        }
        //        var orderModel = new OrderModel
        //        {
        //            BuyerName = model.Name,
        //            BuyerAddress = model.Address,
        //            BuyerEmail = model.Email,
        //            BuyerPhone = model.Phone,
        //            OtherBuyerPhone = model.OtherPhone,
        //            Note = model.Note,
        //            DistrictSelectedValue = model.DistrictSelectedValue
        //        };
        //        Session[SessionKey.OrderModel] = orderModel; 
        //        var checkoutViewModel = new CheckoutViewModel
        //        {
        //            OrderModel = orderModel,
        //            ShoppingCartItems = (List<ShoppingCartItem>)Session[SessionKey.ShoppingCartItems]
        //        };
        //        return View("Confirm", checkoutViewModel);
        //    }
        //    else
        //    {
        //        return new MVCTransferResult(
        //            new
        //            {
        //                controller = "Order",
        //                action = "Index",
        //                message = "Order Invalid"
        //            });
        //    }
        //}

        //[HttpPost]
        //public ActionResult Confirm()
        //{
        //    // Save Order infos
        //    decimal total = 0;
        //    foreach (var item in (List<ShoppingCartItem>)Session["ShoppingCartItems"])
        //    {
        //        total += item.ItemAmount;
        //    }
        //    var orderModel = (OrderModel)Session[SessionKey.OrderModel];
        //    var order = new Order
        //    {
        //        BuyerName = orderModel.BuyerName,
        //        BuyerAddress = orderModel.BuyerAddress,
        //        BuyerEmail = orderModel.BuyerEmail,
        //        BuyerPhone = orderModel.BuyerPhone,
        //        OtherBuyerPhone = orderModel.OtherBuyerPhone,
        //        OrderTime = DateTime.Now,
        //        Amount = total,
        //        OrderStatus = OrderStatusCollection.JustCreate,
        //        District = commonService.GetDistrictById(orderModel.DistrictSelectedValue),
        //        Note = orderModel.Note
        //    };
        //    unitOfWork.OrderRepository.Insert(order);
        //    var shoppingCartItems = (List<ShoppingCartItem>)Session[SessionKey.ShoppingCartItems];
        //    foreach (var item in shoppingCartItems)
        //    {
        //        var product = item.Product;
        //        var orderItem = new OrderItem
        //        {
        //            MainProduct = commonService.GetMainProductId(item.Product.Id),
        //            Order = order,
        //            ItemCount = item.ItemCount,
        //            Amount = item.ItemAmount,
        //            Price = item.Product.Price
        //        };
        //        unitOfWork.OrderItemRepository.Insert(orderItem);
        //    }
        //    unitOfWork.Save();

        //    // Send Email
        //    var listEmail = new List<string>();
        //    listEmail.Add(order.BuyerEmail);
        //    var orderItems = unitOfWork.OrderItemRepository.Find(x => x.Order.Id == order.Id).ToList();
        //    MailHelper.SendOrderInfo(listEmail, order, orderItems, SettingHelper.GetValueSetting("OrderMailBody"));

        //    // Remove Session
        //    Session[SessionKey.ShoppingCartItems] = null;
        //    return new MVCTransferResult(
        //                new
        //                {
        //                    controller = "Home",
        //                    action = "Index",
        //                    message = "Order Success"
        //                });
        //}

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
