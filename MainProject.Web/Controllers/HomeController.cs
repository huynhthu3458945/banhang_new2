﻿using System;
using System.Web.Mvc;
using MainProject.Data;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;
using MainProject.Core.Enums;
using MainProject.Framework.Helper;

namespace MainProject.Web.Controllers
{
    [RoutePrefix("Home")]
    public class HomeController : BaseController
    {
        #region Fields

        private readonly HomePageService homeService;
        private readonly HeaderService headerService;
        private readonly FooterService footerService;
        private readonly UnitOfWork unitOfWork;

        #endregion


        #region Constructors

        public HomeController()
        {
            homeService = new HomePageService();
            headerService = new HeaderService();
            footerService = new FooterService();
            unitOfWork = new UnitOfWork();

        }

        #endregion

        //[OutputCache(CacheProfile = "MyCache")]
        public ActionResult Index()
        {
            var model = homeService.GetDataOfIndexPage();

            //Set page title
            ViewBag.Title = model.SEOModel.MetaTitle;
            //Set Meta data
            ViewBag.MetaTitle = model.SEOModel.MetaTitle;
            ViewBag.MetaDescription = model.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = model.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.MetaImage;
            ViewBag.Type = "website";
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority;      
            return View(model);
        }
        public ActionResult _ShowHeader()
        {
            var model = headerService.GetHeaderData();

            return View(model);
        }
        public ActionResult _ShowFooter()
        {
            var model = footerService.GetFooterData();

            return View(model);
        }

        public ActionResult _ShowMenuSupport(int order)
        {
            var model = headerService.GetMenuSupport(order);
            return View(model);
        }
        public ActionResult _ShowMenuMobie()
        {
            var model = headerService.GetMenuMobiFooter();
            return View(model);
        }

        [Route("LoadHotProducts"), HttpPost]
        public JsonResult LoadHotProducts(long productCategoryId)
        {
            return Json(new { str = homeService.GetProductsString(productCategoryId) });
        }

        [Route("sitemap.xml")]
        public PartialViewResult Sitemap()
        {
            Response.AddHeader("Content-Type", "text/xml");
            ViewBag.host = String.Format("http://{0}", Request.Url.Host);
            return PartialView(homeService.GetSitemap());
        }

        [Route("error-page")]
        public ActionResult Error()
        {
            return View();
        }
        public JsonResult UpdateLanguage(string culture)
        {
            CultureHelper.SaveCurrentLanguage(culture);
            return Json(new { result = true });
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
