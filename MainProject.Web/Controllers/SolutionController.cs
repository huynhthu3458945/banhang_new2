﻿using System.Web.Mvc;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;

namespace MainProject.Web.Controllers
{
    public class SolutionController : BaseController
    {
        /// <summary>
        /// Solution controller
        /// </summary>

        #region Fields

        private readonly SolutionService solutionService;

        #endregion

        #region Constructors

        public SolutionController()
        {
            solutionService = new SolutionService();
        }

        #endregion
        
        public ActionResult Index(long solutionCategoryId, int page)
        {
            var indexViewModel = solutionService.GetDataOfIndexPage(solutionCategoryId, page);

            //Set page title
            ViewBag.Title = indexViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = indexViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = indexViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = indexViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.MetaImage;
            ViewBag.Type = indexViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.Url;

            return View(indexViewModel);
        }

        public ActionResult Detail(long solutionArticleId)
        {
            var model = solutionService.GetDataOfDetailPage(solutionArticleId);

            //Set page title
            ViewBag.Title = model.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = model.SEOModel.MetaTitle;
            ViewBag.MetaDescription = model.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = model.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.MetaImage;
            ViewBag.Type = model.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.Url;

            return View(model);
        }
    }
}
