﻿using System.Web.Mvc;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;

namespace MainProject.Web.Controllers
{
    public class IntroductionController : BaseController
    {
        /// <summary>
        /// Introduction controller
        /// </summary>

        #region Fields

        private readonly IntroductionService introductionService;

        #endregion

        #region Constructors

        public IntroductionController()
        {
            introductionService = new IntroductionService();
        }

        #endregion
        public ActionResult Index(long introductionCategoryId)
        {
            var model = introductionService.GetDataOfIndexPage(introductionCategoryId);

            //Set page title
            ViewBag.Title = model.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = model.SEOModel.MetaTitle;
            ViewBag.MetaDescription = model.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = model.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.MetaImage;
            ViewBag.Type = model.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.Url;

            return View(model);
        }
    }
}
