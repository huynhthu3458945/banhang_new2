﻿using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;
using MainProject.Service.Models.ViewModel.Contact;
using MainProject.Framework.ActionResults;

namespace MainProject.Web.Controllers
{
    public class ContactController : BaseController
    {
        /// <summary>
        /// Contact controller
        /// </summary>

        #region Fields

        private readonly ContactService contactService;

        #endregion

        #region Constructors

        public ContactController()
        {
            contactService = new ContactService();
        }
        #endregion

        public ActionResult Index(string message = "",long contactCategoryId = 0)
        {
            var indexModel = contactService.GetDataOfIndexPage(contactCategoryId);

            //Set page title
            ViewBag.Title = indexModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = indexModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = indexModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = indexModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + indexModel.SEOModel.MetaImage;
            ViewBag.Type = indexModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + indexModel.SEOModel.Url;

            // Set data of contact index page
            ViewBag.IndexViewModel = indexModel;

            // Set message in case Submit action
            if(message != "")
            {
                ViewBag.Message = message;
                //if (message == "Success")
                //{
                //    TempData["message"] = "<script>_showdialog('submit_contact_success')</script>";
                //}
                //else
                //{
                //    TempData["message"] = "<script>_showdialog('submit_contact_error')</script>";
                //}
            }

            return View();
        }

        [Route("ContactSubmit"), HttpPost]
        public ActionResult Submit(FormModel model)
        {
            var message = "";

            if (ModelState.IsValid)
            {
                contactService.SaveContact(model);

                message = "Bạn đã liên hệ thành công!";

                ////// Validate Captcha
                //var response = Request["g-recaptcha-response"];
                ////secret that was generated in key value pair
                //const string secret = "6Lco2KEUAAAAAOJkCZid11xWhCHLZqslAWbX6tP7";

                //var client = new WebClient();
                //var reply =
                //    client.DownloadString(
                //        string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

                //var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponseModel>(reply);

                ////when response is false check for the error message
                //if (!captchaResponse.Success)
                //{
                //    if (captchaResponse.ErrorCodes.Count <= 0) return View();

                //    var error = captchaResponse.ErrorCodes[0].ToLower();
                //    switch (error)
                //    {
                //        case ("missing-input-secret"):
                //            message = "The secret parameter is missing";
                //            break;
                //        case ("invalid-input-secret"):
                //            message = "The secret parameter is invalid or malformed.";
                //            break;

                //        case ("missing-input-response"):
                //            message = "The response parameter is missing";
                //            break;
                //        case ("invalid-input-response"):
                //            message = "The response parameter is invalid or malformed.";
                //            break;

                //        default:
                //            message = "Error occured. Please try again";
                //            break;
                //    }
                //}
                //else
                //{
                //    contactService.SaveContact(model);

                //    message = "Success";
                //}
            }
            else
            {
                message = "Error";
            }

            return 
                new MVCTransferResult
                (   new
                    {
                        controller = "Contact",
                        action = "index",
                        message = message
                });
        }

    }
}
