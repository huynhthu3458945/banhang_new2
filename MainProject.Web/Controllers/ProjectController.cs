﻿using System.Web.Mvc;
using MainProject.Web.BaseControllers;
using MainProject.Service.ServiceLayer;

namespace MainProject.Web.Controllers
{
    public class ProjectController : BaseController
    {
        /// <summary>
        /// Project controller
        /// </summary>

        #region Fields

        private readonly ProjectService projectService;

        #endregion

        #region Constructors

        public ProjectController()
        {
            projectService = new ProjectService();
        }

        #endregion
        
        public ActionResult Index(long projectCategoryId, int page)
        {
            var indexViewModel = projectService.GetDataOfIndexPage(projectCategoryId, page);

            //Set page title
            ViewBag.Title = indexViewModel.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = indexViewModel.SEOModel.MetaTitle;
            ViewBag.MetaDescription = indexViewModel.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = indexViewModel.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.MetaImage;
            ViewBag.Type = indexViewModel.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + indexViewModel.SEOModel.Url;

            return View(indexViewModel);
        }

        public ActionResult Detail(long projectArticleId)
        {
            var model = projectService.GetDataOfDetailPage(projectArticleId);

            //Set page title
            ViewBag.Title = model.SEOModel.Title;

            //Set Meta data
            ViewBag.MetaTitle = model.SEOModel.MetaTitle;
            ViewBag.MetaDescription = model.SEOModel.MetaDescription;
            ViewBag.MetaKeywords = model.SEOModel.MetaKeywords;
            ViewBag.MetaImage = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.MetaImage;
            ViewBag.Type = model.SEOModel.Type;
            ViewBag.Canonical = Request.Url.Scheme + "://" + Request.Url.Authority + model.SEOModel.Url;

            return View(model);
        }
    }
}
