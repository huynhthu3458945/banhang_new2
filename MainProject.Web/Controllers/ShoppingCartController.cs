﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Framework.Constant;
using MainProject.Service.Models.ViewModel.ShopppingCart;
using MainProject.Service.ServiceLayer;
using MainProject.Web.BaseControllers;

namespace MainProject.Web.Controllers
{
    [RoutePrefix("ShoppingCart")]
    public class ShoppingCartController : BaseController
    {
        /// <summary>
        /// ShoppingCart controller
        /// </summary>

        #region Fields

        private readonly UnitOfWork unitOfWork;
        //private readonly CommonService service;
        private readonly string currentLanguage;
        private readonly OrderService orderService;

        #endregion

        #region Constructors

        public ShoppingCartController()
        {
            unitOfWork = new UnitOfWork();
            //service = new CommonService();
            currentLanguage = CultureHelper.GetCurrentLanguage();
            orderService = new OrderService();
        }

        #endregion
        public JsonResult UpdateLanguage(string culture)
        {
            CultureHelper.SaveCurrentLanguage(culture);
            return Json(new { result = true });
        }
        public ActionResult Index(string message = "AddToCartSuccess")
        {
            var shoppingCartItems = (IList<ShoppingCartItem>)Session[SessionKey.ShoppingCartItems];
            ViewBag.OrderTypes = orderService.GetOrderTypes();
            return View(shoppingCartItems);
        }
        public JsonResult AddToCart(int productId, int itemCount = 0)
        {
            List<ShoppingCartItem> shoppingCartItems;
            // ShoppingCart is not exist
            if (Session[SessionKey.ShoppingCartItems] == null)
            {
                shoppingCartItems = new List<ShoppingCartItem>();
                var product = unitOfWork.ProductRepository.FindById(productId);
                var shoppingCartItem = new ShoppingCartItem
                {
                    TokenCode = StringHelper.GetString(),
                    LastUpdateTime = DateTime.Now,
                    Product = new ProductModel
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Url = product.GetUrl(),
                        Price = product.Price,
                        ImageDefault = product.ImageDefault,
                        ImageDefaultAlt = product.Name,
                        ProductCode = product.ProductCode,
                        IsPromotion = product.IsPromotion,
                        PromotionPercent = product.PromotionPercent,
                        PromotionPrice = product.PromotionPrice,
                        ProductCategoryTitle = product.Category.Title,
                        ProductCategoryUrl = product.Category.GetPrefixUrl()
                    },

                    ItemCount = itemCount != 0 ? itemCount : 1
                };
                if (shoppingCartItem.Product.IsPromotion)
                {
                    shoppingCartItem.ItemAmount = shoppingCartItem.Product.PromotionPrice * shoppingCartItem.ItemCount;
                }
                else
                {
                    shoppingCartItem.ItemAmount = shoppingCartItem.Product.Price * shoppingCartItem.ItemCount;
                }
                shoppingCartItems.Add(shoppingCartItem);
                Session[SessionKey.ShoppingCartItems] = shoppingCartItems;
            }
            else // ShoppingCart was existed
            {
                shoppingCartItems = (List<ShoppingCartItem>)Session[SessionKey.ShoppingCartItems];

                // Product was existed in shoppingcart
                if (shoppingCartItems.Any(x => x.Product.Id == productId))
                {
                    var shoppingCartItem = shoppingCartItems.FirstOrDefault(x => x.Product.Id == productId);
                    shoppingCartItem.ItemCount = shoppingCartItem.ItemCount + (itemCount != 0 ? itemCount : 1);
                    if (shoppingCartItem.Product.IsPromotion)
                    {
                        shoppingCartItem.ItemAmount = shoppingCartItem.Product.PromotionPrice * shoppingCartItem.ItemCount;
                    }
                    else
                    {
                        shoppingCartItem.ItemAmount = shoppingCartItem.Product.Price * shoppingCartItem.ItemCount;
                    }
                }
                else
                {
                    var product = unitOfWork.ProductRepository.FindById(productId);
                    var shoppingCartItem = new ShoppingCartItem
                    {
                        TokenCode = StringHelper.GetString(),
                        LastUpdateTime = DateTime.Now,
                        Product = new ProductModel
                        {
                            Id = product.Id,
                            Name = product.Name,
                            Url = product.GetUrl(),
                            Price = product.Price,
                            ImageDefault = product.ImageDefault,
                            ImageDefaultAlt = product.Name,
                            ProductCode = product.ProductCode,
                            IsPromotion = product.IsPromotion,
                            PromotionPercent = product.PromotionPercent,
                            PromotionPrice = product.PromotionPrice,
                            ProductCategoryTitle = product.Category.Title,
                            ProductCategoryUrl = product.Category.GetPrefixUrl()

                        },
                        ItemCount = itemCount != 0 ? itemCount : 1
                    };
                    if (shoppingCartItem.Product.IsPromotion)
                    {
                        shoppingCartItem.ItemAmount = shoppingCartItem.Product.PromotionPrice * shoppingCartItem.ItemCount;
                    }
                    else
                    {
                        shoppingCartItem.ItemAmount = shoppingCartItem.Product.Price * shoppingCartItem.ItemCount;
                    }
                    shoppingCartItems.Add(shoppingCartItem);
                }
                Session[SessionKey.ShoppingCartItems] = shoppingCartItems;
            }
            return Json(new { totalCartItems = shoppingCartItems.Select(x=>x.ItemCount).Sum() });
        }
        public JsonResult DeleteShoppingCartItems(string tokenCode)
        {
            List<ShoppingCartItem> shoppingCartItems = (List<ShoppingCartItem>)Session[SessionKey.ShoppingCartItems];
            var shoppingCartItem = shoppingCartItems.Find(x => x.TokenCode == tokenCode);
            shoppingCartItems.Remove(shoppingCartItem);

            if(shoppingCartItems.Count > 0)
            {
                Session[SessionKey.ShoppingCartItems] = shoppingCartItems;

                //Get total amount 
                decimal totalAmount = 0;
                foreach (var item in shoppingCartItems)
                {
                    totalAmount += item.ItemAmount;
                }
                return Json(new { totalAmount = totalAmount, totalCartItems = shoppingCartItems.Select(x => x.ItemCount).Sum() });
            }
            else // In case: the shopping cart is null
            {
                Session[SessionKey.ShoppingCartItems] = null;
                return Json(new { totalAmount = 0, totalCartItems = 0 });
            }
        }

        public JsonResult UpdateShoppingCartItems(string tokenCode, int itemCount)
        {
            List<ShoppingCartItem> shoppingCartItems = (List<ShoppingCartItem>)Session[SessionKey.ShoppingCartItems];
            var shoppingCartItem = shoppingCartItems.Find(x => x.TokenCode == tokenCode);
            if (shoppingCartItem != null)
            {
                shoppingCartItem.ItemCount = itemCount;
                if (shoppingCartItem.Product.IsPromotion)
                {
                    shoppingCartItem.ItemAmount = shoppingCartItem.Product.PromotionPrice * shoppingCartItem.ItemCount;
                }
                else
                {
                    shoppingCartItem.ItemAmount = shoppingCartItem.Product.Price * shoppingCartItem.ItemCount;
                }
            }
            Session[SessionKey.ShoppingCartItems] = shoppingCartItems;
            return Json(new { totalCartItems = shoppingCartItems.Select(x=>x.ItemCount).Sum() });
        }

        //Dispose UnitOfWork Instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
