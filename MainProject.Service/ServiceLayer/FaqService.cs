﻿using MainProject.Core.Enums;
using MainProject.Service.Helper;
using MainProject.Service.Models.ViewModel.Faq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.ServiceLayer
{
    public class FaqService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage() => new IndexViewModel
        {
           Title = FaqsHelper.BuildTitle(UnitOfWork.CategoryRepository, CurrentLanguage),
           Faqs = FaqsHelper.BuildFaqs(UnitOfWork.FaqRepository, CurrentLanguage),
           SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories,
                                                                    GeneralHelper.GetCategoryByDisplayTemplate(UnitOfWork.CategoryRepository, CurrentLanguage,
                                                                    DisplayTemplateCollection.FaqTemplate).Id)
        };
    }
}
    