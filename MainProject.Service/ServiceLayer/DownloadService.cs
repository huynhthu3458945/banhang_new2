﻿using System.Linq;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.Controller.Download;
using MainProject.Service.Models.ViewModel.Download;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class DownloadService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage(long downloadCategoryId, int page) => new IndexViewModel
        {
            BodyModel = DownloadHelper.BuildBodySectionIndex(UnitOfWork.CategoryRepository, UnitOfWork.DownloadFileRepository, page, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, downloadCategoryId)
        };

        public ProductModel GetProductModel(int downloadFileId)
        => UnitOfWork.DownloadFileRepository.Find(x => x.Id == downloadFileId).Select(x => new ProductModel
        {
            FilePath = x.FilePath
        }).FirstOrDefault();
    }
}