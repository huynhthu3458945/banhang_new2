﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.ViewModel.Project;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class ProjectService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage(long projectCategoryId, int page) => new IndexViewModel
        {
            BodyModel = ProjectHelper.BuildBodySectionIndex(UnitOfWork.CategoryRepository, UnitOfWork.ArticleRepository, page, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, projectCategoryId)
        };

        public DetailViewModel GetDataOfDetailPage(long id) => new DetailViewModel
        {
            BodyModel = ProjectHelper.BuildBodySectionDetail(UnitOfWork.ArticleRepository, id, CurrentLanguage),
            BottomModel = ProjectHelper.BuildBottomSectionDetail(UnitOfWork.ArticleRepository, id, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Articles, id)
        };
    }
}