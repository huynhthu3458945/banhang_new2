﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.ViewModel.Introduction;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class IntroductionService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage(long introductionCategoryId) => new IndexViewModel
        {
            Location1 = IntroductionHelper.GetListLandingpageItem(UnitOfWork.LandingpageItemRepository, CurrentLanguage, PositionTypeCollection.FifthPosition),
            Location2 = IntroductionHelper.GetListLandingpageItem(UnitOfWork.LandingpageItemRepository, CurrentLanguage, PositionTypeCollection.SixthPosition),
            Location3 = IntroductionHelper.GetListLandingpageItem(UnitOfWork.LandingpageItemRepository, CurrentLanguage, PositionTypeCollection.SeventhPosition),
            Location4 = IntroductionHelper.GetListLandingpageItem(UnitOfWork.LandingpageItemRepository, CurrentLanguage, PositionTypeCollection.EighthPosition),
            ListLandingpageItem = HomeHelper.GetLandingpage(UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            ListArticle = IntroductionHelper.GetArticles(UnitOfWork.ArticleRepository, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, introductionCategoryId),
        };
    }
}