﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.ViewModel.Landingpage;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class LandingpageService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage(long landingpageCategoryId) => new IndexViewModel
        {
            SlideShowModel = LandingpageHelper.BuildSlideShowSection(UnitOfWork.SlideShowRepository, 
                                                                        UnitOfWork.CategoryRepository, CurrentLanguage),
            FirstSectionModel = LandingpageHelper.BuildFirstSection(UnitOfWork.LandingpageGroupRepository, 
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
                
            SecondSectionModel = LandingpageHelper.BuildSecondSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            ThirdSectionModel = LandingpageHelper.BuildThirdSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            FourthSectionModel = LandingpageHelper.BuildFourthSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            FifthSectionModel = LandingpageHelper.BuildFifthSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            SixthSectionModel = LandingpageHelper.BuildSixthSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.BranchRepository, CurrentLanguage),
            SeventhSectionModel = LandingpageHelper.BuildSeventhSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            EighthSectionModel = LandingpageHelper.BuildEighthSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.ProductRepository, CurrentLanguage),
            NinthSectionModel = LandingpageHelper.BuildNinthSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            TenthSectionModel = LandingpageHelper.BuildTenthSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            EleventhSectionModel = LandingpageHelper.BuildEleventhSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            TwelvethSectionModel = LandingpageHelper.BuildTwelvethSection(UnitOfWork.LandingpageGroupRepository,
                                                                UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork,EntityTypeCollection.Categories, landingpageCategoryId)
        };
    }
}