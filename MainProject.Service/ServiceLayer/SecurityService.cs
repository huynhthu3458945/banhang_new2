﻿using MainProject.Core.Enums;
using MainProject.Service.Helper;
using MainProject.Service.Models.ViewModel.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.ServiceLayer
{
    public class SecurityService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage()
        {
            var model = SecurityHelper.BuildSlideShowSection(UnitOfWork.ArticleRepository, UnitOfWork.CategoryRepository, CurrentLanguage);
            model.SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories,
                                                                    GeneralHelper.GetCategoryByDisplayTemplate(UnitOfWork.CategoryRepository, CurrentLanguage,
                                                                    DisplayTemplateCollection.SecurityTemplate).Id);
            return model;
        }
    }
}
