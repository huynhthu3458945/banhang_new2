﻿using System;
using System.Linq;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Service.Models.ViewModel.ShopppingCart;
using MainProject.Service.Models.ViewModel.Order;

namespace MainProject.Service.ServiceLayer
{

    public class OrderService : BaseService
    {
        #region Fields
        private readonly UnitOfWork unitOfWork;
        #endregion

        #region Constructor
        public OrderService()
        {
            unitOfWork = new UnitOfWork();
        }
        #endregion

        public List<OrderType> GetOrderTypes()
        {
            return unitOfWork.OrderTypeRepository.FindAll().Where(x => x.IsPublished 
                            && x.Language.LanguageKey == CurrentLanguage).OrderBy(x => x.Order).ToList();
        }

        public void SubmitOrder(OrderFormModel model, List<ShoppingCartItem> cartItems)
        {
            // Save order
            var order = new Order
            {
                BuyerName = model.Name,
                BuyerAddress = model.Address,
                BuyerEmail = model.Email,
                BuyerPhone = model.Phone,
                OrderTime = DateTime.Now,
                Note = model.Note,
                Amount = cartItems.Select(x=>x.ItemAmount).Sum(),
                OrderStatus = OrderStatusCollection.JustCreate,
                OrderType = unitOfWork.OrderTypeRepository.FindById(model.OrderTypeId)
            };
            unitOfWork.OrderRepository.Insert(order);
            foreach (var item in cartItems)
            {
                var product = item.Product;
                var orderItem = new OrderItem
                {
                    Product = unitOfWork.ProductRepository.FindById(item.Product.Id),
                    Order = order,
                    ItemCount = item.ItemCount,
                    Amount = item.ItemAmount,
                    Price = item.Product.Price
                };
                unitOfWork.OrderItemRepository.Insert(orderItem);
            }
            unitOfWork.Save();

            // Send Email
            var listEmail = new List<string>();
            listEmail.Add(order.BuyerEmail);
            var orderItems = unitOfWork.OrderItemRepository.FindAll().Where(x => x.Order.Id == order.Id).ToList();
            // Tạm thời đóng hàm gửi mail đi
            //MailHelper.SendOrderInfo(listEmail, order, orderItems, SettingHelper.GetValueSetting("OrderMailBody"));
        }
    }
}