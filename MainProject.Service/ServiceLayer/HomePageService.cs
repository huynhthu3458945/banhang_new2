﻿using System;
using System.Linq;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.ViewModel;
using MainProject.Service.Models.ViewModel.Home;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class HomePageService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage()
        {
            var model = new IndexViewModel
            {
                SlideShowModel = HomeHelper.BuildSlideShowSection(UnitOfWork.SlideShowRepository, UnitOfWork.CategoryRepository, CurrentLanguage),
                IntroductionModel = HomeHelper.BuildIntroductionSection(UnitOfWork, CurrentLanguage),
                ProductModel = HomeHelper.BuildProductSection(UnitOfWork.CategoryRepository, CurrentLanguage),
                SolutionModel = HomeHelper.BuildSolutionSection(UnitOfWork.SolutionRepository, CurrentLanguage),
                ProjectModel = HomeHelper.BuildProjectSection(UnitOfWork.CategoryRepository, UnitOfWork.ArticleRepository, CurrentLanguage),
                NewsModel = HomeHelper.BuildNewsSection(UnitOfWork.ArticleRepository, CurrentLanguage),
                SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories,
                                                                    GeneralHelper.GetCategoryByDisplayTemplate(UnitOfWork.CategoryRepository, CurrentLanguage,
                                                                    DisplayTemplateCollection.HomeTemplate).Id),
                ListPartners = HomeHelper.GetPartners(UnitOfWork.PartnerRepository, CurrentLanguage).FindAll(n => n.Type == PartnerTypeCollection.Partner),
                ListCertifications = HomeHelper.GetPartners(UnitOfWork.PartnerRepository, CurrentLanguage).FindAll(n => n.Type == PartnerTypeCollection.Certifications),
                ListArticle35 = HomeHelper.GetArticle(UnitOfWork.ArticleRepository, CurrentLanguage, 35).Take(3).ToList(),
                ListArticle50 = HomeHelper.GetArticle(UnitOfWork.ArticleRepository, CurrentLanguage, 50).Take(4).ToList(),
                ListProduct = HomeHelper.GetProduct(UnitOfWork.ProductRepository, CurrentLanguage),
                ListCategory = HomeHelper.GetCategory(UnitOfWork.CategoryRepository, CurrentLanguage),
                ListFeedback = HomeHelper.GetFeedback(UnitOfWork.FeedbackRepository, CurrentLanguage),
                ListLandingpageItem = HomeHelper.GetLandingpage(UnitOfWork.LandingpageItemRepository, CurrentLanguage),
                ListOrder = HomeHelper.GetListOrder(UnitOfWork.OrderRepository),
                Category = HomeHelper.GetCategoryHome(UnitOfWork.CategoryRepository, CurrentLanguage)

            };

            return model;
        }
        public string GetProductsString(long productCatogoryId)
        {
            var category = UnitOfWork.CategoryRepository.FindById(productCatogoryId);
            var products = UnitOfWork.ProductRepository.FindAll().Where(x => x.Category.Id == productCatogoryId && x.IsPublished).Take(8)
                                                                        .OrderByDescending(x => x.Id).ToList();
            var str = "<div class='row'>";
            foreach (var product in products)
            {
                str += "<div class='item_product'>"
                            + "<div class='ctent_item_pduct'>";
                if (!product.IsPriceContact)
                {
                    if (product.IsPromotion)
                    {
                        str += "<span class='sale'>Khuyến mãi " + product.PromotionPercent + "%</span>";
                    }
                }
                str += "<div class='img_item'>"
                        + "<a href='" + product.GetUrl() + "'><img src='" + product.ImageDefault + "' alt='" + product.Name + "'></a>"
                    + "</div>"
                    + "<div class='ctent_txt'>"
                        + "<a href='" + product.GetUrl() + "'>"
                            + "<h3>" + product.Name + "</h3>"
                            + "<p>" + product.ShortDescription + "</p>"
                        + "</a>"
                        + "<div class='box_price'>";
                if (product.IsPriceContact)
                {
                    str += "<span class='new_price'>Giá liên hệ</span>";
                }
                else
                {
                    if (product.IsPromotion)
                    {
                        str += "<span class='new_price'>" + string.Format("{0:0,0}", product.PromotionPrice) + " VNĐ</span>"
                            + "<span class='old_price'><del>" + string.Format("{0:0,0}", product.Price) + " VNĐ</del></span>";
                    }
                    else
                    {
                        str += "<span class='new_price'>" + string.Format("{0:0,0}", product.Price) + " VNĐ</span>";
                    }
                }
                str += "</div>"
                    + "<div class='btncart'>";
                if (product.IsPriceContact)
                {
                    str += "<a href='" + product.GetUrl() + "'><button>Xem Chi tiết</button></a>";
                }
                else
                {
                    str += "<button onclick='AddToCart(" + product.Id + ")'><i class='ic-Purcha-icon'></i>Mua hàng</button>";
                }
                str += "</div>"
                    + "</div>"
                + "</div>"
            + "</div>";
            }
            str += "</div>";
            if (products.Count > 0)
            {
                str += "<div class='see_more_general'>"
                    + "<a href='" + category.GetPrefixUrl() + "'> Xem thêm sản phầm<i class='ic-Arrow-right'></i></a>"
                    + "</div>";
            }

            return str;
        }

        public List<SitemapModel> GetSitemap()
        {
            var results = new List<SitemapModel>();

            // Set categories list
            var categoriesList = UnitOfWork.CategoryRepository.FindAll().ToList();
            foreach (var category in categoriesList)
            {
                if (category.DisplayTemplate != DisplayTemplateCollection.HomeTemplate)
                {
                    if (category.Parent == null)
                    {
                        results.Add(new SitemapModel
                        {
                            Link = category.GetPrefixUrl(),
                            Created = DateTime.Now,
                            Priority = "1.00"
                        });
                    }
                    else
                    {
                        results.Add(new SitemapModel
                        {
                            Link = category.GetPrefixUrl(),
                            Created = DateTime.Now,
                            Priority = "0.80"
                        });
                    }

                }
            }

            // Set articles list
            var articlesList = UnitOfWork.ArticleRepository.FindAll().ToList();
            foreach (var article in articlesList)
            {
                results.Add(new SitemapModel
                {
                    Link = article.GetUrl(),
                    Created = DateTime.Now,
                    Priority = "0.60"
                });
            }

            // Set products list
            var productsList = UnitOfWork.ProductRepository.FindAll().ToList();
            foreach (var product in productsList)
            {
                results.Add(new SitemapModel
                {
                    Link = product.GetUrl(),
                    Created = DateTime.Now,
                    Priority = "0.60"
                });
            }
            return results;
        }
    }
}