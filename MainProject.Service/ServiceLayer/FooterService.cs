﻿using MainProject.Data;
using MainProject.Service.Models.ViewModel.Footer;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class FooterService : BaseService
    {
        public FooterViewModel GetFooterData() => new FooterViewModel
        {
            TopModel = FooterHelper.BuildTopSection(UnitOfWork.PartnerRepository),
            LeftModel = FooterHelper.BuildLeftSection(UnitOfWork.BranchRepository,CurrentLanguage),
            MiddleModel = FooterHelper.BuildMiddleSection(UnitOfWork.CategoryRepository, CurrentLanguage),
            RightModel = FooterHelper.BuildRightSection(CurrentLanguage),
            SocialModel = FooterHelper.BuildSocialSection(UnitOfWork.BranchRepository, CurrentLanguage),
            ListMenu = FooterHelper.GetMenu(UnitOfWork.MenuItemRepository, CurrentLanguage)
        };
    }
}