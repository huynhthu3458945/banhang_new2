﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.ViewModel.Product;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class ProductService : BaseService
    {
        public Child2ViewModel GetDataOfIndexPage(long child2CategoryId, int page) => new Child2ViewModel
        {
            SlideShowModel = ProductHelper.BuildSlideShowSectionProduct(UnitOfWork.SlideShowRepository, UnitOfWork.CategoryRepository, CurrentLanguage),
            BodyModel = ProductHelper.BuildBodySectionIndex(UnitOfWork.CategoryRepository, UnitOfWork.ProductRepository, child2CategoryId, page, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, child2CategoryId),
            Category = ProductHelper.GetCategory(UnitOfWork.CategoryRepository, child2CategoryId)
        };

        public Child1ViewModel GetDataOfChild1Page(long child1CategoryId) => new Child1ViewModel
        {
            SlideShowModel = ProductHelper.BuildSlideShowSectionProduct(UnitOfWork.SlideShowRepository, UnitOfWork.CategoryRepository, CurrentLanguage),
            BodyModel = ProductHelper.BuildBodySectionChild1(UnitOfWork.CategoryRepository, child1CategoryId, CurrentLanguage),
            ListProduct = ProductHelper.GetProduct(UnitOfWork.ProductRepository, child1CategoryId, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork,EntityTypeCollection.Categories, child1CategoryId),
            Category = ProductHelper.GetCategory(UnitOfWork.CategoryRepository, child1CategoryId)

        };

        public Child2ViewModel GetDataOfChild2Page(long child2CategoryId, int page) => new Child2ViewModel
        {
            BodyModel = ProductHelper.BuildBodySectionChild2(UnitOfWork.CategoryRepository, UnitOfWork.ProductRepository, child2CategoryId, page, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, child2CategoryId)
        };
     
        public Child3ViewModel GetDataOfChild3Page(long child3CategoryId, int page) => new Child3ViewModel
        {
            BodyModel = ProductHelper.BuildBodySectionChild3(UnitOfWork.CategoryRepository, UnitOfWork.ProductRepository, child3CategoryId, page, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, child3CategoryId)
        };

        public DetailViewModel GetDataOfDetailPage(long productId) => new DetailViewModel
        {
            BranchModel = ContactHelper.BuildBranchSection(UnitOfWork.CategoryRepository, UnitOfWork.BranchRepository, CurrentLanguage),
            TopModel = ProductHelper.BuildTopSectionDetail(UnitOfWork.ProductRepository, productId),
            ListImage= GeneralHelper.GetImageModelsOfEntity(UnitOfWork, EntityImageTypeCollection.Product, productId, string.Empty),
            BodyModel = ProductHelper.BuildBodySectionDetail(UnitOfWork, UnitOfWork.ArticleRepository, UnitOfWork.ProductRepository, productId),
            BottomModel = ProductHelper.BuildBottomSectionDetail(UnitOfWork.ProductRepository, productId,CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Products, productId),
        };
    }
}