﻿using MainProject.Data;
using MainProject.Framework.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class BaseService
    {
        #region Fields
        public readonly UnitOfWork UnitOfWork;
        public readonly string CurrentLanguage;
        #endregion

        #region Constructor

        public BaseService()
        {
            UnitOfWork = new UnitOfWork();
            CurrentLanguage = CultureHelper.GetCurrentLanguage();
        }
        #endregion



        //public bool CheckingHaveChildren(Category category)
        //{
        //    var childrens = unitOfWork.CategoryRepository.FindAll().Where(x => x.Parent.Id == category.Id && x.IsPublished).OrderBy(x => x.Order).ToList();
        //    if (childrens.Any())
        //    {
        //        return true;
        //    }
        //    return false;
        //}
        //public List<Category> GetChildrens(Category category)
        //{
        //    var childrens = unitOfWork.CategoryRepository.FindAll().Where(x => x.Parent.Id == category.Id && x.IsPublished).OrderBy(x => x.Order).ToList();
        //    return childrens;
        //}
        //public bool CheckingHaveChildrenArticles(Category category)
        //{
        //    var childrens = unitOfWork.ArticleRepository.FindAll().Where(x => x.Category.Id == category.Id && x.IsPublished).OrderBy(x => x.Order).ToList();
        //    if (childrens.Any())
        //    {
        //        return true;
        //    }
        //    return false;
        //}
        //public List<Article> GetChildrensArticles(Category category)
        //{
        //    var childrens = unitOfWork.ArticleRepository.FindAll().Where(x => x.Category.Id == category.Id && x.IsPublished).OrderBy(x => x.Order).ToList();
        //    return childrens;
        //}
        //public List<string> GetImagesOfArticle(Article article)
        //{
        //    var imagesList = new List<string>();

        //    if (article != null)
        //    {
        //        String[] files = Directory.GetFiles(System.Web.HttpContext.Current.Server.MapPath(article.ImageFolder));
        //        foreach (var file in files)
        //        {
        //            var imagePath = article.ImageFolder + "/" +
        //                        file.Replace(System.Web.HttpContext.Current.Server.MapPath(article.ImageFolder), "").Replace("\\", "");

        //            imagesList.Add(imagePath);
        //        }
        //    }
        //    return imagesList;
        //}

        //public List<string> GetImagesOfProduct(Product product)
        //{
        //    var imagesList = new List<string>();

        //    if (product != null)
        //    {
        //        if (product.ImageDefault != null)
        //        {
        //            imagesList.Add(product.ImageDefault);
        //        }
        //        String[] files = Directory.GetFiles(System.Web.HttpContext.Current.Server.MapPath(product.ImageFolder));
        //        foreach (var file in files)
        //        {
        //            var imagePath = product.ImageFolder + "/" +
        //                        file.Replace(System.Web.HttpContext.Current.Server.MapPath(product.ImageFolder), "").Replace("\\", "");
        //            if (imagePath != product.ImageDefault)
        //            {
        //                imagesList.Add(imagePath);
        //            }
        //        }
        //    }
        //    return imagesList;
        //}

        //public Category GetHomeCategory()
        //{
        //    var cat = unitOfWork.CategoryRepository.FindUnique(x => x.DisplayTemplate == DisplayTemplateCollection.HomeTemplate
        //                                                        && x.Language.LanguageKey == CurrentLanguage);
        //    return cat;
        //}

        //public Category GetNewsCategory()
        //{
        //    var cat = unitOfWork.CategoryRepository.FindUnique(x => x.DisplayTemplate == DisplayTemplateCollection.NewsTemplate
        //                                                             && x.Language.LanguageKey == CurrentLanguage);
        //    return cat;
        //}

    }
}