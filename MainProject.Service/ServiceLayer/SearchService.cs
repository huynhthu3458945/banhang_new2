﻿using System.Collections.Generic;
using System.Linq;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Service.Models.ViewModel.Search;

namespace MainProject.Service.ServiceLayer
{

    public class SearchService : BaseService
    {
        #region Fields
        private readonly UnitOfWork unitOfWork;
        private int searchPageItems;
        #endregion

        #region Constructor
        public SearchService()
        {
            unitOfWork = new UnitOfWork();
            searchPageItems = 6;
        }
        #endregion

        public SearchViewModel Search(string text, int page)
        {
            if (page < 1) page = 1;
            var textS = text.Trim().ToLower();
            var searchObjects = new List<SearchObject>();

            //var news = unitOfWork.ArticleRepository.Find(
            //        x => x.Title.Contains(textS) || x.Description.Contains(textS) || x.Body.Contains(textS))
            //        .Where(x => (x.Category.DisplayTemplate == DisplayTemplateCollection.NewsTemplate 
            //                    || x.Category.DisplayTemplate == DisplayTemplateCollection.SolutionTemplate
            //                    || x.Category.DisplayTemplate == DisplayTemplateCollection.ProjectTemplate) &&
            //        x.Language.LanguageKey == CurrentLanguage && x.IsPublished)
            //        .OrderByDescending(x => x.Id).ToList();

            var news = unitOfWork.ArticleRepository.FindAll().ToList();

            foreach (var newsItem in news)
            {
                var newsObject = new SearchObject
                {
                    Name = newsItem.Title,
                    ImageItem = new Models.SectionModel.General.G_ImageModel { Image = newsItem.ImageDefault, Alt = newsItem.Title },
                    Description = newsItem.Description,
                    Url = newsItem.GetUrl()
                };
                searchObjects.Add(newsObject);
            }

            var products = unitOfWork.ProductRepository.Find(x => x.Name.Contains(textS) || x.ProductCode.Contains(textS)
                                                                || x.Description.Contains(textS) || x.DetailInfo.Contains(textS)
                                                                || x.DetailInfo.Contains(textS) || x.DetailInfo.Contains(textS))
                        .Where(x => x.Language.LanguageKey == CurrentLanguage && x.IsPublished)
                        .OrderByDescending(x => x.Id).ToList();

                            foreach (var product in products)
                            {
                                var productObject = new SearchObject
                                {
                                    Name = product.Name,
                                    ImageItem = new Models.SectionModel.General.G_ImageModel { Image = product.ImageDefault, Alt= product.Name },
                                    Description = product.ShortDescription,
                                    Url = product.GetUrl()
                                };
                                searchObjects.Add(productObject);
                            }

            var count = searchObjects.Count();

            var SearchObjects = searchObjects.Skip((page - 1) * searchPageItems).Take(searchPageItems).ToList();

            var searchViewModel = new SearchViewModel()
            {
                SearchObjects = SearchObjects,
                PagingModel = new PagingModel(count, searchPageItems, page, "href='/Search?text=" + text + "&page=" + "{0}'"),
                TotalResults = searchObjects.Count,
                Text = text
            };

            return searchViewModel;
        }
    }
}