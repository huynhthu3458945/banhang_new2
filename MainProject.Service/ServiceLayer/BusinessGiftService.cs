﻿using MainProject.Core.Enums;
using MainProject.Service.Helper;
using MainProject.Service.Models.ViewModel.BusinessGift;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.ServiceLayer
{
    public class BusinessGiftService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage() => new IndexViewModel
        {
            SlideShowModel = BusinessGiftHelper.BuildSlideShowSection(UnitOfWork.SlideShowRepository, UnitOfWork.CategoryRepository, CurrentLanguage),
            ListPartners = BusinessGiftHelper.BuilListPartners(UnitOfWork.LandingpageGroupRepository, UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            DescriptionModel = BusinessGiftHelper.BuilDescriptionModel(UnitOfWork.LandingpageGroupRepository, UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            ListContentModel01 = BusinessGiftHelper.BuilListContentModel01(UnitOfWork ,UnitOfWork.LandingpageGroupRepository, UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            ListProducts = BusinessGiftHelper.BuilListProducts(UnitOfWork.LandingpageGroupRepository, UnitOfWork.LandingpageItemRepository, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories,
                                                                    GeneralHelper.GetCategoryByDisplayTemplate(UnitOfWork.CategoryRepository, CurrentLanguage,
                                                                    DisplayTemplateCollection.BusinessGiftsTemplate).Id)
        };
    }
}
