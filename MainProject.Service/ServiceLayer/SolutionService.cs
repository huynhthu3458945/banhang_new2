﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.ViewModel.Solution;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class SolutionService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage(long solutionCategoryId, int page) => new IndexViewModel
        {
            BodyModel = SolutionHelper.BuildBodySectionIndex(UnitOfWork.CategoryRepository, UnitOfWork.ArticleRepository, page, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, solutionCategoryId)
        };

        public DetailViewModel GetDataOfDetailPage(long id) => new DetailViewModel
        {
            BodyModel = SolutionHelper.BuildBodySectionDetail(UnitOfWork.ArticleRepository, id, CurrentLanguage),
            BottomModel = SolutionHelper.BuildBottomSectionDetail(UnitOfWork.ArticleRepository, id, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Articles, id)
        };
    }
}