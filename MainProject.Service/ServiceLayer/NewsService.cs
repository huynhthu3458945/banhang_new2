﻿using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.ViewModel.News;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{

    public class NewsService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage(long newsCategoryId, int page) => new IndexViewModel
        {
            SlideShowModel = NewsHelper.BuildSlideShowSection(UnitOfWork.SlideShowRepository, UnitOfWork.CategoryRepository, CurrentLanguage),
            BodyModel = NewsHelper.BuildBodySectionIndex(UnitOfWork.CategoryRepository, UnitOfWork.ArticleRepository, UnitOfWork.SlideShowRepository, page, newsCategoryId, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, newsCategoryId)           
        }; 
        public QuaTangViewModel GetQuaTang(long newsCategoryId) => new QuaTangViewModel
        {
            SlideShowModel = NewsHelper.BuildSlideShowSectionQuaTang(UnitOfWork.SlideShowRepository, UnitOfWork.CategoryRepository, CurrentLanguage),
            BranchModel = ContactHelper.BuildBranchSection(UnitOfWork.CategoryRepository, UnitOfWork.BranchRepository, CurrentLanguage),
            Location1 = NewsHelper.GetListLandingpageItem(UnitOfWork.LandingpageItemRepository, CurrentLanguage, PositionTypeCollection.FirstPosition),
            Location2 = NewsHelper.GetListLandingpageItem(UnitOfWork.LandingpageItemRepository, CurrentLanguage, PositionTypeCollection.SecondPosition),
            Location3 = NewsHelper.GetListLandingpageItem(UnitOfWork.LandingpageItemRepository, CurrentLanguage, PositionTypeCollection.ThirdPosition),
            Location4 = NewsHelper.GetListLandingpageItem(UnitOfWork.LandingpageItemRepository, CurrentLanguage, PositionTypeCollection.FourthPosition),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories, newsCategoryId)           
        };

        public DetailViewModel GetDataOfDetailPage(long id) => new DetailViewModel
        {
            SlideShowModel = NewsHelper.BuildSlideShowSection(UnitOfWork.SlideShowRepository, UnitOfWork.CategoryRepository, CurrentLanguage),
            BodyModel = NewsHelper.BuildBodySectionDetail(UnitOfWork.ArticleRepository, UnitOfWork.SlideShowRepository, id, CurrentLanguage),
            BottomModel = NewsHelper.BuildBottomSectionDetail(UnitOfWork.ArticleRepository, id, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Articles, id)
        };
    }
}