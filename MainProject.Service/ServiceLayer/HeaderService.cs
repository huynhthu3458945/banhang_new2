﻿using MainProject.Data;
using MainProject.Service.Models.Header;
using MainProject.Service.Helper;
using System.Collections.Generic;
using MainProject.Core;

namespace MainProject.Service.ServiceLayer
{

    public class HeaderService : BaseService
    {
        public HeaderViewModel GetHeaderData() => new HeaderViewModel
        {
            MenuModel = HeaderHelper.BuildMenuSection(UnitOfWork.CategoryRepository, CurrentLanguage),
            ListMenu = HeaderHelper.GetMenu(UnitOfWork.MenuItemRepository, CurrentLanguage),
            InformationModel = HeaderHelper.BuildInformationSection(CurrentLanguage),
            ListLanguage = HeaderHelper.GetLanguage(UnitOfWork.LanguageRepository),

        };

        public MenuSupportModel GetMenuSupport(int order)
        {
            var model = new MenuSupportModel();
            model.ListMenu = HeaderHelper.BuildMenuSupport(UnitOfWork.MenuRepository, UnitOfWork.MenuItemRepository, CurrentLanguage);
            model.Order = order;

            return model;
        }

        public List<MenuItem> GetMenuMobiFooter()
       => HeaderHelper.GetMenuMobiFooter(UnitOfWork.MenuItemRepository, CurrentLanguage);

    }
}