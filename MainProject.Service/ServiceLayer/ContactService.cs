﻿using System;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Service.Models.ViewModel.Contact;
using MainProject.Service.Helper;

namespace MainProject.Service.ServiceLayer
{
    public class ContactService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage(long contactCategoryId) => new IndexViewModel
        {
            PlaceholderModel = ContactHelper.BuildPlaceHolderSection(CurrentLanguage),
            BranchModel = ContactHelper.BuildBranchSection(UnitOfWork.CategoryRepository, UnitOfWork.BranchRepository, CurrentLanguage),
            SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories,
                contactCategoryId != 0 ? contactCategoryId : GeneralHelper.GetCategoryByDisplayTemplate(UnitOfWork.CategoryRepository,
                                                        CurrentLanguage, DisplayTemplateCollection.ContactTemplate).Id)
        };

        public void SaveContact(FormModel model)
        {
            var entity = new Contacts
            {
                Name = model.Name,
                Email = model.Email,
                Phone = model.Phone,
                Content = model.Content,
                CreatedDate = DateTime.Now
            };
            var listEmail = new List<string>();

            // In case: have email from users
            if (!string.IsNullOrEmpty(entity.Email))
            {
                listEmail.Add(entity.Email);
            }

            UnitOfWork.ContactRepository.Insert(entity);
            UnitOfWork.Save();
            MailHelper.SendContactInfomation(listEmail, entity);
        }
    }
}