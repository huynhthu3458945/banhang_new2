﻿using MainProject.Core.Enums;
using MainProject.Service.Helper;
using MainProject.Service.Models.ViewModel.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.ServiceLayer
{
    public class PayService : BaseService
    {
        public IndexViewModel GetDataOfIndexPage()
        {
            var model = PayHelper.BuildSlideShowSection(UnitOfWork.ArticleRepository, UnitOfWork.CategoryRepository, CurrentLanguage);
            model.SEOModel = GeneralHelper.BuildSEOModelByEntityType(UnitOfWork, EntityTypeCollection.Categories,
                                                                    GeneralHelper.GetCategoryByDisplayTemplate(UnitOfWork.CategoryRepository, CurrentLanguage,
                                                                    DisplayTemplateCollection.PayTemplate).Id);
            return model;
        }
    }
}
