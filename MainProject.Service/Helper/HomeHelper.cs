﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Home;

namespace MainProject.Service.Helper
{
    public static class HomeHelper
    {
        public static SlideShowModel BuildSlideShowSection(GenericRepository<SlideShow> slideShowRepository,
                                                            GenericRepository<Category> categoryRepository,
                                                            string currentLanguage)
        {
            var category = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.HomeTemplate);
            return new SlideShowModel
            {
                SlideShowImages = slideShowRepository.Find(x => x.IsPublished && x.Category.Id == category.Id && x.Language.LanguageKey==currentLanguage).OrderBy(x => x.Order).ToList().Select
                                          (x => new SlideShowImage
                                          {
                                              Image = x.Image,
                                              Alt = x.Name,
                                              IsVideo = x.IsVideo,
                                              VideoId = x.VideoId,
                                          }).ToList()
            };
        }

        public static Category GetCategoryHome(GenericRepository<Category> categoryRepository,
                                                            string currentLanguage)
        {
            return GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.HomeTemplate);
        }

        public static IntroductionModel BuildIntroductionSection(UnitOfWork unitOfWork, string currentLanguage)
        {
            var category = GeneralHelper.GetCategoryByDisplayTemplate(unitOfWork.CategoryRepository, currentLanguage, DisplayTemplateCollection.IntroductionTemplate);

            return category != null ? new IntroductionModel
            {
                Title = ResourceHelper.GetResource(ResourceKeyCollection.IntroductionSectionTitle, currentLanguage),
                Description = category.Description,
                ImageItems = GeneralHelper.GetImageModelsOfEntity(unitOfWork, EntityImageTypeCollection.Category, category.Id, category.Title + category.Id + "_"),
                Url = category.GetPrefixUrl()
            } : null;
        }
        public static ProductModel BuildProductSection(GenericRepository<Category> categoryRepository, string currentLanguage)
        => new ProductModel
        {
            Title = ResourceHelper.GetResource(ResourceKeyCollection.ProductSectionTitle, currentLanguage),
            HotProductCategoryItems = categoryRepository.Find(x => x.IsPublished && x.IsHotProductCategory
                                                                && x.Language.LanguageKey == currentLanguage)
                                                                .OrderBy(x => x.Order).ToList()
                                                                .Select(x => new HotProductCategoryItem
                                                                {
                                                                    ProductCategoryId = x.Id,
                                                                    Title = x.Title,
                                                                    Url = x.GetPrefixUrl()
                                                                }).ToList()
        };
        public static SolutionModel BuildSolutionSection(GenericRepository<Solution> solutionRepository, string currentLanguage)
        => new SolutionModel
        {
            Title = ResourceHelper.GetResource(ResourceKeyCollection.SolutionSectionTitle, currentLanguage),
            SolutionItems = solutionRepository.Find(x => x.IsPublished && x.Language.LanguageKey == currentLanguage)
                                                                .OrderBy(x => x.Order)
                                                                .Select(x => new SolutionItem
                                                                {
                                                                    Title = x.Title,
                                                                    Url = x.Link
                                                                }).ToList()
        };
        public static ProjectModel BuildProjectSection(GenericRepository<Category> categoryRepository, GenericRepository<Article> articleRepository, string currentLanguage)
        {
            //var sql = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.DisplayTemplate == DisplayTemplateCollection.ProjectTemplate).Take(4);
            return new ProjectModel
            {
                //Title = ResourceHelper.GetResource(ResourceKeyCollection.ProjectSectionTitle, currentLanguage),
                //First_ProjectItem = ProjectHelper.GetCustomOrderedProjects(sql).Select(x => new ProjectItem
                //{
                //    Title = x.Title,
                //    Description = x.Description,
                //    Image = x.BigImage,
                //    Alt = x.Title,
                //    Url = x.GetUrl(),
                //    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                //                                              : x.CreateDate.ToString("MMM dd, yyyy"),

                //}).FirstOrDefault(),
                //NextThree_ProjectItems = ProjectHelper.GetCustomOrderedProjects(sql).Skip(1).Take(3).Select(x => new ProjectItem
                //{
                //    Title = x.Title,
                //    Description = x.Description,
                //    Image = x.ImageDefault,
                //    Alt = x.Title,
                //    Url = x.GetUrl(),
                //    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                //                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                //}).ToList(),
                //Url = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.ProjectTemplate).GetPrefixUrl()
            };
        }

        public static NewsModel BuildNewsSection(GenericRepository<Article> articleRepository, string currentLanguage)
        => new NewsModel
        {
            Title = ResourceHelper.GetResource(ResourceKeyCollection.NewsSectionTitle, currentLanguage),
            //Three_NewsItems = articleRepository.FindAll().Where(x => x.Category.DisplayTemplate == DisplayTemplateCollection.NewsTemplate
            //                                                    && x.IsPublished && x.Language.LanguageKey == currentLanguage).ToList()
            //                                                .OrderByDescending(x => x.CreateDate)
            //                                                .Select(x => new NewsItem
            //                                                {
            //                                                    Title = x.Title,
            //                                                    Description = x.Description,
            //                                                    Image = x.ImageDefault,
            //                                                    Alt = x.Title,
            //                                                    Url = x.GetUrl(),
            //                                                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
            //                                              : x.CreateDate.ToString("MMM dd, yyyy"),
            //                                                }).ToList()
        };



        public static List<Partner> GetPartners(GenericRepository<Partner> partnerRepository, string currentLanguage)
        {
            var categories = partnerRepository.FindAll().ToList();
            return categories.AsQueryable().ToList();
        }
        public static List<Article> GetArticle(GenericRepository<Article> articleRepository, string currentLanguage, int id)
        {
            if (id != 35)
            {
                var categories = articleRepository.Find(v => v.Category.Id == id && v.Language.LanguageKey == currentLanguage).Take(3);
                return categories.AsQueryable().ToList();
            }
            else
            {
                var categories = articleRepository.Find(v => v.Category.Id == id && v.Language.LanguageKey == currentLanguage).Take(4);
                return categories.AsQueryable().ToList();
            }

        }
        public static List<Product> GetProduct(GenericRepository<Product> productRepository, string currentLanguage)
        {
            var product = productRepository.Find(v => v.Language.LanguageKey == currentLanguage);
            return product.AsQueryable().ToList();

        }
        public static List<Category> GetCategory(GenericRepository<Category> categoryRepository, string currentLanguage)
        {
            var product = categoryRepository.Find(v => v.Language.LanguageKey == currentLanguage);
            return product.AsQueryable().Where(n => n.Parent != null).ToList();

        }

        public static List<Feedback> GetFeedback(GenericRepository<Feedback> FeedbackRepository, string currentLanguage)
        {
            var product = FeedbackRepository.Find(v => v.Language.LanguageKey == currentLanguage);
            return product.AsQueryable().ToList();

        }
        public static List<LandingpageItem> GetLandingpage(GenericRepository<LandingpageItem> LandingpagRepository, string currentLanguage)
        {
            var product = LandingpagRepository.Find(v => v.Language.LanguageKey == currentLanguage && v.LandingpageGroup.PositionType==PositionTypeCollection.NinethPosition);
            return product.AsQueryable().ToList();

        }
        public static List<Order> GetListOrder(GenericRepository<Order> OrderRepository)
        {
            var order = OrderRepository.FindAll();
            return order.AsQueryable().Where(n=>n.OrderTime <= DateTime.Now).ToList();

        }
        public static List<string> GetImagesOfProduct(string imageFolder)
        {
            var imagesList = new List<string>();
            string folder = System.Web.HttpContext.Current.Server.MapPath(imageFolder) ?? "";

            if (Directory.Exists(folder))
            {
                String[] files = Directory.GetFiles(folder);
                foreach (var file in files)
                {
                    var imagePath = imageFolder + "/" +
                                file.Replace(System.Web.HttpContext.Current.Server.MapPath(imageFolder), "").Replace("\\", "");

                    imagesList.Add(imagePath);

                }
            }

            return imagesList;
        }
    }
}
