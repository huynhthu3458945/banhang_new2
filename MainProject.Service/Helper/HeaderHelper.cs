﻿using System.Collections.Generic;
using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.Header;

namespace MainProject.Service.Helper
{
    public static class HeaderHelper
    {
        public static InformationModel BuildInformationSection(string currentLanguage)
        => new InformationModel
        {
            HotLineTitle = ResourceHelper.GetResource(ResourceKeyCollection.HotLineTitle, currentLanguage),
            HotLineContent = ResourceHelper.GetResource(ResourceKeyCollection.HotLineContent, currentLanguage),
            EmailContent = ResourceHelper.GetResource(ResourceKeyCollection.EmailContent, currentLanguage),
            InputSearchKeywords = ResourceHelper.GetResource(ResourceKeyCollection.InputSearchKeywords, currentLanguage)
        };
        public static List<MenuItem> GetMenu(GenericRepository<MenuItem> categoryRepository, string currentLanguage)
        {
            var categories = categoryRepository.Find(n => n.Language.LanguageKey == currentLanguage && n.Menu.MenuType==MenuTypeCollection.HeaderMenu).ToList();
            return categories.AsQueryable().ToList();
        }
        public static List<MenuItem> GetMenuMobiFooter(GenericRepository<MenuItem> categoryRepository, string currentLanguage)
        {
            var categories = categoryRepository.Find(n => n.Language.LanguageKey == currentLanguage && n.Menu.MenuType == MenuTypeCollection.MobiFooterMenu).ToList();
            return categories.AsQueryable().ToList();
        }
        public static List<Language> GetLanguage(GenericRepository<Language> LanguageRepository)
        {
            var language = LanguageRepository.Find(z=>z.IsPublished).ToList();
            return language.AsQueryable().ToList();
        }
        public static MenuModel BuildMenuSection(GenericRepository<Category> categoryRepository, string currentLanguage)
        {
            // Get menuHtmlString
            var categories = categoryRepository.FindAll().Where(x => x.IsSystem == false && x.Parent.Parent == null
                                                                            && x.Language.LanguageKey == currentLanguage)
                                                                            .OrderBy(x => x.Order).ToList();
            var menuHtmlString = "";
            foreach (var category in categories)
            {
                if (category.DisplayTemplate == DisplayTemplateCollection.ProductTemplate)
                {
                    menuHtmlString += "<li class='product'><a href='#'>" + category.Title + "</a>";
                    if (GeneralHelper.CheckingHaveChildrenCategories(categoryRepository, category.Id))
                    {
                        menuHtmlString += "<span class='ic-Arrow-right arrow_menu_first'><i class='ic-Arrow-right'></i></span>"
                                        + "<div class='subcatemenu'>"
                                        + "<div class='back_to_page' id='back_to_page'>Quay về menu trước</div>"
                                        + "<div class='ctent_subcatemenu'>"
                                        + "<div class='container'>"
                                        + "<ul class='col_sub_menu'>";
                        foreach (var child1Category in GeneralHelper.GetChildrenCategories(categoryRepository, category.Id))
                        {
                            if (!GeneralHelper.CheckingHaveChildrenCategories(categoryRepository, child1Category.Id))
                            {
                                menuHtmlString += "<li><p><a href='" + child1Category.GetPrefixUrl() + "'><i><img src='" + child1Category.ImageIcon + "' alt='" + child1Category.Title + "' /></i>" + child1Category.Title + "</a></p></li>";
                            }
                            else
                            {
                                menuHtmlString += "<li>"
                                                + "<p>"
                                                + "<a href='" + child1Category.GetPrefixUrl() + "'><i><img src='" + child1Category.ImageIcon + "' alt='" + child1Category.Title + "' /></i>" + child1Category.Title + "</a>"
                                                + "<span class='ic-Arrow-right arrow_menu_second'></span>"
                                                + "</p>"
                                                + "<ul class='list_sub_second'>";
                                foreach (var child2Category in GeneralHelper.GetChildrenCategories(categoryRepository, child1Category.Id))
                                {
                                    if (!GeneralHelper.CheckingHaveChildrenCategories(categoryRepository, child2Category.Id))
                                    {
                                        menuHtmlString += "<li><a href='" + child2Category.GetPrefixUrl() + "'>" + child2Category.Title + "</a></li>";
                                    }
                                    else
                                    {
                                        menuHtmlString += "<li>"
                                                        + "<a href='" + child2Category.GetPrefixUrl() + "'>" + child2Category.Title + "</a>"
                                                        + "<span class='arrow_menu_first'><i class='ic-Arrow-right'></i></span>"
                                                        + "<ul>";
                                        foreach (var child3Category in GeneralHelper.GetChildrenCategories(categoryRepository, child2Category.Id))
                                        {
                                            menuHtmlString += "<li><a href='" + child3Category.GetPrefixUrl() + "'>" + child3Category.Title + "</a></li>";
                                        }
                                        menuHtmlString += "</ul>"
                                                        + "</li>";
                                    }
                                }

                                menuHtmlString += "</ul>"
                                                + "</li>";
                            }
                        }
                        menuHtmlString += "</ul>"
                                        + "</div>"
                                        + "</div>"
                                        + "</div>";
                    }

                    menuHtmlString += "</li>";
                }
                else if (category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate)
                {
                    menuHtmlString += "<li class='product'><a href='#'>" + category.Title + "</a>";
                    if (GeneralHelper.CheckingHaveChildrenCategories(categoryRepository, category.Id))
                    {
                        menuHtmlString += "<span class='ic-Arrow-right arrow_menu_first'><i class='ic-Arrow-right'></i></span>"
                                        + "<div class='subcatemenu'>"
                                        + "<div class='back_to_page' id='back_to_page'>Quay về menu trước</div>"
                                        + "<div class='ctent_subcatemenu'>"
                                        + "<div class='container'>"
                                        + "<ul class='col_sub_menu'>";
                        foreach (var child1Category in GeneralHelper.GetChildrenCategories(categoryRepository, category.Id))
                        {
                            if (!GeneralHelper.CheckingHaveChildrenCategories(categoryRepository, child1Category.Id))
                            {
                                menuHtmlString += "<li><p><a href='" + child1Category.GetPrefixUrl() + "'><i><img src='" + child1Category.ImageIcon + "' alt='" + child1Category.Title + "' /></i>" + child1Category.Title + "</a></p></li>";
                            }
                            else
                            {
                                menuHtmlString += "<li>"
                                                + "<p>"
                                                + "<a href='" + child1Category.GetPrefixUrl() + "'><i><img src='" + child1Category.ImageIcon + "' alt='" + child1Category.Title + "' /></i>" + child1Category.Title + "</a>"
                                                + "<span class='ic-Arrow-right arrow_menu_second'></span>"
                                                + "</p>"
                                                + "<ul class='list_sub_second'>";
                                foreach (var child2Category in GeneralHelper.GetChildrenCategories(categoryRepository, child1Category.Id))
                                {
                                    if (!GeneralHelper.CheckingHaveChildrenCategories(categoryRepository, child2Category.Id))
                                    {
                                        menuHtmlString += "<li><a href='" + child2Category.GetPrefixUrl() + "'>" + child2Category.Title + "</a></li>";
                                    }
                                    else
                                    {
                                        menuHtmlString += "<li>"
                                                        + "<a href='" + child2Category.GetPrefixUrl() + "'>" + child2Category.Title + "</a>"
                                                        + "<span class='arrow_menu_first'><i class='ic-Arrow-right'></i></span>"
                                                        + "<ul>";
                                        foreach (var child3Category in GeneralHelper.GetChildrenCategories(categoryRepository, child2Category.Id))
                                        {
                                            menuHtmlString += "<li><a href='" + child3Category.GetPrefixUrl() + "'>" + child3Category.Title + "</a></li>";
                                        }
                                        menuHtmlString += "</ul>"
                                                        + "</li>";
                                    }
                                }

                                menuHtmlString += "</ul>"
                                                + "</li>";
                            }
                        }
                        menuHtmlString += "</ul>"
                                        + "</div>"
                                        + "</div>"
                                        + "</div>";
                    }

                    menuHtmlString += "</li>";
                }
                else
                {
                    menuHtmlString += "<li><a href='" + category.GetPrefixUrl() + "'>" + category.Title + "</a></li>";
                }

            }

            return new MenuModel { MenuHtmlString = menuHtmlString };
        }

        public static List<MenuItem> BuildMenuSupport(GenericRepository<Menu> menucRepository, GenericRepository<MenuItem> menuItemRepository, string currentLanguage)
        {
            var menu = menucRepository.Find(z => z.MenuType == MenuTypeCollection.SupportMenu).FirstOrDefault();
            var menuitem = menuItemRepository.Find(z => z.Menu.Id == menu.Id && z.Language.LanguageKey == currentLanguage).ToList();
            return menuitem;
        }
    }
}
