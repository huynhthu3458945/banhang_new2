﻿using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.SectionModel.Introduction;
using MainProject.Service.Models.SectionModel.General;
using System.Collections.Generic;
using System.Linq;
using System;

namespace MainProject.Service.Helper
{
    public static class IntroductionHelper
    {
        public static TopModel BuildTopSection(
                                                GenericRepository<Article> articleRepository,
                                                GenericRepository<Category> categoryRepository,
                                                string currentLanguage)
        {
            var introductionCategory = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.IntroductionTemplate);
            var firstIntroductionArticle = articleRepository.FindUnique(x => x.Category.Id == introductionCategory.Id
                                                                                && x.Order == 1 && x.IsPublished);
            return new TopModel {
                Title = introductionCategory.Title,
                TopArticleTitle = firstIntroductionArticle.Title,
                TopArticleBody = firstIntroductionArticle.Body,
                TopArticleImage = firstIntroductionArticle.ImageDefault
            };
        }

        internal static List<Article> GetArticles(GenericRepository<Article> articleRepository, string currentLanguage)
        {
            var res = articleRepository.Find(z => z.IsPublished && z.Language.LanguageKey == currentLanguage && z.IsIntroduction).Take(3).ToList();
            return res;
        }

        public static BodyModel BuildBodySection(
                                                GenericRepository<Article> articleRepository,
                                                GenericRepository<Category> categoryRepository,
                                                string currentLanguage)
        {

            var introductionCategory = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.IntroductionTemplate);
            var secondIntroductionArticle = articleRepository.FindUnique(x => x.Category.Id == introductionCategory.Id
                                                                                && x.Order == 2 && x.IsPublished);
            return new BodyModel
            {
                BodyArticleTitle = secondIntroductionArticle.Title,
                BodyArticleBody = secondIntroductionArticle.Body,
                BodyArticleImageModel = new G_ImageModel
                {
                    Image = secondIntroductionArticle.ImageDefault,
                    Alt = secondIntroductionArticle.Title
                }
            };
        }
        public static BottomModel BuildBottomSection(
                                                GenericRepository<Article> articleRepository,
                                                GenericRepository<Category> categoryRepository,
                                                string currentLanguage)
        {
            var introductionCategory = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.IntroductionTemplate);
            var thirdIntroductionArticle = articleRepository.FindUnique(x => x.Category.Id == introductionCategory.Id
                                                                                && x.Order == 3 && x.IsPublished);
            return new BottomModel
            {
                BottomArticleTitle = thirdIntroductionArticle.Title,
                BottomArticleBody = thirdIntroductionArticle.Body,
                BottomArticleImage = thirdIntroductionArticle.ImageDefault
            };
        }

        public static List<LandingpageItem> GetListLandingpageItem(
                                                GenericRepository<LandingpageItem> landingpageItemRepository,
                                                string currentLanguage,
                                                PositionTypeCollection positionTypeCollection)
        {
            var res = landingpageItemRepository.Find(z => z.Language.LanguageKey == currentLanguage && z.LandingpageGroup.PositionType == positionTypeCollection);
            return res.AsQueryable().ToList();
        }
    }
}
