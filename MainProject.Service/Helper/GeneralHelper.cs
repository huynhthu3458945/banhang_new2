﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.SectionModel.General;
using MainProject.Service.Models.ViewModel.Base;

namespace MainProject.Service.Helper
{
    public static class GeneralHelper
    {
        public static Article GetArticleByDisplayTemplate(GenericRepository<Article> articleRepository,
                                                          string currentLanguage,
                                                          DisplayTemplateCollection type)
        => articleRepository.FindUnique(x => x.Category.DisplayTemplate == type && x.Language.LanguageKey == currentLanguage);

        public static Category GetCategoryByDisplayTemplate(GenericRepository<Category> categoryRepository,
                                                            string currentLanguage,
                                                            DisplayTemplateCollection type)
        => categoryRepository.FindUnique(x => x.DisplayTemplate == type && x.Language.LanguageKey == currentLanguage);

        public static LandingpageGroup GetLandingpageGroupyByPositionTemplate(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                            string currentLanguage,
                                                            PositionTypeCollection type)
        => landingpageGroupRepository.FindUnique(x => x.PositionType == type && x.Language.LanguageKey == currentLanguage && x.IsPublished);

        public static SEOModel BuildSEOModelByEntityType(UnitOfWork unitOfWork, EntityTypeCollection entityType, long entityId)
        {
            switch (entityType)
            {
                case EntityTypeCollection.Articles:
                    return unitOfWork.ArticleRepository.Find(x => x.Id == entityId && x.IsPublished).ToList()
                         .Select(x => new SEOModel
                         {
                             Title = x.Title,
                             MetaTitle = x.MetaTitle,
                             MetaDescription = x.MetaDescription,
                             MetaKeywords = x.MetaKeywords,
                             MetaImage = x.MetaImage,
                             Type = "article",
                             Url = x.GetUrl()
                         }).FirstOrDefault();
                case EntityTypeCollection.Categories:
                    return unitOfWork.CategoryRepository.Find(x => x.Id == entityId && x.IsPublished).ToList()
                        .Select(x => new SEOModel
                        {
                            Title = x.Title,
                            MetaTitle = x.MetaTitle,
                            MetaDescription = x.MetaDescription,
                            MetaKeywords = x.MetaKeywords,
                            MetaImage = x.MetaImage,
                            Type = "article",
                            Url = x.GetPrefixUrl()
                        }).FirstOrDefault();
                default:
                    return unitOfWork.ProductRepository.Find(x => x.Id == entityId && x.IsPublished).ToList()
                        .Select(x => new SEOModel
                        {
                            Title = x.Name,
                            MetaTitle = x.MetaTitle,
                            MetaDescription = x.MetaDescription,
                            MetaKeywords = x.MetaKeywords,
                            MetaImage = x.MetaImage,
                            Type = "article",
                            Url = x.GetUrl()
                        }).FirstOrDefault();

            }
        }
        public static List<Article> GetListArticle(GenericRepository<Article> articleRepository, string currentLanguage, int id, int pagezize)
        {
            var categories = articleRepository.Find(v => v.Category.Id == id && v.Language.LanguageKey == currentLanguage).Take(pagezize);
            return categories.AsQueryable().ToList();

        }

        public static bool CheckingHaveChildrenCategories(GenericRepository<Category> categoryRepository, long categoryId)
        {
            var childrens = categoryRepository.FindAll().Where(x => x.Parent.Id == categoryId && x.IsPublished)
                                                        .OrderBy(x => x.Order).ToList();
            if (childrens.Any())
            {
                return true;
            }
            return false;
        }
        public static List<Category> GetChildrenCategories(GenericRepository<Category> categoryRepository, long categoryId)
        => categoryRepository.FindAll().Where(x => x.Parent.Id == categoryId && x.IsPublished).OrderBy(x => x.Order).ToList();

        public static List<G_ImageModel> GetImageModelsOfEntity(UnitOfWork unitOfWork, EntityImageTypeCollection entityImageType, long entityId, string altKey)
        {
            // Get list image paths
            var imagePaths = new List<string>();
            var imageFolder = "";

            switch (entityImageType)
            {
                case EntityImageTypeCollection.Article:
                    imageFolder = unitOfWork.ArticleRepository.FindById(entityId).ImageFolder;
                    break;
                case EntityImageTypeCollection.Product:
                    imageFolder = unitOfWork.ProductRepository.FindById(entityId).ImageFolder;
                    break;
                case EntityImageTypeCollection.Category:
                    imageFolder = unitOfWork.CategoryRepository.FindById(entityId).ImageFolder;
                    break;
                case EntityImageTypeCollection.SecondPosition:
                    imageFolder = unitOfWork.LandingpageItemRepository.FindById(entityId).ImageFolder;
                    break;
            }
            if (imageFolder != "")
            {
                if (Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(imageFolder)))
                {
                    String[] files = Directory.GetFiles(System.Web.HttpContext.Current.Server.MapPath(imageFolder));
                    foreach (var file in files)
                    {
                        imagePaths.Add(imageFolder + "/" + file.Replace(System.Web.HttpContext.Current.Server.MapPath(imageFolder), "").Replace("\\", ""));
                    }
                }
            }

            // Generate ImageModels base on list image paths
            var imageModels = new List<G_ImageModel>();
            for (int i = 0; i < imagePaths.Count; i++)
            {
                imageModels.Add(new G_ImageModel { Image = imagePaths[i], Alt = altKey + (i + 1) });
            }
            return imageModels;
        }
    }
}
