﻿using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.BusinessGift;
using MainProject.Service.Models.SectionModel.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.Helper
{
    public class BusinessGiftHelper
    {
        public static SlideShowModel BuildSlideShowSection(GenericRepository<SlideShow> slideShowRepository,
                                                           GenericRepository<Category> categoryRepository,
                                                           string currentLanguage)
        {
            var category = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.BusinessGiftsTemplate);
            return new SlideShowModel
            {
                SlideShowImages = slideShowRepository.Find(x => x.IsPublished && x.Category.Id == category.Id).OrderBy(x => x.Order).ToList().Select
                                          (x => new SlideShowImage
                                          {
                                              Image = x.Image,
                                              Alt = x.Name,
                                              IsVideo = x.IsVideo,
                                              VideoId = x.VideoId
                                          }).ToList()
            };
        }

        public static List<LandingpageItem> BuilListPartners(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                       GenericRepository<LandingpageItem> landingpageItemRepository,
                                                       string currentLanguage)
        {
            var listpartners = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.FirstPosition);
            return listpartners == null ? null : landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == listpartners.Id).OrderBy(x => x.Order).ToList();

        }

        public static LandingpageItem BuilDescriptionModel(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                    GenericRepository<LandingpageItem> landingpageItemRepository,
                                                    string currentLanguage)
        {
            var listpartners = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.SecondPosition);
            return listpartners == null ? null : landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == listpartners.Id).OrderBy(x => x.Order).FirstOrDefault();

        }
        public static List<ContentModel01> BuilListContentModel01(UnitOfWork unitOfWork, GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                 GenericRepository<LandingpageItem> landingpageItemRepository,
                                                 string currentLanguage)
        {
            var listpartners = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.ThirdPosition);
            var model = new List<ContentModel01>();
            var res = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == listpartners.Id).OrderBy(x => x.Order).ToList().Take(4);
            foreach (var item in res)
            {
                model.Add(new ContentModel01
                {
                    ContentModel = item,
                    ListImage = GeneralHelper.GetImageModelsOfEntity(unitOfWork, EntityImageTypeCollection.SecondPosition, item.Id, ""),
                });
            }

            return model;
        }

        public static List<LandingpageItem> BuilListProducts(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                     GenericRepository<LandingpageItem> landingpageItemRepository,
                                                     string currentLanguage)
        {
            var listpartners = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.FourthPosition);
            return listpartners == null ? null : landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == listpartners.Id).OrderBy(x => x.Order).ToList();

        }
    }
}
