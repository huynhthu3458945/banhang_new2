﻿using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Service.Models.SectionModel.Download.Index;

namespace MainProject.Service.Helper
{
    public static class DownloadHelper
    {
        private static int pageItems = 5;

        #region Methods of index page
        public static BodyModel BuildBodySectionIndex(
                                                               GenericRepository<Category> categoryRepository,
                                                               GenericRepository<DownloadFile> downloadFileRepository,
                                                               int page,
                                                               string currentLanguage)
        {
            //var downloadCategory = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.DownloadpageTemplate);

            //var sql = downloadFileRepository.FindAll().Where(x => x.IsPublished);
            //var count = sql.Count();
            //// Paging
            //if (page < 1) page = 1;
            //var list = sql.OrderBy(x => x.Order).Skip((page - 1) * pageItems).Take(pageItems);
            //// End Paging

            return new BodyModel
            {
                //Title = downloadCategory.Title,
                //MainDownloadItems = list.Select(x => new MainDownloadItem
                //{
                //    Order = x.Order,
                //    Id = x.Id,
                //    Title = x.Title,
                //    Description = x.Description

                //}).ToList(),
                //PagingModel = new PagingModel(count, pageItems, page, "href='" + downloadCategory.GetPrefixUrl() + "/{0}'")
            };
        }
        #endregion
    }
}
