﻿using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.General;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Landingpage;

namespace MainProject.Service.Helper
{
    public static class LandingpageHelper
    {
        public static SlideShowModel BuildSlideShowSection(GenericRepository<SlideShow> slideShowRepository, 
                                                            GenericRepository<Category> categoryRepository, 
                                                            string currentLanguage)
        {
            //var category = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.LandingpageTemplate);
            return new SlideShowModel
            {
                //SlideShowImages = slideShowRepository.Find(x => x.IsPublished && x.Category.Id == category.Id).OrderBy(x => x.Order).ToList().Select
                //                          (x => new SlideShowImage
                //                          {
                //                              Image = x.Image,
                //                              Alt = x.Name,
                //                              IsVideo = x.IsVideo,
                //                              VideoId = x.VideoId
                //                          }).ToList()
            };
        }

        public static FirstSectionModel BuildFirstSection(GenericRepository<LandingpageGroup> landingpageGroupRepository, 
                                                         GenericRepository<LandingpageItem> landingpageItemRepository, 
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.FirstPosition);
            return landingpageGroup == null? null: new FirstSectionModel
            {
                Title = landingpageGroup.Title,
                FirstSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id).OrderBy(x => x.Order).Select
                                          (x => new FirstSectionItem
                                          {
                                              Title = x.Title,
                                              Url = x.Url,
                                              Image = x.ImageDefault,
                                              Alt = x.Title
                                          }).ToList()
            };
        }
        public static SecondSectionModel BuildSecondSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.SecondPosition);
            return landingpageGroup == null ? null : new SecondSectionModel
            {
                Title = landingpageGroup.Title,
                SecondSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id).OrderBy(x => x.Order).Select
                                          (x => new SecondSectionItem
                                          {
                                              Title = x.Title,
                                              Order = x.Order,
                                              Image = new G_ImageModel { Image = x.ImageDefault, Alt = x.Title}
                                          }).ToList()
            };
        }

        public static ThirdSectionModel BuildThirdSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.ThirdPosition);
            return landingpageGroup == null ? null : new ThirdSectionModel
            {
                Three_ThirdSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id).Take(3).OrderBy(x => x.Order).Select
                                          (x => new ThirdSectionItem
                                          {
                                              Description = x.Description
                                          }).ToList()
            };
        }

        public static FourthSectionModel BuildFourthSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.FourthPosition);
            return landingpageGroup == null ? null : new FourthSectionModel
            {
                Title = landingpageGroup.Title,
                BackgroundImage = landingpageGroup.ImageDefault,
                FourthSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id).OrderBy(x => x.Order).Select
                                          (x => new FourthSectionItem
                                          {
                                              Title = x.Title,
                                              Url = x.Url
                                          }).ToList()
            };
        }

        public static FifthSectionModel BuildFifthSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.FifthPosition);
            return landingpageGroup == null ? null : new FifthSectionModel
            {
                FifthSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id).OrderBy(x => x.Order).Select
                                          (x => new FifthSectionItem
                                          {
                                              Image = new G_ImageModel {Image = x.ImageDefault, Alt = x.Title},
                                              Url = x.Url
                                          }).ToList()
            };
        }

        public static SixthSectionModel BuildSixthSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<Branch> branchRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.SixthPosition);
            return landingpageGroup == null ? null : new SixthSectionModel
            {
                Title = landingpageGroup.Title,
                Description = landingpageGroup.Description,
                SixthSectionItems = branchRepository.Find(x => x.IsPublished).OrderBy(x => x.Order).Select
                                          (x => new SixthSectionItem
                                          {
                                              Name = x.Name,
                                              Address = x.Street,
                                              Phone = x.Phone,
                                              Lat = x.Lat,
                                              Long = x.Lng
                                          }).ToList()
            };
        }

        public static SeventhSectionModel BuildSeventhSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.SeventhPosition);
            return landingpageGroup == null ? null : new SeventhSectionModel
            {
                Title = landingpageGroup.Title,
                SeventhSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id)
                                                                .OrderBy(x => x.Order).Select
                                                                (x => new SeventhSectionItem
                                                                {
                                                                    Title = x.Title,
                                                                    Url = x.Url,
                                                                    Image = x.ImageDefault,
                                                                    Alt = x.Title
                                                                }).ToList()
            };
        }

        public static EighthSectionModel BuildEighthSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<Product> productRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.EighthPosition);
            return landingpageGroup == null ? null : new EighthSectionModel
            {
                Title = landingpageGroup.Title,
                EighthSectionItems = productRepository.Find(x => x.IsPublished && x.IsLandingpage)
                                            .OrderBy(x => x.Order).ToList().Select
                                            (x => new EighthSectionItem
                                            {
                                                Title = x.Name,
                                                ShortDescription = x.ShortDescription,
                                                IsPriceContact = x.IsPriceContact,
                                                Price = MvcHtmlHelper.FormatCurrency(x.Price),
                                                IsPromotion = x.IsPromotion,
                                                PromotionPercent = x.PromotionPercent,
                                                PromotionPrice = MvcHtmlHelper.FormatCurrency(x.PromotionPrice),
                                                Image = x.ImageDefault,
                                                Alt = x.Name,
                                                Url = x.GetUrl(),
                                                Id = x.Id
                                            }).ToList()
            };
        }

        public static NinthSectionModel BuildNinthSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.NinethPosition);
            return landingpageGroup == null ? null : new NinthSectionModel
            {
                Title = landingpageGroup.Title,
                Description = landingpageGroup.Description,
                BackgroundImage = landingpageGroup.ImageDefault,
                Two_NinthSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id)
                                                    .Take(2).OrderBy(x => x.Order).Select
                                                    (x => new NinthSectionItem
                                                    {
                                                        Title = x.Title
                                                    }).ToList()
            };
        }
        public static TenthSectionModel BuildTenthSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.TenthPosition);
            return landingpageGroup == null ? null : new TenthSectionModel
            {
                Title = landingpageGroup.Title,
                TenthSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id)
                                                    .OrderBy(x => x.Order).Select
                                                    (x => new TenthSectionItem
                                                    {
                                                        Title = x.Title,
                                                        Description = x.Description,
                                                        Url = x.Url,
                                                        Image = x.ImageDefault,
                                                        Alt = x.Title
                                                    }).ToList()
            };
        }
        public static EleventhSectionModel BuildEleventhSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.EleventhPosition);
            return landingpageGroup == null ? null : new EleventhSectionModel
            {
                Title = landingpageGroup.Title,
                Description = landingpageGroup.Description,
                EleventhSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id)
                                                    .OrderBy(x => x.Order).Select
                                                    (x => new EleventhSectionItem
                                                    {
                                                        Title = x.Title,
                                                        Description = x.Description
                                                    }).ToList()
            };
        }

        public static TwelvethSectionModel BuildTwelvethSection(GenericRepository<LandingpageGroup> landingpageGroupRepository,
                                                         GenericRepository<LandingpageItem> landingpageItemRepository,
                                                         string currentLanguage)
        {
            var landingpageGroup = GeneralHelper.GetLandingpageGroupyByPositionTemplate(landingpageGroupRepository, currentLanguage, PositionTypeCollection.TwelvethPosition);
            return landingpageGroup == null ? null : new TwelvethSectionModel
            {
                Fourth_TwelvethSectionItems = landingpageItemRepository.Find(x => x.IsPublished && x.LandingpageGroup.Id == landingpageGroup.Id)
                                                    .Take(4).OrderBy(x => x.Order).Select
                                                    (x => new TwelvethSectionItem
                                                    {
                                                        Title = x.Title
                                                    }).ToList()
            };
        }
    }
}
