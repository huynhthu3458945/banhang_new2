﻿using System.Linq;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.Project.Index;
using MainProject.Service.Models.SectionModel.Project.Detail;

namespace MainProject.Service.Helper
{
    public static class ProjectHelper
    {
        private static int pageItems = 13;

        #region Methods of index page
        public static Models.SectionModel.Project.Index.BodyModel BuildBodySectionIndex(
                                                               GenericRepository<Category> categoryRepository,
                                                               GenericRepository<Article> articleRepository,
                                                               int page,
                                                               string currentLanguage)
        {

            //var projectCategory = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.ProjectTemplate);
            var projectCategory = new Category();

            var sql = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.Id == projectCategory.Id);
            var count = sql.Count();
            // Paging
            if (page < 1) page = 1;
            var list = GetCustomOrderedProjects(sql).Skip((page - 1) * pageItems).Take(pageItems).ToList();
            // End Paging

            return new Models.SectionModel.Project.Index.BodyModel
            {
                Title = projectCategory.Title,
                FirstMainProjectArticle = list.Select(x => new MainProjectArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.BigImage,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                }).FirstOrDefault(),
                SixMainProjectArticles = list.Skip(1).Select(x => new MainProjectArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                }).ToList(),
                PagingModel = new PagingModel(count, pageItems, page, "href='" + projectCategory.GetPrefixUrl() + "/{0}'")
            };
        }
        #endregion

        public static Models.SectionModel.Project.Detail.BodyModel BuildBodySectionDetail(
                                                                GenericRepository<Article> articleRepository, 
                                                                long articleId, 
                                                                string currentLanguage)
            => articleRepository.Find(x => x.Id == articleId && x.IsPublished).ToList().Select(x => new Models.SectionModel.Project.Detail.BodyModel
            {
                Title = x.Category.Title,
                ArticleTitle = x.Title,
                Body = x.Body,
                CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),
            }).FirstOrDefault();

        public static BottomModel BuildBottomSectionDetail(GenericRepository<Article> articleRepository, 
                                                            long articleId, 
                                                            string currentLanguage)
        {
            var article = articleRepository.FindById(articleId);
            return new BottomModel
            {
                Title = ResourceHelper.GetResource(ResourceKeyCollection.OtherProjects, currentLanguage),
                OtherProjectArticles = articleRepository.FindAll().Where(x => x.Category.Id == article.Category.Id && x.Id != article.Id
                                                                && x.IsPublished && x.Language.LanguageKey == currentLanguage)
                                                                .OrderByDescending(x=>x.CreateDate).Take(3).ToList()
                                                                .Select(x => new OtherProjectArticle
                                                                {
                                                                    Title = x.Title,
                                                                    Description = x.Description,
                                                                    Image = x.BigImage,
                                                                    Alt = x.Title,
                                                                    Url = x.GetUrl(),
                                                                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                                    : x.CreateDate.ToString("MMM dd, yyyy"),
                                                                }).ToList()
            };
        }

        #region General Mothods

        // Just use in small projects
        public static List<Article> GetCustomOrderedProjects(IQueryable<Article> iQueryableArticle)
        {
            // Declare custom ordered Articles
            var articles = new List<Article>();

            // Get products order by ProductOrder
            var orderByOrderArticles = iQueryableArticle.Where(x => x.Order != 0).OrderBy(x => x.Order).ToList();
            if (orderByOrderArticles.Count > 0)
            {
                articles.AddRange(orderByOrderArticles);
            }

            // Get articles order by createDated
            var orderByCreatedDateArticles = iQueryableArticle.Where(x => x.Order == 0).OrderByDescending(x => x.CreateDate).ToList();
            if (orderByCreatedDateArticles.Count > 0)
            {
                articles.AddRange(orderByCreatedDateArticles);
            }

            return articles;
        }
        #endregion
    }
}
