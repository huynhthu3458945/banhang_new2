﻿using System.Linq;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.Product.Child1;
using MainProject.Service.Models.SectionModel.Product.Child2;
using MainProject.Service.Models.SectionModel.Product.Child3;
using MainProject.Service.Models.SectionModel.Product.Detail;
using MainProject.Service.Models.SectionModel.News.Index;
using MainProject.Service.Models.SectionModel.Product;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Helper
{
    public static class ProductHelper
    {
        private static int pageItems = 12;
        public static SlideShowModel BuildSlideShowSectionProduct(GenericRepository<SlideShow> slideShowRepository,
                                                           GenericRepository<Category> categoryRepository,
                                                           string currentLanguage)
        {
            var category = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.ProductTemplate);
            return new SlideShowModel
            {
                SlideShowImages = slideShowRepository.Find(x => x.IsPublished && x.Category.Id == category.Id && x.Language.LanguageKey == currentLanguage).OrderBy(x => x.Order).ToList().Select
                                          (x => new SlideShowImage
                                          {
                                              Image = x.Image,
                                              Alt = x.Name,
                                              IsVideo = x.IsVideo,
                                              VideoId = x.VideoId,
                                              Url = x.Link
                                          }).ToList()
            };
        }

        #region Methods of child2 page
        public static Models.SectionModel.Product.Child2.BodyModel BuildBodySectionIndex(
                                                        GenericRepository<Category> categoryRepository,
                                                        GenericRepository<Product> productRepository,
                                                                              long child2CategoryId,
                                                                               int page,
                                                                              string currentLanguage)
        {

            var child2Category = categoryRepository.FindById(child2CategoryId);

            var sql = productRepository.FindAll().Where(x => x.IsPublished && x.Language.LanguageKey == currentLanguage
                                                              );
            var count = sql.Count();
            // Paging
            if (page < 1) page = 1;
            var list = GetCustomOrderedProducts(sql).Skip((page - 1) * pageItems).Take(pageItems);
            // End Paging

            return new Models.SectionModel.Product.Child2.BodyModel
            {
                Title = child2Category.Title,
                Child2ProductCategories = categoryRepository.FindAll().Where(x => x.Parent.Id == child2CategoryId && x.IsPublished)
                                                                                .OrderBy(x => x.Order).ToList()
                                                                                .Select(x => new Child2ProductCategory
                                                                                {
                                                                                    Title = x.Title,
                                                                                    Url = x.GetPrefixUrl()
                                                                                }).ToList(),
                ListProduct = list.ToList(),
                PagingModel = new PagingModel(count, pageItems, page, "href='" + child2Category.GetPrefixUrl() + "/{0}'")
            };
        }
        #endregion


        #region Methods of child1 page
        public static Models.SectionModel.Product.Child1.BodyModel BuildBodySectionChild1(
                                                                                GenericRepository<Category> categoryRepository,
                                                                                                       long child1CategoryId,
                                                                                                      string currentLanguage)
        {
            var child1Category = categoryRepository.FindById(child1CategoryId);
            return new Models.SectionModel.Product.Child1.BodyModel
            {
                Title = child1Category.Title,
                Child1ProductArticles = categoryRepository.FindAll().Where(x => x.Parent.Id == child1Category.Id
                                                                    && x.IsPublished
                                                                    && x.Language.LanguageKey == currentLanguage)
                                                                    .OrderBy(x => x.Order).ToList()
                                                                .Select(x => new Child1ProductArticle
                                                                {
                                                                    Title = x.Title,
                                                                    Image = x.ImageDefault,
                                                                    Alt = x.Title,
                                                                    Url = x.GetPrefixUrl()
                                                                }).ToList()
            };
        }

        public static List<Product> GetProduct(GenericRepository<Product> productRepository, long categoryId, string currentLanguage)
        {
            var product = productRepository.Find(v => v.Language.LanguageKey == currentLanguage && v.Category.Id == categoryId);
            return product.AsQueryable().ToList();

        }
        public static Category GetCategory(GenericRepository<Category> genericRepository, long categoryId)
        {
            var categories = genericRepository.Find(v => v.Id == categoryId).FirstOrDefault();
            return categories;
        }
        #endregion

        #region Methods of child2 page
        public static Models.SectionModel.Product.Child2.BodyModel BuildBodySectionChild2(
                                                        GenericRepository<Category> categoryRepository,
                                                        GenericRepository<Product> productRepository,
                                                                              long child2CategoryId,
                                                                               int page,
                                                                              string currentLanguage)
        {

            var child2Category = categoryRepository.FindById(child2CategoryId);

            var sql = productRepository.FindAll().Where(x => x.IsPublished && x.Language.LanguageKey == currentLanguage
                                                                     && (x.Category.Id == child2Category.Id
                                                                     || x.Category.Parent.Id == child2Category.Id));
            var count = sql.Count();
            // Paging
            if (page < 1) page = 1;
            var list = GetCustomOrderedProducts(sql).Skip((page - 1) * pageItems).Take(pageItems);
            // End Paging

            return new Models.SectionModel.Product.Child2.BodyModel
            {
                Title = child2Category.Title,
                Child2ProductCategories = categoryRepository.FindAll().Where(x => x.Parent.Id == child2CategoryId && x.IsPublished)
                                                                                .OrderBy(x => x.Order).ToList()
                                                                                .Select(x => new Child2ProductCategory
                                                                                {
                                                                                    Title = x.Title,
                                                                                    Url = x.GetPrefixUrl()
                                                                                }).ToList(),
                Child2ProductArticles = list.Select(x => new Child2ProductArticle
                {
                    Title = x.Name,
                    ShortDescription = x.ShortDescription,
                    IsPriceContact = x.IsPriceContact,
                    Price = MvcHtmlHelper.FormatCurrency(x.Price),
                    IsPromotion = x.IsPromotion,
                    PromotionPercent = x.PromotionPercent,
                    PromotionPrice = MvcHtmlHelper.FormatCurrency(x.PromotionPrice),
                    Image = x.ImageDefault,
                    Alt = x.Name,
                    Url = x.GetUrl(),
                    Id = x.Id
                }).ToList(),
                PagingModel = new PagingModel(count, pageItems, page, "href='" + child2Category.GetPrefixUrl() + "/{0}'")
            };
        }
        #endregion

        #region Methods of child3 page
        public static Models.SectionModel.Product.Child3.BodyModel BuildBodySectionChild3(
                                                        GenericRepository<Category> categoryRepository,
                                                        GenericRepository<Product> productRepository,
                                                                              long child3CategoryId,
                                                                               int page,
                                                                              string currentLanguage)
        {

            var child3Category = categoryRepository.FindById(child3CategoryId);

            var sql = productRepository.FindAll().Where(x => x.IsPublished && x.Language.LanguageKey == currentLanguage
                                                                     && (x.Category.Id == child3Category.Id));
            var count = sql.Count();
            // Paging
            if (page < 1) page = 1;
            var list = GetCustomOrderedProducts(sql).Skip((page - 1) * pageItems).Take(pageItems);
            // End Paging

            return new Models.SectionModel.Product.Child3.BodyModel
            {
                Title = child3Category.Title,
                Child3ProductCategories = categoryRepository.FindAll().Where(x => x.Parent.Id == child3Category.Parent.Id && x.IsPublished)
                                                                                .OrderBy(x => x.Order).ToList()
                                                                                .Select(x => new Child3ProductCategory
                                                                                {
                                                                                    IsActive = x.Id == child3CategoryId ? true : false,
                                                                                    Title = x.Title,
                                                                                    Url = x.GetPrefixUrl()
                                                                                }).ToList(),
                Child3ProductArticles = list.Select(x => new Child3ProductArticle
                {
                    Title = x.Name,
                    ShortDescription = x.ShortDescription,
                    IsPriceContact = x.IsPriceContact,
                    Price = MvcHtmlHelper.FormatCurrency(x.Price),
                    IsPromotion = x.IsPromotion,
                    PromotionPercent = x.PromotionPercent,
                    PromotionPrice = MvcHtmlHelper.FormatCurrency(x.PromotionPrice),
                    Image = x.ImageDefault,
                    Alt = x.Name,
                    Url = x.GetUrl(),
                    Id = x.Id
                }).ToList(),
                PagingModel = new PagingModel(count, pageItems, page, "href='" + child3Category.GetPrefixUrl() + "/{0}'")
            };
        }
        #endregion

        #region Methods of detail page
        public static TopModel BuildTopSectionDetail(GenericRepository<Product> productRepository, long productId)
        {
            var product = productRepository.FindById(productId);
            return new TopModel { BreadcrumbHtmlString = MvcHtmlHelper.GenerateBreadcum(product.GetUrl(), "/") };
        }
        public static Models.SectionModel.Product.Detail.BodyModel BuildBodySectionDetail(
                                                                       UnitOfWork unitOfWork,
                                                                         GenericRepository<Article> articleRepository,
                                                       GenericRepository<Product> productRepository,
                                                                             long productId)
        {

            var product = productRepository.FindById(productId);
            //var sql = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.Id == product.Category.Id);
            //var sqlRes = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.Id == product.Category.Id).ToList();
            var sql = articleRepository.FindAll().Where(x => x.IsPublished);
            var sqlRes = articleRepository.FindAll().Where(x => x.IsPublished).ToList();
            var article = articleRepository.Find(x => x.IsPublished).FirstOrDefault();
            return new Models.SectionModel.Product.Detail.BodyModel
            {
                ThumbImages = GeneralHelper.GetImageModelsOfEntity(unitOfWork, EntityImageTypeCollection.Product, product.Id, product.Name + product.Id + "thumb_"),
                MainImages = GeneralHelper.GetImageModelsOfEntity(unitOfWork, EntityImageTypeCollection.Product, product.Id, product.Name + product.Id + "main_"),
                ProductName = product.Name,
                ProductCode = product.ProductCode,
                ProductDescription = product.Description,
                IsPriceContact = product.IsPriceContact,
                IsPromotion = product.IsPromotion,
                PromotionPrice = MvcHtmlHelper.FormatCurrency(product.PromotionPrice),
                Price = MvcHtmlHelper.FormatCurrency(product.Price),
                IsVideo = product.IsVideo,
                ProductVideoId = product.VideoId,
                SalesCommitment = product.SalesCommitment,
                DetailInformation = product.DetailInfo,
                Id = product.Id,
                NewsArticles = sqlRes.OrderBy(z => z.CreateDate).Select(x => new MainNewsArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = article.Language.LanguageKey == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                       : x.CreateDate.ToString("MMM dd, yyyy"),

                }).Take(3).ToList(),
                HotArticles = sqlRes.Where(z => z.IsHot).Select(x => new MainNewsArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = article.Language.LanguageKey == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                            : x.CreateDate.ToString("MMM dd, yyyy"),

                }).Take(3).ToList()

            };
        }
        public static BottomModel BuildBottomSectionDetail(
                                                        GenericRepository<Product> productRepository,
                                                                              long productId,
                                                                              string currentLanguage)
        {
            var currentProduct = productRepository.FindById(productId);

            return new BottomModel
            {
                Title = ResourceHelper.GetResource(ResourceKeyCollection.OtherProducts, currentLanguage),
                DetailProductArticles = productRepository.FindAll().Where(x => x.Category.Id == currentProduct.Category.Id
                                                                                && x.Id != productId && x.IsPublished)
                                                                                .OrderByDescending(x => x.Id).Take(8).ToList()
                                                                                .Select(x => new DetailProductArticle
                                                                                {
                                                                                    Title = x.Name,
                                                                                    ShortDescription = x.ShortDescription,
                                                                                    IsPriceContact = x.IsPriceContact,
                                                                                    Price = MvcHtmlHelper.FormatCurrency(x.Price),
                                                                                    IsPromotion = x.IsPromotion,
                                                                                    PromotionPercent = x.PromotionPercent,
                                                                                    PromotionPrice = MvcHtmlHelper.FormatCurrency(x.PromotionPrice),
                                                                                    Image = x.ImageDefault,
                                                                                    Alt = x.Name,
                                                                                    Url = x.GetUrl(),
                                                                                    Id = x.Id
                                                                                }).ToList()
            };
        }
        #endregion Methods of Detail Section

        #region General Mothods

        // Just use in small projects
        private static List<Product> GetCustomOrderedProducts(IQueryable<Product> iQueryableProduct)
        {
            // Declare custom ordered Products
            var products = new List<Product>();

            // Get products order by ProductOrder
            var orderByOrderProducts = iQueryableProduct.Where(x => x.Order != 0).OrderBy(x => x.Order).ToList();
            if (orderByOrderProducts.Count > 0)
            {
                products.AddRange(orderByOrderProducts);
            }

            // Get products order by ProductId
            var orderByIdProducts = iQueryableProduct.Where(x => x.Order == 0).OrderBy(x => x.Id).ToList();
            if (orderByIdProducts.Count > 0)
            {
                products.AddRange(orderByIdProducts);
            }

            return products;
        }
        #endregion
    }
}
