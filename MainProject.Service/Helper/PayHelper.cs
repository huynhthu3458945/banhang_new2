﻿using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Service.Models.ViewModel.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.Helper
{
    public class PayHelper
    {
        public static IndexViewModel BuildSlideShowSection(GenericRepository<Article> articleRepository,
                                                                GenericRepository<Category> categoryRepository,
                                                                string currentLanguage)
        {
            var category = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.PayTemplate);
            var article = articleRepository.Find(z => z.Category.Id == category.Id).FirstOrDefault();
            return new IndexViewModel
            {
                Title = article.Title,
                Description = article.Body
            };
        }
    }
}
