﻿using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.News.Index;
using MainProject.Service.Models.SectionModel.News.Detail;
using MainProject.Service.Models.SectionModel.News;
using MainProject.Service.Models.SectionModel.Base.Sections;
using System.Collections.Generic;

namespace MainProject.Service.Helper
{
    public static class NewsHelper
    {
        private static int pageItems = 10;
        public static SlideShowModel BuildSlideShowSection(GenericRepository<SlideShow> slideShowRepository,
                                                           GenericRepository<Category> categoryRepository,
                                                           string currentLanguage)
        {
            var category = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.AdviseTemplate);
            return new SlideShowModel
            {
                SlideShowImages = slideShowRepository.Find(x => x.IsPublished && x.Category.Id == category.Id && x.Language.LanguageKey == currentLanguage).OrderBy(x => x.Order).ToList().Select
                                          (x => new SlideShowImage
                                          {
                                              Image = x.Image,
                                              Alt = x.Name,
                                              IsVideo = x.IsVideo,
                                              VideoId = x.VideoId,
                                          }).ToList()
            };
        }  
        public static SlideShowModel BuildSlideShowSectionQuaTang(GenericRepository<SlideShow> slideShowRepository,
                                                           GenericRepository<Category> categoryRepository,
                                                           string currentLanguage)
        {
            var category = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.BusinessGiftsTemplate);
            return new SlideShowModel
            {
                SlideShowImages = slideShowRepository.Find(x => x.IsPublished && x.Category.Id == category.Id && x.Language.LanguageKey == currentLanguage).OrderBy(x => x.Order).ToList().Select
                                          (x => new SlideShowImage
                                          {
                                              Image = x.Image,
                                              Alt = x.Name,
                                              IsVideo = x.IsVideo,
                                              VideoId = x.VideoId,
                                          }).ToList()
            };
        }
        public static Models.SectionModel.News.Index.BodyModel BuildBodySectionIndex(
                                                               GenericRepository<Category> categoryRepository,
                                                               GenericRepository<Article> articleRepository,
                                                               GenericRepository<SlideShow> slideShowRepository,
                                                               int page,
                                                                 long newsCategoryId,
                                                               string currentLanguage)
        {
            var newsCategory = categoryRepository.Find(z => z.Id == newsCategoryId).FirstOrDefault();
            var sql = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.Id == newsCategoryId);
            var sqlRes = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.Id == newsCategoryId).ToList();
            var count = sql.Count();
            // Paging
            if (page < 1) page = 1;
            var list = sql.OrderByDescending(x => x.Id).Skip((page - 1) * pageItems).Take(pageItems).ToList();
            // End Paging

            return new Models.SectionModel.News.Index.BodyModel
            {
                Title = newsCategory.Title,
                MainNewsArticles = list.Select(x => new MainNewsArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                }).ToList(),
                Image1 = slideShowRepository.Find(z=>z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 1 && z.Language.LanguageKey == currentLanguage).FirstOrDefault().Image,
                Url1 = slideShowRepository.Find(z=>z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 1 && z.Language.LanguageKey == currentLanguage).FirstOrDefault().Link,
                Image2 = slideShowRepository.Find(z=>z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 2 && z.Language.LanguageKey == currentLanguage).FirstOrDefault().Image,
                Url2 = slideShowRepository.Find(z=>z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 2 && z.Language.LanguageKey == currentLanguage).FirstOrDefault().Link,
                Image3 = slideShowRepository.Find(z=>z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 3 && z.Language.LanguageKey == currentLanguage).FirstOrDefault().Image,
                Url3 = slideShowRepository.Find(z=>z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 3 && z.Language.LanguageKey == currentLanguage).FirstOrDefault().Link,
                NewsArticles = sqlRes.OrderBy(z => z.CreateDate).Select(x => new MainNewsArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                }).Take(4).ToList(),
                HotArticles = sqlRes.Where(z=>z.IsHot).Select(x => new MainNewsArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                }).Take(4).ToList(),
                PagingModel = new PagingModel(count, pageItems, page, "href='" + newsCategory.GetPrefixUrl() + "/{0}'")
            };
        }

        public static Models.SectionModel.News.Detail.BodyModel BuildBodySectionDetail(
                                                                GenericRepository<Article> articleRepository,
                                                                GenericRepository<SlideShow> slideShowRepository,
                                                                long articleId,
                                                                string currentLanguage)
        {

            var article = articleRepository.Find(x => x.Id == articleId && x.IsPublished).FirstOrDefault();

            var newsCategoryId = article.Category.Id;
            var sql = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.Id == newsCategoryId);
            var sqlRes = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.Id == newsCategoryId).ToList();
            return new Models.SectionModel.News.Detail.BodyModel
            {
                Title = article.Category.Title,
                ArticleTitle = article.Title,
                Body = article.Body,
                CreatedDate = currentLanguage == "vi" ? article.CreateDate.ToString("dd/MM/yyyy")
                                                           : article.CreateDate.ToString("MMM dd, yyyy"),
                Image1 = slideShowRepository.Find(z => z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 1 && z.Language.LanguageKey == article.Language.LanguageKey).FirstOrDefault().Image,
                Url1 = slideShowRepository.Find(z => z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 1 && z.Language.LanguageKey == article.Language.LanguageKey).FirstOrDefault().Link,
                Image2 = slideShowRepository.Find(z => z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 2 && z.Language.LanguageKey == article.Language.LanguageKey).FirstOrDefault().Image,
                Url2 = slideShowRepository.Find(z => z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 2 && z.Language.LanguageKey == article.Language.LanguageKey).FirstOrDefault().Link,
                Image3 = slideShowRepository.Find(z => z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 3 && z.Language.LanguageKey == article.Language.LanguageKey).FirstOrDefault().Image,
                Url3 = slideShowRepository.Find(z => z.Category.DisplayTemplate == DisplayTemplateCollection.AdviseTemplate && z.Order == 3 && z.Language.LanguageKey == article.Language.LanguageKey).FirstOrDefault().Link,
                NewsArticles = sqlRes.OrderBy(z => z.CreateDate).Select(x => new MainNewsArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = article.Language.LanguageKey == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                }).Take(4).ToList(),
                HotArticles = sqlRes.Where(z => z.IsHot).Select(x => new MainNewsArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = article.Language.LanguageKey == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                            : x.CreateDate.ToString("MMM dd, yyyy"),

                }).Take(4).ToList()
            };

        }

        public static BottomModel BuildBottomSectionDetail(GenericRepository<Article> articleRepository,
                                                            long articleId,
                                                            string currentLanguage)
        {
            var article = articleRepository.FindById(articleId);
            return new BottomModel
            {
                Title = ResourceHelper.GetResource(ResourceKeyCollection.OtherNews, currentLanguage),
                OtherNewsArticles = articleRepository.FindAll().Where(x => x.Category.Id == article.Category.Id && x.Id != article.Id
                                                                && x.IsPublished && x.Language.LanguageKey == currentLanguage)
                                                                .OrderByDescending(x => x.CreateDate).Take(3).ToList()
                                                                .Select(x => new OtherNewsArticle
                                                                {
                                                                    Title = x.Title,
                                                                    Description = x.Description,
                                                                    Image = x.BigImage,
                                                                    Alt = x.Title,
                                                                    Url = x.GetUrl(),
                                                                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                                    : x.CreateDate.ToString("MMM dd, yyyy"),
                                                                }).ToList()
            };
        }
        public static List<LandingpageItem> GetListLandingpageItem(
                                                GenericRepository<LandingpageItem> landingpageItemRepository,
                                                string currentLanguage,
                                                PositionTypeCollection positionTypeCollection)
        {
            var res = landingpageItemRepository.Find(z => z.Language.LanguageKey == currentLanguage && z.LandingpageGroup.PositionType == positionTypeCollection);
            return res.AsQueryable().ToList();
        }
    }
}
