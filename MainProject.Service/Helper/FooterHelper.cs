﻿using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.General;
using MainProject.Service.Models.SectionModel.Footer;
using System.Collections.Generic;

namespace MainProject.Service.Helper
{
    public static class FooterHelper
    {
        public static TopModel BuildTopSection(GenericRepository<Partner> partnerRepository)
        => new TopModel
        {
            TopItems = partnerRepository.Find(x => x.IsPublished).OrderBy(x => x.Order).Select(
                x => new G_ImageModel
                {
                    Image = x.ImageDefault,
                    Alt = x.Name 
                }).ToList()
        };

        public static LeftModel BuildLeftSection(GenericRepository<Branch> branchRepository, string currentLanguage)
        => new LeftModel
        {
            Title = ResourceHelper.GetResource(ResourceKeyCollection.Address, currentLanguage),
            LeftItems = branchRepository.Find(x => x.IsPublished && x.IsOnFooter).OrderBy(x => x.Order).Select
                                        (x => new LeftItem
                                        {
                                            Name = x.Name,
                                            Address = x.Street,
                                            Phone = x.Phone,
                                            Fax = x.Fax
                                        }).ToList()
        };

        public static RightModel BuildRightSection(string currentLanguage)
        => new RightModel
        {
            Title = ResourceHelper.GetResource(ResourceKeyCollection.RightTitleFooter, currentLanguage),
            FacebookPageLink = SettingHelper.GetValueSetting(ConfigItemHelper.GetFacebookKey())
        };

        public static MiddleModel BuildMiddleSection(GenericRepository<Category> categoryRepository, string currentLanguage)
        {
            // Get menuHtmlString
            var categories = categoryRepository.Find(x => x.IsSystem == false && x.Parent.Parent == null
                                                                            && x.Language.LanguageKey == currentLanguage)
                                                                            .OrderBy(x => x.Order).ToList();
            var firstProductCategory = categoryRepository.FindAll().Where(x => x.Parent.DisplayTemplate == DisplayTemplateCollection.ProductTemplate
                                                                           && x.IsPublished && x.Language.LanguageKey == currentLanguage)
                                                                           .OrderBy(x => x.Order).ToList().FirstOrDefault();
            var menuHtmlString = "";
            foreach(var category in categories)
            {
                if(category.DisplayTemplate != DisplayTemplateCollection.ProductTemplate)
                {
                    menuHtmlString += "<li><a href='" + category.GetPrefixUrl() + "'>" + category.Title + "</a></li>";
                }
                else
                {
                    menuHtmlString += "<li><a href='" + (firstProductCategory != null ? firstProductCategory.GetPrefixUrl():"#") + "'>" + category.Title + "</a></li>";
                }
            }

            return new MiddleModel
            {
                Title = ResourceHelper.GetResource(ResourceKeyCollection.MiddleTitleFooter, currentLanguage),
                MenuHtmlString = menuHtmlString,
                HotLineTitle = ResourceHelper.GetResource(ResourceKeyCollection.HotLineTitle, currentLanguage),
                HotLineContent = ResourceHelper.GetResource(ResourceKeyCollection.HotLineContent, currentLanguage),
                EmailContent = ResourceHelper.GetResource(ResourceKeyCollection.EmailContent, currentLanguage),
                WorkingHourTitle = ResourceHelper.GetResource(ResourceKeyCollection.WorkingHourTitle, currentLanguage),
                WorkingHourContent = ResourceHelper.GetResource(ResourceKeyCollection.WorkingHourContent, currentLanguage)
            };
        }

        public static SocialModel BuildSocialSection(GenericRepository<Branch> branchRepository, string currentLanguage)
        => new SocialModel
        {
            BranchItems = branchRepository.Find(x => x.IsPublished && x.IsOnFooter).OrderBy(x => x.Order).Select
                                        (x => new BranchItem
                                        {
                                            Name = x.Name,
                                            Address = x.Street
                                        }).ToList(),

            HotLineContent = ResourceHelper.GetResource(ResourceKeyCollection.HotLineContent, currentLanguage),
            EmailContent = ResourceHelper.GetResource(ResourceKeyCollection.EmailContent, currentLanguage),
            FacebookPageUrl = SettingHelper.GetValueSetting("FacebookPage"),
            YoutubePageUrl = SettingHelper.GetValueSetting("YoutubePage")
        };
        public static List<MenuItem> GetMenu(GenericRepository<MenuItem> categoryRepository, string currentLanguage)
        {
            var categories = categoryRepository.Find(n => n.Language.LanguageKey == currentLanguage && n.Menu.MenuType == MenuTypeCollection.FooterMenu).ToList();
            return categories.AsQueryable().ToList();
        }
    }
}
