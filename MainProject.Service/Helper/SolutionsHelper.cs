﻿using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.Solution.Index;
using MainProject.Service.Models.SectionModel.Solution.Detail;

namespace MainProject.Service.Helper
{
    public static class SolutionHelper
    {
        private static int pageItems = 7;

        #region Methods of index page
        public static Models.SectionModel.Solution.Index.BodyModel BuildBodySectionIndex(
                                                               GenericRepository<Category> categoryRepository,
                                                               GenericRepository<Article> articleRepository,
                                                               int page,
                                                               string currentLanguage)
        {

            //var solutionCategory = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.SolutionTemplate);
            var solutionCategory = new Category();

            var sql = articleRepository.FindAll().Where(x => x.IsPublished && x.Category.Id == solutionCategory.Id);
            var count = sql.Count();
            // Paging
            if (page < 1) page = 1;
            var list = sql.OrderByDescending(x => x.Id).Skip((page - 1) * pageItems).Take(pageItems).ToList();
            // End Paging

            return new Models.SectionModel.Solution.Index.BodyModel
            {
                Title = solutionCategory.Title,
                FirstMainSolutionArticle = list.Select(x => new MainSolutionArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.BigImage,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                }).FirstOrDefault(),
                TwelveMainSolutionArticles = list.Skip(1).Select(x => new MainSolutionArticle
                {
                    Title = x.Title,
                    Description = x.Description,
                    Image = x.ImageDefault,
                    Alt = x.Title,
                    Url = x.GetUrl(),
                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),

                }).ToList(),
                PagingModel = new PagingModel(count, pageItems, page, "href='" + solutionCategory.GetPrefixUrl() + "/{0}'")
            };
        }
        #endregion

        public static Models.SectionModel.Solution.Detail.BodyModel BuildBodySectionDetail(
                                                                GenericRepository<Article> articleRepository, 
                                                                long articleId, 
                                                                string currentLanguage)
            => articleRepository.Find(x => x.Id == articleId && x.IsPublished).ToList().Select(x => new Models.SectionModel.Solution.Detail.BodyModel
            {
                Title = x.Category.Title,
                ArticleTitle = x.Title,
                Body = x.Body,
                CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                          : x.CreateDate.ToString("MMM dd, yyyy"),
            }).FirstOrDefault();

        public static BottomModel BuildBottomSectionDetail(GenericRepository<Article> articleRepository, 
                                                            long articleId, 
                                                            string currentLanguage)
        {
            var article = articleRepository.FindById(articleId);
            return new BottomModel
            {
                Title = ResourceHelper.GetResource(ResourceKeyCollection.OtherSolutions, currentLanguage),
                OtherSolutionArticles = articleRepository.FindAll().Where(x => x.Category.Id == article.Category.Id && x.Id != article.Id
                                                                && x.IsPublished && x.Language.LanguageKey == currentLanguage)
                                                                .OrderByDescending(x=>x.CreateDate).Take(3).ToList()
                                                                .Select(x => new OtherSolutionArticle
                                                                {
                                                                    Title = x.Title,
                                                                    Description = x.Description,
                                                                    Image = x.BigImage,
                                                                    Alt = x.Title,
                                                                    Url = x.GetUrl(),
                                                                    CreatedDate = currentLanguage == "vi" ? x.CreateDate.ToString("dd/MM/yyyy")
                                                                    : x.CreateDate.ToString("MMM dd, yyyy"),
                                                                }).ToList()
            };
        }
    }
}
