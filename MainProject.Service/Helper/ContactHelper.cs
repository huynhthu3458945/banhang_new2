﻿using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Helper;
using MainProject.Service.Models.SectionModel.Contact;

namespace MainProject.Service.Helper
{
    public static class ContactHelper
    {
        public static PlaceholderModel BuildPlaceHolderSection(string currentLanguage)
        => new PlaceholderModel
        {
            FullNameTitle = ResourceHelper.GetResource(ResourceKeyCollection.Fullname, currentLanguage),
            EmailTitle = ResourceHelper.GetResource(ResourceKeyCollection.Email, currentLanguage),
            PhoneTitle = ResourceHelper.GetResource(ResourceKeyCollection.PhoneNumber, currentLanguage),
            ContentTitle = ResourceHelper.GetResource(ResourceKeyCollection.Content, currentLanguage),
        };

        public static BranchModel BuildBranchSection(GenericRepository<Category> categoryRepository,
                                                        GenericRepository<Branch> branchRepository,
                                                                         string currentLanguage)
        => 
            new BranchModel
        {
            Title = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.ContactTemplate).Title,
            BranchItems = branchRepository.Find(x => x.IsPublished).OrderBy(x => x.Order).Select
                                        (x => new BranchItem
                                        {
                                            Name = x.Name,
                                            Address = x.Street,
                                            Phone = x.Phone,
                                            Fax = x.Fax,
                                            Lat = x.Lat,
                                            Long = x.Lng,
                                            Url = x.Url,
                                        }).ToList()
        };
    }
}
