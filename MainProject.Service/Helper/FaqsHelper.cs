﻿using System.Linq;
using MainProject.Core;
using MainProject.Core.Enums;
using MainProject.Data;
using MainProject.Framework.Models;
using MainProject.Framework.Helper;
using System.Collections.Generic;

namespace MainProject.Service.Helper
{
    public static class FaqsHelper
    {
        public static List<Faq> BuildFaqs(GenericRepository<Faq> faqRepository,
                                                           string currentLanguage)
        {
            return faqRepository.Find(z => z.IsPublished && z.Language.LanguageKey == currentLanguage).OrderBy(z => z.Order).ToList();
        } 
        public static string BuildTitle(GenericRepository<Category> categoryRepository,
                                                           string currentLanguage)
        {
            var category = GeneralHelper.GetCategoryByDisplayTemplate(categoryRepository, currentLanguage, DisplayTemplateCollection.FaqTemplate);

            return category == null ? string.Empty : category.Description;
        }
    }
}
