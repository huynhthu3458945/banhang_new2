﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
//using System.Web.Mvc;

namespace MainProject.Service.ViewModels
{
    public class OrderViewModel
    {

        [StringLength(100, ErrorMessage = "Độ dài Email không quá 100 ký tự!")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email không hợp lệ.")]
        [Required(ErrorMessage = "Vui lòng điền Email!")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Vui lòng nhập tên!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập số điện thoại!")]
        [RegularExpression("^[0-9]{10,11}$", ErrorMessage = "Số điện thoại không hợp lệ")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn tỉnh/thành phố!")]
        public long ProvinceSelectedValue { get; set; }

        public List<System.Web.Mvc.SelectListItem> Provinces { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn quận/huyện!")]
        public long DistrictSelectedValue { get; set; }

        public List<System.Web.Mvc.SelectListItem> Districts { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập đường!")]
        public string Address { get; set; }

        public string Note { get; set; }
    }
}