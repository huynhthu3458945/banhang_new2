﻿using System;

namespace MainProject.Service.Models.ViewModel
{
    public class SitemapModel
    {
        public string Link { get; set; }
        public DateTime Created { get; set; }
        public string Priority { get; set; }
    }
}