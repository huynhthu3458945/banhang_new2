﻿namespace MainProject.Service.Models.Controller.Download
{
    /// <summary>
    /// Product model of Download Controller
    /// </summary>
    public class ProductModel
    {
        // File path
        public string FilePath { get; set; }
    }
}