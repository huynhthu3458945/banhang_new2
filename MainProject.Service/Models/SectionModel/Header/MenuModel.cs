﻿namespace MainProject.Service.Models.SectionModel.Header
{
    /// <summary>
    /// The model of menu section in Header
    /// </summary>
    public class MenuModel
    {
        // Html string of menu
        public string MenuHtmlString { get; set; }
    }
}