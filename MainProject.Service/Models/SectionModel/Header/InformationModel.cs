﻿namespace MainProject.Service.Models.SectionModel.Header
{
    /// <summary>
    /// The model of information section in Header 
    /// </summary>
    public class InformationModel
    {
        // Hotline title
        public string HotLineTitle { get; set; }

        // Hotline content
        public string HotLineContent { get; set; }

        // Email content
        public string EmailContent { get; set; }

        // Input search keywords
        public string InputSearchKeywords { get; set; }
    }
}