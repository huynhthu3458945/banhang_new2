﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Home
{
    /// <summary>
    /// The model of solution section in Home page
    /// </summary>
    public class SolutionModel : S_TitleModel
    {
        // Solution items
        public List<SolutionItem> SolutionItems { get; set; }
    }

    public class SolutionItem : IT_CategoryModel { }
}