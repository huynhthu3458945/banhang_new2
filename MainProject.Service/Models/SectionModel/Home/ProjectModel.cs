﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Home
{
    /// <summary>
    /// The model of project section in Home page
    /// </summary>
    public class ProjectModel : S_TitleModel
    {
        // First projectItem
        public ProjectItem First_ProjectItem { get; set; }

        // Three next projectItems
        public List<ProjectItem> NextThree_ProjectItems { get; set; }

        // Url of section
        public string Url { get; set; }
    }
    public class ProjectItem : IT_ArticleModel
    {
        // Created date of news
        public string CreatedDate { get; set; }

        // Description of news 
        public string Description { get; set; }
    }
}