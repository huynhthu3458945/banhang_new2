﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Home
{
    /// <summary>
    /// The model of product section in Home page
    /// </summary>
    public class ProductModel : S_TitleModel
    {
        // Hot product category items
        public List<HotProductCategoryItem> HotProductCategoryItems { get; set; }
    }

    public class HotProductCategoryItem : IT_CategoryModel {

        // Id of product category
        public long ProductCategoryId { get; set; }
    }
}