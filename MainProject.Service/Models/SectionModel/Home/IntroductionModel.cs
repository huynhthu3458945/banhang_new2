﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.General;

namespace MainProject.Service.Models.SectionModel.Home
{
    /// <summary>
    /// The model of introduction section in Home page
    /// </summary>
    public class IntroductionModel : S_TitleModel
    {
        // Description of introduction category
        public string Description { get; set; }

        // Images of introduction categories
        public List<G_ImageModel> ImageItems { get; set; }

        // Url of introduction category
        public string Url { get; set; }
    }
}