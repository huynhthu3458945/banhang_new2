﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Home
{
    /// <summary>
    /// The model of news section in Home page
    /// </summary>
    public class NewsModel : S_TitleModel
    {
        // Three newsItems
        public List<NewsItem> Three_NewsItems { get; set; }
    }
    public class NewsItem : IT_ArticleModel
    {
        // Created date of news
        public string CreatedDate { get; set; }

        // Description of news 
        public string Description { get; set; }
    }
}