﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Footer
{
    /// <summary>
    /// The model of left Section
    /// </summary>
    public class LeftModel: S_TitleModel
    {
        // Main items of section
        public List<LeftItem> LeftItems { get; set; }
    }

    public class LeftItem
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
    }
}