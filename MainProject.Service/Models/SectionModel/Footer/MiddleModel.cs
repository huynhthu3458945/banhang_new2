﻿using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Footer
{
    /// <summary>
    /// The model of middle section Footer
    /// </summary>
    public class MiddleModel: S_TitleModel
    {
        // Html string
        public string MenuHtmlString { get; set; }

        // Hotline title
        public string HotLineTitle { get; set; }

        // Hotline content
        public string HotLineContent { get; set; }

        // Email content
        public string EmailContent { get; set; }

        // Working hour title
        public string WorkingHourTitle { get; set; }

        // Working hour content
        public string WorkingHourContent { get; set; }
    }
}