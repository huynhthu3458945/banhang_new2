﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Footer
{
    /// <summary>
    /// The model of social Section
    /// </summary>
    public class SocialModel
    {
        // Branch items of section
        public List<BranchItem> BranchItems { get; set; }

        // Hotline content
        public string HotLineContent { get; set; }

        // Email content
        public string EmailContent { get; set; }

        // Facebook page url
        public string FacebookPageUrl { get; set; }

        // Youtube page url
        public string YoutubePageUrl { get; set; }
    }

    public class BranchItem
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}