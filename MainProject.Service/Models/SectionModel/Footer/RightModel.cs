﻿using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Footer
{
    /// <summary>
    /// The model of right section Footer
    /// </summary>
    public class RightModel: S_TitleModel
    {
        // Link of facebook page
        public string FacebookPageLink { get; set; }
    }
}