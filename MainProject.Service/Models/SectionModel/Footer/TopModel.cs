﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.General;

namespace MainProject.Service.Models.SectionModel.Footer
{
    /// <summary>
    /// The model of top Section
    /// </summary>
    public class TopModel
    {
        // Main items of section
        public List<G_ImageModel> TopItems { get; set; }
    }
}