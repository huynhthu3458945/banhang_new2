﻿namespace MainProject.Service.Models.SectionModel.Base.Items
{
    /// <summary>
    /// The model of category item
    /// </summary>
    public class IT_CategoryModel
    {
        // The title
        public string Title { get; set; }

        // The Url
        public string Url { get; set; }
    }
}