﻿using MainProject.Service.Models.SectionModel.General;
namespace MainProject.Service.Models.SectionModel.Base.Items
{
    /// <summary>
    /// The model of article item
    /// </summary>
    public class IT_ArticleModel: G_ImageModel
    {
        // The title
        public string Title { get; set; }

        // The Url
        public string Url { get; set; }
    }
}