﻿namespace MainProject.Service.Models.SectionModel.Base.Sections
{
    /// <summary>
    /// The base model of video section
    /// </summary>
    public class S_VideoModel
    {
        // The videoId 
        public string VideoId { get; set; }
    }
}