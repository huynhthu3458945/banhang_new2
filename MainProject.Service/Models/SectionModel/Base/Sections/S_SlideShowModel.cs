﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.General;
namespace MainProject.Service.Models.SectionModel.Base.Sections
{
    /// <summary>
    /// The base model of slideshow section
    /// </summary>
    public class S_SlideShowModel
    {
        // ImageModels
        public List<SlideShowImage> SlideShowImages { get; set; }
    }

    public class SlideShowImage : G_ImageModel
    {
        // Check slideshow is video or not
        public bool IsVideo { get; set; }
        // VideoId of slideShow
        public string VideoId { get; set; }
        public string Url { get; set; }
    }
}