﻿namespace MainProject.Service.Models.SectionModel.Base.Sections
{
    /// <summary>
    /// The base model of title section
    /// </summary>
    public class S_TitleModel
    {
        // The title 
        public string Title { get; set; }
    }
}