﻿namespace MainProject.Service.Models.SectionModel.General
{
    /// <summary>
    /// The general model of image 
    /// </summary>
    public class G_ImageModel
    {
        // The image
        public string Image { get; set; }

        // The alt
        public string Alt { get; set; }
    }
}