﻿using MainProject.Core;
using MainProject.Service.Models.SectionModel.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.Models.SectionModel.BusinessGift
{
    public class ContentModel01
    {
        public LandingpageItem ContentModel { get; set; }
        public List<G_ImageModel> ListImage { get; set; }
    }
}
