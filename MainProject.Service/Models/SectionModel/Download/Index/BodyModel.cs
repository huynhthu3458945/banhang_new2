﻿using System.Collections.Generic;
using MainProject.Framework.Models;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Download.Index
{
    /// <summary>
    /// The model of body section in Index Download Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {
        // Main items of index download page
        public List<MainDownloadItem> MainDownloadItems { get; set; }

        // Paging model of index download page
        public PagingModel PagingModel { get; set; }
    }
    public class MainDownloadItem
    {
        // Order of DownloadFile
        public int Order { get; set; }

        // Id to get download file path by DownloadFile object
        public int Id { get; set; }

        // Title of DownloadFile
        public string Title { get; set; }

        // Description of DownloadFile 
        public string Description { get; set; }
    }
}