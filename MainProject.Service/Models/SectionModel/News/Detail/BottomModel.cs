﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.News.Detail
{
    /// <summary>
    /// Model of bottom section in Detail News Page
    /// </summary>
    public class BottomModel : S_TitleModel
    {
        // Other news of detail page
        public List<OtherNewsArticle> OtherNewsArticles { get; set; }
    }
    public class OtherNewsArticle : IT_ArticleModel
    {
        // Created date of news
        public string CreatedDate { get; set; }

        // Description of news 
        public string Description { get; set; }
    }
}