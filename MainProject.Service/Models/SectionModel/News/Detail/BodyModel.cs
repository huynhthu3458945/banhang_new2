﻿using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.News.Index;
using System.Collections.Generic;

namespace MainProject.Service.Models.SectionModel.News.Detail
{
    /// <summary>
    /// The model of body section in Detail News Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {
        // The title of detail article
        public string ArticleTitle { get; set; }

        // The created date of detail article
        public string CreatedDate { get; set; }

        // The body of detail article
        public string Body { get; set; }
        public List<MainNewsArticle> NewsArticles { get; set; }
        public List<MainNewsArticle> HotArticles { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Url1 { get; set; }
        public string Url2 { get; set; }
        public string Url3 { get; set; }
    }
}