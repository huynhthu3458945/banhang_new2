﻿using System.Collections.Generic;
using MainProject.Framework.Models;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.News.Index
{
    /// <summary>
    /// The model of body section in Index News Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {

        //articles of index news page
        public List<MainNewsArticle> MainNewsArticles { get; set; }
        public List<MainNewsArticle> NewsArticles { get; set; }
        public List<MainNewsArticle> HotArticles { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Url1 { get; set; }
        public string Url2 { get; set; }
        public string Url3 { get; set; }

        public PagingModel PagingModel { get; set; }
    }
    public class MainNewsArticle : IT_ArticleModel
    {
        // Created date of news
        public string CreatedDate { get; set; }

        // Description of news 
        public string Description { get; set; }
    }
}