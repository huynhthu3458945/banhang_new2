﻿using System.Collections.Generic;
using MainProject.Framework.Models;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Solution.Index
{
    /// <summary>
    /// The model of body section in Index News Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {
        // First article of index solution page
        public MainSolutionArticle FirstMainSolutionArticle { get; set; }

        // Twelve articles of index solution page
        public List<MainSolutionArticle> TwelveMainSolutionArticles { get; set; }

        public PagingModel PagingModel { get; set; }
    }
    public class MainSolutionArticle : IT_ArticleModel
    {
        // Created date of solution
        public string CreatedDate { get; set; }

        // Description of solution 
        public string Description { get; set; }
    }
}