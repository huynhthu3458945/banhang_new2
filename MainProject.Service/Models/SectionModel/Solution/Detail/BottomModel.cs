﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Solution.Detail
{
    /// <summary>
    /// Model of bottom section in Detail Solution Page
    /// </summary>
    public class BottomModel : S_TitleModel
    {
        // Other solution of detail page
        public List<OtherSolutionArticle> OtherSolutionArticles { get; set; }
    }
    public class OtherSolutionArticle : IT_ArticleModel
    {
        // Created date of news
        public string CreatedDate { get; set; }

        // Description of news 
        public string Description { get; set; }
    }
}