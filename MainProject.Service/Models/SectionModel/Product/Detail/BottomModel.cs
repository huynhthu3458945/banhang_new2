﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Product.Detail
{
    /// <summary>
    /// The model of body section in Detail Product Page
    /// </summary>
    public class BottomModel : S_TitleModel
    {
        // Articles of detail product page
        public List<DetailProductArticle> DetailProductArticles { get; set; }
    }
    public class DetailProductArticle : IT_ArticleModel
    {
        // Check product is contact price or not
        public bool IsPriceContact { get; set; }

        // Price of product
        public string Price { get; set; }

        // Check product is promotion or not
        public bool IsPromotion { get; set; }

        // Promotion percent of product 
        public int PromotionPercent { get; set; }

        // Promotion price of product 
        public string PromotionPrice { get; set; }

        // Short Description of product 
        public string ShortDescription { get; set; }

        // Id of product 
        public long Id { get; set; }
    }
}