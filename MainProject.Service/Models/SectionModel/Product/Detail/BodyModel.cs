﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.General;
using MainProject.Service.Models.SectionModel.News.Index;

namespace MainProject.Service.Models.SectionModel.Product.Detail
{
    /// <summary>
    /// The model of body section in Detail Product Page
    /// </summary>
    public class BodyModel
    {
        // Thumb Images of product
        public List<G_ImageModel> ThumbImages { get; set; }

        // Main Images of product
        public List<G_ImageModel> MainImages { get; set; }

        // Name of product
        public string ProductName { get; set; }

        // Code of product
        public string ProductCode { get; set; }

        // Description of product
        public string ProductDescription { get; set; }

        // Check product is contact price or not
        public bool IsPriceContact { get; set; }

        // Check product is promotion or not
        public bool IsPromotion { get; set; }

        // Promotion price of product
        public string PromotionPrice { get; set; }

        // Origin price of product
        public string Price { get; set; }
        
        // Check product have video or not
        public bool IsVideo { get; set; }

        // Video of product
        public string ProductVideoId { get; set; }

        // Commitment  of product
        public string SalesCommitment { get; set; }

        // DetailInformation of product
        public string DetailInformation { get; set; }

        // Id of product 
        public long Id { get; set; }

        public List<MainNewsArticle> NewsArticles { get; set; }
        public List<MainNewsArticle> HotArticles { get; set; }

    }
}