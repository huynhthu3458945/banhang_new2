﻿namespace MainProject.Service.Models.SectionModel.Product.Detail
{
    /// <summary>
    /// The model of top section in Detail Product Page
    /// </summary>
    public class TopModel
    {
        // Html string of breadcrumb
        public string BreadcrumbHtmlString { get; set; }
    }
}