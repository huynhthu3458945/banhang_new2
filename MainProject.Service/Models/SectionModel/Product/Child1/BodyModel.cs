﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Product.Child1
{
    /// <summary>
    /// The model of body section in Child1 Product Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {
        // Articles of child1 product page
        public List<Child1ProductArticle> Child1ProductArticles { get; set; }
    }
    public class Child1ProductArticle : IT_ArticleModel { }
}