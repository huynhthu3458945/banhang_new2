﻿using System.Collections.Generic;
using MainProject.Framework.Models;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Product.Child2
{
    /// <summary>
    /// The model of body section in Child2 Product Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {
        // Categories of child2 product page
        public List<Child2ProductCategory> Child2ProductCategories { get; set; }

        // Articles of child2 product page
        public List<Child2ProductArticle> Child2ProductArticles { get; set; }
        public List<MainProject.Core.Product> ListProduct { get; set; }
        public PagingModel PagingModel { get; set; }
    }
    public class Child2ProductCategory : IT_CategoryModel { }
    public class Child2ProductArticle : IT_ArticleModel
    {
        // Check product is contact price or not
        public bool IsPriceContact{ get; set; }

        // Price of product
        public string Price { get; set; }

        // Check product is promotion or not
        public bool IsPromotion { get; set; }

        // Promotion percent of product 
        public int PromotionPercent { get; set; }

        // Promotion price of product 
        public string PromotionPrice { get; set; }

        // Short Description of product 
        public string ShortDescription { get; set; }

        // Id of product 
        public long Id { get; set; }
    }
}