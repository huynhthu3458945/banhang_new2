﻿using System.Collections.Generic;
using MainProject.Framework.Models;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Product.Child3
{
    /// <summary>
    /// The model of body section in Child3 Product Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {
        // Categories of child2 product page
        public List<Child3ProductCategory> Child3ProductCategories { get; set; }

        // Articles of child2 product page
        public List<Child3ProductArticle> Child3ProductArticles { get; set; }

        public PagingModel PagingModel { get; set; }
    }
    public class Child3ProductCategory : IT_CategoryModel {
        // Check product category is active or not
        public bool IsActive { get; set; }
    }
    public class Child3ProductArticle : IT_ArticleModel
    {
        // Check product is contact price or not
        public bool IsPriceContact{ get; set; }

        // Price of product
        public string Price { get; set; }

        // Check product is promotion or not
        public bool IsPromotion { get; set; }

        // Promotion percent of product 
        public int PromotionPercent { get; set; }

        // Promotion price of product 
        public string PromotionPrice { get; set; }

        // Short Description of product 
        public string ShortDescription { get; set; }

        // Id of product 
        public long Id { get; set; }
    }
}