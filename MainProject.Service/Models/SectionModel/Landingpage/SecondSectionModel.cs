﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.General;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of second section in Landing page
    /// </summary>
    public class SecondSectionModel : S_TitleModel
    {
        // Main items of section
        public List<SecondSectionItem> SecondSectionItems { get; set; }
    }

    public class SecondSectionItem
    {
        public int Order { get; set; }

        public string Title { get; set; }

        public G_ImageModel Image { get; set; }
    }
}