﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.General;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of fifth section in Landing page
    /// </summary>
    public class FifthSectionModel
    {
        // Main items of section
        public List<FifthSectionItem> FifthSectionItems { get; set; }
    }

    public class FifthSectionItem
    {
        public G_ImageModel Image { get; set; }

        public string Url { get; set; }
    }
}