﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of sixth section in Landing page
    /// </summary>
    public class SixthSectionModel: S_TitleModel
    {
        // Description of section
        public string Description { get; set; }

        // Main items of section
        public List<SixthSectionItem> SixthSectionItems { get; set; }
    }

    public class SixthSectionItem
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
    }
}