﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of eighth section in Landing page
    /// </summary>
    public class EighthSectionModel : S_TitleModel
    {
        // Main items of section
        public List<EighthSectionItem> EighthSectionItems { get; set; }
    }

    public class EighthSectionItem : IT_ArticleModel
    {
        // Check product is contact price or not
        public bool IsPriceContact { get; set; }

        // Price of product
        public string Price { get; set; }

        // Check product is promotion or not
        public bool IsPromotion { get; set; }

        // Promotion percent of product 
        public int PromotionPercent { get; set; }

        // Promotion price of product 
        public string PromotionPrice { get; set; }

        // Short Description of product 
        public string ShortDescription { get; set; }

        // Id of product 
        public long Id { get; set; }
    }
}