﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of first section in Landing page
    /// </summary>
    public class FirstSectionModel : S_TitleModel
    {
        // Main items of section
        public List<FirstSectionItem> FirstSectionItems { get; set; }
    }

    public class FirstSectionItem : IT_ArticleModel
    {}
}