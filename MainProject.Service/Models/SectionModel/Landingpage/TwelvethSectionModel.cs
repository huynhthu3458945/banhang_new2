﻿using System.Collections.Generic;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of twelveth section in Landing page
    /// </summary>
    public class TwelvethSectionModel
    {
        // Get only fourth items of section base on design
        public List<TwelvethSectionItem> Fourth_TwelvethSectionItems { get; set; }
    }

    public class TwelvethSectionItem
    {
        public string Title { get; set; }
    }
}