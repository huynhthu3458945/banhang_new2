﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of ninth section in Landing page
    /// </summary>
    public class NinthSectionModel: S_TitleModel
    {
        // Backgroup image of section
        public string BackgroundImage { get; set; }

        // Description of section
        public string Description { get; set; }

        // Get only 2 items of section base on design
        public List<NinthSectionItem> Two_NinthSectionItems { get; set; }
    }

    public class NinthSectionItem
    {
        public string Title { get; set; }
    }
}