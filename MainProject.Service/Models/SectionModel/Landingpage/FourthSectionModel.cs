﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of fourth section in Landing page
    /// </summary>
    public class FourthSectionModel: S_TitleModel
    {
        // Backgroup image of section
        public string BackgroundImage { get; set; }

        // Main items of section
        public List<FourthSectionItem> FourthSectionItems { get; set; }
    }

    public class FourthSectionItem: IT_CategoryModel
    {}
}