﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of seventh section in Landing page
    /// </summary>
    public class SeventhSectionModel : S_TitleModel
    {
        // Main items of section
        public List<SeventhSectionItem> SeventhSectionItems { get; set; }
    }

    public class SeventhSectionItem : IT_ArticleModel
    {}
}