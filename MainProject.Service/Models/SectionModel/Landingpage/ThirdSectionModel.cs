﻿using System.Collections.Generic;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of third section in Landing page
    /// </summary>
    public class ThirdSectionModel
    {
        // Get only 3 items of section base on design
        public List<ThirdSectionItem> Three_ThirdSectionItems { get; set; }
    }

    public class ThirdSectionItem
    {
        public string Description { get; set; }
    }
}