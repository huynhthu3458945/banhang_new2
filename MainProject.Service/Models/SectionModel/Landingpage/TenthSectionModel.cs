﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of tenth section in Landing page
    /// </summary>
    public class TenthSectionModel : S_TitleModel
    {
        // Main items of section
        public List<TenthSectionItem> TenthSectionItems { get; set; }
    }

    public class TenthSectionItem : IT_ArticleModel
    {
        public string Description { get; set; }
    }
}