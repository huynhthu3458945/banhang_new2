﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Landingpage
{
    /// <summary>
    /// The model of eleventh section in Landing page
    /// </summary>
    public class EleventhSectionModel: S_TitleModel
    {
        // Description of section
        public string Description { get; set; }

        // Main items of section
        public List<EleventhSectionItem> EleventhSectionItems { get; set; }
    }

    public class EleventhSectionItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}