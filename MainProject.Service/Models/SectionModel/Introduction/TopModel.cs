﻿using MainProject.Service.Models.SectionModel.Base.Sections;
namespace MainProject.Service.Models.SectionModel.Introduction
{
    /// <summary>
    /// The model of top section in Introduction Page
    /// </summary>
    public class TopModel: S_TitleModel
    {
        // Title of top article
        public string TopArticleTitle { get; set; }

        // Body of top article
        public string TopArticleBody { get; set; }

        // Image of top article
        public string TopArticleImage { get; set; }
    }
}