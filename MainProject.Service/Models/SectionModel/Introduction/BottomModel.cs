﻿using MainProject.Service.Models.SectionModel.Base.Sections;
namespace MainProject.Service.Models.SectionModel.Introduction
{
    /// <summary>
    /// The model of bottom section in Introduction Page
    /// </summary>
    public class BottomModel
    {
        // Title of bottom article
        public string BottomArticleTitle { get; set; }

        // Body of bottom article
        public string BottomArticleBody { get; set; }

        // Image of bottom article
        public string BottomArticleImage { get; set; }
    }
}