﻿using MainProject.Service.Models.SectionModel.General;
namespace MainProject.Service.Models.SectionModel.Introduction
{
    /// <summary>
    /// The model of body section in Introduction Page
    /// </summary>
    public class BodyModel
    {
        // Title of body article
        public string BodyArticleTitle { get; set; }

        // Body of body article
        public string BodyArticleBody { get; set; }

        // Image of body article
        public G_ImageModel BodyArticleImageModel { get; set; }
    }
}