﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;
using MainProject.Service.Models.SectionModel.Base.Items;

namespace MainProject.Service.Models.SectionModel.Project.Detail
{
    /// <summary>
    /// Model of bottom section in Detail Project Page
    /// </summary>
    public class BottomModel : S_TitleModel
    {
        // Other project of detail page
        public List<OtherProjectArticle> OtherProjectArticles { get; set; }
    }
    public class OtherProjectArticle : IT_ArticleModel
    {
        // Created date of news
        public string CreatedDate { get; set; }

        // Description of news 
        public string Description { get; set; }
    }
}