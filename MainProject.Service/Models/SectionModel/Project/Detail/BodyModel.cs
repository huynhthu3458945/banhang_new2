﻿using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Project.Detail
{
    /// <summary>
    /// The model of body section in Detail Project Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {
        // The title of detail article
        public string ArticleTitle { get; set; }

        // The created date of detail article
        public string CreatedDate { get; set; }

        // The body of detail article
        public string Body { get; set; }
    }
}