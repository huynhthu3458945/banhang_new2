﻿using System.Collections.Generic;
using MainProject.Framework.Models;
using MainProject.Service.Models.SectionModel.Base.Items;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Project.Index
{
    /// <summary>
    /// The model of body section in Index Project Page
    /// </summary>
    public class BodyModel : S_TitleModel
    {
        // First article of index project page
        public MainProjectArticle FirstMainProjectArticle { get; set; }

        // Six articles of index project page
        public List<MainProjectArticle> SixMainProjectArticles { get; set; }

        public PagingModel PagingModel { get; set; }
    }
    public class MainProjectArticle : IT_ArticleModel
    {
        // Created date of project
        public string CreatedDate { get; set; }

        // Description of project 
        public string Description { get; set; }
    }
}