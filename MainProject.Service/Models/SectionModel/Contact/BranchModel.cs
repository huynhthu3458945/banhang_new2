﻿using System.Collections.Generic;
using MainProject.Service.Models.SectionModel.Base.Sections;

namespace MainProject.Service.Models.SectionModel.Contact
{
    /// <summary>
    /// The model of branch section in Contact Page
    /// </summary>
    public class BranchModel: S_TitleModel
    {
        // Main items of section
        public List<BranchItem> BranchItems { get; set; }
    }

    public class BranchItem
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public string Url { get; set; }
    }
}