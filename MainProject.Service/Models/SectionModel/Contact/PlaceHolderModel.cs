﻿namespace MainProject.Service.Models.SectionModel.Contact
{
    /// <summary>
    /// The model of placeholder section in Contact Page
    /// </summary>

    public class PlaceholderModel
    {
        // Full name
        public string FullNameTitle { get; set; }

        // Email
        public string EmailTitle { get; set; }

        // Phone
        public string PhoneTitle { get; set; }

        // Content
        public string ContentTitle { get; set; }
    }
}