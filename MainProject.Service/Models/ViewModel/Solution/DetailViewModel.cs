﻿using MainProject.Service.Models.SectionModel.Solution.Detail;
using MainProject.Service.Models.ViewModel.Base;

namespace MainProject.Service.Models.ViewModel.Solution
{
    /// <summary>
    /// Detail page model of Solution
    /// </summary>
    public class DetailViewModel:BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }

        // Bottom model
        public BottomModel BottomModel { get; set; }
    }
}