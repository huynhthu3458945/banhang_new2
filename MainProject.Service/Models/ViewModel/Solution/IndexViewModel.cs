﻿using MainProject.Service.Models.ViewModel.Base;
using MainProject.Service.Models.SectionModel.Solution.Index;


namespace MainProject.Service.Models.ViewModel.Solution
{
    /// <summary>
    /// Index page model of Solution
    /// </summary>
    public class IndexViewModel : BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }
    }
}