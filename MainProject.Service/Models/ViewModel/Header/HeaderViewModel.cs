﻿using MainProject.Core;
using MainProject.Service.Models.SectionModel.Header;
using System.Collections.Generic;

namespace MainProject.Service.Models.Header
{
    /// <summary>
    /// Header view model 
    /// </summary>
    public class HeaderViewModel
    {
        // Menu model
        public MenuModel MenuModel { get; set; }

        // Information model
        public InformationModel InformationModel { get; set; }
        public List<MenuItem> ListMenu { get; set; }
        public List<Language> ListLanguage { get; set; }
    }

    public class MenuSupportModel
    { 
        public int Order { get; set; }
        public List<MenuItem> ListMenu { get; set; }
    }
}
