﻿namespace MainProject.Service.Models.ViewModel.Base
{
    /// <summary>
    /// The model of SEO data
    /// </summary>

    public class SEOModel
    {
        // The title
        public string Title { get; set; }

        // The metaTitle
        public string MetaTitle { get; set; }

        // The MetaDescription
        public string MetaDescription { get; set; }

        // The MetaKeywords
        public string MetaKeywords { get; set; }

        // The MetaImage
        public string MetaImage { get; set; }

        // The Type
        public string Type { get; set; }

        // The Url
        public string Url { get; set; }
    }
}