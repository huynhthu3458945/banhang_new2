﻿using System.ComponentModel.DataAnnotations;

namespace MainProject.Service.Models.ViewModel.Order
{
    public class OrderFormModel
    {
       
        public string Name { get; set; }
       
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public int OrderTypeId { get; set; }
    }
    public class OrderFormModelBK
    {
        [Required(ErrorMessage = "Vui lòng nhập tên!")]
        public string Name { get; set; }

        [StringLength(100, ErrorMessage = "Độ dài Email không quá 100 ký tự!")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email không hợp lệ.")]
        [Required(ErrorMessage = "Vui lòng điền Email!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập số điện thoại!")]
        [RegularExpression("^[0-9]{10,11}$", ErrorMessage = "Số điện thoại không hợp lệ")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập đường!")]
        public string Address { get; set; }
        public string Note { get; set; }
        public int OrderTypeId { get; set; }
    }
}