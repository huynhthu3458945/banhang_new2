﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace MainProject.Service.Models.ViewModel.Contact
{
    /// <summary>
    /// Form model of Contact
    /// </summary>

    public class FormModel
    {
        public long Id { get; set; }

        [StringLength(200, ErrorMessage = "Chiều dài không được quá 200 ký tự")]
        [Required(ErrorMessage = "Vui lòng nhập tên!")]
        public string Name { get; set; }

        [RegularExpression("^[0-9]{10,11}$", ErrorMessage = "Số điện thoại không hợp lệ")]
        [Required(ErrorMessage = "Chưa nhập điện thoại")]
        public string Phone { get; set; }
        
        public string Email { get; set; }
        
        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }


    }
}