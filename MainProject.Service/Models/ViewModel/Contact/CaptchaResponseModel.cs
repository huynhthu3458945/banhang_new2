﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MainProject.Service.Models.ViewModel.Contact
{
    /// <summary>
    /// Captcha response model 
    /// </summary>
    /// 
    public class CaptchaResponseModel
    {
        // The message in case success response
        [JsonProperty("success")]
        public bool Success { get; set; }

        // The message in case error response
        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }
}