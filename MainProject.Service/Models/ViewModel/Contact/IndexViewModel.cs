﻿using MainProject.Service.Models.SectionModel.Contact;
using MainProject.Service.Models.ViewModel.Base;

namespace MainProject.Service.Models.ViewModel.Contact
{
    /// <summary>
    /// Index page model of Contact
    /// </summary>
    public class IndexViewModel : BaseModel
    {
        // Placeholder model
        public PlaceholderModel PlaceholderModel { get; set; }

        // Branch model
        public BranchModel BranchModel { get; set; }
    }
}