﻿using MainProject.Service.Models.SectionModel.Footer;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using MainProject.Core;

namespace MainProject.Service.Models.ViewModel.Footer
{
    /// <summary>
    /// Footer view model 
    /// </summary>
    public class FooterViewModel
    {
        // Top side model
        public TopModel TopModel { get; set; }

        // Left side model
        public LeftModel LeftModel { get; set; }

        // Middle side model
        public MiddleModel MiddleModel { get; set; }

        // Right side model
        public RightModel RightModel { get; set; }

        // Social model
        public SocialModel SocialModel { get; set; }
        public List<MainProject.Core.MenuItem> ListMenu { get; set; }
    }
}
