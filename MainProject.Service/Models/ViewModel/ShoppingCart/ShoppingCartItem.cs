﻿using System;

namespace MainProject.Service.Models.ViewModel.ShopppingCart
{
    /// <summary>
    /// Shopping Cart Item 
    /// </summary>
    public class ShoppingCartItem
    {
        public string TokenCode { get; set; }

        public DateTime LastUpdateTime { get; set; }

        public ProductModel Product { get; set; }

        public int ItemCount { get; set; }

        public decimal ItemAmount { get; set; }

    }

    public class ProductModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public long Id { get; set; }

        public decimal Price { get; set; }

        public string ProductCode { get; set; }

        public bool IsPromotion { get; set; }

        public string ImageDefault { get; set; }

        public string ImageDefaultAlt { get; set; }

        public string ProductCategoryTitle { get; set; }
        public string ProductCategoryUrl { get; set; }

        public int PromotionPercent { get; set; }
        public decimal PromotionPrice { get; set; }
    }
}