﻿using MainProject.Service.Models.ViewModel.Base;
using MainProject.Service.Models.SectionModel.Product.Child1;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Service.Models.SectionModel.Product;

namespace MainProject.Service.Models.ViewModel.Product
{
    /// <summary>
    /// Child1 page model of Product
    /// </summary>
    public class Child1ViewModel: BaseModel
    {
        // SlideShowModel
        public SlideShowModel SlideShowModel { get; set; }
        // Body model
        public BodyModel BodyModel { get; set; }
        //danh sách sản phẩm
        public List<MainProject.Core.Product> ListProduct { get; set; }
        public Category Category { get; set; }
    }
}