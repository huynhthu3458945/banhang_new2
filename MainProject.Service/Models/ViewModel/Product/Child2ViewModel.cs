﻿using MainProject.Service.Models.ViewModel.Base;
using MainProject.Service.Models.SectionModel.Product.Child2;
using MainProject.Service.Models.SectionModel.Product;
using MainProject.Core;

namespace MainProject.Service.Models.ViewModel.Product
{
    /// <summary>
    /// Child2 page model of Product
    /// </summary>
    public class Child2ViewModel : BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }
        public SlideShowModel SlideShowModel { get; set; }
        public Category Category { get; set; }
    }
}