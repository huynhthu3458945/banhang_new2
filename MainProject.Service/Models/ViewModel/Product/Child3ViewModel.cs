﻿using MainProject.Service.Models.ViewModel.Base;
using MainProject.Service.Models.SectionModel.Product.Child3;


namespace MainProject.Service.Models.ViewModel.Product
{
    /// <summary>
    /// Child3 page model of Product
    /// </summary>
    public class Child3ViewModel : BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }
    }
}