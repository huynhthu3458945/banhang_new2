﻿using MainProject.Service.Models.SectionModel.Contact;
using MainProject.Service.Models.SectionModel.General;
using MainProject.Service.Models.SectionModel.Product.Detail;
using MainProject.Service.Models.ViewModel.Base;
using System.Collections.Generic;

namespace MainProject.Service.Models.ViewModel.Product
{
    /// <summary>
    /// Detail page model of Product
    /// </summary>
    public class DetailViewModel:BaseModel
    {
        // Top side model
        public TopModel TopModel { get; set; }
        public BranchModel BranchModel { get; set; }
        // Body model
        public BodyModel BodyModel { get; set; }

        // Bottom side model
        public BottomModel BottomModel { get; set; }
        public List<G_ImageModel> ListImage { get; set; }
    }
}