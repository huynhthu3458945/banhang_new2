﻿using MainProject.Service.Models.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.Models.ViewModel.Exchange
{
    public class IndexViewModel : BaseModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
