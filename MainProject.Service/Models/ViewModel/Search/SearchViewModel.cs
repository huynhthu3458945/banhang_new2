﻿using System.Collections.Generic;
using MainProject.Framework.Models;
using MainProject.Service.Models.SectionModel.General;

namespace MainProject.Service.Models.ViewModel.Search
{
    /// <summary>
    /// Search page model 
    /// </summary>
    public class SearchViewModel
    {
        // Search objects
        public List<SearchObject> SearchObjects { get; set; }

        // Paging model
        public PagingModel PagingModel { get; set; }

        // Search text
        public string Text { get; set; }

        // Total results
        public int TotalResults { get; set; }
    }
    public class SearchObject
    {
        public string Name { get; set; }
        public G_ImageModel ImageItem { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
    }
}