﻿using MainProject.Service.Models.SectionModel.Landingpage;
using MainProject.Service.Models.ViewModel.Base;

namespace MainProject.Service.Models.ViewModel.Landingpage
{
    /// <summary>
    /// Index page model of Landing page
    /// </summary>
    public class IndexViewModel: BaseModel
    {
        // SlideShowModel
        public SlideShowModel SlideShowModel { get; set; }

        // FirstSectionModel
        public FirstSectionModel FirstSectionModel { get; set; }

        // SecondSectionModel
        public SecondSectionModel SecondSectionModel { get; set; }

        // ThirdSectionModel
        public ThirdSectionModel ThirdSectionModel { get; set; }

        // FourthSectionModel
        public FourthSectionModel FourthSectionModel { get; set; }

        // FifthSectionModel
        public FifthSectionModel FifthSectionModel { get; set; }

        // SixthSectionModel
        public SixthSectionModel SixthSectionModel { get; set; }

        // SeventhSectionModel
        public SeventhSectionModel SeventhSectionModel { get; set; }

        // EighthSectionModel
        public EighthSectionModel EighthSectionModel { get; set; }

        // NinthSectionModel
        public NinthSectionModel NinthSectionModel { get; set; }

        // TenthSectionModel
        public TenthSectionModel TenthSectionModel { get; set; }

        // EleventhSectionModel
        public EleventhSectionModel EleventhSectionModel { get; set; }

        // TwelvethSectionModel
        public TwelvethSectionModel TwelvethSectionModel { get; set; }
    }
}