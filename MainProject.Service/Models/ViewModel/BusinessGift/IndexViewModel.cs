﻿using MainProject.Core;
using MainProject.Service.Models.SectionModel.BusinessGift;
using MainProject.Service.Models.SectionModel.Home;
using MainProject.Service.Models.ViewModel.Base;
using System.Collections.Generic;

namespace MainProject.Service.Models.ViewModel.BusinessGift
{
    /// <summary>
    /// Index page model of Home
    /// </summary>
    public class IndexViewModel: BaseModel
    {
        // SlideShowModel
        public SlideShowModel SlideShowModel { get; set; }
        public List<LandingpageItem> ListPartners { get; set; }
        public LandingpageItem DescriptionModel { get; set; }
        public List<ContentModel01> ListContentModel01 { get; set; }
        //Sản phẩm quà danh nghiệp
        public List<LandingpageItem> ListProducts { get; set; }

    }
}