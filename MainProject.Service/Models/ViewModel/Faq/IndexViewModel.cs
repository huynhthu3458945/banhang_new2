﻿using MainProject.Core;
using MainProject.Service.Models.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.Models.ViewModel.Faq
{
   public class IndexViewModel:BaseModel
    {
        public string Title { get; set; }
        public List<MainProject.Core.Faq> Faqs { get; set; }
    }
}
