﻿
using MainProject.Core;
using MainProject.Service.Models.SectionModel.Home;
using MainProject.Service.Models.ViewModel.Base;
using System.Collections.Generic;

namespace MainProject.Service.Models.ViewModel.Home
{
    /// <summary>
    /// Index page model of Home
    /// </summary>
    public class IndexViewModel: BaseModel
    {
       
   
        // SlideShowModel
        public SlideShowModel SlideShowModel { get; set; }

        // IntroductionModel
        public IntroductionModel IntroductionModel { get; set; }

        // ProductModel
        public ProductModel ProductModel { get; set; }

        // SolutionModel
        public SolutionModel SolutionModel { get; set; }

        // ProjectModel
        public ProjectModel ProjectModel { get; set; }

        // NewsModel
        public NewsModel NewsModel { get; set; }
        public List<Partner> ListPartners { get; set; }
        public List<Partner> ListCertifications { get; set; }
        //Cẩm Nang và Tài Liệu
        public List<Article> ListArticle35 { get; set; }
        //Về Yến Xào
        public List<Article> ListArticle50 { get; set; }
        //danh sách sản phẩm
        public List<MainProject.Core.Product> ListProduct { get;set; }
        // danh sách menu 
        public List<Category> ListCategory { get; set; }
        public List<Feedback> ListFeedback { get; set; }
        public List<LandingpageItem> ListLandingpageItem { get; set; }
        public List<MainProject.Core.Order> ListOrder { get; set; }
        public Category Category { get; set; }

    }
}