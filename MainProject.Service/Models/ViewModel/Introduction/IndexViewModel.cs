﻿using MainProject.Service.Models.SectionModel.Introduction;
using MainProject.Service.Models.ViewModel.Base;
using MainProject.Core;
using System.Collections.Generic;

namespace MainProject.Service.Models.ViewModel.Introduction
{
    /// <summary>
    /// Index page model of Introduction
    /// </summary>
    public class IndexViewModel : BaseModel
    {
        public List<LandingpageItem> Location1 { get; set; }
        public List<LandingpageItem> Location2 { get; set; }
        public List<LandingpageItem> Location3 { get; set; }
        public List<LandingpageItem> Location4 { get; set; }
        public List<LandingpageItem> ListLandingpageItem { get; set; }
        public List<Article> ListArticle { get; set; }
    }
}