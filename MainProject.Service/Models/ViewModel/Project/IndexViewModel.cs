﻿using MainProject.Service.Models.ViewModel.Base;
using MainProject.Service.Models.SectionModel.Project.Index;


namespace MainProject.Service.Models.ViewModel.Project
{
    /// <summary>
    /// Index page model of Project
    /// </summary>
    public class IndexViewModel : BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }
    }
}