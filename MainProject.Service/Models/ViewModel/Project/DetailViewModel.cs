﻿using MainProject.Service.Models.SectionModel.Project.Detail;
using MainProject.Service.Models.ViewModel.Base;

namespace MainProject.Service.Models.ViewModel.Project
{
    /// <summary>
    /// Detil page model of Project 
    /// </summary>
    public class DetailViewModel:BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }

        // Bottom model
        public BottomModel BottomModel { get; set; }
    }
}