﻿using MainProject.Service.Models.ViewModel.Base;
using MainProject.Service.Models.SectionModel.Download.Index;

namespace MainProject.Service.Models.ViewModel.Download
{
    /// <summary>
    /// Index page model of Download
    /// </summary>
    public class IndexViewModel : BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }
    }
}