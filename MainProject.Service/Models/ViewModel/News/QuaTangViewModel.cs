﻿using MainProject.Core;
using MainProject.Service.Models.SectionModel.Contact;
using MainProject.Service.Models.SectionModel.News;
using MainProject.Service.Models.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainProject.Service.Models.ViewModel.News
{
    public class QuaTangViewModel : BaseModel
    {
        public SlideShowModel SlideShowModel { get; set; }
        public BranchModel BranchModel { get; set; }
        public List<LandingpageItem> Location1 { get; set; }
        public List<LandingpageItem> Location2 { get; set; }
        public List<LandingpageItem> Location3 { get; set; }
        public List<LandingpageItem> Location4 { get; set; }
    }
}
