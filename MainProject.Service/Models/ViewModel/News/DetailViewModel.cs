﻿using MainProject.Service.Models.SectionModel.News;
using MainProject.Service.Models.SectionModel.News.Detail;
using MainProject.Service.Models.ViewModel.Base;

namespace MainProject.Service.Models.ViewModel.News
{
    /// <summary>
    /// Detil page model of News 
    /// </summary>
    public class DetailViewModel:BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }

        // Bottom model
        public BottomModel BottomModel { get; set; }
        public SlideShowModel SlideShowModel { get; set; }
    }
}