﻿using MainProject.Service.Models.ViewModel.Base;
using MainProject.Service.Models.SectionModel.News.Index;
using System.Collections.Generic;
using MainProject.Core;
using MainProject.Service.Models.SectionModel.News;

namespace MainProject.Service.Models.ViewModel.News
{
    /// <summary>
    /// Index page model of News
    /// </summary>
    public class IndexViewModel : BaseModel
    {
        // Body model
        public BodyModel BodyModel { get; set; }
        public SlideShowModel SlideShowModel { get; set; }
    }
}